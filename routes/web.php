<?php


use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('home');
Route::get('/orcamentos', 'PagesController@orcamentos')->name('orcamento');
Route::get('/despesas', 'PagesController@despesas')->name('despesas');
Route::get('/receitas', 'PagesController@receitas')->name('receitas');
Route::get('/servidores', 'PagesController@servidores')->name('servidores');
Route::get('/licitacoes', 'PagesController@licitacoes')->name('licitacoes');

Route::get('/contratos', 'PagesController@contratos')->name('contratos');
Route::get('/atas-resgitro-pagemento', 'PagesController@atas')->name('atas');
Route::get('/dispensa-licitacao', 'PagesController@dispensa')->name('dispensa');
Route::get('/concorrencia', 'PagesController@concorrencia')->name('concorrencia');
Route::get('/carta-convite', 'PagesController@cartaConvite')->name('carta');
Route::get('/contrato-pessoa-fisica', 'PagesController@contratoPessoaFisica')->name('contrato.fisica');
Route::get('/contrato-pessoa-juridica', 'PagesController@contratoPessoaJuridica')->name('contrato.juridica');
Route::get('/inexigibilidade', 'PagesController@inexigibilidade')->name('inexigibilidade');
Route::get('/pregao-presencial', 'PagesController@pregaoPresencial')->name('pregao.presencial');
Route::get('/pregao-eletronico', 'PagesController@pregaoEletronico')->name('pregao.eletronico');
Route::get('/tomada-precos', 'PagesController@tomadaDePrecos')->name('tomada.precos');


Route::get('/diarias', 'PagesController@diarias')->name('diarias');
Route::get('/publicacoes-oficiais', 'PagesController@publicacoesOficiais')->name('publicacoes-oficiais');
Route::get('/legislacoes', 'PagesController@legislacoes')->name('legislacoes');
Route::get('/pedidos-esic', 'PagesController@esic');
Route::post('/salvar-pedido', 'PagesController@salvarPedido')->name('salvarPedido');
Route::get('/salvar-pedido/consultar', 'PagesController@consultarPedido')->name('consultarPedido');
Route::post('/responder-pedido', 'PagesController@responderPedido')->name('responderPedido');
Route::get('/covid19', 'PagesController@covid19')->name('covid19');
Route::get('/covid19-despesas', 'PagesController@covid19Desespesas')->name('covid19-despesas');
Route::get('/covid19-receitas', 'PagesController@covid19Receitas')->name('covid19-receitas');
Route::get('/covid19-licitacoes', 'PagesController@covid19Licitacoes')->name('covid19-licitacoes');
Route::get('/covid19-contratos', 'PagesController@covid19Contratos')->name('covid19-contratos');
Route::get('/estatisticas', 'PagesController@estatisticas')->name('estatisticas');
Route::get('/estrutura-organizacional', 'PagesController@estruturas')->name('estrutura-organizacional');
Route::get('/lrf', 'PagesController@lrf')->name('lrf');
Route::get('/verbas', 'PagesController@verbas')->name('verba');
Route::post('/search-options','SearchController@searchOptions')->name('search');
Route::fallback(function () {
    return redirect()->route('home');
});

// Demo routes
// Route::get('/dashboards', 'PagesController@dashboards');
// Route::get('/datatables', 'PagesController@datatables');
// Route::get('/ktdatatables', 'PagesController@ktDatatables');
// Route::get('/select2', 'PagesController@select2');
// Route::get('/jquerymask', 'PagesController@jQueryMask');
// Route::get('/icons/custom-icons', 'PagesController@customIcons');
// Route::get('/icons/flaticon', 'PagesController@flaticon');
// Route::get('/icons/fontawesome', 'PagesController@fontawesome');
// Route::get('/icons/lineawesome', 'PagesController@lineawesome');
// Route::get('/icons/socicons', 'PagesController@socicons');
// Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
// Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Route::prefix('adm')->group(function () {
    Auth::routes(['register' => false]);
    Route::middleware(['auth'])->group(function () {
        Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home_adm');
        Route::resource('users', UserController::class);
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);
        Route::resource('app-settings', AppSettingController::class);
        Route::resource('attachments', AttachmentController::class);
        Route::resource('type-documents', TypeDocumentController::class);
        Route::resource('documents', DocumentController::class);
        Route::resource('attachment-documents', AttachmentDocumentController::class);
        Route::resource('verbas', VerbaController::class);
        Route::resource('attachment-verbas', AttachmentVerbaController::class);
        Route::resource('empenhos', EmpenhoController::class);
        Route::resource('attachment-empenhos', AttachmentEmpenhoController::class);
        Route::resource('pagamentos', PagamentoController::class);
        Route::resource('liquidacoes', LiquidacaoController::class);
        Route::resource('receitas', ReceitaController::class);
        Route::resource('attachment-receitas', AttachmentReceitaController::class);
        Route::resource('cargos', CargoController::class);
        Route::resource('servidores', ServidorController::class);
        Route::get('servidores/{id}/restore', [App\Http\Controllers\ServidorController::class, 'restore'])->name('servidores.restore');
        Route::resource('attachment-servidores', AttachmentServidorController::class);
        Route::resource('contratos', ContratoController::class);
        Route::resource('attachment-contratos', AttachmentContratoController::class);
        Route::resource('diarias', DiariaController::class);
        Route::resource('attachment-diarias', AttachmentDiariaController::class);
        Route::resource('tipo-planejamento-orcamentarios', TipoPlanejamentoOrcamentarioController::class);
        Route::resource('arquivo-plan-orcamentarios', ArquivoPlanOrcamentarioController::class);
        Route::resource('covid-resumos', CovidResumoController::class);
        Route::resource('estruturas', EstruturaController::class);
        Route::resource('responsabilidades', ResponsabilidadeController::class);
        Route::resource('arquivo-responsabilidades', ArquivoResponsabilidadeController::class);

        Route::prefix('sicap')->group(function () {
            Route::resource('sicap-empenho', SicapEmpenhoController::class);
            Route::get('sicap-empenho/{id}/restore', [App\Http\Controllers\SicapEmpenhoController::class, 'restore'])->name('sicap-empenho.restore');

            Route::resource('sicap-servidores', SicapServidorController::class);
            Route::get('sicap-servidores/{id}/restore', [App\Http\Controllers\SicapServidorController::class, 'restore'])->name('sicap-servidor.restore');

            Route::resource('sicap-receitas', SicapReceitaController::class);
            Route::get('sicap-receitas/{id}/restore', [App\Http\Controllers\SicapReceitaController::class, 'restore'])->name('sicap-receitas.restore');
        });
        Route::prefix('siap')->group(function () {
            Route::resource('siap-empenho', SiapEmpenhoController::class);
            Route::get('siap-empenho/{id}/restore', [App\Http\Controllers\SiapEmpenhoController::class, 'restore'])->name('siap-empenho.restore');
        });
        Route::prefix('sagres')->group(function () {
            Route::resource('sagres-empenhos', SagresEmpenhoController::class);
            Route::get('sagres-empenho/{id}/restore', [App\Http\Controllers\SagresEmpenhoController::class, 'restore'])->name('sagres-empenho.restore');
        });

        Route::resource('pedidos', PedidoController::class);
        Route::resource('controle-menu',OptionController::class);
        Route::get('controle-submenu/{id}',[App\Http\Controllers\OptionController::class,'editSubmenu'])->name('submenu.edit');
        Route::put('controle-submenu/{id}',[App\Http\Controllers\OptionController::class,'updateSubmenu'])->name('submenu.update');
        Route::resource('respostas', RespostaController::class);
        Route::resource('tipo-pedidos', TipoPedidoController::class);
    });
});

