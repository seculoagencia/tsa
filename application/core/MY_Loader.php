<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ILHA
 * Date: 25/06/15
 * Time: 11:39
 */

class MY_Loader extends CI_Loader {
    public function view_site($nome,$dados = array()) {
        $this->view('templates/site/header.php', $dados);
        $this->view($nome, $dados);
        $this->view('templates/site/footer.php');
    }
    public function view_admin($nome,$dados = array()) {
        $this->view('templates/admin/header.php', $dados);
        $this->view($nome, $dados);
        $this->view('templates/admin/footer.php');
    }
}
