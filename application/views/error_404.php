<div class="error-page">
  <h2 class="headline text-yellow" style="margin-top: -23px;"> 404</h2>
  <div class="error-content">
    <h3><i class="fa fa-warning text-yellow"></i> Oops! Página não encontrada.</h3>
    <p>
      Não foi possível encontrar a página que você estava procurando.
      Enquanto isso, você pode <a href="<?php echo site_url('/users') ?>">retornar ao dashboard</a>.
    </p>
    <!-- <form class="search-form">
      <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Search">
        <div class="input-group-btn">
          <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </form> -->
  </div><!-- /.error-content -->
</div><!-- /.error-page -->
