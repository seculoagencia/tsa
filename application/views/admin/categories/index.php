<div class="row">
  <div class="col-md-4">
    <div class="card">
      <?php echo form_open('categories/add_category'); ?>
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <h3>Categorias</h3>
      </div>
      <div class="content">
        <div class="form-inline">
          <input type="text" class="form-control" name="name" placeholder="Nome" required>
          <button type="submit" class="btn btn-fill btn-primary" name="button">Cadastrar</button>
        </div>
        <table class="table table-hover table-striped">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th style="min-width:92px;">Ações</th>
          </thead>
          <tbody>
            <?php $cont = 0 ?>
            <?php foreach ($categories as $category): ?>

              <tr>
                <td><?php $cont = $cont + 1; echo $cont; ?></td>
                <td><?php echo $category['name'] ?></td>
                <td>
                  <?php echo anchor("#", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-success btn-fill','rel'=>'tooltip','title'=>'Editar','data-toggle'=>'modal','data-target'=>"#myModalCategoria{$category['id']}")) ?>
                  <?php $report = $this->reports_model->select_for_category($category['id']) ?>
                  <?php if ($report != null): ?>
                    <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalCategoriaRemove1{$category['id']}")) ?>
                  <?php else: ?>
                    <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalCategoriaRemove2{$category['id']}")) ?>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>
      <?php echo form_close();?>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <div class="row">
          <div class="col-lg-12">
            <h3>Sub-Categorias</h3>
          </div>
        </div>
      </div>
      <div class="content">
        <?php echo form_open('categories/add_subcategory'); ?>
          <div class="form-inline">
            <input type="text" name="name" class="form-control" placeholder="Nome" required>
            <select class="form-control" name="category_id" required>
              <option value="">Categoria</option>
              <?php foreach ($categories as $category): ?>
              <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
              <?php endforeach; ?>
            </select>
            <button type="submit" class="btn btn-fill btn-primary">Cadastrar</button>
          </div>
          <?php echo form_close();?>
        <table class="table table-hover table-striped">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th>Categoria</th>
            <th style="min-width:92px;">Ações</th>
          </thead>
          <tbody>
            <?php $cont2 = 0; ?>
            <?php foreach ($subcategories as $subcategory): ?>
            <?php $category_id = $this->categories_model->select_id($subcategory['category_id']) ?>
            <?php $report2 = $this->reports_model->select_for_subcategory($subcategory['id']) ?>
            <tr>
              <td><?php $cont2 = $cont2 + 1; echo $cont2; ?></td>
              <td><?php echo $subcategory['name'] ?></td>
              <td><?php echo $category_id['name']; ?></td>
              <td>
                <a href="#" rel="tooltip" title="Editar" class="btn btn-xs btn-success btn-fill" data-toggle="modal" data-target="#myModalSubCategoria<?php echo $subcategory['id'] ?>"><i class="fa fa-pencil"></i></a>
                <?php if ($report2 != null): ?>
                  <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalSubCategoriaRemove1{$subcategory['id']}")) ?>
                <?php else: ?>
                  <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalSubCategoriaRemove2{$subcategory['id']}")) ?>
                  <?php //echo anchor("admin/categories/remove_category/{$category['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
                <?php endif; ?>
                <?php //echo anchor("admin/categories/remove_subcategory/{$subcategory['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php foreach ($categories as $category): ?>
<?php $report = $this->reports_model->select_for_category($category['id']) ?>
<div class="modal fade" id="myModalCategoria<?php echo $category['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Categoria</h4>
      </div>
      <?php echo form_open("admin/categories/edit_category/{$category['id']}"); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo $category['name'] ?>" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalCategoriaRemove1<?php echo $category['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Você não pode excluir essa categoria, pois ela está atrelada à <?php count($report) ?> relatórios. Apague-os primeiro!
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/reports", 'Ir para Relatórios', array('class'=>'btn btn-primary btn-fill')) ?>
        <?php //echo anchor("admin/categories/remove_subcategory/{$subcategory['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalCategoriaRemove2<?php echo $category['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Tem certeza que deseja excluir a categoria?
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/categories/remove_category/{$category['id']}", 'Remover', array('class'=>'btn btn-danger btn-fill')) ?>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>

<?php foreach ($subcategories as $subcategory): ?>
  <?php $report = $this->reports_model->select_for_subcategory($subcategory['id']) ?>
<div class="modal fade" id="myModalSubCategoria<?php echo $subcategory['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Sub-Categoria</h4>
      </div>
      <?php echo form_open("admin/categories/edit_subcategory/{$subcategory['id']}"); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo $subcategory['name'] ?>" required>
              </div>
              <div class="form-group">
                <label>Categoria</label>
                <select class="form-control" name="category_id" required>
                  <?php foreach ($categories as $category): ?>
                  <option value="<?php echo $category['id'] ?>" <?php echo $selected = ($subcategory['category_id'] == $category['id']) ? 'selected' : '' ; ?>><?php echo $category['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalSubCategoriaRemove1<?php echo $subcategory['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Você não pode excluir essa subcategoria, pois ela está atrelada à <strong><?php count($report) ?></strong> relatórios. Apague-os primeiro!
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/reports", 'Ir para Relatórios', array('class'=>'btn btn-primary btn-fill')) ?>
        <?php //echo anchor("admin/categories/remove_subcategory/{$subcategory['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalSubCategoriaRemove2<?php echo $subcategory['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Tem certeza que deseja excluir a categoria?
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/categories/remove_subcategory/{$subcategory['id']}", 'Remover', array('class'=>'btn btn-danger btn-fill')) ?>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
