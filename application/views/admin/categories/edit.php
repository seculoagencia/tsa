<div class="row">
  <div class="col-md-3">
    <div class="box">
      <?php echo form_open("groups/edit/{$group['id']}"); ?>
      <div class="box-header with-border">
        <h3 class="box-title">Editar Categoria</h3>
      </div>
      <div class="box-body">
        <div class="form-group">
          <label for="name">Nome</label>
          <input type="text" class="form-control" name="name" value="<?php echo $group['name'] ?>" required>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success" name="button">Salvar Alterações</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
