<div class="col-md-8">
  <div class="card">
    <div class="header">
      <h3>Editar Prestação de Contas</h3>
    </div>
    <div class="content">
      <?php echo form_open_multipart('reports/edit/'.$report['id']); ?>
        <div class="row">
          <div class="col-md-6" style="padding-left:5px;">
            <div class="form-group">
              <label>Ano</label>
              <select class="form-control" name="year" required>
                <?php for ($i=0; $i < count($year); $i++): ?>
                  <option value="<?php echo $year[$i] ?>"<?php echo $selected = ($report['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                <?php endfor; ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Mês</label>
              <select class="form-control" name="month" required>
                <?php $cont = 0; ?>
                <?php foreach ($month as $key): ?>
                  <?php $cont = $cont + 1 ?>
                  <option value="<?php echo $cont ?>" <?php echo $selected = ($report['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Título</label>
              <input type="text" name="name" class="form-control" value="<?php echo $report['name'] ?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Categoria</label>
              <select class="form-control" name="category_id" required>
                <option>Selecione...</option>
                <?php foreach ($categories as $category): ?>
                  <option value="<?php echo $category['id'] ?>" <?php echo $selected = ($report['category_id'] == $category['id']) ? 'selected' : '' ; ?>><?php echo $category['name'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Sub-Categoria</label>
              <select class="form-control" name="subcategory_id" required>
                <option>Selecione...</option>
                <?php foreach ($subcategories as $subcategory): ?>
                  <option value="<?php echo $subcategory['id'] ?>"<?php echo $selected = ($report['subcategory_id'] == $subcategory['id']) ? 'selected' : '' ; ?>><?php echo $subcategory['name'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
          </div>
        </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
