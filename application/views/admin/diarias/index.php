  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/diarias') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <option value="">Todos...</option>
                  <?php for ($i=0; $i < count($year); $i++): ?>
                    <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/diarias') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="button" class="btn btn-primary btn-fill" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <th>Servidor</th>
          <th>Cargo</th>
          <th>Data</th>
          <th>Destino</th>
          <th>Motivo da Viagem</th>
          <th>Valor</th>
          <th style="text-align:center">Anexo</th>
          <th style="min-width:92px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php foreach ($diarias as $diaria): ?>
            <tr>
              <td>
                <?php echo $diaria['favorecido'] ?>
              </td>
              <td>
                <?php $cargo = $this->cargos_model->select_id($diaria['cargo_id']) ?>
                <?php echo $cargo['name'] ?>
              </td>
              <td><?php echo date('d/m/Y', strtotime($diaria['data'])) ?></td>
              <td><?php echo $diaria['destino'] ?></td>
              <td><?php echo $diaria['motivo'] ?></td>
              <td>R$ <?php echo number_format($diaria['valor'],2,',','.') ?></td>
              <td style="text-align:center">
                  <?php $anexos = $this->diariasanexos_model->select($diaria['id']) ?>
                <?php foreach($anexos as $key => $anexo): ?>
                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
                <?php endforeach; ?>
              </td>
              <td>
                <a href='#' data-toggle="modal" data-target="#myModalEdit<?php echo $diaria['id'] ?>" rel="tooltip" title="Editar" class="btn btn-xs btn-success btn-fill"><i class="fa fa-pencil"></i></a>
                <a href='<?php echo site_url("admin/diarias/remove/{$diaria['id']}") ?>' rel="tooltip" title="Remover" class="btn btn-xs btn-danger btn-fill"><i class="fa fa-trash"></i></a>

                <div class="modal fade" id="myModalEdit<?php echo $diaria['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalEdit<?php echo $diaria['id'] ?>Label">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalEdit<?php echo $diaria['id'] ?>Label">Nova Receita</h4>
                      </div>
                      <?php echo form_open_multipart("diarias/edit/{$diaria['id']}"); ?>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Favorecido</label>
                              <input type="text" name="favorecido" class="form-control" value="<?php echo $diaria['favorecido'] ?>">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Cargo</label>
                              <select class="form-control" name="cargo_id">
                                <option value="">Selecione...</option>
                                <?php foreach ($cargos as $key => $cargo): ?>
                                  <option value="<?php echo $cargo['id'] ?>" <?php echo $selected = ($diaria['cargo_id'] == $cargo['id']) ? 'selected':'' ?>><?php echo $cargo['name'].' '.$diaria['cargo_id'] ?> </option>
                                <?php endforeach; ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Data</label>
                              <input type="date" name="data" class="form-control" value="<?php echo date('Y-m-d', strtotime($diaria['data'])) ?>">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Destino</label>
                              <input type="text" name="destino" class="form-control" value="<?php echo $diaria['destino'] ?>">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Valor</label>
                              <input type="text" name="valor" class="form-control" value="<?php echo $diaria['valor'] ?>">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Motivo</label>
                              <textarea class="form-control" name="motivo"><?php echo $diaria['motivo'] ?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success btn-fill">Salvar Alterações</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                  </div>

                  <a href="#" data-toggle="modal" data-target="#myModalAnexo<?php echo $diaria['id'] ?>" class="btn btn-xs btn-fill btn-info"><i class="fa fa-paperclip"></i></a>

                <div class="modal fade" id="myModalAnexo<?php echo $diaria['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalAnexol<?php echo $diaria['id'] ?>Label">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexo<?php echo $diaria['id'] ?>Label">Novo Anexo</h4>
                      </div>
                      <?php echo form_open_multipart("diarias/add_anexo/{$diaria['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <input type="file" name="anexo" class="form-control" required>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                  </div>

              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar</h4>
      </div>
      <?php echo form_open_multipart('diarias/add'); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Favorecido</label>
                <input type="text" name="favorecido" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Cargo</label>
                <select class="form-control" name="cargo_id">
                  <option value="">Selecione...</option>
                  <?php foreach ($cargos as $key => $cargo): ?>
                    <option value="<?php echo $cargo['id'] ?>"><?php echo $cargo['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Data</label>
                <input type="date" name="data" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Destino</label>
                <input type="text" name="destino" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Motivo</label>
                <textarea class="form-control" name="motivo"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
