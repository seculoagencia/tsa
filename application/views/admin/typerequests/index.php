<div class="row">
  <div class="col-md-4">
    <div class="card">
      <?php echo form_open('typerequests/add'); ?>
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <h3>Tipos de Solicitações</h3>
      </div>
      <div class="content">
        <div class="form-inline">
          <input type="text" class="form-control" name="name" placeholder="Nome">
          <button type="submit" class="btn btn-fill btn-primary" name="button">Cadastrar</button>
        </div>
        <table class="table table-hover table-striped">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th style="min-width:92px;">Ações</th>
          </thead>
          <tbody>
            <?php foreach ($typerequests as $typerequest): ?>
              <tr>
                <td><?php echo $typerequest['id'] ?></td>
                <td><strong><?php echo $typerequest['name'] ?></strong></td>
                <td>
                  <!-- <?php echo anchor("typerequests/edit/{$typerequest['id']}", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-success btn-fill','rel'=>'tooltip','title'=>'Editar')) ?> -->
                  <?php echo anchor("#", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-success btn-fill','rel'=>'tooltip','title'=>'Editar','data-toggle'=>'modal','data-target'=>"#myModalCategoria{$typerequest['id']}")) ?>
                  <?php $report = $this->requests_model->select_for_category($typerequest['id']) ?>
                  <?php if ($report != null): ?>
                    <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalCategoriaRemove1{$typerequest['id']}")) ?>
                  <?php else: ?>
                    <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalCategoriaRemove2{$typerequest['id']}")) ?>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<?php foreach ($typerequests as $typerequest): ?>
<?php $report = $this->requests_model->select_for_category($typerequest['id']) ?>
<div class="modal fade" id="myModalCategoria<?php echo $typerequest['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Categoria</h4>
      </div>
      <?php echo form_open("admin/typerequests/edit/{$typerequest['id']}"); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo $typerequest['name'] ?>" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalCategoriaRemove1<?php echo $typerequest['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              O tipo de solicitação encontra-se em uso, você pode apenas editá-lo!
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/requests", 'Ir para Pedidos', array('class'=>'btn btn-primary btn-fill')) ?>
        <?php //echo anchor("admin/categories/remove_subcategory/{$subcategory['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalCategoriaRemove2<?php echo $typerequest['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Tem certeza que deseja excluir o tipo de solicitação?
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/typerequests/remove/{$typerequest['id']}", 'Remover', array('class'=>'btn btn-danger btn-fill')) ?>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
