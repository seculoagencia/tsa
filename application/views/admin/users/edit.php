<div class="row">
  <div class="col-lg-4 col-md-6">
    <div class="box">
      <?php echo form_open("users/edit/{$user['id']}"); ?>
      <div class="box-header with-border">
        <h3 class="box-title">Editar Usuário</h3>
      </div>
      <div class="box-body">
        <div class="form-group">
          <label for="grupo">Alterar Grupo</label>
          <select class="form-control" name="group_id">
            <?php foreach ($groups as $group): ?>
              <?php $selected = ($user['group_id'] == $group['id']) ? 'selected':''; ?>
                <option value="<?php echo $group['id'] ?>" <?php echo $selected ?>><?php echo $group['name'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>

      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success" name="button">Salvar Alterações</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
  <div class="col-lg-4 col-md-6">
    <div class="box">
      <?php echo form_open('users/edit'); ?>
      <div class="box-header with-border">
        <h3 class="box-title">Recuperação de Senha</h3>
      </div>
      <div class="box-body">
        <div class="form-group">
          <?php echo anchor('users/senha_provisoria', 'Enviar Senha Provisória', array('class'=>'btn bg-purple')) ?>
        </div>

      </div>
      <!-- <div class="box-footer">
      </div> -->
      <?php echo form_close();?>
    </div>
  </div>
</div>
