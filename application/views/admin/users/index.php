  <div class="col-md-4">
    <div class="card">
      <?php echo form_open('users/add'); ?>
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <h3>Novo Usuário</h3>
      </div>
      <div class="content">
        <div class="form-group">
          <label for="grupo">Grupo</label>
          <select class="form-control" name="group_id">
            <?php foreach ($groups as $group): ?>
              <?php if ($group['id'] != 1): ?>
                <option value="<?php echo $group['id'] ?>"><?php echo $group['name'] ?></option>
              <?php endif ?>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="name">Nome</label>
          <input type="text" class="form-control" name="name" value="">
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" class="form-control" name="username" value="">
        </div>
        <div class="form-group" id="password_id">
          <label for="password">Senha</label>
          <input type="password" class="form-control" name="password" id="password" value="">
        </div>
        <div id="r_password_id" class="form-group">
          <label for="r_password">Repetir Senha</label>
          <input type="password" class="form-control" name="r_password" id="r_password" value="">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" name="email" value="">
        </div>
        <div class="form-group">
          <label for="phone">Telefone</label>
          <input type="text" class="form-control" id="phone" name="phone" placeholder="(99) 99999-9999">
        </div>
        <div class="form-group text-right">
          <button type="submit" class="btn btn-fill btn-primary submit-perfil">Cadastrar</button>
        </div>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="header">
        <h3 class="box-title">Lista de Usuários</h3>
      </div>
      <div class="content">

        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th></th>
              <th>Nome</th>
              <th>Usuário</th>
              <th>Permissão</th>
              <th>Email</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($users as $user): ?>
              <?php if ($user['group_id'] != 1): ?>
                <tr>
                <td><?php echo $user['id'] ?></td>
                <td>
                  <?php $status = ($user['status'] == 1) ? '<i class="fa fa-circle text-success"></i>' : '<i class="fa fa-circle text-danger"></i>' ; ?>
                  <?php echo $status; ?>
                </td>
                <td>
                  <?php echo anchor("#","<strong>{$user['name']}</strong>", array('data-toggle'=>'modal','data-target'=>"#myModal{$user['id']}")) ?>
                </td>
                <td><?php echo $user['username'] ?></td>
                <td>
                  <?php foreach ($groups as $group): ?>
                    <?php if ($group['id']==$user['group_id']): ?>
                      <?php echo $group['name'] ?>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </td>
                <td><?php echo $user['email'] ?></td>
                <td>
                  <?php if ($user['group_id'] == 1): ?>
                    <?php if ($this->session->userdata('user_on')['group_id'] == 1): ?>
                      <?php echo anchor("users/profile/{$user['id']}", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-fill btn-success')) ?>
                      <?php if ($user['status'] == 1): ?>
                        <?php echo anchor("users/user_block/{$user['id']}", '<i class="fa fa-lock"></i>', array('class'=>'btn btn-xs btn-fill btn-danger')) ?>
                      <?php endif; ?>
                      <?php if ($user['status'] == 0): ?>
                        <?php echo anchor("users/user_unblock/{$user['id']}", '<i class="fa fa-unlock"></i>', array('class'=>'btn btn-xs btn-fill btn-primary')) ?>
                      <?php endif; ?>
                    <?php endif; ?>
                  <?php else: ?>
                    <?php echo anchor("users/profile/{$user['id']}", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-fill btn-success')) ?>
                    <?php if ($user['status'] == 1): ?>
                      <?php echo anchor("users/user_block/{$user['id']}", '<i class="fa fa-lock"></i>', array('class'=>'btn btn-xs btn-fill btn-danger')) ?>
                    <?php endif; ?>
                    <?php if ($user['status'] == 0): ?>
                      <?php echo anchor("users/user_unblock/{$user['id']}", '<i class="fa fa-unlock"></i>', array('class'=>'btn btn-xs btn-fill btn-primary')) ?>
                    <?php endif; ?>
                  <?php endif; ?>

                  <!-- <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a> -->
                </td>
              </tr>
              <?php endif ?>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

<?php foreach ($users as $user): ?>
  <div class="modal fade" id="myModal<?php echo $user['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $user['id'] ?>">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Detalhes do Usuário</h4>
        </div>
        <div class="modal-body">
          <table class="table table-hover">
            <tr><td><strong>Status:</strong> <?php $status = ($user['status'] == 1) ? 'Ativo' : 'Inativo' ;  ?><?php echo $status ?></td></tr>
            <tr><td><strong>Nome:</strong> <?php echo $user['name'] ?></td></tr>
            <tr><td><strong>Usuário:</strong> <?php echo $user['username'] ?></td></tr>
            <tr><td><strong>Email:</strong> <?php echo $user['email'] ?></td></tr>
            <tr><td><strong>Telefone:</strong> <?php echo $user['phone'] ?></td></tr>
            <?php foreach ($groups as $group): ?>
              <?php if ($group['id']==$user['group_id']): ?>
                <tr><td><strong>Grupo:</strong> <?php echo $group['name'] ?></td></tr>
              <?php endif; ?>
            <?php endforeach; ?>
            <tr><td><strong>Data de Criação:</strong> <?php echo date('d/m/Y H:i:s',strtotime($user['created'])) ?></td></tr>
            <tr><td><strong>Última Modificação:</strong> <?php echo date('d/m/Y H:i:s',strtotime($user['modified'])) ?></td></tr>

          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
