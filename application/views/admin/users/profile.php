    <div class="col-md-6">
      <div class="card">
        <div class="header">
          <h3>Editar Perfil</h3>
        </div>
        <div class="content">
              <?php echo form_open("users/profile/{$user['id']}"); ?>
              <div class="form-group">
                <label for="grupo">Alterar Grupo</label>
                <select class="form-control" name="group_id">
                  <?php foreach ($groups as $group): ?>
                    <?php $selected = ($user['group_id'] == $group['id']) ? 'selected':''; ?>
                      <option value="<?php echo $group['id'] ?>" <?php echo $selected ?>><?php echo $group['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo $user['name'] ?>">
              </div>
              <div class="form-group">
                <label for="username">Usuário</label>
                <input type="text" name="username" class="form-control" value="<?php echo $user['username'] ?>">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" class="form-control" value="<?php echo $user['email'] ?>">
              </div>
              <div class="form-group">
                <label for="phone">Telefone</label>
                <input type="text" class="form-control" value="<?php echo $user['phone'] ?>" id="phone" name="phone" placeholder="(99) 99999-9999">
              </div>

              <button type="submit" class="btn btn-success btn-fill" name="button">Salvar Alterações</button>
              <?php echo form_close();?>

        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="header">
          <h3>Alterar Senha</h3>
        </div>
        <div class="content">
          <?php echo form_open("users/alterar_senha/{$user['id']}"); ?>

          <!-- <div class="form-group">
            <label for="o_password">Senha Atual</label>
            <input type="password" name="o_password" class="form-control">
          </div> -->
          <div class="form-group" id="password_id">
            <label for="password">Nova Senha</label>
            <input type="password" name="password" id="password" class="form-control">
          </div>
          <div class="form-group" id="r_password_id">
            <label for="r_password">Repita a Nova Senha</label>
            <input type="password" name="r_password" id="r_password" class="form-control">
          </div>

          <button type="submit" class="btn btn-success btn-fill submit-perfil" name="button">Alterar</button>
          <?php echo anchor('users/senha_provisoria', 'Enviar Senha Provisória', array('class'=>'btn btn-primary btn-fill')) ?>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
