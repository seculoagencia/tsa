<div style="margin-bottom:1em">
  <a href="<?php echo site_url('admin/imports/empenhos/sicap') ?>" class="btn btn-fill btn-primary">EMPENHOS</a>
  <a href="#" class="btn btn-fill btn-primary active">RECEITAS</a>
</div>
  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/receitas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/receitas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <a href="<?php echo site_url('admin/imports/add') ?>" class="btn btn-primary btn-fill"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <th>ID</th>
          <th>Código do Órgão</th>
          <th>Data da Arrecadação</th>
          <th>Unidade Orçamentária</th>
          <th>Código da Conta Receita</th>
          <th>Código do Banco</th>
          <th>Agência</th>
          <th>Número da Conta</th>
          <th>Valor</th>
          <th>Código da Receita Vinculado</th>
          <th style="min-width:92px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php foreach ($receitas as $receita): ?>
            <?php $cont = $cont + 1; ?>
            <tr>
              <td><?php echo $cont ?></td>
              <td><?php echo $receita['CodOrgao'] ?></td>
              <td><?php echo date('d/m/Y', strtotime($receita['DataArrecadacao'])) ?></td>
              <?php $undorcamentaria = $this->sicaps_model->getVerifica('sicap_UndOrcamentaria','CodUndOrcamentaria',$receita['CodUndOrcamentaria']) ?>
              <td><?php echo $receita['CodUndOrcamentaria'] ?> - <?php echo $undorcamentaria['Nome'] ?></td>
              <td><?php echo $receita['CodContaReceita'] ?></td>
              <td><?php echo $receita['CodBanco'] ?></td>
              <td><?php echo $receita['CodAgencia'] ?></td>
              <td><?php echo $receita['NumConta'] ?></td>
              <td>R$ <?php echo number_format($receita['Valor'],2,',','.') ?></td>
              <td><?php echo $receita['CodRecVinculado'] ?></td>
              <td>
                <a href='<?php echo site_url("admin/receitas/remove/{$receita['id']}") ?>' rel="tooltip" title="Remover" class="btn btn-xs btn-danger btn-fill"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nova Prestação de Contas</h4>
      </div>
      <?php echo form_open_multipart('receitas/add'); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Código</label>
                <input type="text" name="codigo" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Data</label>
                <input type="date" name="data" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Descrição</label>
                <textarea class="form-control" name="descricao"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
