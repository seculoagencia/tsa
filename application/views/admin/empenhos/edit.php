<div class="col-md-12">
  <div class="card">
    <div class="header">
      <div class="row">
        <div class="col-md-6">
          <h4 style="margin: 0">Editar Empenhos</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?php echo site_url('admin/empenhos/details/'.$empenho['id']) ?>" class="btn btn-fill btn-primary">Adicionar Liquidações e Pagamentos</a>
        </div>
      </div>
    </div>
    <div class="content">
      <?php echo form_open_multipart('empenhos/edit/'.$empenho['id']); ?>
        <div class="modal-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Data</label>
            <input type="date" name="data" class="form-control" value="<?php echo $empenho['data'] ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Número</label>
            <input type="text" name="numero" class="form-control" value="<?php echo $empenho['numero'] ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Credor</label>
            <input type="text" name="credor" class="form-control" value="<?php echo $empenho['credor'] ?>" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Unidade Orçamentária</label>
            <input type="text" name="unidade_orcamentaria" class="form-control" value="<?php echo $empenho['unidade_orcamentaria'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Função</label>
            <input type="text" name="funcao" class="form-control" value="<?php echo $empenho['funcao'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Sub-Função</label>
            <input type="text" name="subfuncao" class="form-control" value="<?php echo $empenho['subfuncao'] ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Programa</label>
            <input type="text" name="programa" class="form-control" value="<?php echo $empenho['programa'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Ação</label>
            <input type="text" name="acao" class="form-control" value="<?php echo $empenho['acao'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Fonte de Recurso</label>
            <input type="text" name="fonte_de_recurso" class="form-control" value="<?php echo $empenho['fonte_de_recurso'] ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Elemento de Despesa</label>
            <input type="text" name="elemento_de_despesa" class="form-control" value="<?php echo $empenho['elemento_de_despesa'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>CPF/CNPJ</label>
            <input type="text" name="cpf_cnpj" class="form-control" value="<?php echo $empenho['cpf_cnpj'] ?>">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Número do Processo</label>
            <input type="text" name="numero_do_processo" class="form-control" value="<?php echo $empenho['numero_do_processo'] ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Histórico</label>
            <textarea class="form-control" required name="historico"><?php echo $empenho['historico'] ?></textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
    <a href="<?php echo site_url('admin/empenhos') ?>" class="btn btn-success">Cancelar</a>
    <button type="submit" class="btn btn-success btn-fill">Salvar</button>
    </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
