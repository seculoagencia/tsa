<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<div class="row">
			<div class="col-md-6">
				<h4 style="margin: 0">Adicionar Liquidações e Pagamentos</h4>
			</div>
			<div class="col-md-6 text-right">
				<a href="<?php echo site_url('admin/empenhos/edit/'.$empenho['id']) ?>" class="btn btn-fill btn-success">Editar Empenho</a>
			</div>
		</div>
	</div>
	<div class="content table-responsive table-full-width">
		<?php //echo form_open_multipart('empenhos/add'); ?>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Data</label>
		        <p><?php echo $empenho['data'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número</label>
		        <p><?php echo $empenho['numero'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Credor</label>
		        <p><?php echo $empenho['credor'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Unidade Orçamentária</label>
		        <p><?php echo $empenho['unidade_orcamentaria'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Função</label>
		        <p><?php echo $empenho['funcao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Sub-Função</label>
		        <p><?php echo $empenho['subfuncao'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Programa</label>
		        <p><?php echo $empenho['programa'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Ação</label>
		        <p><?php echo $empenho['acao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Fonte de Recurso</label>
		      	<p><?php echo $empenho['fonte_de_recurso'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Elemento de Despesa</label>
		      	<p><?php echo $empenho['elemento_de_despesa'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>CPF/CNPJ</label>
		      	<p><?php echo $empenho['cpf_cnpj'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número do Processo</label>
		      	<p><?php echo $empenho['numero_do_processo'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Valor</label>
		      	<p>
		      		<?php $cont_liq = 0; ?>
		      		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		      			<?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_liq,2,',','.') ?>
		      		
		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Liquidado</label>
		      	<p>
		      		<?php $cont_liq = 0; ?>
		      		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		      			<?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_liq,2,',','.') ?>
		      		
		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Pago</label>
		      	<p>
		      		<?php $cont_pag = 0; ?>
		      		<?php foreach ($pagamentos as $key => $pagamento): ?>
		      			<?php $cont_pag = $cont_pag + $pagamento['valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_pag,2,',','.') ?>
		      		
		      	</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Histórico</label>
		        <p><?php echo $empenho['historico'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Anexo de Arquivo</label>
		        <div>
		            <a href="#" data-toggle="modal" data-target="#myModalAnexo" class="btn btn-xs btn-fill btn-info">Novo Anexo</a>
                
                <div class="modal fade" id="myModalAnexo" tabindex="-1" role="dialog" aria-labelledby="myModalAnexolLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexoLabel">Novo Anexo</h4>
                      </div>
                      <?php echo form_open_multipart("empenhos/add_anexo/{$empenho['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <input type="file" name="anexo" class="form-control" required>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
		        </div>
		        <br><br>
		        <p>
		            <?php $anexos = $this->empenhosanexos_model->select($empenho['id']) ?>
                    <?php foreach($anexos as $key => $anexo): ?>
                        <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
                    <?php endforeach; ?>
		        </p>
		      </div>
		    </div>
		  </div>
		  <hr>
		  <div class="col-md-12">
		      <h4 style="margin: 0">Liquidações</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Número</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  			<th>Ações</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		  		<tr>
		  			<td><?php echo $liquidacao['numero'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($liquidacao['data'])) ?></td>
		  			<td>R$ <?php echo number_format($liquidacao['valor'],2,',','.') ?></td>
		  			<td>
		  				<a href="#" data-toggle="modal" data-target="#modalLiq<?php echo $liquidacao['id'] ?>" class="btn btn-xs btn-fill btn-success"><i class="fa fa-pencil"></i></a>
						<a href="<?php echo site_url('empenhos/remove_liquidacao/'.$liquidacao['id']) ?>" class="btn btn-xs btn-fill btn-danger"><i class="fa fa-remove"></i></a>

		  			</td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  	<tfoot>
		  		<tr>
		  			<td colspan="4">
		  				<center>
				  			<a href="#" data-toggle="modal" data-target="#modalLiq" class="btn btn-fill btn-primary">Nova Liquidação</a>
				  		</center>
		  			</td>
		  		</tr>
		  	</tfoot>
		  </table>

		  </div>
		  <hr>
		  <div class="col-md-12">
		  <h4 style="margin: 0">Pagamentos</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Banco</th>
		  			<th>Agência</th>
		  			<th>Número da Conta</th>
		  			<th>Documento</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  			<th>Ações</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  	<?php foreach ($pagamentos as $key => $pagamento): ?>
		  		<tr>
		  			<td><?php echo $pagamento['banco'] ?></td>
		  			<td><?php echo $pagamento['agencia'] ?></td>
		  			<td><?php echo $pagamento['conta'] ?></td>
		  			<td><?php echo $pagamento['documento'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($pagamento['data'])) ?></td>
		  			<td>R$ <?php echo number_format($pagamento['valor'],2,',','.') ?></td>
		  			<td>
		  				<a href="#" data-toggle="modal" data-target="#modalPag<?php echo $pagamento['id'] ?>" class="btn btn-xs btn-fill btn-success"><i class="fa fa-pencil"></i></a>
		  				<a href="<?php echo site_url('empenhos/remove_pagamento/'.$pagamento['id']) ?>" class="btn btn-xs btn-fill btn-danger"><i class="fa fa-remove"></i></a>

		  				<div class="modal fade" id="modalPag<?php echo $pagamento['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalPag<?php echo $pagamento['id'] ?>Label">
						  <div class="modal-dialog modal-sm" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="modalPag<?php echo $pagamento['id'] ?>Label">Novo Pagamento</h4>
						      </div>
						      <?php echo form_open('empenhos/pagamento_edit/'.$pagamento['id']); ?>
						      <div class="modal-body">
						          <div class="row">
						            <div class="col-md-12">
						              <div class="form-group">
						                <label>Banco</label>
						                <input type="text" name="banco" class="form-control" value="<?php echo $pagamento['banco'] ?>" required>
						              </div>
						            </div>
						            <div class="col-md-12">
						              <div class="form-group">
						                <label>Agência</label>
						                <input type="text" name="agencia" class="form-control" value="<?php echo $pagamento['agencia'] ?>" required>
						              </div>
						            </div>
						            <div class="col-md-12">
						              <div class="form-group">
						                <label>Conta</label>
						                <input type="text" name="conta" class="form-control" value="<?php echo $pagamento['conta'] ?>" required>
						              </div>
						            </div>
						            <div class="col-md-12">
						              <div class="form-group">
						                <label>Documento</label>
						                <input type="text" name="documento" class="form-control" value="<?php echo $pagamento['documento'] ?>" required>
						              </div>
						            </div>
						            <div class="col-md-12">
								      <div class="form-group">
								        <label>Data</label>
								        <input type="date" name="data" class="form-control" value="<?php echo $pagamento['data'] ?>" required>
								      </div>
								    </div>
								    <div class="col-md-12">
						              <div class="form-group">
						                <label>Valor</label>
						                <input type="text" name="valor" class="form-control" value="<?php echo $pagamento['valor'] ?>" required>
						              </div>
						            </div>
						          </div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
						        <button type="submit" class="btn btn-success btn-fill">Salvar</button>
						      </div>
						      <?php echo form_close();?>
						    </div>
						  </div>
						</div>
		  			</td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  	<tfoot>
		  		<tr>
		  			<td colspan="7">
		  				<center>
				  			<a href="#" data-toggle="modal" data-target="#modalPag" class="btn btn-fill btn-primary">Novo Pagamento</a>
				  		</center>
		  			</td>
		  		</tr>
		  	</tfoot>
		  </table>
		  </div>
		</div>
		<?php //echo form_close();?>
	</div>
</div>

<div class="modal fade" id="modalLiq" tabindex="-1" role="dialog" aria-labelledby="modalLiqLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLiqLabel">Nova Liquidação</h4>
      </div>
      <?php echo form_open('empenhos/liquidacao_add/'.$empenho['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Número</label>
                <input type="text" name="numero" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="modalPag" tabindex="-1" role="dialog" aria-labelledby="modalPagLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPagLabel">Novo Pagamento</h4>
      </div>
      <?php echo form_open('empenhos/pagamento_add/'.$empenho['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Banco</label>
                <input type="text" name="banco" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Agência</label>
                <input type="text" name="agencia" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Conta</label>
                <input type="text" name="conta" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Documento</label>
                <input type="text" name="documento" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control" required>
		      </div>
		    </div>
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<?php foreach ($liquidacoes as $key => $liquidacao): ?>
	
<div class="modal fade" id="modalLiq<?php echo $liquidacao['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalLiq<?php echo $liquidacao['id'] ?>Label">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLiq<?php echo $liquidacao['id'] ?>Label">Editar Liquidação</h4>
      </div>
      <?php echo form_open('empenhos/liquidacao_edit/'.$liquidacao['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Número</label>
                <input type="text" name="numero" class="form-control" value="<?php echo $liquidacao['numero'] ?>" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control" value="<?php echo $liquidacao['data'] ?>" required>
		      </div>
		    </div>
          </div>
          <div class="row">
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" value="<?php echo $liquidacao['valor'] ?>" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Salvar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<?php endforeach ?>