<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<h4 style="margin: 0">Novo Empenho</h4>
	</div>
	<div class="content table-responsive table-full-width">
		<?php echo form_open_multipart('empenhos/add'); ?>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control" required>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número</label>
		        <input type="text" name="numero" class="form-control" required>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Credor</label>
		        <input type="text" name="credor" class="form-control" required>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Unidade Orçamentária</label>
		        <input type="text" name="unidade_orcamentaria" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Função</label>
		        <input type="text" name="funcao" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Sub-Função</label>
		        <input type="text" name="subfuncao" class="form-control">
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Programa</label>
		        <input type="text" name="programa" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Ação</label>
		        <input type="text" name="acao" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Fonte de Recurso</label>
		        <input type="text" name="fonte_de_recurso" class="form-control">
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Elemento de Despesa</label>
		        <input type="text" name="elemento_de_despesa" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>CPF/CNPJ</label>
		        <input type="text" name="cpf_cnpj" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número do Processo</label>
		        <input type="text" name="numero_do_processo" class="form-control">
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Histórico</label>
		        <textarea class="form-control" name="historico"></textarea>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal-footer">
		<a href="<?php echo site_url('admin/empenhos') ?>" class="btn btn-primary" data-dismiss="modal">Voltar</a>
		<button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
		</div>
		<?php echo form_close();?>
	</div>
</div>