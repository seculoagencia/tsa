<div class="col-md-12">
  <div class="card">
    <div class="header">
      <h3>Detalhes do Pedido: <?php echo $request['serial'] ?></h3>
    </div>
    <div class="content">
      <div>
        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome</strong><br> <span><?php echo $request['name'] ?></span></p>
            <p><strong>Email</strong><br> <span id="cEmail"><?php echo $request['email'] ?></span></p>
            <p><strong>CPF/CNPJ</strong><br> <span id="cCpf"><?php echo $request['cpf'] ?></span></p>
            <p><strong>Telefone</strong><br> <span id="cPhone"><?php echo $request['phone'] ?></span></p>
          </div>
          <div class="col-md-4">
            <p><strong>Prioridade</strong><br>
              <span id="cPriority">
                <?php
                switch ($request['priority']) {
                  case '1':
                  echo "Alta";
                  break;
                  case '2':
                  echo "Normal";
                  break;
                  case '3':
                  echo "Baixa";
                  break;
                }
                ?>
              </span>
            </p>
            <p><strong>Endereço</strong><br> <span id="cLocation"><?php echo $request['location'] ?></span></p>
            <p><strong>Como deseja receber sua resposta?</strong><br> <span id="cType_answer"><?php echo $request['type_answer'] ?></span></p>
            <?php $typerequest = $this->typerequests_model->select($request['typerequest_id']) ?>
            <p><strong>Tipo de Solicitação</strong><br> <span id="cTyperequests"><?php echo $typerequest['name'] ?></span></p>
          </div>
          <div class="col-md-4">
            <p><strong>Assunto</strong><br> <span id="cSubject"><?php echo $request['subject'] ?></span></p>
            <p><strong>Nº de Protocolo</strong><br> <span id="cSerial"><?php echo $request['serial'] ?></span></p>
            <p><strong>Data de Registro</strong><br> <span id="cCreated"><?php echo date('d/m/Y', strtotime($request['created'])) ?></span></p>
            <p><strong>Status</strong><br>
              <span class="" id="cStatus">
                <?php
                switch ($request['status']) {
                  case 0:
                    echo "Aberto";
                    break;
                  case 1:
                    echo "Respondido";
                    break;
                  case 2:
                    echo "Em Tramitação";
                    break;
                  case 3:
                    echo "Negado";
                    break;
                }
                ?>

              </span>
            </p>
          </div>

        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="12">
              <p><strong>Mensagem</strong></p>
              <p id="cMessage"><?php echo $request['message'] ?></p>
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <?php if (!$answers): ?>
              <h3>Responder</h3>
              <?php echo form_open_multipart("requests/anwser_add/{$request['id']}") ?>
              <div class="form-group">
                <label>Mensagem</label>
                <textarea name="description" rows="8" class="form-control" required></textarea>
              </div>
              <div class="form-group">
                <label>Anexo</label>
                <input type="file" name="anexo" class="form-control">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-fill">Enviar</button>
              </div>
              <?php echo form_close(); ?>
            <?php endif; ?>
            <?php if ($answers): ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Respostas</h3>
                </div>
                <div class="panel-body">
                  <?php foreach ($answers as $answer): ?>
                    <p class="text-<?php echo $align = ($answer['user_id'] == null) ? 'right':'left' ?>">
                      <?php if ($answer['user_id'] == null): ?>
                        <small><strong><?php echo $request['name'] ?></strong></small>
                      <?php else: ?>
                        <small><strong>Ouvidoria</strong></small>
                      <?php endif; ?>
                    <br>
                    <?php echo $answer['description'] ?><br>
                    <?php if ($answer['anexo'] != null): ?>
                    <a href="<?php echo site_url($answer['anexo']) ?>" target="_blank"><i class="fa fa-download"></i> Anexo</a><br>
                    <?php endif; ?>
                    <small><?php echo date('d/m/Y', strtotime($answer['created'])) ?></small></p>
                    <hr>
                  <?php endforeach; ?>
                  <?php echo form_open_multipart("requests/anwser_add/{$request['id']}") ?>
                  <div class="form-group">
                    <label>Mensagem</label>
                    <textarea name="description" rows="8" class="form-control" required></textarea>
                  </div>
                    <div class="form-group">
                      <label>Anexo</label>
                      <input type="file" name="anexo" class="form-control">
                    </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-fill">Enviar Nova Resposta</button>
                  </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            <?php endif; ?>
          </div>
          <hr>
        </div>
      </div>

    </div>
  </div>
</div>
