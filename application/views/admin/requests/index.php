  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/requests') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php for ($i=0; $i < count($year); $i++): ?>
                    <option value="<?php echo $year[$i] ?>" <?php echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Prioridade</label><br>
                <select class="form-control" name="priority">
                  <option value="">Todas...</option>
                  <?php $cont = 0; ?>
                  <?php foreach ($priority as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>"><?php echo $key ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Tipo de Solicitações</label><br>
                <select class="form-control" name="typeresquest">
                  <option value="">Todas...</option>
                  <?php foreach ($typerequests as $typerequest): ?>
                    <option value="<?php echo $typerequest['id'] ?>"><?php echo $typerequest['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/requests') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por assunto...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <!-- <button type="button" class="btn btn-primary btn-fill" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tipo de Solicitação</button> -->
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <tr>
            <th></th>
            <th>ID</th>
            <th>Data</th>
            <th>Protocolo</th>
            <th>Nome</th>
            <th>Assunto</th>
            <th>Tipo de Solicitação</th>
            <th>Status</th>
            <!-- <th style="text-align:center">Anexo</th> -->
          </tr>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php foreach ($requests as $request): ?>
            <?php $cont = $cont + 1; ?>
            <?php //$category = $this->categories_model->select_id($request['category_id']) ?>
            <?php //$subcategory = $this->subcategories_model->select_id($request['subcategory_id']) ?>
            <?php $typerequest = $this->typerequests_model->select($request['typerequest_id']) ?>
            <tr>
              <td>
                <?php
                switch ($request['priority']) {
                  case 1:
                    $priority = "danger";
                    break;
                  case 2:
                    $priority = "warning";
                    break;
                  case 3:
                    $priority = "success";
                    break;

                }
                 ?>
                <i class="fa fa-circle text-<?php echo $priority ?>"></i>
              </td>
              <td><?php echo $cont ?></td>
              <td><?php echo date('d/m/Y', strtotime($request['created'])) ?></td>
              <td><a href="<?php echo site_url('admin/requests/details/'.$request['id']) ?>"><strong><?php echo $request['serial'] ?></strong></a></td>
              <td>
                <?php echo $request['name'] ?>
              </td>
              <td><?php echo $request['subject'] ?></td>
              <td><?php echo $typerequest['name'] ?></td>
              <td>
                <?php
                  switch ($request['status']) {
                    case 0:
                      echo "Aberto";
                      break;
                    case 1:
                      echo "Respondido";
                      break;
                    case 2:
                      echo "Em Tramitação";
                      break;
                    case 3:
                      echo "Negado";
                      break;
                  }
                 ?>
              </td>
              <td style="text-align:center"><a href="javascript:;"data-toggle="modal" data-target="#alterarStatus" class="btn btn-xs btn-dark btn-fill" target="_blank">Alterar Status</i></a></td>
              <div class="modal fade" id="alterarStatus" tabindex="-1" role="dialog" aria-labelledby="alterarStatusLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="alterarStatusLabel">Alterar Status</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form class="" action="<?php echo site_url('requests/alterarStatus') ?>/<?php echo $request['id'] ?>" method="post">
                      <div class="modal-body">
                          <select class="form-control" name="status">
                            <option value="0">Aberto</option>
                            <option value="1">Respondido</option>
                            <option value="2">Em Tramitação</option>
                            <option value="3">Negado</option>
                          </select>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-fill btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-fill btn-primary">Alterar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <i class="fa fa-circle text-success"></i> = Prioridade Baixa<br>
  <i class="fa fa-circle text-warning"></i> = Prioridade Normal<br>
  <i class="fa fa-circle text-danger"></i> = Prioridade Alta
</div>
