<div class="card">
  <div class="header">
    <h3>Configurações</h3>
  </div>
  <div class="content">
    <?php echo form_open("settings/edit/{$settings['id']}") ?>
    <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Nome da Instituição</label>
            <input type="text" name="name" class="form-control" value="<?php echo $settings['name'] ?>">
          </div>
          <div class="form-group">
            <label>URL do Site</label>
            <input type="text" name="site_url" class="form-control" value="<?php echo $settings['site_url'] ?>">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $settings['email'] ?>">
          </div>
          <div class="form-group">
            <input type="checkbox" name="view_last_update" <?php echo ($settings['view_last_update'] == true) ? 'checked':'' ?>>
            <label>Exibir Última Atualização</label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ReCAPTCHA - Chave do Site</label>
            <input type="text" name="recaptcha_site" class="form-control" value="<?php echo $settings['recaptcha_site'] ?>">
          </div>
          <div class="form-group">
            <label>ReCAPTCHA - Chave Secreta</label>
            <input type="text" name="recaptcha_secret" class="form-control" value="<?php echo $settings['recaptcha_secret'] ?>">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form_group">
            <button type="submit" class="btn btn-success btn-fill"> Salvar Alterações</button>
          </div>
        </div>
    </div>
    <?php echo form_close() ?>
  </div>
</div>
