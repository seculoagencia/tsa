<div class="col-md-8">
  <div class="card">
    <div class="header">
      <h3>Editar Prestação de Contas</h3>
    </div>
    <div class="content">
      <?php echo form_open_multipart('licitacoes/edit/'.$receita['id']); ?>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Título</label>
              <input type="text" name="name" class="form-control" value="<?php echo $receita['name'] ?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Categoria</label>
              <select class="form-control" name="category_id" required>
                <option>Selecione...</option>
                <?php foreach ($categories as $category): ?>
                  <option value="<?php echo $category['id'] ?>" <?php echo $selected = ($receita['category_id'] == $category['id']) ? 'selected' : '' ; ?>><?php echo $category['name'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Sub-Categoria</label>
              <select class="form-control" name="subcategory_id" required>
                <option>Selecione...</option>
                <?php foreach ($subcategories as $subcategory): ?>
                  <option value="<?php echo $subcategory['id'] ?>"<?php echo $selected = ($receita['subcategory_id'] == $subcategory['id']) ? 'selected' : '' ; ?>><?php echo $subcategory['name'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <p>
              <a href="<?php echo site_url($receita['anexo']) ?>">Ver PDF</a>
            </p>
            <div class="form-group">
              <label>Trocar Anexo de PDF</label>
              <input type="file" name="anexo" class="form-control">
            </div>
          </div>
          <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
          </div>
        </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
