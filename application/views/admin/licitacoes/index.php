  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/licitacoes') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <option value="">Todos...</option>
                  <?php for ($i=0; $i < count($year); $i++): ?>
                    <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/licitacoes') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="button" class="btn btn-primary btn-fill" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <th>Tipo</th>
          <th>Órgão</th>
          <th>Modalidade</th>
          <th>Data/Hora</th>
          <th>Objeto</th>
          <th>Preço</th>
          <!-- <th style="text-align:center">Anexo</th> -->
          <th>Status</th>
          <th>Vencedor</th>
          <th style="min-width:130px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php foreach ($licitacoes as $licitacao): ?>
            <tr>
              <td><?php echo $tipo = ($licitacao['tipo'] == 1) ? 'Contrato' : 'Licitação' ; ?></td>
              <td><?php echo $licitacao['orgao'] ?></td>
              <td><?php echo $licitacao['modalidade'] ?></td>
              <td><?php echo date('d/m/Y H:i', strtotime($licitacao['data'])) ?></td>
              <td>
                <?php if ($this->db->database != 'sistemasalfa_trans2'): ?>
                  <button class="btn btn-primary" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<?php echo $licitacao['objeto'] ?>"><i class="fa fa-eye"></i> Visualizar/Ocultar</button> 
                <?php else: ?>
                  <?php echo $licitacao['objeto'] ?>
                <?php endif; ?>
              </td>
              <td>R$ <?php echo number_format($licitacao['valor'],2,',','.') ?></td>
              <!-- <td style="text-align:center">
                  <?php //$anexos = $this->licitacoesanexos_model->select($licitacao['id']) ?>
                <?php //foreach($anexos as $key => $anexo): ?>
                    <a href="<?php //echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php //echo $key + 1 ?></a><br>
                <?php //endforeach; ?>
              </td> -->
              <td>
                  <?php 
                    if ($licitacao['status'] == null) {
                      echo "Suspenso";
                    } elseif ($licitacao['status']) {
                      echo "Concluído";
                    } else {
                      echo "Em Andamento";
                    }
                  ?>
              </td>
              <td><?php echo $licitacao['vencedor'] ?></td>
              <td>
                <a href='#' data-toggle="modal" data-target="#myModalEdit<?php echo $licitacao['id'] ?>" rel="tooltip" title="Editar" class="btn btn-xs btn-success btn-fill">
                  <i class="fa fa-pencil"></i>
                </a>
                <a href='<?php echo site_url("admin/licitacoes/remove/{$licitacao['id']}") ?>' rel="tooltip" title="Remover" class="btn btn-xs btn-danger btn-fill">
                  <i class="fa fa-trash"></i>
                </a>

                <div class="modal fade" id="myModalEdit<?php echo $licitacao['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalEdit<?php echo $licitacao['id'] ?>Label">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalEdit<?php echo $licitacao['id'] ?>Label">Editar Licitação</h4>
                      </div>
                      <?php echo form_open_multipart("licitacoes/edit/{$licitacao['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="tipo">
                                  <option value="1" <?php echo ($licitacao['tipo'] == 1) ? 'selected':'' ?>>Contrato</option>
                                  <option value="2" <?php echo ($licitacao['tipo'] == 2) ? 'selected':'' ?>>Licitações</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Órgão</label>
                                <input type="text" name="orgao" class="form-control" value="<?php echo $licitacao['orgao'] ?>">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Modalidade</label>
                                <input type="text" name="modalidade" class="form-control" value="<?php echo $licitacao['modalidade'] ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Data</label>
                                <input type="datetime-local" name="data" class="form-control" value="<?php echo $this->licitacoes_model->get_date($licitacao['data']) ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Preço</label>
                                <input type="text" name="valor" class="form-control" value="<?php echo $licitacao['valor'] ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                  <option value="1">Concluído</option>
                                  <option value="0">Em Andamento</option>
                                  <option value="">Suspenso</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Vencedor</label>
                                <input type="text" name="vencedor" class="form-control" value="<?php echo $licitacao['vencedor'] ?>">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Objeto</label>
                                <textarea class="form-control" name="objeto"><?php echo $licitacao['objeto'] ?></textarea>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success btn-fill">Salvar Alterações</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                  </div>

                  <a href="#" data-toggle="modal" data-target="#myModalAnexo<?php echo $licitacao['id'] ?>" class="btn btn-xs btn-fill btn-info"><i class="fa fa-paperclip"></i></a>

                <div class="modal fade" id="myModalAnexo<?php echo $licitacao['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalAnexol<?php echo $licitacao['id'] ?>Label">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexo<?php echo $licitacao['id'] ?>Label">Novo Anexo</h4>
                      </div>
                      <?php echo form_open_multipart("licitacoes/add_anexo/{$licitacao['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <input type="file" name="anexo" class="form-control" required>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <?php $anexos = $this->licitacoesanexos_model->select($licitacao['id']) ?>
                              <table class="table table-striped">
                              <?php foreach($anexos as $key => $anexo): ?>
                                <tr>
                                  <td>
                                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">
                                      <?php echo ($anexo['name']) ? $anexo['name'] : 'Anexo '.($key+1).'' ?>
                                    </a>
                                  </td>
                                  <td class="text-right">
                                    <a href="<?php echo site_url('licitacoes/remove_anexo/'.$anexo['id']) ?>" class="btn btn-fill btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                  </td>
                                </tr>
                              <?php endforeach; ?>
                              </table>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success btn-fill">Salvar Alterações</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar</h4>
      </div>
      <?php echo form_open_multipart('licitacoes/add'); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Tipo</label>
                <select class="form-control" name="tipo">
                  <option value="1">Contrato</option>
                  <option value="2" selected>Licitações</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Órgão</label>
                <input type="text" name="orgao" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Modalidade</label>
                <input type="text" name="modalidade" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Data</label>
                <input type="datetime-local" name="data" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Preço</label>
                <input type="text" name="valor" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                  <option value="1">Concluído</option>
                  <option value="0">Em Andamento</option>
                  <option value="">Suspenso</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Vencedor</label>
                <input type="text" name="vencedor" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Objeto</label>
                <textarea class="form-control" name="objeto"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
