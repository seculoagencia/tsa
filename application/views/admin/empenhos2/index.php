  <div style="margin-bottom:1em">
    <a href="#" class="btn btn-fill btn-primary active">EMPENHOS</a>
    <a href="<?php echo site_url('admin/imports/receitas/tc') ?>" class="btn btn-fill btn-primary">RECEITAS</a>
  </div>

  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/imports/tc') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>

              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/imports') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <a href="<?php echo site_url('admin/imports/add') ?>" class="btn btn-primary btn-fill"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <th>Tipo</th>
          <th>Número</th>
          <th>Credor</th>
          <th>Data</th>
          <th>Liquidação</th>
          <th style="min-width:92px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php if ($empenhos): ?>
            <?php foreach($empenhos as $empenho): ?>
              <?php $cont = $cont + 1; ?>
              <tr>
                <td>TC Cloud</td>
                <td>
                  <a href="<?php echo site_url('admin/imports/tc/details/'.$empenho['Numero']) ?>"><strong><?php echo $empenho['Numero'] ?></strong></a>
                </td>
                <td>
                  <?php $credor = $this->imports_model->credor($empenho['CPF'],$empenho['CNPJ']) ?>
                  <?php echo $credor['Nome'] ?>
                </td>
                <td><?php echo date('d/m/Y', strtotime($empenho['Data'])) ?></td>
                <td>
                  R$ <?php echo number_format($empenho['ValorLiqAteAgora'],2,',','.') ?>
                </td>
                <td>
                  <!-- <a href='<?php echo site_url("admin/imports/edit/{$empenho['id']}") ?>' rel="tooltip" title="Editar" class="btn btn-xs btn-success btn-fill"><i class="fa fa-pencil"></i></a> -->
                  <a href='<?php echo site_url("admin/imports/remove/{$empenho['id']}") ?>' rel="tooltip" title="Remover" class="btn btn-xs btn-danger btn-fill"><i class="fa fa-trash"></i></a>
                  <!-- <a href="#" data-toggle="modal" data-target="#myModalAnexo" class="btn btn-xs btn-fill btn-info"><i class="fa fa-paperclip"></i></a> -->

                <div class="modal fade" id="myModalAnexo" tabindex="-1" role="dialog" aria-labelledby="myModalAnexolLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexoLabel">Novo Anexo</h4>
                      </div>
                      <?php echo form_open_multipart("empenhos/add_anexo/{$empenho['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <input type="file" name="anexo" class="form-control" required>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
