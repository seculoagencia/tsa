<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<div class="row">
			<div class="col-md-6">
				<h4 style="margin: 0">Detalhes do Empenho</h4>
			</div>
			<div class="col-md-6 text-right">
				<!-- <a href="<?php echo site_url('admin/empenhos/edit/'.$empenho['id']) ?>" class="btn btn-fill btn-success">Editar Empenho</a> -->
			</div>
		</div>
	</div>
	<div class="content table-responsive table-full-width">
		<?php //echo form_open_multipart('empenhos/add'); ?>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Data</label>
		        <p><?php echo date('d/m/Y', strtotime($empenho['Data'])) ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número</label>
		        <p><?php echo $empenho['Numero'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Credor</label>
		        <p>
							<?php $credor = $this->imports_model->credor($empenho['CPF'],$empenho['CNPJ']) ?>
							<?php echo $credor['Nome'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
				<?php $acao = $this->imports_model->acao($empenho['AcaoCodigo']) ?>
				<?php $fonte = $this->imports_model->fonte($empenho['FonteCodigo']) ?>
				<?php $rubrica = $this->imports_model->rubrica($empenho['RubricaCodigo']) ?>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Unidade Orçamentária</label>
		        <p>
							<?php $unidade = $this->imports_model->unidade($acao['unidadecodigo']) ?>
							<?php echo $unidade['descricao'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Função</label>
						<?php $funcao = $this->imports_model->funcao($acao['funcaocodigo']) ?>
		        <p><?php echo $funcao['descricao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Sub-Função</label>
						<?php $subfuncao = $this->imports_model->subfuncao($acao['subfuncaocodigo']) ?>
		        <p><?php echo $subfuncao['descricao'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Programa</label>
						<?php $programa = $this->imports_model->programa($acao['programacodigo']) ?>
		        <p><?php echo $programa['descricao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Ação</label>
		        <p><?php echo $acao['descricao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Fonte de Recurso</label>
		      	<p><?php echo $fonte['descricao'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Elemento de Despesa</label>
		      	<p><?php echo $rubrica['descricao'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>CPF/CNPJ</label>
		      	<p><?php echo $empenho['CPF'] ?> <?php echo $empenho['CNPJ'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número do Processo</label>
		      	<p><?php echo $empenho['Processo'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Valor</label>
		      	<p>
			  		R$ <?php echo number_format($empenho['ValorEmpAteAgora'],2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Liquidado</label>
		      	<p>
		      		R$ <?php echo number_format($empenho['ValorLiqAteAgora'],2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Pago</label>
		      	<p>
		      		R$ <?php echo number_format($empenho['ValorPagAteAgora'],2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		  </div>

		<?php //echo form_close();?>
	</div>
</div>

<div class="modal fade" id="modalLiq" tabindex="-1" role="dialog" aria-labelledby="modalLiqLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLiqLabel">Nova Liquidação</h4>
      </div>
      <?php echo form_open('empenhos/liquidacao_add/'.$empenho['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Número</label>
                <input type="text" name="numero" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control">
		      </div>
		    </div>
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="modalPag" tabindex="-1" role="dialog" aria-labelledby="modalPagLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPagLabel">Novo Pagamento</h4>
      </div>
      <?php echo form_open('empenhos/pagamento_add/'.$empenho['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Banco</label>
                <input type="text" name="banco" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Agência</label>
                <input type="text" name="agencia" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Conta</label>
                <input type="text" name="conta" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Documento</label>
                <input type="text" name="documento" class="form-control" required>
              </div>
            </div>
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control" required>
		      </div>
		    </div>
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<?php foreach ($liquidacoes as $key => $liquidacao): ?>

<div class="modal fade" id="modalLiq<?php echo $liquidacao['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalLiq<?php echo $liquidacao['id'] ?>Label">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLiq<?php echo $liquidacao['id'] ?>Label">Editar Liquidação</h4>
      </div>
      <?php echo form_open('empenhos/liquidacao_edit/'.$liquidacao['id']); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Número</label>
                <input type="text" name="numero" class="form-control" value="<?php echo $liquidacao['numero'] ?>" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
		      <div class="form-group">
		        <label>Data</label>
		        <input type="date" name="data" class="form-control" value="<?php echo $liquidacao['data'] ?>" required>
		      </div>
		    </div>
          </div>
          <div class="row">
		    <div class="col-md-12">
              <div class="form-group">
                <label>Valor</label>
                <input type="text" name="valor" class="form-control" value="<?php echo $liquidacao['valor'] ?>" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Salvar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<?php endforeach ?>
