<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<div class="row">
			<div class="col-md-6">
				<h4 style="margin: 0">Adicionar Liquidações e Pagamentos</h4>
			</div>
			<div class="col-md-6 text-right">
				<!-- <a href="<?php echo site_url('admin/empenhos/edit/'.$empenho['id']) ?>" class="btn btn-fill btn-success">Editar Empenho</a> -->
			</div>
		</div>
	</div>
	<div class="content table-responsive table-full-width">
		<?php //echo form_open_multipart('empenhos/add'); ?>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Data</label>
		        <p><?php echo date('d/m/Y', strtotime($empenho['DataEmpenho'])) ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número</label>
		        <p><?php echo $empenho['NumEmpenho'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Credor</label>
		        <p>
							<?php $credor = $this->sicaps_model->getVerifica('sicap_Credor', 'CodCredor',$empenho['CodCredor'], $empenho['CodUndGestora']) ?>
							<?php echo $credor['Nome'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Unidade Orçamentária</label>
		        <p>
							<?php $undorcamentaria = $this->sicaps_model->getVerifica('sicap_UndOrcamentaria', 'CodUndOrcamentaria',$empenho['CodUndOrcamentaria'], $empenho['CodUndGestora']) ?>
							<?php echo $undorcamentaria['Nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Função</label>
		        <p>
							<?php $funcao = $this->sicaps_model->getVerifica('sicap_Funcao', 'CodFuncao',$empenho['CodFuncao'], $empenho['CodUndGestora']) ?>
							<?php echo $funcao['Nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Sub-Função</label>
		        <p>
							<?php $subfuncao = $this->sicaps_model->getVerifica('sicap_SubFuncao', 'CodSubFuncao',$empenho['CodSubFuncao'], $empenho['CodUndGestora']) ?>
							<?php echo $subfuncao['Nome'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Programa</label>
		        <p>
							<?php $programa = $this->sicaps_model->getVerifica('sicap_Programa', 'CodPrograma',$empenho['CodPrograma'], $empenho['CodUndGestora']) ?>
							<?php echo $programa['Nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Projeto / Atividade</label>
		        <p>
							<?php $acao = $this->sicaps_model->getVerifica('sicap_ProjAtividade', 'CodProjAtividade',$empenho['CodProjAtividade'], $empenho['CodUndGestora']) ?>
							<?php echo $acao['Nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Fonte de Recurso</label>
						<p>
							<?php $acao = $this->sicaps_model->getVerifica('sicap_RecursoVinculado', 'CodRecVinculado',$empenho['CodRecVinculado'], $empenho['CodUndGestora']) ?>
							<?php echo $acao['Nome'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Elemento de Despesa</label>
						<p>
							<?php $rubrica = $this->sicaps_model->getVerifica('sicap_RubricaDespesa', 'CodRubrica',$empenho['CodContaDespesa'], $empenho['CodUndGestora']) ?>
							<?php echo $rubrica['Especificacao'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número do Processo</label>
		      	<p><?php echo $empenho['NumProcesso'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Valor</label>
						<p>
						R$ <?php echo number_format($empenho['Valor'],2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Liquidado</label>
		      	<p>
		      		<?php $cont_liq = 0; ?>
		      		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		      			<?php $cont_liq = $cont_liq + $liquidacao['Valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_liq,2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Pago</label>
		      	<p>
		      		<?php $cont_pag = 0; ?>
		      		<?php foreach ($pagamentos as $key => $pagamento): ?>
		      			<?php $cont_pag = $cont_pag + $pagamento['Valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_pag,2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Histórico</label>
		        <p><?php echo $empenho['Historico'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
				<hr>
		  <div class="col-md-12">
		      <h4 style="margin: 0">Liquidações</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Número</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		  		<tr>
		  			<td><?php echo $liquidacao['NumLiquidacao'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($liquidacao['DataLiquidacao'])) ?></td>
		  			<td>R$ <?php echo number_format($liquidacao['Valor'],2,',','.') ?></td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  </table>

		  </div>
		  <hr>
		  <div class="col-md-12">
		  <h4 style="margin: 0">Pagamentos</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Código do Banco</th>
		  			<th>Agência</th>
		  			<th>Número da Conta</th>
		  			<th>Número do Processo</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  	<?php foreach ($pagamentos as $key => $pagamento): ?>
					<?php $pagfin = $this->sicaps_model->pagamento_fin($empenho['NumEmpenho'], $pagamento['NumPagamento']) ?>
		  		<tr>
		  			<td><?php echo $pagfin['CodBanco'] ?></td>
		  			<td><?php echo $pagfin['CodAgenciaBanco'] ?></td>
		  			<td><?php echo $pagfin['NumContaBancaria'] ?></td>
		  			<td><?php echo $pagamento['NumProcesso'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($pagamento['DataPagamento'])) ?></td>
		  			<td>R$ <?php echo number_format($pagamento['Valor'],2,',','.') ?></td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  </table>
		  </div>
		</div>
		<?php //echo form_close();?>
	</div>
</div>
