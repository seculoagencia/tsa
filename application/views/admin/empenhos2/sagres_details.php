<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<div class="row">
			<div class="col-md-6">
				<h4 style="margin: 0">Adicionar Liquidações e Pagamentos</h4>
			</div>
			<div class="col-md-6 text-right">
				<!-- <a href="<?php echo site_url('admin/empenhos/edit/'.$empenho['id']) ?>" class="btn btn-fill btn-success">Editar Empenho</a> -->
			</div>
		</div>
	</div>
	<div class="content table-responsive table-full-width">
		<?php //echo form_open_multipart('empenhos/add'); ?>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Data</label>
		        <p><?php echo date('d/m/Y', strtotime($empenho['data_emissao_empenho'])) ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Número</label>
		        <p><?php echo $empenho['num_empenho'] ?></p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Credor</label>
		        <p>
							<?php $credor = $this->sagres_model->getVerifica('sagres_Fornecedores', 'cpf_cnpj',$empenho['cpf_cnpj_credor']) ?>
							<?php echo ($credor) ? $credor['nome'] : $empenho['cpf_cnpj_credor'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Unidade Orçamentária</label>
		        <p>
							<?php $undorcamentaria = $this->sagres_model->getVerifica('sagres_UnidadeOrcamentaria', 'codigo',$empenho['cod_unid_orc']) ?>
							<?php echo $undorcamentaria['denominacao'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Função</label>
		        <p>
							<?php $funcao = $this->sagres_model->getVerifica('sagres_Funcoes', 'codigo',$empenho['funcao']) ?>
							<?php echo $funcao['nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Sub-Função</label>
		        <p>
							<?php $subfuncao = $this->sagres_model->getVerifica('sagres_SubFuncoes', 'codigo',$empenho['subfuncao']) ?>
							<?php echo $subfuncao['nome'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Programa</label>
		        <p>
							<?php $programa = $this->sagres_model->getVerifica('sagres_Programas', 'codigo',$empenho['programa']) ?>
							<?php echo $programa['nome'] ?>
						</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Ação</label>
		        <p>
							<?php $acao = $this->sagres_model->getVerifica('sagres_Acao', 'codigo',$empenho['acao']) ?>
							<?php echo $acao['nome'] ?>
						</p>
		      </div>
		    </div>
		    <!-- <div class="col-md-4">
		      <div class="form-group">
		        <label>Fonte de Recurso</label>
						<p>
							<?php //$acao = $this->sicaps_model->getVerifica('sicap_RecursoVinculado', 'CodRecVinculado',$empenho['CodRecVinculado'], $empenho['CodUndGestora']) ?>
							<?php //echo $acao['Nome'] ?>
						</p>
		      </div>
		    </div> -->
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Elemento de Despesa</label>
						<p>
							<?php $rubrica = $this->sagres_model->getVerifica('sagres_ElementoDespesa', 'codigo',$empenho['cod_elem_desp']) ?>
							<?php echo $rubrica['descricao'] ?>
						</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <!-- <div class="col-md-4">
		      <div class="form-group">
		        <label>Número do Procedimento</label>
		      	<p><?php //echo $empenho['num_procedimento'] ?></p>
		      </div>
		    </div> -->
		  	<div class="col-md-4">
		      <div class="form-group">
		        <label>Valor</label>
						<p>
							R$ <?php echo number_format($empenho['valor_empenhado'],2,',','.') ?>
		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Liquidado</label>
		      	<p>
		      		<?php $cont_liq = 0; ?>
		      		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		      			<?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_liq,2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		    <div class="col-md-4">
		      <div class="form-group">
		        <label>Valor Pago</label>
		      	<p>
		      		<?php $cont_pag = 0; ?>
		      		<?php foreach ($pagamentos as $key => $pagamento): ?>
		      			<?php $cont_pag = $cont_pag + $pagamento['valor']; ?>
				  	<?php endforeach ?>
			  		R$ <?php echo number_format($cont_pag,2,',','.') ?>

		      	</p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Histórico</label>
		        <p><?php echo $empenho['historico'] ?></p>
		      </div>
		    </div>
		  </div>
		  <div class="row">
				<hr>
		  <div class="col-md-12">
		      <h4 style="margin: 0">Liquidações</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Número</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
		  		<tr>
		  			<td><?php echo $liquidacao['num_liquidacao'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($liquidacao['data'])) ?></td>
		  			<td>R$ <?php echo number_format($liquidacao['valor'],2,',','.') ?></td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  </table>

		  </div>
		  <hr>
		  <div class="col-md-12">
		  <h4 style="margin: 0">Pagamentos</h4>
		  <table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Código do Banco</th>
		  			<th>Agência</th>
		  			<th>Número da Conta</th>
		  			<th>Data</th>
		  			<th>Valor</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  	<?php foreach ($pagamentos as $key => $pagamento): ?>
					<?php $pagfin = $this->sicaps_model->pagamento_fin($empenho['num_empenho'], $pagamento['num_parc_pagamento']) ?>
		  		<tr>
		  			<td><?php echo $pagamento['cod_banco'] ?></td>
		  			<td><?php echo $pagamento['num_agencia_bancaria'] ?></td>
		  			<td><?php echo $pagamento['num_conta_bancaria'] ?></td>
		  			<td><?php echo date('d/m/Y', strtotime($pagamento['data'])) ?></td>
		  			<td>R$ <?php echo number_format($pagamento['valor'],2,',','.') ?></td>
		  		</tr>
		  	<?php endforeach ?>
		  	</tbody>
		  </table>
		  </div>
		</div>
		<?php //echo form_close();?>
	</div>
</div>
