<div class="row">
  <div class="col-md-6">
    <div class="card">
      <?php echo form_open('cargos/add'); ?>
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <h3>Cargos</h3>
      </div>
      <div class="content">
        <div class="form-inline">
          <input type="text" class="form-control" name="name" placeholder="Nome" required>
          <button type="submit" class="btn btn-fill btn-primary" name="button">Cadastrar</button>
        </div>
        <table class="table table-hover table-striped">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th style="min-width:92px;">Ações</th>
          </thead>
          <tbody>
            <?php $cont = 0 ?>
            <?php foreach ($cargos as $cargo): ?>

              <tr>
                <td><?php $cont = $cont + 1; echo $cont; ?></td>
                <td><?php echo $cargo['name'] ?></td>
                <td>
                  <?php echo anchor("#", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-success btn-fill','rel'=>'tooltip','title'=>'Editar','data-toggle'=>'modal','data-target'=>"#myModalCategoria{$cargo['id']}")) ?>
                  <?php echo anchor("#", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover','data-toggle'=>'modal','data-target'=>"#myModalCategoriaRemove2{$cargo['id']}")) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<?php foreach ($cargos as $cargo): ?>
<div class="modal fade" id="myModalCategoria<?php echo $cargo['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Categoria</h4>
      </div>
      <?php echo form_open("admin/cargos/edit_cargo/{$cargo['id']}"); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo $cargo['name'] ?>" required>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success btn-fill">Atualizar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalCategoriaRemove2<?php echo $cargo['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              Tem certeza que deseja excluir a categoria?
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">Fechar</button>
        <?php echo anchor("admin/cargos/remove/{$cargo['id']}", 'Remover', array('class'=>'btn btn-danger btn-fill')) ?>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
