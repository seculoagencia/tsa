  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/servidores') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/servidores') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="button" class="btn btn-primary btn-fill" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <th>ID</th>
          <th>Nome</th>
          <?php if ($this->db->database != 'noia_trans2'): ?>
            <th>Valor Bruto</th>
            <th>Desconto</th>
            <th>Valor Líquido</th>
          <?php endif; ?>
          <th>Mês de Referência</th>
          <th style="text-align:center">Anexo</th>
          <th style="min-width:92px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php foreach ($servidores as $servidor): ?>
            <?php $cont = $cont + 1; ?>
            <tr>
              <td><?php echo $cont ?> - <?php echo $servidor['id'] ?></td>
              <td><?php echo $servidor['nome'] ?></td>
              <?php if ($this->db->database != 'noia_trans2'): ?>
                <td><?php echo number_format($servidor['valor_bruto'],2,',','.') ?></td>
                <td><?php echo number_format($servidor['descontos'],2,',','.') ?></td>
                <td><?php echo number_format($servidor['valor_liquido'],2,',','.') ?></td>
              <?php endif; ?>
              <td>
                <?php foreach ($month as $key => $mes): ?>
                  <?php if ($key == explode('/', $servidor['mes_ref'])[0]): ?>
                    <?php echo $mes ?> / <?php echo explode('/', $servidor['mes_ref'])[1] ?>
                  <?php endif; ?>
                <?php endforeach; ?>
              </td>
              <td style="text-align:center">
                 <?php $anexos = $this->servidoresanexos_model->select($servidor['id']) ?>
                <?php foreach($anexos as $key => $anexo): ?>
                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
                <?php endforeach; ?>
              </td>
              <td>
                <a href='#' data-toggle="modal" data-target="#myModalEdit<?php echo $servidor['id'] ?>" rel="tooltip" title="Editar" class="btn btn-xs btn-success btn-fill"><i class="fa fa-pencil"></i></a>
                <a href='<?php echo site_url("admin/servidores/remove/{$servidor['id']}") ?>' rel="tooltip" title="Remover" class="btn btn-xs btn-danger btn-fill"><i class="fa fa-trash"></i></a>

                <div class="modal fade" id="myModalEdit<?php echo $servidor['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalEdit<?php echo $servidor['id'] ?>Label">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalEdit<?php echo $servidor['id'] ?>Label">Nova servidor</h4>
                      </div>
                      <?php echo form_open_multipart("servidores/edit/{$servidor['id']}"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" value="<?php echo $servidor['nome'] ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Valor Bruto</label>
                                <input type="text" name="valor_bruto" class="form-control" value="<?php echo $servidor['valor_bruto'] ?>">
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                <label>Desconto</label>
                                <input type="text" name="descontos" class="form-control" value="<?php echo $servidor['descontos'] ?>">
                              </div>
                            </div>
                             <div class="col-md-6">
                              <div class="form-group">
                                <label>Valor Líquido</label>
                                <input type="text" name="valor_liquido" class="form-control" value="<?php echo $servidor['valor_liquido'] ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mês</label>
                                <select class="form-control" name="mes">
                                    <option value="1">Janeiro</option>
                                    <option value="2">Fevereiro</option>
                                    <option value="3">Março</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Maio</option>
                                    <option value="6">Junho</option>
                                    <option value="7">Julho</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Setembro</option>
                                    <option value="10">Outubro</option>
                                    <option value="11">Novembro</option>
                                    <option value="12">Dezembro</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Ano</label>
                                <select class="form-control" name="ano">
                                  <?php for ($i=0; $i < count($year); $i++): ?>
                                    <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                                  <?php endfor; ?>
                                </select>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success btn-fill">Salvar</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                  </div>

                <a href="#" data-toggle="modal" data-target="#myModalAnexo<?php echo $servidor['id'] ?>" class="btn btn-xs btn-fill btn-info"><i class="fa fa-paperclip"></i></a>

                <div class="modal fade" id="myModalAnexo<?php echo $servidor['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalAnexol<?php echo $servidor['id'] ?>Label">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexo<?php echo $servidor['id'] ?>Label">Novo Anexo</h4>
                      </div>
                      <?php echo form_open_multipart("servidores/add_anexo"); ?>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <input type="file" name="anexo" class="form-control" required>
                                <input type="hidden" name="report_id" value="<?php echo $servidor['id'] ?>">
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
                      </div>
                      <?php echo form_close();?>
                    </div>
                  </div>
                  </div>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nova Prestação de Contas</h4>
      </div>
      <?php echo form_open_multipart('servidores/add'); ?>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="nome" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Valor Bruto</label>
                <input type="text" name="valor_bruto" class="form-control">
              </div>
            </div>
             <div class="col-md-6">
              <div class="form-group">
                <label>Desconto</label>
                <input type="text" name="descontos" class="form-control">
              </div>
            </div>
             <div class="col-md-6">
              <div class="form-group">
                <label>Valor Líquido</label>
                <input type="text" name="valor_liquido" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Mês</label>
                <select class="form-control" name="mes">
                    <option value="1">Janeiro</option>
                    <option value="2">Fevereiro</option>
                    <option value="3">Março</option>
                    <option value="4">Abril</option>
                    <option value="5">Maio</option>
                    <option value="6">Junho</option>
                    <option value="7">Julho</option>
                    <option value="8">Agosto</option>
                    <option value="9">Setembro</option>
                    <option value="10">Outubro</option>
                    <option value="11">Novembro</option>
                    <option value="12">Dezembro</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Ano</label>
                <select class="form-control" name="ano">
                  <?php for ($i=0; $i < count($year); $i++): ?>
                    <option value="<?php echo $year[$i] ?>" <?php echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
  </div>
