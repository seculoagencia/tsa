<div class="row">
  <div class="col-md-4">
    <div class="card">
      <?php echo form_open('groups/add'); ?>
      <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
        <h3>Grupos</h3>
      </div>
      <div class="content">
        <div class="form-inline">
          <input type="text" class="form-control" name="name" placeholder="Nome">
          <button type="submit" class="btn btn-fill btn-primary" name="button">Cadastrar</button>
        </div>
        <table class="table table-hover table-striped">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th style="min-width:92px;">Ações</th>
          </thead>
          <tbody>
            <?php foreach ($groups as $group): ?>
              <tr>
                <td><?php echo $group['id'] ?></td>
                <td><strong><?php echo $group['name'] ?></strong></td>
                <td>
                  <?php echo anchor("groups/edit/{$group['id']}", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-success btn-fill','rel'=>'tooltip','title'=>'Editar')) ?>
                  <?php echo anchor("groups/remove/{$group['id']}", '<i class="fa fa-remove"></i>', array('class'=>'btn btn-xs btn-danger btn-fill','rel'=>'tooltip','title'=>'Remover')) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
