<div class="row">
  <div class="col-md-3">
    <div class="card">
      <?php echo form_open("groups/edit/{$group['id']}"); ?>
      <div class="header">
        <h3 class="box-title">Novo Grupo</h3>
      </div>
      <div class="content">
        <div class="form-group">
          <label for="name">Nome</label>
          <input type="text" class="form-control" name="name" value="<?php echo $group['name'] ?>">
        </div>
        <button type="submit" class="btn btn-success btn-fill" name="button">Salvar Alterações</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
