<div class="card">
	<div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
		<h4 style="margin: 0">Importação de Servidores</h4>
	</div>
	<div class="content table-responsive table-full-width">
		<?php echo form_open_multipart('impservidores/add'); ?>
		<div class="modal-body">
			<input type="file" name="import[]" value="" multiple required>
		</div>
		<div class="modal-footer">
		<a href="<?php echo site_url('admin/imports/servidores/sicap/') ?>" class="btn btn-primary" data-dismiss="modal">Voltar</a>
		<button type="submit" class="btn btn-primary btn-fill">Cadastrar</button>
		</div>
		<?php echo form_close();?>
	</div>
</div>
