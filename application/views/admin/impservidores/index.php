  <div class="card">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-7">
          <form class="" action="<?php echo site_url('admin/imports/servidores/sicap') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>

              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo site_url('admin/imports/servidores/sicap') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <a href="<?php echo site_url('admin/imports/servidores/sicap/add') ?>" class="btn btn-primary btn-fill"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-bordered">
        <thead>
          <th>ID</th>
          <th>Exercício</th>
          <th>Nome</th>
          <th>Data de Admissão</th>
          <th>Valor Bruto</th>
          <th>Valor Líquido</th>
          <th>Cargo</th>
          <th style="min-width:92px;">Ações</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php if (count($servidores) > 0): ?>
            <?php foreach ($servidores as $servidor): ?>
              <?php $cargo = $this->scargos_model->select_cod($servidor['CodCargo']); ?>
              <?php $cont = $cont + 1; ?>
              <tr class="<?php echo $bg = ($servidor['deleted']) ? 'bg-danger' : '' ; ?>">
                <td><?php echo $cont ?></td>
                <td><?php echo $servidor['Exercicio'] ?></td>
                <td><?php echo $servidor['Nome'] ?></td>
                <td><?php echo date('d/m/Y', strtotime($servidor['DataAdmissao'])) ?></td>
                <td><?php echo "R$ ".number_format($servidor['SalarioBruto'],2,',','.') ?></td>
                <td><?php echo "R$ ".number_format($servidor['Salarioliquido'],2,',','.') ?></td>
                <td><?php echo $cargo['Descricao'] ?></td>
                <td>
                  <?php if ($servidor['deleted']): ?>
                    <a href='<?php echo site_url("impservidores/back_sicap/{$servidor['id']}") ?>' rel="tooltip" title="Exibir" class="btn btn-xs btn-success btn-fill"><i class="fa fa-check"></i></a>
                  <?php else: ?>
                    <a href='<?php echo site_url("impservidores/remove_sicap/{$servidor['id']}") ?>' rel="tooltip" title="Ocultar" class="btn btn-xs btn-danger btn-fill"><i class="fa fa-trash"></i></a>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
