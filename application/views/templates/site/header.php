<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico') ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title><?php echo $settings['name'] ?></title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />


  <!-- Bootstrap core CSS     -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

  <!-- Animation library for notifications   -->
  <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet"/>
  <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css') ?>" rel="stylesheet"/>
  <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


  <!--     Fonts and icons     -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />

  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />

  <link href="<?php echo base_url('assets/css/custom-green.css') ?>" rel="stylesheet" />

  <style media="screen">
  .page-topo {
    position: relative;
  }
  .page-topo h3 {
    position: absolute;
    right:30px;
    bottom: 1em;
    color: #fff;
  }
  .page-topo .menu {
    position: absolute;
    right: 30px;
    top: 1em;
  }
  .btn.btn-black {
    background-color: #222;
    border-color: #222;
  }
  .btn.btn-black:hover {
    background-color: #2980b9;
    border-color: #2980b9;
  }
  .btn.btn-black.active {
    background-color: #2980b9;
    border-color: #2980b9;
  }
  .consult-info p {
    font-size: 1em;
  }
  .navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover {
    background-color: #2980b9;
  }
  .sidebar:after, body > .navbar-collapse:after {
    background: #2980b9;
  }
  body > .navbar-collapse .nav > li > a {
    padding: 1em;
    font-size: 1.2em;
  }
  .box {

    height: 100px;
    display: flex;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    align-items: center;
    -webkit-align-items: center;
    -moz-align-items: center;
    -ms-align-items: center;
    justify-content: center;
    -webkit-justify-content: center;
    -moz-justify-content: center;
    -ms-justify-content: center;
    margin-bottom: 1em;
  }
  .box a {
    color:#999;
    text-decoration: none;
    text-transform: uppercase;
    text-align: center;
    letter-spacing: 1px;
  }
  .box a:hover {
    color:#fff;
  }
  .box:hover {
    background-color: #2980b9;
  }
  .box:hover a {
    background-color: #2980b9;
    color:#fff;
  }

  .dataTables_length label {
      margin-top:1em;
  }
  .table.dataTable.no-footer {
      border:none;
  }
  .table.dataTable thead th, table.dataTable thead td {
      border-bottom:#ccc;
  }
  body {
    background-color: #fff !important;
  }
  table.dataTable tbody th, table.dataTable tbody td {
    padding: 2px 10px;
  }
  </style>
  <script src="<?php echo base_url('plugins/jQuery/jQuery-2.1.4.min.js') ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

</head>
<body class="base-modal" style="background: #fff;">

  <div class="container">
    <!-- <div class="page-topo" style="margin:1em 0;"> -->
      <!-- <div class="menu"> -->
        <!-- <a href="<?php echo site_url() ?>" class="btn btn-black btn-fill <?php echo $active = ($menu == 'home') ? 'active':''; ?>">Home</a> -->
        <!-- <a href="<?php echo site_url('pages/esic') ?>" class="btn btn-black btn-fill <?php echo $active = ($menu == 'esic') ? 'active':''; ?>">e-SIC</a> -->
        <!-- <a href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm" target="_blank" class="btn btn-xs btn-primary btn-fill">Lei da Informação</a> -->
        <!-- <div class="dropdown">
        <button class="btn btn-xs btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <i class="fa fa-bars">&nbsp;</i> Leis
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      <li><a href="#">Lei de Portal da Transparência</a></li>
      <li><a href="#">Another action</a></li>
      <li><a href="#">Something else here</a></li>
      <li role="separator" class="divider"></li>
      <li><a href="#">Separated link</a></li>
    </ul>
  </div> -->
<!-- </div> -->
<!-- <img src="<?php echo base_url('assets/img/page-topo.jpg') ?>" alt="" style="width:100%;border-radius:1em;" /> -->
<!-- <h3 class="hidden-xs"><?php echo $settings['name'] ?></h3> -->
<!-- </div> -->

<!-- <nav class="navbar navbar-inverse">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="<?php echo site_url() ?>"><i class="fa fa-home"></i></a>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-left:0">
<ul class="nav navbar-nav">
<li class="<?php echo $active = ($menu == 'home') ? 'active':''; ?>" style="margin-right:.5em"><a href="<?php echo site_url() ?>">Transparência <span class="sr-only">(current)</span></a></li>
<li class="<?php echo $active = ($menu == 'esic') ? 'active':''; ?>"><a href="<?php echo site_url('pages/esic') ?>">e-Sic</a></li>
</ul>
</div>
</div>
</nav> -->
<br>
<?php if ($settings['view_last_update']): ?>
  <?php if (($this->uri->segment(3) != 'sicap' && $this->uri->segment(2) != 'esic' && $this->uri->segment(3) != 'tc' && $this->uri->segment(2) != 'empenhos')): ?>
    <center style="font-size:1.4em"><small>Atualizado em: <?php echo date('d/m/Y H:i:s', strtotime($settings['last_update'])) ?></small> </center>
  <?php endif; ?>
  <?php if (($this->uri->segment(3) == 'sicap' || $this->uri->segment(3) == 'tc' || $this->uri->segment(2) == 'empenhos') && $this->uri->segment(2) != 'esic' && $this->db->database == 'iprev_trans2'): ?>
    <center style="font-size:1.4em"><small>Atualizado em: <?php echo date('d/m/Y H:i:s', strtotime($settings['last_update'])) ?></small> </center>
  <?php endif; ?>
  <?php //if ($this->uri->segment(3) != 'sicap' && $this->uri->segment(3) != 'tc' && $this->uri->segment(2) != 'empenhos'): ?>
    <!-- <center><small>Atualizado em: <?php //echo date('d/m/Y H:i:s', strtotime($settings['last_update'])) ?></small> </center> -->
  <?php //endif; ?>
<?php endif; ?>
<br>
