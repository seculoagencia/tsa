
</div>
</div>
<!-- <center style=""><?php echo $settings['name'] ?> • Todos os Direitos Reservados • 2017 • <a href="<?php echo $settings['site_url'] ?>">Site Principal</a></center><br> -->
<!-- <center style="margin-bottom:2em">Versão 20.0. - (86) 99455-6306 (Whatsapp) - contato@plumasoft.com.br</small></center> -->
</div>


<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js') ?>"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="<?php echo base_url('assets/js/bootstrap-checkbox-radio-switch.js') ?>"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>

<!--  Google Maps Plugin    -->
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> -->

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script type="text/javascript" src="<?php echo base_url('js/jquery.maskedinput.min.js') ?>"></script>
<script type="text/javascript">
$(function() {
  if ($("select[name='tipo_pessoa']").val() == 'f') {
    $("#cpf").mask("999.999.999-99",{placeholder:"___.___.___-__"});
    $(".cpf").mask("999.999.999-99",{placeholder:"___.___.___-__"});
    $("#label_cpf").text('CPF');
  } else {
    $("#cpf").mask("99.9999999/9999-99",{placeholder:"__._______/____-__"});
    $(".cpf").mask("99.9999999/9999-99",{placeholder:"__._______/____-__"});
    $("#label_cpf").text('CNPJ');
  }
});
$("select[name='tipo_pessoa']").change(function() {
  if ($(this).val() == 'j') {
    $("#cpf").mask("99.9999999/9999-99",{placeholder:"__._______/____-__"});
    $("#label_cpf").text('CNPJ');
  } else {
    $("#cpf").mask("999.999.999-99",{placeholder:"___.___.___-__"});
    $("#label_cpf").text('CPF');
  }
});
</script>

<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>
<script type="text/javascript">
  $('[data-toggle="popover"]').popover();

  $("#phone").mask("(99) 99999-9999",{placeholder:"(__) _____-____"});
  $('#datatable01').DataTable( {
    "searching": false,
    "language": {
      "sProcessing":   "A processar...",
      "sLengthMenu":   "Mostrar _MENU_ registos",
      "sZeroRecords":  "Não foram encontrados resultados",
      "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
      "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
      "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
      "sInfoPostFix":  "",
      "sSearch":       "Buscar:",
      "sUrl":          "",
      "oPaginate": {
        "sFirst":    "Primeiro",
        "sPrevious": "Anterior",
        "sNext":     "Seguinte",
        "sLast":     "Último"
      }
    },
    "lengthMenu": [ 20 ],
    "lengthChange": false
  } );
  $('#datatable01NoPage').DataTable( {
    "searching": false,
    "paging":   false,
    "language": {
      "sProcessing":   "A processar...",
      "sLengthMenu":   "Mostrar _MENU_ registos",
      "sZeroRecords":  "Não foram encontrados resultados",
      "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
      "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
      "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
      "sInfoPostFix":  "",
      "sSearch":       "Buscar:",
      "sUrl":          "",
      "oPaginate": {
        "sFirst":    "Primeiro",
        "sPrevious": "Anterior",
        "sNext":     "Seguinte",
        "sLast":     "Último"
      }
    },
    "lengthMenu": [ 20 ],
    "lengthChange": false
  } );
  $("select[name='category_id']").change(function() {
    console.log($(this).val());
    $.ajax({
      url : url_site + 'reports/consultar_subcategorias/'+$(this).val(), /* URL que será chamada */
      type : 'GET', /* Tipo da requisição */
      dataType: 'json', /* Tipo de transmissão */

      success: function(data){
        console.log(data);
        if (data.length > 0) {
          $('#subcat').show();
          $.each(data, function(){
            $("select[name='subcategory_id']").find('.option_before').remove();
            for (var i = 0; i < data.length; i++) {
              $("select[name='subcategory_id']").append('<option value="'+ data[i].id +'" class="option_before">'+ data[i].name +'</option>')
            }
          });
        } else {
          $('#subcat').hide();
        }
      }
    });
  });

  function DoSubmit() {

    var serial = $('#formConsult input[name="serial"]').val();
    var cpf = $('#formConsult input[name="cpf"]').val();
    $.ajax({
      url : url_site + 'requests/consultar/?cpf='+cpf+'&serial='+serial, /* URL que será chamada */
      type : 'GET', /* Tipo da requisição */
      dataType: 'json', /* Tipo de transmissão */

      success: function(data){
        if (data != null) {
          $('.consult-info-in').show();
          $('#cName').text(data.name);
          $('#cEmail').text(data.email);
          $('#cCpf').text(data.cpf);
          $('#cPhone').text(data.phone);
          switch (data.priority) {
            case '1':
            var mypriority = "Alta";
            break;
            case '2':
            var mypriority = "Normal";
            break;
            case '3':
            var mypriority = "Baixa";
            break;
          }
          $('#cPriority').text(mypriority);
          $('#cLocation').text(data.location);
          $('#cType_answer').text(data.type_answer);
          $.ajax({
            url : url_site + 'requests/consultar_typerequests/'+data.typerequest_id, /* URL que será chamada */
            type : 'GET', /* Tipo da requisição */
            dataType: 'json', /* Tipo de transmissão */

            success: function(data2){
              $('#cTyperequests').text(data2.name);
            }
          });
          $('#cSubject').text(data.subject);
          $('#cSerial').text(data.serial);
          $('#cCreated').text(data.created);
          $('#responder').attr('action', url_site+'requests/anwser_add_front/'+data.id);
          switch (data.status) {
            case '0':
            var mystatus = "Aberto";
            var classe = "text-dark";
            break;
            case '1':
            var mystatus = "Respondido";
            var classe = "text-dark";
            break;
            case '2':
            var mystatus = "Em Tramitação";
            var classe = "text-dark";
            break;
            case '3':
            var mystatus = "Negado";
            var classe = "text-dark";
            break;
          }
          $("#cStatus").removeAttr('class');
          $("#cStatus").attr('class', classe);
          $('#cStatus').text(mystatus);
          $('#cMessage').text(data.message);
          $('#cAnexo').html("<a href='/"+data.anexo+"' target='_blank'>Visualizar</a>");
          $.ajax({
            url : url_site + 'requests/consultar_answers/'+data.id, /* URL que será chamada */
            type : 'GET', /* Tipo da requisição */
            dataType: 'json', /* Tipo de transmissão */

            success: function(data3){
              if ($.trim(data3)) {
                $('#fechar_pedido').removeAttr('href');
                $('#fechar_pedido').attr('href', url_site + 'requests/fechar_pedido/' + data.id);
                if (data.status == 2) {
                  $('#responder').hide();
                } else {
                  $('#responder').show();
                }
                $('#answer_box').show();
                for (var i = 0; i < data3.length; i++) {
                  console.log(data3[i].user_id );
                  if (data3[i].user_id == null) {
                    var autor = data.name;
                  } else {
                    var autor = "Ouvidoria";
                  }
                  if (data3[i].anexo == null) {
                    var align = "right";
                    $("#listaRespostas").append(
                      "<p class='text-"+align+"'><small><strong>"+autor+"</strong></small><br>"+
                      "<span id='description'>"+data3[i].description+"</span><br>"+
                      "<small id='created'>"+data3[i].created+"</small></p>"+
                      "<hr>"
                    );
                  } else {
                    var align = "left";
                    $("#listaRespostas").append(
                      "<p class='text-"+align+"'><small><strong>"+autor+"</strong></small><br>"+
                      "<span id='description'>"+data3[i].description+"</span><br>"+
                      "<a href="+url_site+data3[i].anexo+" id='anexo' target='_blank'><i class='fa fa-download'></i> Anexo</a><br>"+
                      "<small id='created'>"+data3[i].created+"</small></p>"+
                      "<hr>"
                    );
                  }
                }
              } else {
                $('#answer_box').hide();
              }
            }
          });
        } else {
          $('.consult-info-in').hide();
          $("#menu1 .consult-info").append('<div class="alert alert-danger">Nenhum pedido encontrado. Verifique o número do protocolo ou CPF!</div>');
        }
      }
    });
  }
</script>

<script>

</script>
</body>
</html>
