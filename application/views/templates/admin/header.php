<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico') ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Painel Administrador - Transparência</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />


  <!-- Bootstrap core CSS     -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

  <!-- Animation library for notifications   -->
  <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet"/>

  <!--  Light Bootstrap Table core CSS    -->

  <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css') ?>" rel="stylesheet"/>

  <!--  CSS for Demo Purpose, don't include it in your project     -->
  <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


  <!--     Fonts and icons     -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />

</head>
<body class=" base-modal">
  <!--

  Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
  Tip 2: you can also add an image using data-image tag

-->

  <div class="wrapper">
    <div class="sidebar" style="background:#2980b9">


      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="<?php echo site_url() ?>" class="simple-text" style="line-height:20px">
            <span style="">Transparência</span>
            <br>
            <small style="font-size:11px;">• <?php echo $settings['name'] ?> •</small>
          </a>
        </div>
        <?php $this->load->view('includes/aside'); ?>

      </div>
    </div>

    <div class="main-panel" style="position:initial">
      <nav class="navbar navbar-default navbar-fixed visible-xs">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Dashboard</a>
          </div>
          <div class="collapse navbar-collapse"></div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <?php if ($this->session->flashdata('success')): ?>
              <div class="alert alert-success">
                <span><?php echo $this->session->flashdata('success'); ?></span>
              </div>
            <?php endif ?>
            <?php if ($this->session->flashdata('danger')): ?>
              <div class="alert alert-danger">
                <span><?php echo $this->session->flashdata('danger'); ?></span>
              </div>
            <?php endif ?>
