</div>


</div>
</div>

</div>
</div>


<!--   Core JS Files   -->
<!-- <script src="<?php echo base_url('assets/js/jquery-1.10.2.js') ?>" type="text/javascript"></script> -->
<script src="<?php echo base_url('plugins/jQuery/jQuery-2.1.4.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="<?php echo base_url('assets/js/bootstrap-checkbox-radio-switch.js') ?>"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js') ?>"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script type="text/javascript" src="<?php echo base_url('js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>

<script>
$(function () {
  //Initialize Select2 Elements
  // $(".select2").select2();

  $("#phone").mask("(99) 99999-9999",{placeholder:"(__) _____-____"});
  $(".data").mask("99/99/9999",{placeholder:"__/__/____"});

  // $("#example1").DataTable();

  // $(".textarea").wysihtml5();


  $("#r_password_id").find('#r_password').bind("change paste keyup", function() {
    if($("#r_password_id").find('#r_password').val() != $("#password").val()){
      $("#r_password_id").addClass('has-error');
      $("#r_password_id").removeClass('has-success');
      $('.submit-perfil').prop("disabled",true);
    } else {
      $("#r_password_id").removeClass('has-error');
      $("#r_password_id").addClass('has-success');
      $('.submit-perfil').prop("disabled",false);
    }
  });
  $("#password_id").find('#password').bind("change paste keyup", function() {
    if($("#r_password_id").find('#r_password').val() != ''){
      if($("#r_password_id").find('#r_password').val() != $("#password").val()){
        $("#r_password_id").addClass('has-error');
        $('.submit-perfil').prop("disabled",true);
      }else {
        $("#r_password_id").removeClass('has-error');
        $('.submit-perfil').prop("disabled",false);
      }
    }
  });

  $('[data-toggle="popover"]').popover();

  $(".cat select[name='category_id']").change(function() {
    console.log($(this).val());
    $.ajax({
      url : url_site + 'reports/consultar_subcategorias/'+$(this).val(), /* URL que será chamada */
      type : 'GET', /* Tipo da requisição */
      dataType: 'json', /* Tipo de transmissão */

      success: function(data){
        console.log(data);
        if (data.length > 0) {
          $('#subcat').show();
          $.each(data, function(){
              $(".subcat select[name='subcategory_id']").find('.option_before').remove();
              for (var i = 0; i < data.length; i++) {
                $(".subcat select[name='subcategory_id']").append('<option value="'+ data[i].id +'" class="option_before">'+ data[i].name +'</option>')
              }
          });
        } else {
          $('#subcat').hide();
        }
      }
    });
  });
  $(".catadd select[name='category_id']").change(function() {
    console.log($(this).val());
    $.ajax({
      url : url_site + 'reports/consultar_subcategorias/'+$(this).val(), /* URL que será chamada */
      type : 'GET', /* Tipo da requisição */
      dataType: 'json', /* Tipo de transmissão */

      success: function(data){
        console.log(data);
        if (data.length > 0) {
          $('#subcatadd').show();
          $.each(data, function(){
              $(".subcatadd select[name='subcategory_id']").find('.option_before').remove();
              for (var i = 0; i < data.length; i++) {
                $(".subcatadd select[name='subcategory_id']").append('<option value="'+ data[i].id +'" class="option_before">'+ data[i].name +'</option>')
              }
          });
        } else {
          $('#subcat').hide();
        }
      }
    });
  });


});
</script>

</body>
</html>
