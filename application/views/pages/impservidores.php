
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
       
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/receitas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Exercício</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>


        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">

  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <tr>
        <th>ID</th>
        <th>Exercício</th>
        <th>Nome</th>
        <th>Data de Admissão</th>
        <th>Valor Bruto</th>
        <th>Valor Líquido</th>
        <th>Cargo</th>
      </tr>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php if (count($servidores) > 0): ?>
        <?php foreach ($servidores as $servidor): ?>
          <?php $cargo = $this->scargos_model->select_cod($servidor['CodCargo']); ?>
          <?php $cont = $cont + 1; ?>
          <tr>
            <td><?php echo $cont ?></td>
            <td><?php echo $servidor['Exercicio'] ?></td>
            <td><?php echo $servidor['Nome'] ?></td>
            <td><?php echo date('d/m/Y', strtotime($servidor['DataAdmissao'])) ?></td>
            <td><?php echo "R$ ".number_format($servidor['SalarioBruto'],2,',','.') ?></td>
            <td><?php echo "R$ ".number_format($servidor['Salarioliquido'],2,',','.') ?></td>
            <td><?php echo $cargo['Descricao'] ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
</div>
</div>
