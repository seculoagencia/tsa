
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
      
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/licitacoes') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
          <a href="<?php echo site_url('pages/licitacoesxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
          <a href="<?php echo site_url('pages/licitacoesxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <th>ID</th>
      <th>Órgão</th>
      <th>Modalidade</th>
      <th>Data/Hora</th>
      <th>Objeto</th>
      <?php if (
        $this->db->database != 'noia_trans2' and 
        $this->db->database != 'coite_trans2' and 
        $this->db->database != 'prefouro_trans2' and 
        $this->db->database != 'preflimo_trans' and
        $this->db->database != 'olhodagu_trans2' and
        $this->db->database != 'coitedon_trans2'
      ): ?>
      <th>Preço</th>
      <?php endif; ?>
      <th style="text-align:center">Anexos</th>
      <th>Status</th>
      <th>Vencedor</th>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($licitacoes as $licitacao): ?>
        <?php if ($licitacao['tipo'] == 2): ?>
          <?php $cont = $cont + 1; ?>
          <tr>
            <td><?php echo $cont ?></td>
            <td><?php echo $licitacao['orgao'] ?></td>
            <td><?php echo $licitacao['modalidade'] ?></td>
            <td><?php echo date('d/m/Y H:i', strtotime($licitacao['data'])) ?></td>
            <?php if ($this->db->database != 'sistemasalfa_trans2'): ?>
            <td><button class="btn btn-primary" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<?php echo $licitacao['objeto'] ?>"><i class="fa fa-eye"></i> Visualizar/Ocultar</button> </td>
            <?php else: ?>
            <td><?php echo $licitacao['objeto'] ?></td>
            <?php endif; ?>
            <?php if (
              $this->db->database != 'noia_trans2' and 
              $this->db->database != 'coite_trans2' and 
              $this->db->database != 'prefouro_trans2' and 
              $this->db->database != 'preflimo_trans' and
              $this->db->database != 'olhodagu_trans2' and
              $this->db->database != 'coitedon_trans2'
            ): ?>
            <td>R$ <?php echo number_format($licitacao['valor'],2,',','.') ?></td>
            <?php endif; ?>
            <td>
              <div style="text-align:center"><a href="#" data-toggle="modal" data-target="#myModalAnexo<?php echo $licitacao['id'] ?>" class="btn btn-xs btn-fill btn-info">VISUALIZAR</a></div>
              <div class="modal fade" id="myModalAnexo<?php echo $licitacao['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalAnexol<?php echo $licitacao['id'] ?>Label">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalAnexo<?php echo $licitacao['id'] ?>Label">Anexos</h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <?php $anexos = $this->licitacoesanexos_model->select($licitacao['id']) ?>
                            <table class="table table-striped">
                            <?php foreach($anexos as $key => $anexo): ?>
                              <tr>
                                <td>
                                  <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">
                                    <?php echo ($anexo['name']) ? $anexo['name'] : 'Anexo '.($key+1).'' ?>
                                  </a>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                      </div>
                    </div>
                  </div>
                </div>
                
            </td>
            <td>
              <?php 
                if ($licitacao['status'] == null) {
                  echo "Suspenso";
                } elseif ($licitacao['status']) {
                  echo "Concluído";
                } else {
                  echo "Em Andamento";
                }
              ?>
              <?php //echo $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento' ; ?>
            </td>
            <td><?php echo $licitacao['vencedor'] ?></td>
          </tr>
        <?php endif; ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
