
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
      
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/verbas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Exercício</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                    <?php foreach ($month as $key): ?>
                      <?php $cont = $cont + 1 ?>
                      <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php foreach ($month as $key): ?>
                      <?php $cont = $cont + 1 ?>
                      <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>

              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">

  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <th>ID</th>
      <th>Data</th>
      <th>Beneficiário</th>
      <th>Valor</th>
      <th>Mês de Referência</th>
      <th style="text-align:center">Anexo</th>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($verbas as $key => $verba): ?>
        <?php $cont = $cont + 1; ?>
        <tr>
          <td><?php echo $key + 1 ?></td>
          <td><?php echo date('d/m/Y', strtotime($verba['data'])) ?></td>
          <td><?php echo $verba['beneficiario'] ?></td>
          <td><?php echo number_format($verba['valor'],2,',','.') ?></td>
          <td>
            <?php echo $this->verbas_model->retornaMes($verba['mes']) ?>
          </td>
          <td style="text-align:center">
              <?php $anexos = $this->verbasanexos_model->select($verba['id']) ?>
            <?php foreach($anexos as $key => $anexo): ?>
                <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
            <?php endforeach; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
