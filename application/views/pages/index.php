<div class="row">
  <div class="col-lg-9">
      <div class="panel">
        <div class="panel-body" style="min-height: 450px;">
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/empenhos') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Empenhos
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/imports/sicap') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Empenhos SICAP
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/imports/sagres') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Empenhos SAGRES
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/receitas') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Receitas
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/servidores') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Servidores
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/licitacoes') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Licitações
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/contratos') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Contratos
            </a>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/diarias') ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                Diárias
            </a>
          </div>
      <?php foreach ($categories as $category): ?>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php echo site_url('pages/list_for_category/'.$category['id']) ?>" class="btn btn-fill btn-primary box" style="font-weight:600;text-transform:uppercase">
                <?php echo $category['name'] ?>
            </a>
          </div>
      <?php endforeach; ?>
    </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel">
      <div class="panel-body">
        <ol class="breadcrumb">
          <li class="active">e-SIC</li>
        </ol>
        <p>
          <a href="<?php echo site_url('pages/esic') ?>">
            <img src="<?php echo site_url('assets/img/acesso-a-informacao.png') ?>" style="width:100%" />
          </a>
        </p>
        <br>
        <ol class="breadcrumb">
          <li class="active">Links Importantes</li>
        </ol>
        <div class="list-group">
          <a href="#" class="list-group-item">Manual do Portal da Transparência</a>
          <a href="#" class="list-group-item">SICs do Governo Federal</a>
          <a href="#" class="list-group-item">Perguntas e Respostas sobre a Lei</a>
          <a href="#" class="list-group-item">Acesso a Informação no Brasil</a>
        </div>
      </div>
    </div>
  </div>
</div>
