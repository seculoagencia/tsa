
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        
        </div>
        <div class="col-md-7">

          <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <!-- <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php //$cont = 0; ?>
                  <?php //if(isset($_GET['month'])): ?>
                  <?php //foreach ($month as $key): ?>
                    <?php //$cont = $cont + 1 ?>
                    <option value="<?php //echo $cont ?>" <?php //echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php //echo $key ?></option>
                  <?php //endforeach; ?>
                  <?php //else: ?>
                  <?php //foreach ($month as $key): ?>
                    <?php //$cont = $cont + 1 ?>
                    <option value="<?php //echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php //echo $key ?></option>
                  <?php //endforeach; ?>
                  <?php //endif; ?>
                </select>
              </div> -->
              <!-- <div class="form-group">
              <label>Categoria</label><br>
              <select class="form-control" name="category_id">
              <option value="">Selecione...</option>
              <?php foreach ($categories as $category): ?>
              <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
            <?php endforeach; ?>
          </select>
        </div> -->
        <?php if ($subcategories): ?>
          <div class="form-group">
            <label>Sub-Categoria</label><br>
            <select class="form-control" name="subcategory_id">
              <option value="">Selecione...</option>
              <?php foreach ($subcategories as $subcategory): ?>
                <option value="<?php echo $subcategory['id'] ?>"><?php echo $subcategory['name'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        <?php endif; ?>
        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;" >
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <tr>
        <th>ID</th>
        <th>Ano</th>
        <th>Mês</th>
        <th>Título</th>
        <th>Categoria - Subcategoria</th>
        <th>Publicação</th>
        <th style="text-align:center">Anexo</th>
      </tr>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($reports as $report): ?>
        <?php $cont = $cont + 1; ?>
        <?php $category = $this->categories_model->select_id($report['category_id']) ?>
        <?php $subcategory = $this->subcategories_model->select_id($report['subcategory_id']) ?>
        <tr>
          <td><?php echo $cont ?></td>
          <td><?php echo $report['year'] ?></td>
          <td><?php echo $month[$report['month']] ?></td>
          <td><?php echo $report['name'] ?></td>
          <td><?php echo $category['name'] ?> - <?php echo $subcategory['name'] ?></td>
          <td><?php echo date('d/m/Y', strtotime($report['created'])) ?></td>
          <td style="text-align:center;width: 72px;">
              <?php $anexos = $this->reportsanexos_model->select($report['id']) ?>
                <?php foreach($anexos as $key => $anexo): ?>
                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Ver Anexo</a><br>
                <?php endforeach; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
