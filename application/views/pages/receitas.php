
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/receitas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>

        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/receitas/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
          <a href="<?php echo site_url('pages/receitasxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
          <a href="<?php echo site_url('pages/receitasxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <tr>
        <th>ID</th>
        <th>Código</th>
        <th>Descrição</th>
        <th>Data</th>
        <th>Valor</th>
        <th style="text-align:center">Anexo</th>
      </tr>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($receitas as $receita): ?>
        <?php $cont = $cont + 1; ?>
        <tr>
          <td><?php echo $cont ?></td>
              <td><?php echo $receita['codigo'] ?></td>
              <td>
                <?php echo $receita['descricao'] ?>
              </td>
              <td>
                <?php echo date('d/m/Y', strtotime($receita['data'])) ?>
              </td>
              <td><?php echo number_format($receita['valor'],2,',','.') ?></td>
              <td style="text-align:center">
                <?php //if (!empty($receita['anexo'])): ?>
                  <?php $anexos = $this->receitasanexos_model->select($receita['id']) ?>
                <?php foreach($anexos as $key => $anexo): ?>
                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Ver Anexo</a><br>
                <?php endforeach; ?>
                <?php //endif ?>
              </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
