<html>
<title><?php echo $settings['name'] ?></title>
<link rel="stylesheet" type="text/css" media="screen" href="https://bootswatch.com/3/flatly/bootstrap.min.css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
</head>
<body>
<!-- Tabela -->
<div class="container">
  <!-- CalendГЎrio -->
   <form action="<?php echo site_url('pages/relatorio') ?>" method="GET">
   <div class='col-md-5'>
        <div class="form-group">
            <div class='input-group date' name="de" id='datetimepicker10'>
                <input type='text' class="form-control" name="de" placeholder="Data Inicial" required="required"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
            </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker10').datetimepicker({
                    viewMode: 'years',
                    format: 'DD-MM-YYYY'
                });
            });
        </script>
        </div>
    </div>
    <div class='col-md-5'>
        <div class="form-group">
            <div class="form-group">
            <div class='input-group date' name="ate" id='datetimepicker11'>
                <input type='text' class="form-control" name="ate" placeholder="Data Final" required="required"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker11').datetimepicker({
                        viewMode: 'years',
                        format: 'DD-MM-YYYY'
                    });
                });
            </script>
            </div>
        </div>
    </div>
    <div class='col-md-2'>
        <div class="form-group">
            <button class="btn btn-success" type="submit"><b>Gerar Relatório</b></button>
        </div>
    </div>
    </form>
    <!-- CalendГЎrio -->
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Respondido</th>
        <th>Aberto</th>
        <th>Em tramitação</th>
        <th>Negado</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php echo count($requests_1) ?></td>
        <td><?php echo count($requests_0) ?></td>
        <td><?php echo count($requests_2) ?></td>
        <td><?php echo count($requests_3) ?></td>
        <td><?php echo count($requests_0) + count($requests_1) + count($requests_2) + count($requests_3) ?></td>
    </tr>


    </tbody>
  </table>

  <table class="table table-striped">
    <thead>
      <tr>
        <!-- <th>Pessoa Física</th> -->
        <!-- <th>Pessoa Jurídica</th> -->
        <th>Data Inicial</th>
        <th>Data Final</th>
      </tr>
    </thead>
    <tbody>
    <tr>
        <!-- <td>0</td> -->
        <!-- <td>0</td> -->
        <td><?php echo $data_de = ($de != null) ? $de:"//" ?></td>
        <td><?php echo $data_ate = ($ate != null) ? $ate:"//" ?></td>
    </tr>
    </tbody>
  </table>
  <hr>
  <div class='col-md-12 text-center'>
        <div class="form-group">
            <a href="<?php echo site_url('pages/relatorioxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
            <a href="<?php echo site_url('pages/relatorioxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
    </div>
</div>

<!-- Fim da tabela -->

<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
<center>
</body>
</html>
