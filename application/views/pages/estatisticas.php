
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
      
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/esic/estatisticas') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label>
                <select class="form-control" name="ano">
                  <?php if(isset($_GET['ano'])): ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['ano'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
        <div class="form-group">
          <!-- <label>&nbsp;</label><br> -->
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>


</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <div class="col-md-8">
    <canvas id="canvas"></canvas>
  </div>
  <div class="col-md-4">
    <canvas id="chart-area"></canvas>
  </div>
</div>
</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('js/chart-utils.js') ?>"></script>

<script>
  var barChartData = {
    labels: ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL","AGO","SET","OUT","NOV","DEZ"],
    datasets: [{
      label: 'Abertos',
      backgroundColor: window.chartColors.blue,
      data: [
        <?php echo $estatisticas['total_jan_a'] ?>,
        <?php echo $estatisticas['total_fev_a'] ?>,
        <?php echo $estatisticas['total_mar_a'] ?>,
        <?php echo $estatisticas['total_abr_a'] ?>,
        <?php echo $estatisticas['total_mai_a'] ?>,
        <?php echo $estatisticas['total_jun_a'] ?>,
        <?php echo $estatisticas['total_jul_a'] ?>,
        <?php echo $estatisticas['total_ago_a'] ?>,
        <?php echo $estatisticas['total_set_a'] ?>,
        <?php echo $estatisticas['total_out_a'] ?>,
        <?php echo $estatisticas['total_nov_a'] ?>,
        <?php echo $estatisticas['total_dez_a'] ?>
      ]
    }, {
      label: 'Em Andamento',
      backgroundColor: window.chartColors.yellow,
      data: [
        <?php echo $estatisticas['total_jan_e'] ?>,
        <?php echo $estatisticas['total_fev_e'] ?>,
        <?php echo $estatisticas['total_mar_e'] ?>,
        <?php echo $estatisticas['total_abr_e'] ?>,
        <?php echo $estatisticas['total_mai_e'] ?>,
        <?php echo $estatisticas['total_jun_e'] ?>,
        <?php echo $estatisticas['total_jul_e'] ?>,
        <?php echo $estatisticas['total_ago_e'] ?>,
        <?php echo $estatisticas['total_set_e'] ?>,
        <?php echo $estatisticas['total_out_e'] ?>,
        <?php echo $estatisticas['total_nov_e'] ?>,
        <?php echo $estatisticas['total_dez_e'] ?>
      ]
    }, {
      label: 'Resolvidos',
      backgroundColor: window.chartColors.green2,
      data: [
        <?php echo $estatisticas['total_jan_f'] ?>,
        <?php echo $estatisticas['total_fev_f'] ?>,
        <?php echo $estatisticas['total_mar_f'] ?>,
        <?php echo $estatisticas['total_abr_f'] ?>,
        <?php echo $estatisticas['total_mai_f'] ?>,
        <?php echo $estatisticas['total_jun_f'] ?>,
        <?php echo $estatisticas['total_jul_f'] ?>,
        <?php echo $estatisticas['total_ago_f'] ?>,
        <?php echo $estatisticas['total_set_f'] ?>,
        <?php echo $estatisticas['total_out_f'] ?>,
        <?php echo $estatisticas['total_nov_f'] ?>,
        <?php echo $estatisticas['total_dez_f'] ?>
      ]
    }]

  };

    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'pie',
        // fullWidth: true,
        data: {
            datasets: [{
                data: [
                    <?php echo $estatisticas['total_a'] ?>,
                    <?php echo $estatisticas['total_e'] ?>,
                    <?php echo $estatisticas['total_f'] ?>
                ],
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.yellow,
                    window.chartColors.green2,
                ],
                label: 'Dataset 1'
            }],
            labels: [
              // fontSize: 9,
                "Abertos",
                "Em Andamento",
                "Fechados"
            ]
        },
        options: {
          responsive: true,
          legend: {
            display: false,
            labels: {
                // fontColor: 'rgb(255, 99, 132)'
            }
          },
          title:{
            display:true,
            text:"Total"
          },
        }
    };

    window.onload = function() {
        var ctx2 = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx2, config);

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
          type: 'bar',
          data: barChartData,
          options: {
            title:{
              display:true,
              text:"Exibindo por Mês"
            },
            tooltips: {
              mode: 'index',
              intersect: false
            },
            responsive: true,
            scales: {
              xAxes: [{
                stacked: true,
              }],
              yAxes: [{
                stacked: true
              }]
            }
          }
        });
    };

    // document.getElementById('randomizeData').addEventListener('click', function() {
    //     config.data.datasets.forEach(function(dataset) {
    //         dataset.data = dataset.data.map(function() {
    //             return randomScalingFactor();
    //         });
    //     });
    //
    //     window.myPie.update();
    // });

    // var colorNames = Object.keys(window.chartColors);
    // document.getElementById('addDataset').addEventListener('click', function() {
    //     var newDataset = {
    //         backgroundColor: [],
    //         data: [],
    //         label: 'New dataset ' + config.data.datasets.length,
    //     };
    //
    //     for (var index = 0; index < config.data.labels.length; ++index) {
    //         newDataset.data.push(randomScalingFactor());
    //
    //         var colorName = colorNames[index % colorNames.length];;
    //         var newColor = window.chartColors[colorName];
    //         newDataset.backgroundColor.push(newColor);
    //     }
    //
    //     config.data.datasets.push(newDataset);
    //     window.myPie.update();
    // });

    // document.getElementById('removeDataset').addEventListener('click', function() {
    //     config.data.datasets.splice(0, 1);
    //     window.myPie.update();
    // });
    </script>
