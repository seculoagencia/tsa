
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-12">
            
        </div>
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/imports') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
                <a href="<?php echo site_url('pages/importsxml/tc') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
                <a href="<?php echo site_url('pages/importsxls/tc') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>

    <div class="content table-responsive table-full-width" style="padding:0 1em;">
      <table class="table table-hover table-striped" id="datatable01">
        <thead>
          <th>ID</th>
          <th>Número</th>
          <th>Credor</th>
          <th>Data</th>
          <th>Valor Liquidado</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php if ($empenhos): ?>
            <?php foreach($empenhos as $empenho): ?>
              <?php $liquidacoes = $this->empenhos_model->liquidacoes_select($empenho['id']); ?>
              <?php $pagamentos = $this->empenhos_model->pagamentos_select($empenho['id']); ?>

              <?php $cont = $cont + 1; ?>
              <tr>
                <td><?php echo $cont; ?></td>
                <td>
                  <a href="#" data-toggle="modal" data-target="#modalEmp<?php echo $empenho['id'] ?>"><strong><?php echo $empenho['Numero'] ?></strong></a>
                  <div class="modal fade" id="modalEmp<?php echo $empenho['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEmp<?php echo $empenho['id'] ?>Label">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="modalEmp<?php echo $empenho['id'] ?>Label">Detalhes do Empenho</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Data</label>
                    		        <p><?php echo date('d/m/Y', strtotime($empenho['Data'])) ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Número</label>
                    		        <p><?php echo $empenho['Numero'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Credor</label>
                    		        <p>
                    							<?php $credor = $this->imports_model->credor($empenho['CPF'],$empenho['CNPJ']) ?>
                    							<?php echo $credor['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    				<?php $acao = $this->imports_model->acao($empenho['AcaoCodigo']) ?>
                    				<?php $fonte = $this->imports_model->fonte($empenho['FonteCodigo']) ?>
                    				<?php $rubrica = $this->imports_model->rubrica($empenho['RubricaCodigo']) ?>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Unidade Orçamentária</label>
                    		        <p>
                    							<?php $unidade = $this->imports_model->unidade($acao['unidadecodigo']) ?>
                    							<?php echo $unidade['descricao'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Função</label>
                    						<?php $funcao = $this->imports_model->funcao($acao['funcaocodigo']) ?>
                    		        <p><?php echo $funcao['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Sub-Função</label>
                    						<?php $subfuncao = $this->imports_model->subfuncao($acao['subfuncaocodigo']) ?>
                    		        <p><?php echo $subfuncao['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Programa</label>
                    						<?php $programa = $this->imports_model->programa($acao['programacodigo']) ?>
                    		        <p><?php echo $programa['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Ação</label>
                    		        <p><?php echo $acao['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Fonte de Recurso</label>
                    		      	<p><?php echo $fonte['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Elemento de Despesa</label>
                    		      	<p><?php echo $rubrica['descricao'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>CPF/CNPJ</label>
                    		      	<p><?php echo $empenho['CPF'] ?> <?php echo $empenho['CNPJ'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Número do Processo</label>
                    		      	<p><?php echo $empenho['Processo'] ?></p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor</label>
                    		      	<p>
                    			  		R$ <?php echo number_format($empenho['ValorEmpAteAgora'],2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor Liquidado</label>
                    		      	<p>
                    		      		R$ <?php echo number_format($empenho['ValorLiqAteAgora'],2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor Pago</label>
                    		      	<p>
                    		      		R$ <?php echo number_format($empenho['ValorPagAteAgora'],2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		  </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <?php $credor = $this->imports_model->credor($empenho['CPF'],$empenho['CNPJ']) ?>
                    <?php echo $credor['Nome'] ?>
                  </td>
                  <td><?php echo date('d/m/Y', strtotime($empenho['Data'])) ?></td>
                  <td>
                    R$ <?php echo number_format($empenho['ValorLiqAteAgora'],2,',','.') ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
