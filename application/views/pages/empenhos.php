
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        <div class="col-lg-12">
            
        </div>
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/empenhos') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
                <a href="<?php echo site_url('pages/empenhosxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
                <a href="<?php echo site_url('pages/empenhosxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>

    <div class="content table-responsive table-full-width" style="padding:0 1em;">
      <table class="table table-hover table-striped" id="datatable01">
        <thead>
          <th>ID</th>
          <th>Número</th>
          <th>Credor</th>
          <th>Data</th>
          <th>Valor</th>
        </thead>
        <tbody>
          <?php $cont = 0; ?>
          <?php if ($empenhos): ?>
            <?php foreach($empenhos as $empenho): ?>
              <?php $liquidacoes = $this->empenhos_model->liquidacoes_select($empenho['id']); ?>
              <?php $pagamentos = $this->empenhos_model->pagamentos_select($empenho['id']); ?>

              <?php $cont = $cont + 1; ?>
              <tr>
                <td><?php echo $cont; ?></td>
                <td>
                  <a href="#" data-toggle="modal" data-target="#modalEmp<?php echo $empenho['id'] ?>"><strong><?php echo $empenho['numero'] ?></strong></a>
                  <div class="modal fade" id="modalEmp<?php echo $empenho['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEmp<?php echo $empenho['id'] ?>Label">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="modalEmp<?php echo $empenho['id'] ?>Label">Detalhes do Empenho</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Data</label>
                                <p><?php echo $empenho['data'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Número</label>
                                <p><?php echo $empenho['numero'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Credor</label>
                                <p><?php echo $empenho['credor'] ?></p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Unidade Orçamentária</label>
                                <p><?php echo $empenho['unidade_orcamentaria'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Função</label>
                                <p><?php echo $empenho['funcao'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Sub-Função</label>
                                <p><?php echo $empenho['subfuncao'] ?></p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Programa</label>
                                <p><?php echo $empenho['programa'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Ação</label>
                                <p><?php echo $empenho['acao'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Fonte de Recurso</label>
                                <p><?php echo $empenho['fonte_de_recurso'] ?></p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Elemento de Despesa</label>
                                <p><?php echo $empenho['elemento_de_despesa'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>CPF/CNPJ</label>
                                <p><?php echo $empenho['cpf_cnpj'] ?></p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Número do Processo</label>
                                <p><?php echo $empenho['numero_do_processo'] ?></p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Valor</label>
                                <p>
                                  <?php $cont_liq = 0; ?>
                                  <?php foreach ($liquidacoes as $key => $liquidacao): ?>
                                    <?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
                                  <?php endforeach ?>
                                  R$ <?php echo number_format($cont_liq,2,',','.') ?>

                                </p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Valor Liquidado</label>
                                <p>
                                  <?php $cont_liq = 0; ?>
                                  <?php foreach ($liquidacoes as $key => $liquidacao): ?>
                                    <?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
                                  <?php endforeach ?>
                                  R$ <?php echo number_format($cont_liq,2,',','.') ?>

                                </p>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Valor Pago</label>
                                <p>
                                  <?php $cont_pag = 0; ?>
                                  <?php foreach ($pagamentos as $key => $pagamento): ?>
                                    <?php $cont_pag = $cont_pag + $pagamento['valor']; ?>
                                  <?php endforeach ?>
                                  R$ <?php echo number_format($cont_pag,2,',','.') ?>

                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Histórico</label>
                                <p><?php echo $empenho['historico'] ?></p>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Anexo de Arquivo</label>
                                <p>
                                  <?php $anexos = $this->empenhosanexos_model->select($empenho['id']) ?>
                                  <?php foreach($anexos as $key => $anexo): ?>
                                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Ver Anexo</a> |
                                  <?php endforeach; ?>
                                </div>
                              </div>
                            </div>
                            <hr>
                            <h4 style="margin: 0">Liquidações</h4>
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Número</th>
                                  <th>Data</th>
                                  <th>Valor</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($liquidacoes as $key => $liquidacao): ?>
                                  <tr>
                                    <td><?php echo $liquidacao['numero'] ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($liquidacao['data'])) ?></td>
                                    <td>R$ <?php echo number_format($liquidacao['valor'],2,',','.') ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>

                            <hr>
                            <h4 style="margin: 0">Pagamentos</h4>
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Banco</th>
                                  <th>Agência</th>
                                  <th>Número da Conta</th>
                                  <th>Documento</th>
                                  <th>Data</th>
                                  <th>Valor</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($pagamentos as $key => $pagamento): ?>
                                  <tr>
                                    <td><?php echo $pagamento['banco'] ?></td>
                                    <td><?php echo $pagamento['agencia'] ?></td>
                                    <td><?php echo $pagamento['conta'] ?></td>
                                    <td><?php echo $pagamento['documento'] ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($pagamento['data'])) ?></td>
                                    <td>R$ <?php echo number_format($pagamento['valor'],2,',','.') ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td><?php echo $empenho['credor'] ?></td>
                  <td><?php echo date('d/m/Y H:i:s', strtotime($empenho['data'])) ?></td>
                  <td>
                    <?php $cont_liq = 0; ?>
                    <?php foreach ($liquidacoes as $key => $liquidacao): ?>
                      <?php $cont_liq = $cont_liq + $liquidacao['valor']; ?>
                    <?php endforeach ?>
                    R$ <?php echo number_format($cont_liq,2,',','.') ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
