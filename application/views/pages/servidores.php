
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        
        <div class="col-md-7">

          <form class="" action="<?php echo base_url('pages/servidores') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>

        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/servidores/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
          <a href="<?php echo site_url('pages/servidoresxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
          <a href="<?php echo site_url('pages/servidoresxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <tr>
        <th>ID</th>
          <th>Nome</th>
          <?php if (
            $this->db->database != 'noia_trans2' and 
            $this->db->database != 'satubaal_trans2' and 
            $this->db->database != 'prefmajo_trans2'  and 
            $this->db->database != 'arapirac_trans2'  and 
            $this->db->database != 'prefouro_trans2'  and 
            $this->db->database != 'preflimo_trans'): 
          ?>
            <th>Valor Bruto</th>
            <th>Desconto</th>
            <th>Valor Líquido</th>
          <?php endif; ?>
          <th>Mês de Referência</th>
          <th style="text-align:center">Anexo</th>
      </tr>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($servidores as $servidor): ?>
        <?php $cont = $cont + 1; ?>
        <tr>
          <td><?php echo $cont ?></td>
              <td><?php echo $servidor['nome'] ?></td>
              <?php if 
                ($this->db->database != 'noia_trans2' and 
                $this->db->database != 'satubaal_trans2' and 
                $this->db->database != 'prefmajo_trans2'  and 
                $this->db->database != 'arapirac_trans2'  and 
                $this->db->database != 'prefouro_trans2'  and 
                $this->db->database != 'preflimo_trans'): 
              ?>
                <td><?php echo number_format($servidor['valor_bruto'],2,',','.') ?></td>
                <td><?php echo number_format($servidor['descontos'],2,',','.') ?></td>
                <td><?php echo number_format($servidor['valor_liquido'],2,',','.') ?></td>
              <?php endif; ?>
              <td>
                <?php foreach ($month as $key => $mes): ?>
                  <?php if ($key == explode('/', $servidor['mes_ref'])[0]): ?>
                    <?php echo $mes ?> / <?php echo explode('/', $servidor['mes_ref'])[1] ?>
                  <?php endif; ?>
                <?php endforeach; ?>
              </td>
              <td style="text-align:center">
                  <?php $anexos = $this->servidoresanexos_model->select($servidor['id']) ?>
                <?php foreach($anexos as $key => $anexo): ?>
                    <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Ver Anexo</a><br>
                <?php endforeach; ?>
            </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
