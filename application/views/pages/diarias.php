
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
       
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/diarias') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/diarias/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por favorecido...">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
          <a href="<?php echo site_url('pages/diariasxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
          <a href="<?php echo site_url('pages/diariasxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <th>ID</th>
      <th>Servidor</th>
      <th>Cargo</th>
      <th>Data</th>
      <th>Destino</th>
      <th>Motivo da Viagem</th>
      <th>Valor</th>
      <th style="text-align:center">Anexo</th>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($diarias as $diaria): ?>
          <?php $cont = $cont + 1; ?>
          <tr>
            <td><?php echo $cont ?></td>
            <td>
              <?php echo $diaria['favorecido'] ?>
            </td>
            <td>
              <?php $cargo = $this->cargos_model->select_id($diaria['cargo_id']) ?>
              <?php echo $cargo['name'] ?>
            </td>
            <td><?php echo date('d/m/Y', strtotime($diaria['data'])) ?></td>
            <td><?php echo $diaria['destino'] ?></td>
            <td><?php echo $diaria['motivo'] ?></td>
            <td>R$ <?php echo number_format($diaria['valor'],2,',','.') ?></td>
            <td style="text-align:center">
                <?php $anexos = $this->diariasanexos_model->select($diaria['id']) ?>
              <?php foreach($anexos as $key => $anexo): ?>
                  <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
              <?php endforeach; ?>
            </td>
          </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
