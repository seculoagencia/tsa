
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/imports/sicap') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                      <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>Mês</label><br>
                <select class="form-control" name="month">
                  <?php $cont = 0; ?>
                  <?php if(isset($_GET['month'])): ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php echo $selected = ($_GET['month'] == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                  <?php foreach ($month as $key): ?>
                    <?php $cont = $cont + 1 ?>
                    <option value="<?php echo $cont ?>" <?php //echo $selected = (date('n') == $cont) ? 'selected' : '' ; ?>><?php echo $key ?></option>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="form-group">
                <label>&nbsp;</label><br>
                <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-5" style="text-align:right">
          <form class="" action="<?php echo base_url('pages/list_for_category/'.$this->uri->segment(3)) ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>&nbsp;</label><br>
                <input type="text" name="search" class="form-control" placeholder="Pesquisar por título...">
                <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
                <a href="<?php echo site_url('pages/importsxml/sicap') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
                <a href="<?php echo site_url('pages/importsxls/sicap') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>

    <div class="content table-responsive table-full-width" style="padding:0 1em;">
      <table class="table table-hover table-striped" id="datatable01NoPage">
        <thead>
          <th>ID</th>
          <th>Número</th>
          <th>Credor</th>
          <th>Data</th>
          <th>Valor</th>
        </thead>
        <tbody>
          <?php $cont = ($this->uri->segment(5)) ? 30*($this->uri->segment(5) - 1) : 0; ?>
          <?php if ($empenhos): ?>
            <?php foreach($empenhos as $empenho): ?>
              <?php $liquidacoes = $this->sicaps_model->liquidacoes_select($empenho['NumEmpenho']); ?>
              <?php $pagamentos = $this->sicaps_model->pagamentos_select($empenho['NumEmpenho']); ?>

              <?php $cont = $cont + 1; ?>
              <tr>
                <td><?php echo $cont; ?></td>
                <td>
                  <a href="#" data-toggle="modal" data-target="#modalEmp<?php echo $empenho['id'] ?>"><strong><?php echo $empenho['NumEmpenho'] ?></strong></a>
                  <div class="modal fade" id="modalEmp<?php echo $empenho['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEmp<?php echo $empenho['id'] ?>Label">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="modalEmp<?php echo $empenho['id'] ?>Label">Detalhes do Empenho</h4>
                        </div>
                        <div class="modal-body">
                    		  <div class="row">
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Data</label>
                    		        <p><?php echo date('d/m/Y', strtotime($empenho['DataEmpenho'])) ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Número</label>
                    		        <p><?php echo $empenho['NumEmpenho'] ?></p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Credor</label>
                    		        <p>
                    							<?php $credor = $this->sicaps_model->getVerifica('sicap_Credor', 'CodCredor',$empenho['CodCredor'], $empenho['CodUndGestora']) ?>
                    							<?php echo $credor['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Unidade Orçamentária</label>
                    		        <p>
                    							<?php $undorcamentaria = $this->sicaps_model->getVerifica('sicap_UndOrcamentaria', 'CodUndOrcamentaria',$empenho['CodUndOrcamentaria'], $empenho['CodUndGestora']) ?>
                    							<?php echo $undorcamentaria['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Função</label>
                    		        <p>
                    							<?php $funcao = $this->sicaps_model->getVerifica('sicap_Funcao', 'CodFuncao',$empenho['CodFuncao'], $empenho['CodUndGestora']) ?>
                    							<?php echo $funcao['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Sub-Função</label>
                    		        <p>
                    							<?php $subfuncao = $this->sicaps_model->getVerifica('sicap_SubFuncao', 'CodSubFuncao',$empenho['CodSubFuncao'], $empenho['CodUndGestora']) ?>
                    							<?php echo $subfuncao['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Programa</label>
                    		        <p>
                    							<?php $programa = $this->sicaps_model->getVerifica('sicap_Programa', 'CodPrograma',$empenho['CodPrograma'], $empenho['CodUndGestora']) ?>
                    							<?php echo $programa['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Projeto / Atividade</label>
                    		        <p>
                    							<?php $acao = $this->sicaps_model->getVerifica('sicap_ProjAtividade', 'CodProjAtividade',$empenho['CodProjAtividade'], $empenho['CodUndGestora']) ?>
                    							<?php echo $acao['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Fonte de Recurso</label>
                    						<p>
                    							<?php $acao = $this->sicaps_model->getVerifica('sicap_RecursoVinculado', 'CodRecVinculado',$empenho['CodRecVinculado'], $empenho['CodUndGestora']) ?>
                    							<?php echo $acao['Nome'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Elemento de Despesa</label>
                    						<p>
                    							<?php $rubrica = $this->sicaps_model->getVerifica('sicap_RubricaDespesa', 'CodRubrica',$empenho['CodContaDespesa'], $empenho['CodUndGestora']) ?>
                    							<?php echo $rubrica['Especificacao'] ?>
                    						</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Número do Processo</label>
                    		      	<p><?php echo $empenho['NumProcesso'] ?></p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		  	<div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor</label>
                    						<p>
                    						R$ <?php echo number_format($empenho['Valor'],2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor Liquidado</label>
                    		      	<p>
                    		      		<?php $cont_liq = 0; ?>
                    		      		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
                    		      			<?php $cont_liq = $cont_liq + $liquidacao['Valor']; ?>
                    				  	<?php endforeach ?>
                    			  		R$ <?php echo number_format($cont_liq,2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		    <div class="col-md-4">
                    		      <div class="form-group">
                    		        <label>Valor Pago</label>
                    		      	<p>
                    		      		<?php $cont_pag = 0; ?>
                    		      		<?php foreach ($pagamentos as $key => $pagamento): ?>
                    		      			<?php $cont_pag = $cont_pag + $pagamento['Valor']; ?>
                    				  	<?php endforeach ?>
                    			  		R$ <?php echo number_format($cont_pag,2,',','.') ?>

                    		      	</p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    		    <div class="col-md-12">
                    		      <div class="form-group">
                    		        <label>Histórico</label>
                    		        <p><?php echo $empenho['Historico'] ?></p>
                    		      </div>
                    		    </div>
                    		  </div>
                    		  <div class="row">
                    				<hr>
                    		  <div class="col-md-12">
                    		      <h4 style="margin: 0">Liquidações</h4>
                    		  <table class="table table-striped">
                    		  	<thead>
                    		  		<tr>
                    		  			<th>Número</th>
                    		  			<th>Data</th>
                    		  			<th>Valor</th>
                    		  		</tr>
                    		  	</thead>
                    		  	<tbody>
                    		  		<?php foreach ($liquidacoes as $key => $liquidacao): ?>
                    		  		<tr>
                    		  			<td><?php echo $liquidacao['NumLiquidacao'] ?></td>
                    		  			<td><?php echo date('d/m/Y', strtotime($liquidacao['DataLiquidacao'])) ?></td>
                    		  			<td>R$ <?php echo number_format($liquidacao['Valor'],2,',','.') ?></td>
                    		  		</tr>
                    		  	<?php endforeach ?>
                    		  	</tbody>
                    		  </table>

                    		  </div>
                    		  <hr>
                    		  <div class="col-md-12">
                    		  <h4 style="margin: 0">Pagamentos</h4>
                    		  <table class="table table-striped">
                    		  	<thead>
                    		  		<tr>
                    		  			<th>Código do Banco</th>
                    		  			<th>Agência</th>
                    		  			<th>Número da Conta</th>
                    		  			<th>Número do Processo</th>
                    		  			<th>Data</th>
                    		  			<th>Valor</th>
                    		  		</tr>
                    		  	</thead>
                    		  	<tbody>
                    		  	<?php foreach ($pagamentos as $key => $pagamento): ?>
                    					<?php $pagfin = $this->sicaps_model->pagamento_fin($empenho['NumEmpenho'], $pagamento['NumPagamento']) ?>
                    		  		<tr>
                    		  			<td><?php echo $pagfin['CodBanco'] ?></td>
                    		  			<td><?php echo $pagfin['CodAgenciaBanco'] ?></td>
                    		  			<td><?php echo $pagfin['NumContaBancaria'] ?></td>
                    		  			<td><?php echo $pagamento['NumProcesso'] ?></td>
                    		  			<td><?php echo date('d/m/Y', strtotime($pagamento['DataPagamento'])) ?></td>
                    		  			<td>R$ <?php echo number_format($pagamento['Valor'],2,',','.') ?></td>
                    		  		</tr>
                    		  	<?php endforeach ?>
                    		  	</tbody>
                    		  </table>
                    		  </div>
                    		</div>
                      </div>
                    </div>
                </td>
                  <td>
                    <?php $credor = $this->sicaps_model->getVerifica('sicap_Credor', 'CodCredor',$empenho['CodCredor'], $empenho['CodUndGestora']) ?>
                    <?php echo $credor['Nome'] ?>
                  </td>
                  <td><?php echo date('d/m/Y', strtotime($empenho['DataEmpenho'])) ?></td>
                  <td>
                    <?php //$cont_liq = 0; ?>
                    <?php //foreach ($liquidacoes as $key => $liquidacao): ?>
                      <?php //$cont_liq = $cont_liq + $liquidacao['Valor']; ?>
                    <?php //endforeach ?>
                    R$ <?php echo number_format($empenho['Valor'],2,',','.') ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>

          <tfoot>
          </tfoot>
        </table>
        <div class="text-right">
          <?php echo $pagination ?>
        </div>
      </div>
    </div>
  </div>
