<div class="card">
  <div class="header">
    
    <img src="<?php echo site_url('assets/img/acesso-a-informacao.png') ?>" style="width:20%;margin-bottom:1em">
    <p>
      A Lei nº 12.527, sancionada pela Presidenta da República em 18 de novembro de 2011, tem o propósito de regulamentar o direito constitucional de acesso dos cidadãos às informações públicas e seus dispositivos são aplicáveis aos três Poderes da União, Estados, Distrito Federal e Municípios. Este espaço foi disponíbilizado para permitir a pesquisa das informações publicadas e solicitar informações que não estejam disponíveis.
    </p>
  </div>
  <div class="content">
    <?php if ($this->session->flashdata('success')): ?>
      <div class="alert alert-success">
        <span><?php echo $this->session->flashdata('success'); ?></span>
      </div>
    <?php endif ?>
    <?php if ($this->session->flashdata('danger')): ?>
      <div class="alert alert-danger">
        <span><?php echo $this->session->flashdata('danger'); ?></span>
      </div>
    <?php endif ?>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Enviar Pedido</a></li>
      <li><a data-toggle="tab" href="#menu1">Consultar Pedido</a></li>
    </ul>
    <div class="tab-content" style="padding-top:1em">
      <div id="home" class="tab-pane fade in active">
        <?php echo form_open_multipart('requests/add') ?>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Nome</label>
                <input type="text" name="name" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Tipo de Pessoa</label>
                <select class="form-control" name="tipo_pessoa">
                  <option value="f">Física</option>
                  <option value="j">Jurídica</option>
                </select>
              </div>
              <div class="form-group">
                <label id="label_cpf">CPF</label>
                <input type="text" name="cpf" class="form-control" id="cpf" required>
              </div>

            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Telefone</label>
                <input type="text" name="phone" class="form-control" id="phone" required>
              </div>
              <div class="form-group">
                <label>Prioridade</label>
                <select name="priority" class="form-control" required>
                  <option value="3">Baixa</option>
                  <option value="2">Média</option>
                  <option value="1">Alta</option>
                </select>
              </div>
              <div class="form-group">
                <label>Como deseja receber sua resposta?</label>
                <select name="type_answer" class="form-control" required>
                  <option value="Email">Email</option>
                  <option value="Telefone">Telefone</option>
                  <option value="Correios">Correios</option>
                </select>
              </div>
              <div class="form-group">
                <label>Tipo de Solicitação</label>
                <select name="typerequest_id" class="form-control" required>
                  <option value="">Selecione...</option>
                  <?php foreach ($typerequests as $key): ?>
                    <option value="<?php echo $key['id'] ?>"><?php echo $key['name'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Endereço</label>
                <input type="text" name="location" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Assunto</label>
                <input type="text" name="subject" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Mensagem</label>
                <textarea name="message" rows="5" cols="80" class="form-control" required></textarea>
              </div>
              <div class="form-group">
                <label>Anexo</label>
                <input type="file" name="anexo" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
            <div class="g-recaptcha" data-sitekey="<?php echo $settings['recaptcha_site'] ?>"></div>
            </div>
            <div class="col-md-12 text-right">
              <button type="submit" class="btn btn-primary btn-fill">Gerar Pedido</button>
            </div>
          </div>
          <?php echo form_close(); ?>
      </div>
      <div id="menu1" class="tab-pane fade">
        <div class="row">
          <div class="col-md-3">
            <form id="formConsult" name="formConsult">
              <div class="form-group">
                <label>Número de Protocolo</label>
                <input type="text" name="serial" class="form-control" id="input_serial">
              </div>
              <div class="form-group">
                <label>CPF/CNPJ</label>
                <input type="text" name="cpf" class="form-control cpf">
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-primary btn-block btn-fill" onclick="DoSubmit()">Consultar</button>
              </div>
            </form>
          </div>
          <div class="col-md-9 consult-info">
            <div class="consult-info-in" style="display:none">
              <div class="row">
                <div class="col-md-4">
                  <p><strong>Nome</strong><br> <span id="cName"></span></p>
                  <p><strong>Email</strong><br> <span id="cEmail"></span></p>
                  <p><strong>CPF/CNPJ</strong><br> <span id="cCpf"></span></p>
                  <p><strong>Telefone</strong><br> <span id="cPhone"></span></p>
                </div>
                <div class="col-md-4">
                  <p><strong>Prioridade</strong><br> <span id="cPriority"></span></p>
                  <p><strong>Endereço</strong><br> <span id="cLocation"></span></p>
                  <p><strong>Como deseja receber sua resposta?</strong><br> <span id="cType_answer"></span></p>
                  <p><strong>Tipo de Solicitação</strong><br> <span id="cTyperequests"></span></p>
                </div>
                <div class="col-md-4">
                  <p><strong>Assunto</strong><br> <span id="cSubject"></span></p>
                  <p><strong>Nº de Protocolo</strong><br> <span id="cSerial"></span></p>
                  <p><strong>Data de Registro</strong><br> <span id="cCreated"></span></p>
                  <p><strong>Status</strong><br> <span class="text-success" id="cStatus"></span></p>
                </div>

              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="12">
                    <p><strong>Mensagem</strong></p>
                    <p id="cMessage"></p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="12">
                    <p><strong>Anexo</strong></p>
                    <p id="cAnexo"></p>
                  </div>
                </div>
              </div>

              <hr>
              <div class="row">
                <div class="col-md-12" id="answer_box">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Respostas</h3>
                    </div>
                    <div class="panel-body">
                      <div id="listaRespostas"></div>
                      <form id="responder" action="" method="post">

                      <div class="form-group">
                        <label>Mensagem</label>
                        <textarea name="description" rows="8" class="form-control" required></textarea>
                      </div>
                      <div class="form-group">
                        <a href="javascript:void(0)" id="fechar_pedido" class="btn btn-fill btn-success">Fechar Pedido (Resolvido)</a>
                        <button type="submit" class="btn btn-primary btn-fill">Responder</button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
