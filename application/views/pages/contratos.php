
<div class="panel">
  <div class="panel-body">
    <div class="header" style="border-bottom:1px solid #ddd;padding-bottom:1em;">
      <div class="row">
        
        <div class="col-lg-7">

          <form class="" action="<?php echo base_url('pages/contratos') ?>" method="get">
            <div class="form-inline">
              <div class="form-group">
                <label>Ano</label><br>
                <select class="form-control" name="year">
                  <?php if(isset($_GET['year'])): ?>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php echo $selected = ($_GET['year'] == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php else: ?>
                    <option value="">Todos...</option>
                    <?php for ($i=0; $i < count($year); $i++): ?>
                        <option value="<?php echo $year[$i] ?>" <?php //echo $selected = (date('Y') == $year[$i]) ? 'selected' : '' ; ?>><?php echo $year[$i] ?></option>
                    <?php endfor; ?>
                  <?php endif; ?>
                </select>
              </div>
        <div class="form-group">
          <label>&nbsp;</label><br>
          <button type="submit" class="btn btn-primary btn-fill">Filtrar</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-5" style="text-align:right">
    <form class="" action="<?php echo base_url('pages/contratos/'.$this->uri->segment(3)) ?>" method="get">
      <div class="form-inline">
        <div class="form-group">
          <label>&nbsp;</label><br>
          <input type="text" name="search" class="form-control" placeholder="Pesquisar por título..." value="<?php echo (isset($_GET['search'])) ? $_GET['search'] : ''; ?>">
          <button class="btn btn-fill btn-default" type="submit"><i class="fa fa-search"></i></button>
          <a href="<?php echo site_url('pages/contratosxml') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> XML</a>
          <a href="<?php echo site_url('pages/contratosxls') ?>?<?php echo $_SERVER['QUERY_STRING'] ?>" class="btn btn-fill btn-primary"><i class="fa fa-download"></i> CSV</a>
        </div>
      </div>
    </form>
  </div>

</div>
</div>

<div class="content table-responsive table-full-width" style="padding:0 1em;">
  <table class="table table-hover table-striped" id="datatable01">
    <thead>
      <th>ID</th>
      <th>Órgão</th>
      <th>Modalidade</th>
      <th>Data/Hora</th>
      <th>Objeto</th>
      <?php if (
        $this->db->database != 'noia_trans2' and 
        $this->db->database != 'coite_trans2' and 
        $this->db->database != 'prefouro_trans2' and
        $this->db->database != 'coitedon_trans2'
      ): ?>
      <th>Preço</th>
      <?php endif; ?>
      <th style="text-align:center">Anexo</th>
      <th>Status</th>
      <th>Vencedor</th>
    </thead>
    <tbody>
      <?php $cont = 0; ?>
      <?php foreach ($licitacoes as $licitacao): ?>
        <?php if ($licitacao['tipo'] == 1): ?>
          <?php $cont = $cont + 1; ?>
          <tr>
            <td><?php echo $cont ?></td>
            <td><?php echo $licitacao['orgao'] ?></td>
            <td><?php echo $licitacao['modalidade'] ?></td>
            <td><?php echo date('d/m/Y H:i:s', strtotime($licitacao['data'])) ?></td>
            <td><button class="btn btn-primary" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<?php echo $licitacao['objeto'] ?>"><i class="fa fa-eye"></i> Visualizar/Ocultar</button> </td>
            <?php if (
              $this->db->database != 'noia_trans2' and 
              $this->db->database != 'coite_trans2' and 
              $this->db->database != 'prefouro_trans2' and
              $this->db->database != 'coitedon_trans2'
            ): ?>
            <td>R$ <?php echo number_format($licitacao['valor'],2,',','.') ?></td>
            <?php endif; ?>
            <td style="text-align:center">
                <?php $anexos = $this->licitacoesanexos_model->select($licitacao['id']) ?>
              <?php foreach($anexos as $key => $anexo): ?>
                  <a href="<?php echo site_url($anexo['anexo']) ?>" target="_blank">Anexo <?php echo $key + 1 ?></a><br>
              <?php endforeach; ?>
            </td>
            <td><?php echo $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento' ; ?></td>
            <td><?php echo $licitacao['vencedor'] ?></td>
          </tr>
        <?php endif; ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
</div>
</div>
