<style>
    .nav li a {
        padding: 5px 15px;
    }
    .sidebar::after, .sidebar::before, body > .navbar-collapse::after, body > .navbar-collapse::before {
      display: none;
    }
</style>
<ul class="nav">
  <?php if ($this->session->userdata('user_on')['group_id'] == 1): ?>
    <!-- <li class="<?php echo $active = ($this->uri->segment(2) == 'groups') ? 'active':''; ?>">
      <a href="<?php echo base_url('admin/groups') ?>">
        <i class="pe-7s-users"></i>
        <p>Grupos</p>
      </a>
    </li> -->
  <?php endif; ?>
  <?php if ($this->session->userdata('user_on')['group_id'] == 1 || $this->session->userdata('user_on')['group_id'] == 2): ?>
    <li class="<?php echo $active = ($this->uri->segment(2) == 'users') ? 'active':''; ?>">
      <a href="<?php echo base_url('admin/users') ?>">
        <i class="pe-7s-user"></i>
        <p>Usuários</p>
      </a>
    </li>
    <li class="<?php echo $active = ($this->uri->segment(2) == 'settings') ? 'active':''; ?>">
      <a href="<?php echo base_url('admin/settings') ?>">
        <i class="pe-7s-config"></i>
        <p>Configurações</p>
      </a>
    </li>
  <?php endif; ?>
    <li class="<?php echo $active = ($this->uri->segment(2) == 'categories') ? 'active':''; ?>">
      <a href="<?php echo base_url('admin/categories') ?>">
        <i class="pe-7s-albums"></i>
        <p>Categorias</p>
      </a>
    </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'reports' || $this->uri->segment(2) == '') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/reports') ?>">
      <i class="pe-7s-display2"></i>
      <p>Prestação de Contas</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(3) == 'tc') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/imports/empenhos/tc') ?>">
      <i class="pe-7s-display2"></i>
      <p>Importação TC Cloud</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(3) == 'sicap') ? 'active':''; ?>">
    <a href="<?php echo base_url('imports/index2') ?>">
      <i class="pe-7s-display2"></i>
      <p>Importação SICAP</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(3) == 'sagres') ? 'active':''; ?>">
    <a href="<?php echo base_url('imports/index3') ?>">
      <i class="pe-7s-display2"></i>
      <p>Importação SAGRES</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(3) == 'servidores') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/imports/servidores/sicap') ?>">
      <i class="pe-7s-display2"></i>
      <p>Importação Servidores</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(3) == 'vebas') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/verbas') ?>">
      <i class="pe-7s-display2"></i>
      <p>Verba Indenizatória</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'empenhos') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/empenhos') ?>">
      <i class="pe-7s-display2"></i>
      <p>Empenhos</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'receitas') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/receitas') ?>">
      <i class="pe-7s-display2"></i>
      <p>Receitas</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'cargos') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/cargos') ?>">
      <i class="pe-7s-display2"></i>
      <p>Cargos</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'servidores') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/servidores') ?>">
      <i class="pe-7s-display2"></i>
      <p>Servidores</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'licitacoes') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/licitacoes') ?>">
      <i class="pe-7s-display2"></i>
      <p>Contratos e Licitações</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'diarias') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/diarias') ?>">
      <i class="pe-7s-display2"></i>
      <p>Diárias</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'requests') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/requests') ?>">
      <i class="pe-7s-info"></i>
      <p>Pedidos e-SIC</p>
    </a>
  </li>
  <li class="<?php echo $active = ($this->uri->segment(2) == 'typerequests') ? 'active':''; ?>">
    <a href="<?php echo base_url('admin/typerequests') ?>">
      <i class="pe-7s-info"></i>
      <p>Tipos de Solicitações</p>
    </a>
  </li>
  <li>
    <?php echo anchor('users/logout', '<i class="pe-7s-power"></i><p>Sair</p>', array('class'=>'')); ?>
  </li>

  <!-- <li class="active-pro"> -->
    <?php //echo anchor('users/logout', '<i class="pe-7s-power"></i><p>Sair</p>', array('class'=>'')); ?>
    <!-- <a href="upgrade.html">
      <i class="pe-7s-power"></i>
      <p>Sair</p>
    </a> -->
  <!-- </li> -->
</ul>
