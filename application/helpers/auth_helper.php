<?php

	function auth() {
		$ci = get_instance();

		$userOn = $ci->session->userdata('user_on');

		if (!$userOn) {
			$ci->session->set_flashdata('danger', 'Você precisa fazer login!');
			redirect("admin/login");
		}
		if ($ci->session->userdata('user_on')['status']==0) {
			$ci->session->unset_userdata('user_on');
			$ci->session->set_flashdata('danger', 'Você encontra-se bloqueado, por favor procure o administrador Master!');
			redirect("users/login");
		}
		return $userOn;

	}
