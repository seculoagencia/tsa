<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages';
$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;

$route['pages/esic/estatisticas'] = 'pages/estatisticas';
$route['pages/esic/relatorio'] = 'pages/relatorio';
$route['pages/imports/tc'] = 'pages/imports';
$route['pages/imports/sicap'] = 'pages/imports';
$route['pages/receitas/tc'] = 'pages/receitas';
$route['pages/receitas/sicap'] = 'pages/receitas';
$route['pages/servidores/sicap'] = 'pages/impservidores';
$route['pages/receitasxml/tc'] = 'pages/receitasxml';
$route['pages/receitasxml/sicap'] = 'pages/receitasxml';
$route['pages/des_extra_orcamentarias/sicap'] = 'pages/des_extra_orcamentarias';

$route['admin/login'] = 'users/login';

$route['admin/reports'] = 'reports';
$route['admin/reports/edit/(:num)'] = 'reports/edit/$1';
$route['admin/reports/remove/(:num)'] = 'reports/remove/$1';

$route['admin/empenhos'] = 'empenhos';
$route['admin/empenhos/add'] = 'empenhos/add';
$route['admin/empenhos/edit/(:num)'] = 'empenhos/edit/$1';
$route['admin/empenhos/details/(:num)'] = 'empenhos/details/$1';
$route['admin/empenhos/remove/(:num)'] = 'empenhos/remove/$1';

$route['admin/receitas'] = 'receitas';
$route['admin/receitas/add'] = 'receitas/add';
$route['admin/receitas/edit/(:num)'] = 'receitas/edit/$1';
$route['admin/receitas/details/(:num)'] = 'receitas/details/$1';
$route['admin/receitas/remove/(:num)'] = 'receitas/remove/$1';

$route['admin/licitacoes'] = 'licitacoes';
$route['admin/licitacoes/add'] = 'licitacoes/add';
$route['admin/licitacoes/edit/(:num)'] = 'licitacoes/edit/$1';
$route['admin/licitacoes/details/(:num)'] = 'licitacoes/details/$1';
$route['admin/licitacoes/remove/(:num)'] = 'licitacoes/remove/$1';

$route['admin/servidores'] = 'servidores';
$route['admin/servidores/add'] = 'servidores/add';
$route['admin/servidores/edit/(:num)'] = 'servidores/edit/$1';
$route['admin/servidores/details/(:num)'] = 'servidores/details/$1';
$route['admin/servidores/remove/(:num)'] = 'servidores/remove/$1';

$route['admin/diarias'] = 'diarias';
$route['admin/diarias/add'] = 'diarias/add';
$route['admin/diarias/edit/(:num)'] = 'diarias/edit/$1';
$route['admin/diarias/details/(:num)'] = 'diarias/details/$1';
$route['admin/diarias/remove/(:num)'] = 'diarias/remove/$1';

$route['admin/cargos'] = 'cargos';
$route['admin/cargos/add'] = 'cargos/add';
$route['admin/cargos/edit/(:num)'] = 'cargos/edit/$1';
$route['admin/cargos/details/(:num)'] = 'cargos/details/$1';
$route['admin/cargos/remove/(:num)'] = 'cargos/remove/$1';

$route['admin/verbas'] = 'verbas';
$route['admin/verbas/add'] = 'verbas/add';
$route['admin/verbas/edit/(:num)'] = 'verbas/edit/$1';
$route['admin/verbas/details/(:num)'] = 'verbas/details/$1';
$route['admin/verbas/remove/(:num)'] = 'verbas/remove/$1';

$route['admin/requests'] = 'requests';
$route['admin/requests/edit/(:num)'] = 'requests/edit/$1';
$route['admin/requests/details/(:num)'] = 'requests/details/$1';
$route['admin/requests/remove/(:num)'] = 'requests/remove/$1';

$route['admin/typerequests'] = 'typerequests';
$route['admin/typerequests/edit/(:num)'] = 'typerequests/edit/$1';
$route['admin/typerequests/remove/(:num)'] = 'typerequests/remove/$1';

$route['admin/categories'] = 'categories';
$route['admin/categories/edit_category/(:num)'] = 'categories/edit_category/$1';
$route['admin/categories/remove_category/(:num)'] = 'categories/remove_category/$1';
$route['admin/categories/edit_subcategory/(:num)'] = 'categories/edit_subcategory/$1';
$route['admin/categories/remove_subcategory/(:num)'] = 'categories/remove_subcategory/$1';

$route['admin/groups'] = 'groups';
$route['admin/groups/edit/(:num)'] = 'groups/edit/$1';
$route['admin/users'] = 'users';
$route['admin/users/edit/(:num)'] = 'users/edit/$1';
$route['admin/settings'] = 'settings';
$route['admin'] = 'reports';

$route['admin/imports/empenhos/tc'] = 'imports/index';
$route['admin/imports/empenhos/sicap'] = 'imports/index2';
// $route['admin/imports-empenhos-sicap'] = 'imports/index2';

$route['admin/imports/receitas/tc'] = 'impreceitas/index';
$route['admin/imports/receitas/sicap'] = 'impreceitas/index2';

$route['admin/imports/servidores/sicap'] = 'impservidores/index';
$route['admin/imports/servidores/sicap/add'] = 'impservidores/add';

$route['admin/imports/add'] = 'imports/add';
$route['admin/imports/edit/(:num)'] = 'imports/edit/$1';
$route['admin/imports/tc/details/(:num)'] = 'imports/details/$1';
$route['admin/imports/sicap/details/(:num)'] = 'imports/details/$1';
$route['admin/imports/sagres/details/(:num)'] = 'imports/details/$1';
$route['admin/imports/tc/remove/(:num)'] = 'imports/remove/$1';
$route['admin/imports/sicap/remove/(:num)'] = 'imports/remove/$1';
$route['admin/imports/remove_sicap/(:num)'] = 'imports/remove_sicap/$1';
