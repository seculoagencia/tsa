<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diarias extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('cargos_model');
        // $this->load->model('subcategories_model');
        $this->load->model('diarias_model');
        $this->load->model('diariasanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $diarias = $this->diarias_model->select_search($this->input->get('search'));
      } else {
        $diarias = $this->diarias_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $diarias = $this->diarias_model->select($year_key = date('Y'));
      $diarias = $this->diarias_model->select();
    }
    $settings = $this->settings_model->select_id();
    $cargos = $this->cargos_model->select();
    // $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'diarias' => $diarias,
      'cargos' => $cargos,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/diarias/index', $dados);

  }
  public function add()
  {
      $diaria = array(
        'favorecido' => $this->input->post('favorecido'),
        'cargo_id' => $this->input->post('cargo_id'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'destino' => $this->input->post('destino'),
        'motivo' => $this->input->post('motivo'),
        'valor' => $this->input->post('valor'),
        'created' => date('Y-m-d H:i:s')
      );
      $this->diarias_model->insert($diaria);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Diária criada com sucesso!"));
      redirect('admin/diarias');

  }
  public function add_anexo($id)
  {
    // var_dump('Oi');
    // exit;
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'report_id' => $id,
      'anexo' => $this->do_upload_anexo()
    );
    $this->diariasanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('admin/diarias');
  }
  public function remove_anexo($id)
  {
    $report = $this->diariasanexos_model->select_id($id);
    $this->diariasanexos_model->delete($id);
    $this->last_update();
    // if (file_exists("./{$licitacao['anexo']}")) {
    //   unlink("./{$report['anexo']}");
    // }
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('admin/diarias');
  }
  public function edit($id)
  {
    $diaria = $this->diarias_model->select_num($id);

    if ($this->input->post()) {
      // if (empty($_FILES['anexo']['name'])) {
      //   $anexo = $diaria['anexo'];
      // } else {
      //   $anexo = $this->do_upload_anexo();
      // }
      // $status = ($this->input->post('status') == 0) ? false : true ;
      $diaria_up = array(
        'favorecido' => $this->input->post('favorecido'),
        'cargo_id' => $this->input->post('cargo_id'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'destino' => $this->input->post('destino'),
        'motivo' => $this->input->post('motivo'),
        'valor' => $this->input->post('valor')
        // 'anexo' => $anexo,
      );
      $this->diarias_model->update($diaria_up, $id);
      // if (!empty($_FILES['anexo']['name'])) {
      //   unlink("./{$diaria['anexo']}");
      // }
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/diarias');
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'diarias',
      'page_subtitle' => 'Editar',
      'licitacao' => $diaria,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/diarias/edit', $dados);
  }
  public function details($num)
  {
    $diaria = $this->diarias_model->select_id($num);
    $liquidacoes = $this->diarias_model->liquidacoes_select($diaria['id']);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'diarias',
      'page_subtitle' => 'Detalhes',
      'diaria' => $diaria,
      'liquidacoes' => $liquidacoes,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/diarias/details', $dados);
  }
  //
  public function remove($id)
  {
    $diaria = $this->diarias_model->select_id($id);
    $this->diarias_model->delete($id);
    $this->last_update();
    // if (file_exists("./{$licitacao['anexo']}")) {
    //   unlink("./{$diaria['anexo']}");
    // }
    $this->session->set_flashdata(array("success" => "Receita removida com sucesso!"));
    redirect('diarias');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("diarias");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
