<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('settings_model');

    }
  public function index()
  {
    $settings = $this->settings_model->select_id();
    $groups = $this->groups_model->select_all();
    $dados = array(
      'page_title' => 'Grupos',
      'page_subtitle' => 'Lista',
      'groups' => $groups,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/groups/index', $dados);
  }
  public function add()
  {
    $grupo = array(
      'name' => $this->input->post('name'),
      'created' => date('Y-m-d H:i:s'),
      'modified' => date('Y-m-d H:i:s'),
    );
    $this->groups_model->insert($grupo);
    $this->session->set_flashdata(array("success" => "Grupo <strong>".$this->input->post('name')."</strong> criado com sucesso!"));
    redirect('groups');
  }
  public function edit($id)
  {
    if ($this->input->post()) {
      $grupo = array(
        'name' => $this->input->post('name'),
        'modified' => date('Y-m-d H:i:s'),
      );
      $this->groups_model->update($grupo, $id);
      $this->session->set_flashdata(array("success" => "Grupo alterado para <strong>".$this->input->post('name')."</strong> com sucesso!"));
      redirect('groups');
    }
    $settings = $this->settings_model->select_id();
    $group = $this->groups_model->select($id);
    $dados = array(
      'page_title' => 'Grupos',
      'page_subtitle' => 'Detalhes',
      'group' => $group,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/groups/edit', $dados);
  }

  public function remove($id)
  {
    $this->groups_model->delete($id);
    $this->session->set_flashdata(array("success" => "Grupo apagado para <strong>".$this->input->post('name')."</strong> com sucesso!"));
    redirect('groups');
  }
}
