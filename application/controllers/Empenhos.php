<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empenhos extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('empenhos_model');
        $this->load->model('empenhosanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->empenhos_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->empenhos_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->empenhos_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->empenhos_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/empenhos/index', $dados);

  }
  public function add()
  {
    if ($this->input->post()) {
      if (empty($_FILES['anexo']['name'])) {
        $anexo = null;
      } else {
        $anexo = $this->do_upload_anexo();
      }
      $empenho = array(
        'numero' => $this->input->post('numero'),
        'credor' => $this->input->post('credor'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'unidade_orcamentaria' => $this->input->post('unidade_orcamentaria'),
        'funcao' => $this->input->post('funcao'),
        'subfuncao' => $this->input->post('subfuncao'),
        'programa' => $this->input->post('programa'),
        'acao' => $this->input->post('acao'),
        'fonte_de_recurso' => $this->input->post('fonte_de_recurso'),
        'elemento_de_despesa' => $this->input->post('elemento_de_despesa'),
        'cpf_cnpj' => $this->input->post('cpf_cnpj'),
        'numero_do_processo' => $this->input->post('numero_do_processo'),
        'historico' => $this->input->post('historico'),
        'anexo' => $anexo,
        'created' => date('Y-m-d H:i:s'),
      );
      $this->empenhos_model->insert($empenho);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Prestação de conta <strong>".$this->input->post('name')."</strong> criada com sucesso!"));
      redirect('admin/empenhos');
    } else {
      $settings = $this->settings_model->select_id();
      $dados = array(
        'page_title' => 'Prestação de Contas',
        'page_subtitle' => 'Lista',
        'settings' => $settings,
      );

      $this->load->view_admin('admin/empenhos/add', $dados);
    }

  }
  public function add_anexo($id)
  {
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'report_id' => $id,
      'anexo' => $this->do_upload_anexo()
    );
    $this->empenhosanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('empenhos');
  }
  public function remove_anexo($id)
  {
    $report = $this->empenhosanexos_model->select_id($id);
    $this->empenhosanexos_model->delete($id);
    $this->last_update();
    // unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('empenhos');
  }
  public function liquidacao_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->empenhos_model->select_id($empenho_id);
      $liquidacao = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      $this->empenhos_model->liquidacao_insert($liquidacao);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação inserida com sucesso!"));
      redirect('admin/empenhos/details/'.$empenho['id']);
    }
  }
  public function pagamento_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->empenhos_model->select_id($empenho_id);
      $pagamento = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      // echo $empenho['numero'];
      // exit;
      $this->empenhos_model->pagamento_insert($pagamento);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Pagamento inserido com sucesso!"));
      redirect('admin/empenhos/details/'.$empenho['id']);
    }
  }
  public function edit($id)
  {
    $empenho = $this->empenhos_model->select_id($id);

    if ($this->input->post()) {
      if (empty($_FILES['anexo']['name'])) {
        $anexo = $empenho['anexo'];
      } else {
        $anexo = $this->do_upload_anexo();
      }
      $empenho_up = array(
        'numero' => $this->input->post('numero'),
        'credor' => $this->input->post('credor'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'unidade_orcamentaria' => $this->input->post('unidade_orcamentaria'),
        'funcao' => $this->input->post('funcao'),
        'subfuncao' => $this->input->post('subfuncao'),
        'programa' => $this->input->post('programa'),
        'acao' => $this->input->post('acao'),
        'fonte_de_recurso' => $this->input->post('fonte_de_recurso'),
        'elemento_de_despesa' => $this->input->post('elemento_de_despesa'),
        'cpf_cnpj' => $this->input->post('cpf_cnpj'),
        'numero_do_processo' => $this->input->post('numero_do_processo'),
        'historico' => $this->input->post('historico'),
        'anexo' => $anexo,
      );
      // echo "<pre>";
      // var_dump($empenho_up);
      // exit;
      $this->empenhos_model->update($empenho_up, $id);
      $this->last_update();
      // if (!empty($_FILES['anexo']['name'])) {
      //   unlink("./{$empenho['anexo']}");
      // }
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/empenhos/details/'.$empenho['id']);
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Editar',
      'empenho' => $empenho,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos/edit', $dados);
  }
  public function liquidacao_edit($id)
  {
    if ($this->input->post()) {
      $liquidacao = $this->empenhos_model->liquidacoes_select_id($id);
      $empenho = $this->empenhos_model->select_id($liquidacao['empenho_id']);
      $liquidacaoUp = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->empenhos_model->liquidacao_update($liquidacaoUp, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos/details/'.$empenho['id']);
    }
  }
  public function pagamento_edit($id)
  {
    if ($this->input->post()) {
      $pagamento = $this->empenhos_model->pagamentos_select_id($id);
      $empenho = $this->empenhos_model->select_id($pagamento['empenho_id']);
      $pagamentoUp = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->empenhos_model->pagamento_update($pagamentoUp, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos/details/'.$empenho['id']);
    }
  }
  public function details($id)
  {
    $empenho = $this->empenhos_model->select_id($id);
    $liquidacoes = $this->empenhos_model->liquidacoes_select($empenho['id']);
    $pagamentos = $this->empenhos_model->pagamentos_select($empenho['id']);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Detalhes',
      'empenho' => $empenho,
      'liquidacoes' => $liquidacoes,
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos/details', $dados);
  }
  //
  public function remove($id)
  {
    $empenho = $this->empenhos_model->select_id($id);
    $liquidacoes = $this->empenhos_model->liquidacoes_select($id);
    foreach ($liquidacoes as $key => $liq) {
      $this->empenhos_model->liquidacao_delete($liq['id']);
    }
    $pagamentos = $this->empenhos_model->pagamentos_select($id);
    foreach ($pagamentos as $key => $pag) {
      $this->empenhos_model->pagamento_delete($pag['id']);
    }
    $this->empenhos_model->delete($id);
    $this->last_update();
    // unlink("./{$empenho['anexo']}");
    $this->session->set_flashdata(array("success" => "Emepnho removido com sucesso!"));
    redirect('admin/empenhos');
  }
  public function remove_liquidacao($id)
  {
    $liquidacao = $this->empenhos_model->liquidacoes_select_id($id);
    $empenho = $this->empenhos_model->select_id($liquidacao['empenho_id']);
    $this->empenhos_model->liquidacao_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Liquidação removida com sucesso!"));
    redirect('admin/empenhos/details/'.$empenho['id']);
  }
  public function remove_pagamento($id)
  {
    $pagamento = $this->empenhos_model->pagamentos_select_id($id);
    $empenho = $this->empenhos_model->select_id($pagamento['empenho_id']);
    $this->empenhos_model->pagamento_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Pagamento removido com sucesso!"));
    redirect('admin/empenhos/details/'.$empenho['id']);
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("empenhos/add");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
