<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->model('users_model');
    $this->load->model('groups_model');
    $this->load->model('settings_model');
  }
  public function index()
  {
    auth();
    if ($this->session->userdata('user_on')['group_id'] == 1 || $this->session->userdata('user_on')['group_id'] == 2) {
      $settings = $this->settings_model->select_id();
      $users = $this->users_model->select_all();
      $groups = $this->groups_model->select_all();
      $dados = array(
        'page_title' => 'Usuários',
        'page_subtitle' => 'Lista',
        'users' => $users,
        'groups' => $groups,
        'settings' => $settings,
      );
      $this->load->view_admin('admin/users/index', $dados);
    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }

  }

  public function add()
  {
    if ($this->session->userdata('user_on')['group_id']==1 || $this->session->userdata('user_on')['group_id'] == 2) {
      $user = array(
        'name' => $this->input->post('name'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'email' => $this->input->post('email'),
        'phone' => $this->input->post('phone'),
        'group_id' => $this->input->post('group_id'),
        'created' => date('Y-m-d H:i:s'),
        'modified' => date('Y-m-d H:i:s'),
      );
      $this->users_model->insert($user);
      $this->session->set_flashdata(array("success" => "Usuário <strong>".$this->input->post('name')."</strong> criado com sucesso!"));
      redirect('users');
    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }
  }

  public function edit($id)
  {
    auth();
    $user = $this->users_model->select($id);
    $groups = $this->groups_model->select_all();
    $settings = $this->settings_model->select_id();
    if ($this->session->userdata('user_on')['group_id'] == 1 || $this->session->userdata('user_on')['group_id'] == 2) {
      if ($this->input->post()) {
        $user = array(
          'group_id' => $this->input->post('group_id'),
          'modified' => date('Y-m-d H:i:s'),
        );
        $this->users_model->update($id,$user);
        $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
        redirect('users');
      }
      $dados = array(
        'page_title' => 'Grupos',
        'page_subtitle' => 'Detalhes',
        'groups' => $groups,
        'user' => $user,
        'settings' => $settings,
      );
      $this->load->view_admin('admin/users/edit', $dados);
    } else if ($this->session->userdata('user_on')['group_id'] == 2){
      if ($user['group_id'] != 1) {
        if ($this->input->post()) {
          $user = array(
            'group_id' => $this->input->post('group_id'),
            'modified' => date('Y-m-d H:i:s'),
          );
          $this->users_model->update($id,$user);
          $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
          redirect('users');
        }
        $dados = array(
          'page_title' => 'Grupos',
          'page_subtitle' => 'Detalhes',
          'groups' => $groups,
          'user' => $user,
          'settings' => $settings,
        );
        $this->load->view_admin('admin/users/edit', $dados);
      } else {
        $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
        redirect("users/profile/{$this->session->userdata('user_on')['id']}");
      }

    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }
  }

  public function user_block($id)
  {
    auth();
    if ($this->session->userdata('user_on')['group_id']==1) {
      $user = $this->users_model->select($id);
      $userUp = array(
        'status' => 0,
        'modified' => date('Y-m-d H:i:s'),
      );
      if ($user['group_id'] == 1) {
        $this->session->set_flashdata(array("danger" => "Não é possível bloquear um usuário <strong>Master</strong>!"));
        redirect('users');
      } else {
        $this->users_model->update($id,$userUp);
        $this->session->set_flashdata(array("success" => "Usuário <strong>Bloqueado</strong> com Sucesso!"));
        redirect('users');
      }
    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }

  }
  public function user_unblock($id)
  {
    auth();
    if ($this->session->userdata('user_on')['group_id']==1) {
      $user = array(
        'status' => 1,
        'modified' => date('Y-m-d H:i:s'),
      );
      $this->users_model->update($id,$user);
      $this->session->set_flashdata(array("success" => "Usuário <strong>Desbloqueado</strong> com Sucesso!"));
      redirect('users');
    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }
  }

  public function profile($id)
  {
    auth();
    // if ($id != $this->session->userdata('user_on')['id']) {
    //   $this->session->set_flashdata(array("danger" => "Você só pode acessar o seu <strong>perfil</strong>!"));
    //   redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    // }
    $settings = $this->settings_model->select_id();
    $user = $this->users_model->select($id);
    $user_log_id = $this->session->userdata('user_on')['id'];
    $user_log = $this->users_model->select($user_log_id);
    $groups = $this->groups_model->select_all();

    if ($this->session->userdata('user_on')['group_id'] == 1) {
      if ($this->input->post()) {
        $user = array(
          'group_id' => $this->input->post('group_id'),
          'name' => $this->input->post('name'),
          'username' => $this->input->post('username'),
          'email' => $this->input->post('email'),
          'phone' => $this->input->post('phone'),
          'modified' => date('Y-m-d H:i:s'),
        );
        $this->users_model->update($id,$user);
        $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
        redirect("users/profile/{$id}");
      }
      $dados = array(
        'page_title' => 'Grupos',
        'page_subtitle' => 'Detalhes',
        'user' => $user,
        'user_log' => $user_log,
        'groups' => $groups,
        'settings' => $settings,
      );
      $this->load->view_admin('admin/users/profile', $dados);
    } else if ($this->session->userdata('user_on')['group_id'] == 2) {
      if ($user['group_id'] != 1) {
        if ($this->input->post()) {
          $user = array(
            'group_id' => $this->input->post('group_id'),
            'name' => $this->input->post('name'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'modified' => date('Y-m-d H:i:s'),
          );
          $this->users_model->update($id,$user);
          $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
          redirect("users/profile/{$id}");
        }
        $dados = array(
          'page_title' => 'Grupos',
          'page_subtitle' => 'Detalhes',
          'user' => $user,
          'groups' => $groups,
          'settings' => $settings,
        );
        $this->load->view_admin('admin/users/profile', $dados);
      } else {
        $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
        redirect("users/profile/{$this->session->userdata('user_on')['id']}");
      }
    } else {
      $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
      redirect("users/profile/{$this->session->userdata('user_on')['id']}");
    }

  }

  public function avatar()
  {
    $this->load->view('crop');
  }

  public function senha_provisoria($id)
  {
    if ($this->session->userdata('user_on')['group_id']==1) {
      if ($this->input->post()) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
        }
        $senha_provisoria = implode($pass); //turn the array into a string

        $user = $this->users_model->select($id);

        $this->load->library('email');
        $this->email->from("{$this->session->userdata('user_on')['email']}", "{$this->session->userdata('user_on')['nome']} ({$this->session->userdata('user_on')['email']})");
        $this->email->to("{$user['email']}");

        $this->email->subject("Senha Provisória");
        $this->email->message("Olá, sou {$this->session->userdata('user_on')['name']}, e este é sua senha provisória: {$senha_provisoria}");

        $this->email->send();

        $userUp = array(
          'password' => $senha_provisoria,
          'modified' => date('Y-m-d H:i:s'),
        );
        $this->users_model->update($id,$userUp);
        $this->session->set_flashdata(array("success" => "Senha provisória enviada para: <strong>{$user['email']}</strong> com Sucesso!"));
        redirect('users');
      } else {
        $this->session->set_flashdata(array("danger" => "Você não tem permissão para acessar esta área!"));
        redirect("users/profile/{$this->session->userdata('user_on')['id']}");
      }
    }
  }

  public function alterar_senha($id)
  {
    $user = $this->users_model->select($id);

    if ($this->input->post('password') == $this->input->post('r_password')) {
      $user_up = array(
        'password' => md5($this->input->post('password')),
        'modified' => date('Y-m-d H:i:s'),
      );
      $this->users_model->update($id,$user_up);

      $this->session->set_flashdata(array("success" => "Senha alterada com sucesso!"));
      redirect("users/profile/{$id}");
    } else {
      $this->session->set_flashdata(array("danger" => "Nova senha não confere com a repetição!"));
      redirect("users/profile/{$id}");
    }
  

  }

  public function troca_avatar($id)
  {
    $user = $this->users_model->select($id);
    if ($user['avatar'] != 'uploads/avatar-default.jpg') {
      $user_aux = explode('.',$user['avatar'])[0];
      unlink("./".$user['avatar']);
      unlink("./".$user_aux.'.original.jpeg');
    }

    $avatarUp = array('avatar' => $this->input->post('avatar'));
    $this->users_model->update($id,$avatarUp);

    $username = $this->session->userdata('user_on')['username'];
    $pass = $this->session->userdata('user_on')['password'];

    $this->session->unset_userdata('user_on');

    $log = $this->users_model->auth_user($username, $pass);
    $this->session->set_userdata(array('user_on' => $log));

    $this->session->set_flashdata(array("success" => "Foto alterada com sucesso!"));
    redirect("users/profile/{$id}");
  }

  public function login()
  {
    if($this->input->post()){
      $this->load->model('users_model');
      $user = $this->input->post('username');
      $pass = md5($this->input->post('password'));

      $log = $this->users_model->auth_user($user, $pass);

      if ($log) {
        $this->session->set_userdata(array('user_on' => $log));
        $this->session->set_flashdata(array('success' => 'Login feito com sucesso!'));
        redirect('admin/reports');
      } else {
        $this->session->set_flashdata(array('danger'=>'Falha ao login, verifique se o nome de usuário ou senha estão corretos.'));
        redirect('admin/login');
      }
    } else if ($this->session->userdata('user_on')) {
      $this->session->set_flashdata(array('warning'=>"Você já está logado."));
      redirect('admin/reports');
    } else {
      $settings = $this->settings_model->select_id();
      $dados = array(
        'settings' => $settings, 
      );
      $this->load->view('admin/login', $dados);
    }
  }

  public function logout()
  {
    // auth();
    $this->session->unset_userdata('user_on');
    $this->session->set_flashdata(array('success'=>'Você saiu do Painel de Controle!'));
    redirect('admin/login');
  }
}
