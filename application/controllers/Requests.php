<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends CI_Controller {
  function __construct()
    {
        parent::__construct();

        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('requests_model');
        $this->load->model('answers_model');
        $this->load->model('typerequests_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    auth();
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $requests = $this->requests_model->select_search($this->input->get('search'));
        // echo "<pre>";
        // var_dump($requests);
      } else {
        $requests = $this->requests_model->select_filter(
          $year_key = $this->input->get('year'),
          $month_key = $this->input->get('month'),
          $typeresquest_key = $this->input->get('typeresquest'),
          $priority_key = $this->input->get('priority')
        );
      }

    } else {
      $requests = $this->requests_model->select($year_key = date('Y'),$month_key = date('n'), $typeresquest_key = null, $priority_key = null);
    }
    $settings = $this->settings_model->select_id();
    // $requests = $this->requests_model->select();
    $typerequests = $this->typerequests_model->select_all();
    $priority = array(
      1 => 'Alta',
      2 => 'Normal',
      3 => 'Baixa',
    );
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'requests' => $requests,
      'requests' => $requests,
      'typerequests' => $typerequests,
      'priority' => $priority,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/requests/index', $dados);

  }
  public function add()
  {
    $settings = $this->settings_model->select_id();
    $alphabet = '1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }
    $serial = implode($pass); //turn the array into a string
    $recaptchaResponse = $this->input->post('g-recaptcha-response');
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data1 = array('secret' => $settings['recaptcha_secret'], 'response' => $recaptchaResponse);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $response = curl_exec($ch);
    curl_close($ch);
    $status_captcha = json_decode($response, true);

    if ($status_captcha['success']) {
      $request = array(
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'priority' => $this->input->post('priority'),
        'cpf' => $this->input->post('cpf'),
        'phone' => $this->input->post('phone'),
        'location' => $this->input->post('location'),
        'type_answer' => $this->input->post('type_answer'),
        'typerequest_id' => $this->input->post('typerequest_id'),
        'subject' => $this->input->post('subject'),
        'message' => $this->input->post('message'),
        'anexo' => $this->do_upload_anexo(),
        'status' => 0,
        'serial' => $serial,
        'created' => date('Y-m-d H:i:s'),
      );
      $this->requests_model->insert($request);
  
      $subject_email = $settings['name']." - Pedido de Informação Enviado!";
      $msg = "<p>Seu pedido foi enviado com sucesso! Seu número de protocolo é: <strong>".$request['serial']."</strong></p>";
      $this->send_email($request['email'],$msg,$subject_email);
      $this->send_email($settings['email'],"O Pedido de informação de código:{$request['serial']}, foi recebido em: {$settings['name']}!","{$settings['name']} - Pedido de Informação Recebido");
      $this->session->set_flashdata(array("success" => "Pedido enviado com sucesso! Seu número de protocolo é:<strong>".$request['serial']."</strong> e foi enviado por email. Verifique sua caixa de <strong>Spam!</strong>"));
    } else {
      $this->session->set_flashdata(array("danger" => "CAPTCHA inválido!"));
    }
    

    redirect('pages/esic');
  }
  public function alterarStatus($id)
  {
    $request = $this->requests_model->select_id($id);
    $request_up = array(
      'status'=>$this->input->post('status'),
    );
    $this->requests_model->update($request_up,$id);

    $this->session->set_flashdata(array("success" => "Status do Pedido alterado com sucesso!"));
    redirect("admin/requests");
  }
  public function fechar_pedido($id)
  {
    $request = $this->requests_model->select_id($id);
    $request_up = array(
      'status'=>1,
    );
    $this->requests_model->update($request_up,$id);
    $subject_email = $settings['name']." - Seu pedido foi resolvido e fechado!";
    $msg = "Agradecemos sua interação! Sinta-se a vontade para fazer novas solicitações sempre que quiser.";
    $this->send_email($request['email'],$msg,$subject_email);
    $this->send_email($settings['email'],"O Pedido de informação de código:{$request['serial']}, foi fechado pelo(a) usuário(a)!","Pedido Resolvido e Fechado");
    $this->session->set_flashdata(array("success" => "Pedido resolvido e fechado com sucesso!"));
    redirect("pages/esic");
  }
  public function anwser_add($id)
  {
    $request = $this->requests_model->select_id($id);
    $settings = $this->settings_model->select_id();
    $answer = array(
      'description' => $this->input->post('description'),
      'user_id' => $this->session->userdata('user_on')['id'],
      'request_id' => $id,
      'anexo' => $this->do_upload_anexo(),
      'created' => date('Y-m-d H:i:s'),
    );
    $this->answers_model->insert($answer);
    $request_up = array(
      'status'=>1,
    );
    $this->requests_model->update($request_up,$id);
    $subject_email = $settings['name']." - Seu pedido foi respondido!";
    $msg = "{$answer['description']}<br><a href='".site_url($answer['anexo'])."' target='_blank'>Anexo</a>";
    $this->send_email($request['email'],$msg,$subject_email);
    $this->session->set_flashdata(array("success" => "Resposta enviada com sucesso!"));
    if ($this->session->userdata('user_on')) {
      redirect("admin/requests/details/{$id}");
    } else {
      redirect("pages/esic");
    }
  }
  public function anwser_add_front($id)
  {
    $request = $this->requests_model->select_id($id);
    $settings = $this->settings_model->select_id();
    $answer = array(
      'description' => $this->input->post('description'),
      'request_id' => $id,
      'created' => date('Y-m-d H:i:s'),
    );
    $this->answers_model->insert($answer);
    $settings = $this->settings_model->select_id();
    $subject_email = $settings['name']." - Resposta Enviada!";
    $msg = "Sua resposta foi enviada com sucesso! Aguarde o próximo contato.";
    $this->send_email($request['email'],$msg,$subject_email);
    $this->send_email($settings['email'],$msg,$subject_email);
    $this->session->set_flashdata(array("success" => "Resposta enviada com sucesso!"));
    redirect("pages/esic");
  }
  public function consultar()
  {
    if ($this->input->get()) {
      $cpf = $this->input->get('cpf');
      $serial = $this->input->get('serial');
    }
    $request = $this->requests_model->select_consult($cpf,$serial);
    echo json_encode($request);
  }
  public function consultar_typerequests($id)
  {
    $typerequest = $this->typerequests_model->select($id);
    echo json_encode($typerequest);
  }
  public function consultar_answers($id)
  {
    $answer = $this->answers_model->select_for_request($id);
    echo json_encode($answer);
  }
  public function details($id)
  {
    auth();
    $request = $this->requests_model->select_id($id);
    $answers = $this->answers_model->select($id);
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $settings = $this->settings_model->select_id();


    $dados = array(
      'page_title' => 'Grupos',
      'page_subtitle' => 'Detalhes',
      'request' => $request,
      'answers' => $answers,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/requests/details', $dados);
  }
  //
  public function remove($id)
  {
    // $report = $this->requests_model->select_id($id);
    // $this->requests_model->delete($id);
    // unlink("./{$report['anexo']}");
    // $this->session->set_flashdata(array("success" => "Relatório removido com sucesso!"));
    // redirect('requests');
  }

  public function do_upload_anexo()
  {
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = 'pdf|jpg|png|jpeg';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 6400;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      // $this->session->set_flashdata(array("danger" => $error['error']));
      return null;
      redirect("requests");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  function send_email($to,$message,$subject)
  {
    $settings = $this->settings_model->select_id();
    $this->load->library('email');
    $this->email->set_mailtype("html");
    $this->email->from($settings['email'], 'Ouvidoria');
    $this->email->to($to);

    $this->email->subject($subject);
    $this->email->message($message);

    $this->email->send();

  }
}
