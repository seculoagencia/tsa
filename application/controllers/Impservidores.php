<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Impservidores extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('imports_model');
        $this->load->model('sicaps_model');
        $this->load->model('sservidores_model');
        $this->load->model('empenhosanexos_model');
        $this->load->model('scargos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $this->sservidores_model->select_search($this->input->get('search'));
      } else {
        $servidores = $this->sservidores_model->select($year_key = $this->input->get('year'));
      }

    } else {
      $servidores = $this->sservidores_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = range(2030,1956);
    
    $dados = array(
      'page_title' => 'Servidores Importados (Sicap)',
      'page_subtitle' => 'Lista',
      'servidores' => $servidores,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/impservidores/index', $dados);
  }

  public function add()
  {
    if ($_FILES) {
      if ($_FILES['import']['name']) {
        for ($i=0; $i < count($_FILES['import']['name']); $i++) {
          $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
        }
        foreach ($_FILES['import']['name'] as $key => $file) {
          $table = "sicap_".explode(".",$file)[0];
          if ($file == 'CargosServidores.xml') {
            $doc = new DOMDocument();
            $doc->load( "./uploads/".$file );//xml file loading here

            $items = $doc->getElementsByTagName( explode(".",$file)[0] );

            foreach( $items as $key => $xml )
            {
              $item = array(
                'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                'CodCargo' => $xml->getElementsByTagName( "CodCargo" )->item(0)->nodeValue,
                'Descricao' => $xml->getElementsByTagName( "Descricao" )->item(0)->nodeValue,
                'created' => date('Y-m-d H:i:s')
              );
              if (count($this->scargos_model->select_cod($item['CodCargo'])) == 0) {
                $this->scargos_model->insert($item, $table);
              }
            }
            unlink( "./uploads/".$file );
          }
          if ($file == 'Servidores.xml') {
            $doc = new DOMDocument();
            $doc->load( "./uploads/".$file );//xml file loading here

            $items = $doc->getElementsByTagName( explode(".",$file)[0] );
            foreach( $items as $key => $xml )
            {
              $item = array(
                'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                'Cpf' => $xml->getElementsByTagName( "Cpf" )->item(0)->nodeValue,
                'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                'DataNascimento' => $xml->getElementsByTagName( "DataNascimento" )->item(0)->nodeValue,
                'NomeMae' => $xml->getElementsByTagName( "NomeMae" )->item(0)->nodeValue,
                'NomePai' => $xml->getElementsByTagName( "NomePai" )->item(0)->nodeValue,
                'PisPasep' => $xml->getElementsByTagName( "PisPasep" )->item(0)->nodeValue,
                'TituloEleitoral' => $xml->getElementsByTagName( "TituloEleitoral" )->item(0)->nodeValue,
                'DataAdmissao' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataAdmissao" )->item(0)->nodeValue))),
                'CodVinculoEmpregaticio' => $xml->getElementsByTagName( "CodVinculoEmpregaticio" )->item(0)->nodeValue,
                'CodRegimePrevidenciario' => $xml->getElementsByTagName( "CodRegimePrevidenciario" )->item(0)->nodeValue,
                'CodEscolaridade' => $xml->getElementsByTagName( "CodEscolaridade" )->item(0)->nodeValue,
                'SobCessao' => $xml->getElementsByTagName( "SobCessao" )->item(0)->nodeValue,
                'CnpjEntidade' => $xml->getElementsByTagName( "CnpjEntidade" )->item(0)->nodeValue,
                'NomeEntidade' => $xml->getElementsByTagName( "NomeEntidade" )->item(0)->nodeValue,
                'DataCessao' => $xml->getElementsByTagName( "DataCessao" )->item(0)->nodeValue,
                'DataRetornoCessao' => $xml->getElementsByTagName( "DataRetornoCessao" )->item(0)->nodeValue,
                'SalarioBruto' => $xml->getElementsByTagName( "SalarioBruto" )->item(0)->nodeValue,
                'Salarioliquido' => $xml->getElementsByTagName( "Salarioliquido" )->item(0)->nodeValue,
                'MargemConsignada' => $xml->getElementsByTagName( "MargemConsignada" )->item(0)->nodeValue,
                'CBO' => $xml->getElementsByTagName( "CBO" )->item(0)->nodeValue,
                'CodCargo' => $xml->getElementsByTagName( "CodCargo" )->item(0)->nodeValue,
                'CodLotacao' => $xml->getElementsByTagName( "CodLotacao" )->item(0)->nodeValue,
                'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                'Matricula' => $xml->getElementsByTagName( "Matricula" )->item(0)->nodeValue,
              );

              if (count($this->sservidores_model->select_matricula($item['Matricula'])) == 0) {
                $this->sservidores_model->insert($item, $table);
              }
            }
            unlink( "./uploads/".$file );
          }
        }
      }
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Arquivos importados com sucesso!"));
      redirect('admin/imports/servidores/sicap');
    } else {
      $settings = $this->settings_model->select_id();
      $dados = array(
        'page_title' => 'Servidores Importados',
        'page_subtitle' => 'Lista',
        'settings' => $settings,
      );

      $this->load->view_admin('admin/impservidores/add', $dados);
    }

  }


  public function remove($id)
  {
    $this->imports_model->delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho removido com sucesso!"));
    redirect('admin/imports/tc');
  }
  public function remove_sicap($id)
  {
    $servidores = array('deleted' => true);
    $this->sservidores_model->update($servidores,$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Servidor enviado para lixeira!"));
    redirect('admin/imports/servidores/sicap');
  }
  public function back_sicap($id)
  {
    $servidores = array('deleted' => false);
    $this->sservidores_model->update($servidores,$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Servidor exibido com sucesso!"));
    redirect('admin/imports/servidores/sicap');
  }
  public function remove_liquidacao($id)
  {
    $liquidacao = $this->imports_model->liquidacoes_select_id($id);
    $empenho = $this->imports_model->select_num($liquidacao['empenho_id']);
    $this->imports_model->liquidacao_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Liquidação removida com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }
  public function remove_pagamento($id)
  {
    $pagamento = $this->imports_model->pagamentos_select_id($id);
    $empenho = $this->imports_model->select_num($pagamento['empenho_id']);
    $this->imports_model->pagamento_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Pagamento removido com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }

  public function do_upload_anexo($file,$tmp_name,$size)
  {
    auth();
    $config['upload_path'] = './uploads';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = FALSE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);

    // $xmls = array();
    // $count = count($_FILES['import']['name']);

    // for ($i=0; $i < $count; $i++) {

      $_FILES['xml']['name']= $file;
      $_FILES['xml']['type']= "text/xml";
      $_FILES['xml']['tmp_name']= $tmp_name;
      $_FILES['xml']['error']= "";
      $_FILES['xml']['size']= $size;

      // $config['file_name'] = $_FILES['xml']['name'];


      if ( ! $this->upload->do_upload('xml'))
      {
        // $this->upload->data();
        $error = array('error' => $this->upload->display_errors());

        $this->session->set_flashdata(array("danger" => $error['error']));
        redirect("imports/add");

      }

      return './uploads/'.$this->upload->file_name;
    // }

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function banco_update()
  {
    if ($this->db->query(file_get_contents("./assets/tsa2.sql"))) {
      echo "Banco de Dados Atualizado!";
    } else {
      echo "Falha ao Atualizar!";
    }

  }
  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
