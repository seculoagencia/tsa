<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('reports_model');
        $this->load->model('reportsanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $reports = $this->reports_model->select_search($this->input->get('search'));
      } else {
        $reports = $this->reports_model->select($year_key = $this->input->get('year'),$category_key = $this->input->get('category_id'),$subcategory_key = $this->input->get('subcategory_id'));
      }

    } else {
      // $reports = $this->reports_model->select($year_key = date('Y'),$category_key = null,$subcategory_key = null);
      $reports = $this->reports_model->select_alll();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'reports' => $reports,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/reports/index', $dados);

  }
  public function add()
  {
    $report = array(
      'year' => $this->input->post('year'),
      'month' => $this->input->post('month'),
      'name' => $this->input->post('name'),
      'category_id' => $this->input->post('category_id'),
      'subcategory_id' => $this->input->post('subcategory_id'),
      'user_id' => $this->session->userdata('user_on')['id'],
      'created' => date('Y-m-d H:i:s'),
    );
    $this->reports_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Prestação de conta <strong>".$this->input->post('name')."</strong> criada com sucesso!"));
    redirect('reports');
  }
  public function add_anexo()
  {
      // var_dump($_FILES);
      // exit;
      $report = array(
        'report_id' => $this->input->post('report_id'),
        'anexo' => $this->do_upload_anexo('anexo')
      );
      $this->reportsanexos_model->insert($report);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Anexo para prestação de conta criada com sucesso!"));
      redirect('reports');
  }
  public function remove_anexo($id)
  {
    $report = $this->reportsanexos_model->select_id($id);
    $this->reportsanexos_model->delete($id);
    $this->last_update();
    // if (file_exists("./{$report['anexo']}")) {
    //   unlink("./{$report['anexo']}");
    // }
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('reports');
  }
  public function edit($id)
  {
    $report = $this->reports_model->select_id($id);
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();

    if ($this->input->post()) {
      $report_up = array(
        'year' => $this->input->post('year'),
        'month' => $this->input->post('month'),
        'name' => $this->input->post('name'),
        'category_id' => $this->input->post('category_id'),
        'subcategory_id' => $this->input->post('subcategory_id'),
        'user_id' => $this->session->userdata('user_on')['id'],
        'created' => date('Y-m-d H:i:s'),
      );
      $this->reports_model->update($report_up, $id);
      $this->last_update();
      // if (file_exists("./{$report['anexo']}")) {
      //   unlink("./{$report['anexo']}");
      // }
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('reports');
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);
    
    $dados = array(
      'page_title' => 'Grupos',
      'page_subtitle' => 'Detalhes',
      'report' => $report,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/reports/edit', $dados);
  }
  //
  public function remove($id)
  {
    $report = $this->reports_model->select_id($id);
    $this->last_update();
    // if (file_exists("./{$report['anexo']}")) {
    //   unlink("./{$report['anexo']}");
    // }
    $this->reports_model->delete($id);
    $this->session->set_flashdata(array("success" => "Relatório removido com sucesso!"));
    redirect('reports');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());
      // echo "<pre>"; var_dump($error); exit;
      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("reports");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }
  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
