<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Impreceitas extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('receitas_model');
        $this->load->model('receitasanexos_model');
        $this->load->model('imports_model');
        $this->load->model('sicaps_model');
        $this->load->model('tcreceitas_model');
        $this->load->model('screceitas_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $receitas = $this->tcreceitas_model->select_search($this->input->get('search'));
      } else {
        $receitas = $this->tcreceitas_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $receitas = $this->tcreceitas_model->select($year_key = date('Y'),$month_key = date('n'));
      $receitas = $this->tcreceitas_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'receitas' => $receitas,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/receitas2/index', $dados);

  }

  public function index2()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $receitas = $this->screceitas_model->select_search($this->input->get('search'));
      } else {
        $receitas = $this->screceitas_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $receitas = $this->screceitas_model->select($year_key = date('Y'),$month_key = date('n'));
      $receitas = $this->screceitas_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = array(2022,2021,2020,2019,
      2018,
      2017,
      2016,
      2015,
      2014,
      2013,
    );

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'receitas' => $receitas,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/receitas2/index2', $dados);

  }

  public function add()
  {
      $receita = array(
        'codigo' => $this->input->post('codigo'),
        'descricao' => $this->input->post('descricao'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'created' => date('Y-m-d H:i:s'),
      );
      $this->receitas_model->insert($receita);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Receita criada com sucesso!"));
      redirect('admin/receitas');

  }
  public function add_anexo($id)
  {
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'report_id' => $id,
      'anexo' => $this->do_upload_anexo()
    );
    $this->receitasanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('receitas');
  }
  public function remove_anexo($id)
  {
    $report = $this->receitasanexos_model->select_id($id);
    $this->receitasanexos_model->delete($id);
    $this->last_update();
    // unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('receitas');
  }
  public function edit($id)
  {
    $receita = $this->receitas_model->select_num($id);

    if ($this->input->post()) {
      if (empty($_FILES['anexo']['name'])) {
        $anexo = $receita['anexo'];
      } else {
        $anexo = $this->do_upload_anexo();
      }
      $receita_up = array(
        'codigo' => $this->input->post('codigo'),
        'descricao' => $this->input->post('descricao'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'anexo' => $anexo,
      );
      $this->receitas_model->update($receita_up, $id);
      $this->last_update();
      // if (!empty($_FILES['anexo']['name'])) {
      //   unlink("./{$receita['anexo']}");
      // }
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/receitas');
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'receitas',
      'page_subtitle' => 'Editar',
      'receita' => $receita,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/receitas/edit', $dados);
  }
  public function details($num)
  {
    $receita = $this->receitas_model->select_id($num);
    $liquidacoes = $this->receitas_model->liquidacoes_select($receita['id']);
    $pagamentos = $this->receitas_model->pagamentos_select($receita['id']);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'receitas',
      'page_subtitle' => 'Detalhes',
      'receita' => $receita,
      'liquidacoes' => $liquidacoes,
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/receitas/details', $dados);
  }
  //
  public function remove($id)
  {
    $receita = $this->receitas_model->select_id($id);
    $this->receitas_model->delete($id);
    $this->last_update();
    // unlink("./{$receita['anexo']}");
    $this->session->set_flashdata(array("success" => "Receita removida com sucesso!"));
    redirect('receitas');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("receitas/add");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
