<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class my404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('settings_model');

    }

    public function index()
    {
        $this->output->set_status_header('404');
        $data['content'] = 'error_404'; // View name
        $settings = $this->settings_model->select_id();
        $data = array(
          'page_title' => ' ',
          'page_subtitle' => ' ',
          'settings' => $settings,
        );
        $this->load->view_admin('error_404',$data);//loading in my template
    }
}
