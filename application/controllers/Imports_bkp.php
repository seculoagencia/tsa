<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imports extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('imports_model');
        $this->load->model('sicaps_model');
        $this->load->model('empenhosanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->imports_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->imports_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->imports_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->imports_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = array(2022,2021,2020,2019,
      2018,
      2017,
      2016,
      2015,
      2014,
      2013,
    );
    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/index', $dados);
  }

  public function index2()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->imports_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->imports_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->imports_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->sicaps_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = array(2022,2021,2020,2019,
      2018,
      2017,
      2016,
      2015,
      2014,
      2013,
    );
    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/index2', $dados);
  }

  public function add()
  {
    if ($this->input->post()) {
      if ($this->input->post('type') == "tc") {
        if ($_FILES['import']['name']) {
          for ($i=0; $i < count($_FILES['import']['name']); $i++) {
            $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
          }
          foreach ($_FILES['import']['name'] as $key => $file) {
            $table = "tc_".explode(".",$file)[0];
            if ($file == 'empenho.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'Numero' => $xml->getElementsByTagName( "Numero" )->item(0)->nodeValue,
                  'Processo' => $xml->getElementsByTagName( "Processo" )->item(0)->nodeValue,
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'AcaoCodigo' => $xml->getElementsByTagName( "AcaoCodigo" )->item(0)->nodeValue,
                  'FonteCodigo' => $xml->getElementsByTagName( "FonteCodigo" )->item(0)->nodeValue,
                  'RubricaCodigo' => $xml->getElementsByTagName( "RubricaCodigo" )->item(0)->nodeValue,
                  'CPF' => $xml->getElementsByTagName( "CPF" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                  'ValorEmpAteAgora' => $xml->getElementsByTagName( "ValorEmpAteAgora" )->item(0)->nodeValue,
                  'ValorLiqAteAgora' => $xml->getElementsByTagName( "ValorLiqAteAgora" )->item(0)->nodeValue,
                  'ValorPagAteAgora' => $xml->getElementsByTagName( "ValorPagAteAgora" )->item(0)->nodeValue,
                );

                if (count($this->imports_model->select_id($item['Numero'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'acao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                  'funcaocodigo' => $xml->getElementsByTagName( "funcaocodigo" )->item(0)->nodeValue,
                  'subfuncaocodigo' => $xml->getElementsByTagName( "subfuncaocodigo" )->item(0)->nodeValue,
                  'programacodigo' => $xml->getElementsByTagName( "programacodigo" )->item(0)->nodeValue,
                  'unidadecodigo' => $xml->getElementsByTagName( "unidadecodigo" )->item(0)->nodeValue,
                  'metafinanceiraano01' => $xml->getElementsByTagName( "metafinanceiraano01" )->item(0)->nodeValue,
                  'metafinanceiraano02' => $xml->getElementsByTagName( "metafinanceiraano02" )->item(0)->nodeValue,
                  'metafinanceiraano03' => $xml->getElementsByTagName( "metafinanceiraano03" )->item(0)->nodeValue,
                  'metafinanceiraano04' => $xml->getElementsByTagName( "metafinanceiraano04" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->acao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'funcao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->funcao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'subfuncao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );

                if (count($this->imports_model->subfuncao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'programa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->programa($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'unidade.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->unidade($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'fonte.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->fonte($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'rubrica.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                  'tipo' => $xml->getElementsByTagName( "tipo" )->item(0)->nodeValue,
                  'analiticasintetica' => $xml->getElementsByTagName( "analiticasintetica" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->rubrica($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'credor.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CPF' => $xml->getElementsByTagName( "CPF" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->credor($item['CPF'], $item['CNPJ'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'dotacao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'acaocodigo' => $xml->getElementsByTagName( "acaocodigo" )->item(0)->nodeValue,
                  'fontecodigo' => $xml->getElementsByTagName( "fontecodigo" )->item(0)->nodeValue,
                  'rubricacodigo' => $xml->getElementsByTagName( "rubricacodigo" )->item(0)->nodeValue,
                  'valororcado' => $xml->getElementsByTagName( "valororcado" )->item(0)->nodeValue,
                  'valoratualizado' => $xml->getElementsByTagName( "valoratualizado" )->item(0)->nodeValue,
                  'valorempateagora' => $xml->getElementsByTagName( "valorempateagora" )->item(0)->nodeValue,
                  'valorliqateagora' => $xml->getElementsByTagName( "valorliqateagora" )->item(0)->nodeValue,
                  'valorpagateagora' => $xml->getElementsByTagName( "valorpagateagora" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->dotacao($item['rubricacodigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'receitaclassificada.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( $table );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'rubricacodigo' => $xml->getElementsByTagName( "rubricacodigo" )->item(0)->nodeValue,
                  'valororcado' => $xml->getElementsByTagName( "valororcado" )->item(0)->nodeValue,
                  'valorarrecadado' => $xml->getElementsByTagName( "valorarrecadado" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->receitaclassificada($item['rubricacodigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'receitalancada.xml') {
              unlink( "./uploads/".$file );
            }
          }
        }
      }
      if ($this->input->post('type') == "sicap") {
        if ($_FILES['import']['name']) {
          for ($i=0; $i < count($_FILES['import']['name']); $i++) {
            $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
          }
          foreach ($_FILES['import']['name'] as $key => $file) {
            $table = "sicap_".explode(".",$file)[0];
            if ($file == 'Credor.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );

              foreach( $items as $key => $xml )
              {
                if (count($xml->getElementsByTagName( "NumerodoRegistro" )["length"]) > 0) {
                  $numerodoRegistro = $xml->getElementsByTagName( "NumerodoRegistro" )->item(0)->nodeValue;
                }
                if (count($xml->getElementsByTagName( "Registro" )["length"]) > 0) {
                  $numerodoRegistro = $xml->getElementsByTagName( "Registro" )->item(0)->nodeValue;
                }
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'InscricaoEstadual' => $xml->getElementsByTagName( "InscricaoEstadual" )->item(0)->nodeValue,
                  'InscricaoMunicipal' => $xml->getElementsByTagName( "InscricaoMunicipal" )->item(0)->nodeValue,
                  'Endereco' => $xml->getElementsByTagName( "Endereco" )->item(0)->nodeValue,
                  'Cidade' => $xml->getElementsByTagName( "Cidade" )->item(0)->nodeValue,
                  'UF' => $xml->getElementsByTagName( "UF" )->item(0)->nodeValue,
                  'Cep' => $xml->getElementsByTagName( "Cep" )->item(0)->nodeValue,
                  'Fone' => $xml->getElementsByTagName( "Fone" )->item(0)->nodeValue,
                  'Fax' => $xml->getElementsByTagName( "Fax" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                  'NumerodoRegistro' => $numerodoRegistro,
                  'TipoRegistro' => $xml->getElementsByTagName( "TipoRegistro" )->item(0)->nodeValue,
                );
                if (count($this->sicaps_model->getVerifica($table, 'CodCredor', $item['CodCredor'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'DecretoAltOrcamentaria.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if (count($xml->getElementsByTagName( "CodRubrica" )["length"]) > 0) {
                  $codRubrica = $xml->getElementsByTagName( "CodRubrica" )->item(0)->nodeValue;
                }
                if (count($xml->getElementsByTagName( "CodContaDespesa" )["length"]) > 0) {
                  $codRubrica = $xml->getElementsByTagName( "CodContaDespesa" )->item(0)->nodeValue;
                }

                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                  'CodSubFuncao' => $xml->getElementsByTagName( "CodSubFuncao" )->item(0)->nodeValue,
                  'CodPrograma' => $xml->getElementsByTagName( "CodPrograma" )->item(0)->nodeValue,
                  'CodProjAtividade' => $xml->getElementsByTagName( "CodProjAtividade" )->item(0)->nodeValue,
                  'CodRubrica' => $codRubrica,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'NumDocumento' => $xml->getElementsByTagName( "NumDocumento" )->item(0)->nodeValue,
                  'DataDocumento' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataDocumento" )->item(0)->nodeValue))),
                  'DataAlteracao' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataAlteracao" )->item(0)->nodeValue))),
                  'NumAlteracao' => $xml->getElementsByTagName( "NumAlteracao" )->item(0)->nodeValue,
                  'TipoAlteracao' => $xml->getElementsByTagName( "TipoAlteracao" )->item(0)->nodeValue,
                  'ValorAlteracao' => number_format($xml->getElementsByTagName( "ValorAlteracao" )->item(0)->nodeValue, 2,',','.'),
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumAlteracao',$item['NumAlteracao'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Empenho.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'Valor' => number_format($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue, 2,',','.'),
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                  'CodSubFuncao' => $xml->getElementsByTagName( "CodSubFuncao" )->item(0)->nodeValue,
                  'CodPrograma' => $xml->getElementsByTagName( "CodPrograma" )->item(0)->nodeValue,
                  'CodProjAtividade' => $xml->getElementsByTagName( "CodProjAtividade" )->item(0)->nodeValue,
                  'CodRubrica' => $xml->getElementsByTagName( "CodRubrica" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'ContraPartida' => $xml->getElementsByTagName( "ContraPartida" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                  'ModalLicita' => $xml->getElementsByTagName( "ModalLicita" )->item(0)->nodeValue,
                  'CaracPeculiar' => $xml->getElementsByTagName( "CaracPeculiar" )->item(0)->nodeValue,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                  'NumContrato' => $xml->getElementsByTagName( "NumContrato" )->item(0)->nodeValue,
                  'DataContrato' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataContrato" )->item(0)->nodeValue))),
                  'NumConvenio' => $xml->getElementsByTagName( "NumConvenio" )->item(0)->nodeValue,
                  'NumObra' => $xml->getElementsByTagName( "NumObra" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->select_id($item['NumEmpenho'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Funcao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodFuncao', $item['CodFuncao'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'InfoRemessa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'TipoEnvio' => $xml->getElementsByTagName( "TipoEnvio" )->item(0)->nodeValue,
                  'DataInicio' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataInicio" )->item(0)->nodeValue))),
                  'DataFim' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataFim" )->item(0)->nodeValue))),
                  'DataGeracao' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataGeracao" )->item(0)->nodeValue))),
                );

                if (count($this->sicaps_model->getVerifica($table, 'TipoEnvio',$item['TipoEnvio'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Liquidacao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'Valor' => number_format($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue, 2,',','.'),
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                  'CodOperacao' => $xml->getElementsByTagName( "CodOperacao" )->item(0)->nodeValue,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'Referencia' => $xml->getElementsByTagName( "Referencia" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumLiquidacao', $item['NumLiquidacao'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Orgao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table,'CodOrgao', $item['CodOrgao'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Pagamento.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumPagamento' => $xml->getElementsByTagName( "NumPagamento" )->item(0)->nodeValue,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'Valor' => number_format($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue, 2,',','.'),
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                  'CodOperacao' => $xml->getElementsByTagName( "CodOperacao" )->item(0)->nodeValue,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'PagamentoFinanceiro.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumPagamento' => $xml->getElementsByTagName( "NumPagamento" )->item(0)->nodeValue,
                  'CodContaBalancete' => $xml->getElementsByTagName( "CodContaBalancete" )->item(0)->nodeValue,
                  'Valor' => number_format($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue, 2,',','.'),
                  'CodBanco' => number_format($xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue, 2,',','.'),
                  'CodAgenciaBanco' => $xml->getElementsByTagName( "CodAgenciaBanco" )->item(0)->nodeValue,
                  'NumContaCorrente' => $xml->getElementsByTagName( "NumContaCorrente" )->item(0)->nodeValue,
                  'NumCheque' => $xml->getElementsByTagName( "NumCheque" )->item(0)->nodeValue,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'TipoPagamento' => $xml->getElementsByTagName( "TipoPagamento" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Programa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodPrograma' => $xml->getElementsByTagName( "CodPrograma" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Objetivo' => $xml->getElementsByTagName( "Objetivo" )->item(0)->nodeValue,
                  'PublicoAlvo' => $xml->getElementsByTagName( "PublicoAlvo" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodPrograma', $item['CodPrograma'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'ProjAtividade.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodProjAtividade' => $xml->getElementsByTagName( "CodProjAtividade" )->item(0)->nodeValue,
                  'Identificador' => $xml->getElementsByTagName( "Identificador" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodProjAtividade', $item['CodProjAtividade'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RecursoVinculado.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Finalidade' => $xml->getElementsByTagName( "Finalidade" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodRecVinculado', $item['CodRecVinculado'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RubricaDespesa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodRubrica' => $xml->getElementsByTagName( "CodRubrica" )->item(0)->nodeValue,
                  'Especificacao' => $xml->getElementsByTagName( "Especificacao" )->item(0)->nodeValue,
                  'TipoNivelConta' => $xml->getElementsByTagName( "TipoNivelConta" )->item(0)->nodeValue,
                  'NumNivelConta' => $xml->getElementsByTagName( "NumNivelConta" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodRubrica', $item['CodRubrica'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'SubFuncao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodSubFuncao' => $xml->getElementsByTagName( "CodSubFuncao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodSubFuncao', $item['CodSubFuncao'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'UndOrcamentaria.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Identificador' => $xml->getElementsByTagName( "Identificador" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodUndOrcamentaria', $item['CodUndOrcamentaria'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'ContaDisponibilidade.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodContaBalancete' => $xml->getElementsByTagName( "CodContaBalancete" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'CodBanco' => $xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue,
                  'CodAgenciaBanco' => $xml->getElementsByTagName( "CodAgenciaBanco" )->item(0)->nodeValue,
                  'NumContaCorrente' => $xml->getElementsByTagName( "NumContaCorrente" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                  'Classificacao' => $xml->getElementsByTagName( "Classificacao" )->item(0)->nodeValue,
                );

                // if (count($this->imports_model->select_id($item['CodUndOrcamentaria'])) == 0) {
                  $this->imports_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'LoaReceita.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'CodContaBalancete' => $xml->getElementsByTagName( "CodContaBalancete" )->item(0)->nodeValue,
                  'ValorReceitaOrcada' => number_format($xml->getElementsByTagName( "ValorReceitaOrcada" )->item(0)->nodeValue, 2,',','.'),
                  'Descricao' => $xml->getElementsByTagName( "Descricao" )->item(0)->nodeValue,
                  'TipoNivelConta' => $xml->getElementsByTagName( "TipoNivelConta" )->item(0)->nodeValue,
                  'NumNivelConta' => $xml->getElementsByTagName( "NumNivelConta" )->item(0)->nodeValue,
                  'MetaArrecadacao1Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao1Bimestre" )->item(0)->nodeValue,
                  'MetaArrecadacao2Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao2Bimestre" )->item(0)->nodeValue,
                  'MetaArrecadacao3Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao3Bimestre" )->item(0)->nodeValue,
                  'MetaArrecadacao4Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao4Bimestre" )->item(0)->nodeValue,
                  'MetaArrecadacao5Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao5Bimestre" )->item(0)->nodeValue,
                  'MetaArrecadacao6Bimestre' => $xml->getElementsByTagName( "MetaArrecadacao6Bimestre" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodContaReceita', $item['CodContaReceita'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'ReceitaArrecadada.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodContaReceita' => $xml->getElementsByTagName( "CodContaReceita" )->item(0)->nodeValue,
                  'CodBanco' => number_format($xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue, 2,',','.'),
                  'CodAgencia' => $xml->getElementsByTagName( "CodAgencia" )->item(0)->nodeValue,
                  'NumConta' => $xml->getElementsByTagName( "NumConta" )->item(0)->nodeValue,
                  'DataArrecadacao' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataArrecadacao" )->item(0)->nodeValue))),
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'Valor' => number_format($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue, 2,',','.'),
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                );

                // if (count($this->imports_model->select_id($item['NumPagamento'])) == 0) {
                  $this->imports_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
          }
        }
      }
      $this->session->set_flashdata(array("success" => "Arquivos importados com sucesso!"));
      redirect('admin/imports/add');
    } else {
      $settings = $this->settings_model->select_id();
      $dados = array(
        'page_title' => 'Prestação de Contas',
        'page_subtitle' => 'Lista',
        'settings' => $settings,
      );

      $this->load->view_admin('admin/empenhos2/add', $dados);
    }

  }
  public function add_anexo($id)
  {
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'report_id' => $id,
      'anexo' => $this->do_upload_anexo()
    );
    $this->empenhosanexos_model->insert($report);
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('empenhos');
  }
  public function remove_anexo($id)
  {
    $report = $this->empenhosanexos_model->select_id($id);
    $this->empenhosanexos_model->delete($id);
    unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('empenhos');
  }
  public function liquidacao_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->imports_model->select_num($empenho_id);
      $liquidacao = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      $this->imports_model->liquidacao_insert($liquidacao);
      $this->session->set_flashdata(array("success" => "Liquidação inserida com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function pagamento_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->imports_model->select_num($empenho_id);
      $pagamento = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      // echo $empenho['numero'];
      // exit;
      $this->imports_model->pagamento_insert($pagamento);
      $this->session->set_flashdata(array("success" => "Pagamento inserido com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function edit1($id)
  {
    $empenho = $this->imports_model->select_num($id);

    if ($this->input->post()) {
      if (empty($_FILES['anexo']['name'])) {
        $anexo = $empenho['anexo'];
      } else {
        $anexo = $this->do_upload_anexo();
      }
      $empenho_up = array(
        'numero' => $this->input->post('numero'),
        'credor' => $this->input->post('credor'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'unidade_orcamentaria' => $this->input->post('unidade_orcamentaria'),
        'funcao' => $this->input->post('funcao'),
        'subfuncao' => $this->input->post('subfuncao'),
        'programa' => $this->input->post('programa'),
        'acao' => $this->input->post('acao'),
        'fonte_de_recurso' => $this->input->post('fonte_de_recurso'),
        'elemento_de_despesa' => $this->input->post('elemento_de_despesa'),
        'cpf_cnpj' => $this->input->post('cpf_cnpj'),
        'numero_do_processo' => $this->input->post('numero_do_processo'),
        'historico' => $this->input->post('historico'),
        'anexo' => $anexo,
      );
      $this->imports_model->update($empenho_up, $id);
      if (!empty($_FILES['anexo']['name'])) {
        unlink("./{$empenho['anexo']}");
      }
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Editar',
      'empenho' => $empenho,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/edit', $dados);
  }
  public function liquidacao_edit($id)
  {
    if ($this->input->post()) {
      $liquidacao = $this->imports_model->liquidacoes_select_id($id);
      $empenho = $this->imports_model->select_num($liquidacao['empenho_id']);
      $liquidacaoUp = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->imports_model->liquidacao_update($liquidacaoUp, $id);
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function pagamento_edit($id)
  {
    if ($this->input->post()) {
      $pagamento = $this->imports_model->pagamentos_select_id($id);
      $empenho = $this->imports_model->select_num($pagamento['empenho_id']);
      $pagamentoUp = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->imports_model->pagamento_update($pagamentoUp, $id);
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function details($num)
  {
    if ($this->uri->segment(3) == "tc") {
      $model = $this->imports_model;
      $view = "details";
      $field_emp = "id";
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->sicaps_model;
      $view = "details2";
      $field_emp = "NumEmpenho";
    }
    $empenho = $model->select_id($num);
    $liquidacoes = $model->liquidacoes_select($empenho[$field_emp]);
    $pagamentos = $model->pagamentos_select($empenho[$field_emp]);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Detalhes',
      'empenho' => $empenho,
      'liquidacoes' => $liquidacoes,
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin("admin/empenhos2/{$view}", $dados);

  }

  public function remove($id)
  {
    $this->imports_model->delete($id);
    $this->session->set_flashdata(array("success" => "Empenho removido com sucesso!"));
    redirect('admin/imports');
  }
  public function remove_liquidacao($id)
  {
    $liquidacao = $this->imports_model->liquidacoes_select_id($id);
    $empenho = $this->imports_model->select_num($liquidacao['empenho_id']);
    $this->imports_model->liquidacao_delete($id);
    $this->session->set_flashdata(array("success" => "Liquidação removida com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }
  public function remove_pagamento($id)
  {
    $pagamento = $this->imports_model->pagamentos_select_id($id);
    $empenho = $this->imports_model->select_num($pagamento['empenho_id']);
    $this->imports_model->pagamento_delete($id);
    $this->session->set_flashdata(array("success" => "Pagamento removido com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }

  public function do_upload_anexo($file,$tmp_name,$size)
  {
    auth();
    $config['upload_path'] = './uploads';
    $config['allowed_types'] = 'xml';
    $config['encrypt_name'] = FALSE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);

    // $xmls = array();
    // $count = count($_FILES['import']['name']);

    // for ($i=0; $i < $count; $i++) {

      $_FILES['xml']['name']= $file;
      $_FILES['xml']['type']= "text/xml";
      $_FILES['xml']['tmp_name']= $tmp_name;
      $_FILES['xml']['error']= "";
      $_FILES['xml']['size']= $size;

      // $config['file_name'] = $_FILES['xml']['name'];


      if ( ! $this->upload->do_upload('xml'))
      {
        // $this->upload->data();
        $error = array('error' => $this->upload->display_errors());

        $this->session->set_flashdata(array("danger" => $error['error']));
        redirect("imports/add");

      }

      return './uploads/'.$this->upload->file_name;
    // }

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function banco_update()
  {
    if ($this->db->query(file_get_contents("./assets/tsa2.sql"))) {
      echo "Banco de Dados Atualizado!";
    } else {
      echo "Falha ao Atualizar!";
    }

  }
}
