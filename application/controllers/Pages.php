<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->model('groups_model');
    $this->load->model('users_model');
    $this->load->model('categories_model');
    $this->load->model('subcategories_model');
    $this->load->model('reports_model');
    $this->load->model('requests_model');
    $this->load->model('typerequests_model');
    $this->load->model('settings_model');
    $this->load->model('empenhos_model');
    $this->load->model('imports_model');
    $this->load->model('sicaps_model');
    $this->load->model('licitacoes_model');
    $this->load->model('receitas_model');
    $this->load->model('tcreceitas_model');
    $this->load->model('screceitas_model');
    $this->load->model('servidores_model');
    $this->load->model('diarias_model');
    $this->load->model('cargos_model');
    $this->load->model('diariasanexos_model');
    $this->load->model('reportsanexos_model');
    $this->load->model('empenhosanexos_model');
    $this->load->model('servidoresanexos_model');
    $this->load->model('licitacoesanexos_model');
    $this->load->model('receitasanexos_model');
    $this->load->model('sservidores_model');
    $this->load->model('scargos_model');
    $this->load->model('verbas_model');
    $this->load->model('verbasanexos_model');
    $this->load->model('sagres_model');
    $this->load->library("pagination");

  }

  public function index()
  {
    $categories = $this->categories_model->select();
    $settings = $this->settings_model->select_id();

    $dados = array(
      'categories' => $categories,
      'settings' => $settings,
    );

    $this->load->view_site('pages/index', $dados);
  }

  public function empenhos()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->empenhos_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->empenhos_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->empenhos_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->empenhos_model->select_all();
    }


    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/empenhos', $dados);
  }

  public function empenhosxml()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->empenhos_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->empenhos_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->empenhos_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->empenhos_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
      $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Empenhos');

       foreach ($empenhos as $key => $empenho) {
         $xml->startBranch('Empenho', array('id' => $empenho['id']));

           $xml->addNode('numero', $empenho['numero']);
           $xml->addNode('credor', $empenho['credor']);
           $xml->addNode('data', $empenho['data']);
           $xml->addNode('unidade_orcamentaria', $empenho['unidade_orcamentaria']);
           $xml->addNode('funcao', $empenho['funcao']);
           $xml->addNode('subfuncao', $empenho['subfuncao']);
           $xml->addNode('programa', $empenho['programa']);
           $xml->addNode('acao', $empenho['acao']);
           $xml->addNode('fonte_de_recurso', $empenho['fonte_de_recurso']);
           $xml->addNode('elemento_de_despesa', $empenho['elemento_de_despesa']);
           $xml->addNode('cpf_cnpj', $empenho['cpf_cnpj']);
           $xml->addNode('numero_do_processo', $empenho['numero_do_processo']);
           $xml->addNode('historico', $empenho['historico']);
           $xml->addNode('created', $empenho['created']);

         $xml->endBranch();
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/empenhos/xml', $data);
  }

  public function empenhosxls()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->empenhos_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->empenhos_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->empenhos_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->empenhos_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
    		'id',
        'numero',
        'credor',
        'data',
        'unidade_orcamentaria',
        'funcao',
        'subfuncao',
        'programa',
        'acao',
        'fonte_de_recurso',
        'elemento_de_despesa',
        'cpf_cnpj',
        'numero_do_processo',
        'historico',
        'created'
    	);
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_empenhos';
    	$this->excel->make_from_array($titles, $empenhos);
  }

  public function imports()
  {
    $config = array();
    $config["base_url"] = base_url() . "/pages/imports/sicap/pagina";
    $config["per_page"] = 30;
    $config["uri_segment"] = 5;
    $config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = "<ul class='pagination'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tagl_close'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tagl_close'] = "</li>";
    $config['first_tag_open'] = "<li>";
    $config['first_tagl_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tagl_close'] = "</li>";
    $config['first_link'] = 'Primeiro';
    $config['last_link'] = 'Último';
    $config['next_link'] = '»';
    $config['prev_link'] = '«';
    
    $page = ($this->uri->segment(5)) ? $config["per_page"]*($this->uri->segment(5) - 1) : 0;

    if ($this->uri->segment(3) == "tc") {
      $model = $this->imports_model;
      $view = "imports";
      $field_emp = "id";
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->sicaps_model;
      $view = "imports2";
      $field_emp = "NumEmpenho";
    }
    if ($this->uri->segment(3) == "sagres") {
      $model = $this->sagres_model;
      $view = "imports3";
      $field_emp = "num_empenho";
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        if ($page > 0) {
          if ($this->uri->segment(3) == "sicap") {
            $config["base_url"] = base_url() . "/pages/imports/sicap/pagina?search=".$this->input->get('search');
          } else {
            $config["base_url"] = base_url() . "/pages/imports/sagres/pagina?search=".$this->input->get('search');
          }
        }
        $config["total_rows"] = $model->select_search_page_record_count($this->input->get('search'));
        $config['suffix'] = "?search=".$this->input->get('search');
        $empenhos = $model->select_search_page($this->input->get('search'), $config["per_page"], $page);
      } else {
        if ($page > 0) {
          if ($this->uri->segment(3) == "sicap") {
            $config["base_url"] = base_url() . "/pages/imports/sicap/pagina?year=".$this->input->get('year').'&month='.$this->input->get('month');
          } else {
            $config["base_url"] = base_url() . "/pages/imports/sagres/pagina?year=".$this->input->get('year').'&month='.$this->input->get('month');
          }
        }
        $config["total_rows"] = $model->select_page_record_count($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
        $config['suffix'] = "?year=".$this->input->get('year').'&month='.$this->input->get('month');
        $empenhos = $model->select_page($year_key = $this->input->get('year'),$month_key = $this->input->get('month'),$config["per_page"], $page);
      }

    } else {
      $config["total_rows"] = $model->select_all_no_deleted_page_record_count();
      // $empenhos = $model->select($year_key = date('Y'),$month_key = date('n'));
      // echo $config["total_rows"] - ($config["per_page"] * ($page - 1));
      // exit;
      $empenhos = $model->select_all_no_deleted_page($config["per_page"], $page);
    }
    $this->pagination->initialize($config);

    // $config["total_rows"] = count($empenhos);

    $pagination = $this->pagination->create_links();

    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
      'pagination' => $pagination,
    );

    $this->load->view_site("pages/{$view}", $dados);
  }

  public function des_extra_orcamentarias()
  {
    $extras = $this->sicaps_model->selectRecDespExtraOrcamentarias();
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Despesas Extra Orçamentária',
      'page_subtitle' => 'Lista',
      'extras' => $extras,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings
    );
    $this->load->view_site("pages/des_extra_orcamentarias", $dados);
  }

  public function importsxml()
  {
    if ($this->uri->segment(3) == "tc") {
      $model = $this->imports_model;
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->sicaps_model;
    }
    if ($this->uri->segment(3) == "sagres") {
      $model = $this->sagres_model;
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $model->select_search($this->input->get('search'));
      } else {
        $empenhos = $model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      $empenhos = $model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
      $municipio);

       $this->load->library('xml_writer');

       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       $xml->startBranch('Empenhos');

       foreach ($empenhos as $key => $empenho) {
         $xml->startBranch('Empenho', array('id' => $empenho['id']));

         if ($this->uri->segment(3) == "tc") {
           $xml->addNode('Exercicio', $empenho['Exercicio']);
           $xml->addNode('Numero', $empenho['Numero']);
           $xml->addNode('Processo', $empenho['Processo']);
           $xml->addNode('Data', $empenho['Data']);
           $xml->addNode('AcaoCodigo', $empenho['AcaoCodigo']);
           $xml->addNode('FonteCodigo', $empenho['FonteCodigo']);
           $xml->addNode('RubricaCodigo', $empenho['RubricaCodigo']);
           $xml->addNode('CPF', $empenho['CPF']);
           $xml->addNode('CNPJ', $empenho['CNPJ']);
           $xml->addNode('ValorEmpAteAgora', $empenho['ValorEmpAteAgora']);
           $xml->addNode('ValorLiqAteAgora', $empenho['ValorLiqAteAgora']);
           $xml->addNode('ValorPagAteAgora', $empenho['ValorPagAteAgora']);
         }
         if ($this->uri->segment(3) == "sicap") {
           $xml->addNode('CodUndGestora', $empenho['CodUndGestora']);
           $xml->addNode('CodigoUA', $empenho['CodigoUA']);
           $xml->addNode('Bimestre', $empenho['Bimestre']);
           $xml->addNode('Exercicio', $empenho['Exercicio']);
           $xml->addNode('NumEmpenho', $empenho['NumEmpenho']);
           $xml->addNode('DataEmpenho', $empenho['DataEmpenho']);
           $xml->addNode('Valor', $empenho['Valor']);
           $xml->addNode('Sinal', $empenho['Sinal']);
           $xml->addNode('Tipo', $empenho['Tipo']);
           $xml->addNode('CodOrgao', $empenho['CodOrgao']);
           $xml->addNode('CodUndOrcamentaria', $empenho['CodUndOrcamentaria']);
           $xml->addNode('CodFuncao', $empenho['CodFuncao']);
           $xml->addNode('CodSubFuncao', $empenho['CodSubFuncao']);
           $xml->addNode('CodPrograma', $empenho['CodPrograma']);
           $xml->addNode('CodProjAtividade', $empenho['CodProjAtividade']);
           $xml->addNode('CodContaDespesa', $empenho['CodContaDespesa']);
           $xml->addNode('CodContaContabil', $empenho['CodContaContabil']);
           $xml->addNode('CodRecVinculado', $empenho['CodRecVinculado']);
           $xml->addNode('ContraPartida', $empenho['ContraPartida']);
           $xml->addNode('CodCredor', $empenho['CodCredor']);
           $xml->addNode('ModalLicita', $empenho['ModalLicita']);
           $xml->addNode('RegistroDePreco', $empenho['RegistroDePreco']);
           $xml->addNode('ReferenciaLegal', $empenho['ReferenciaLegal']);
           $xml->addNode('NumProcesso', $empenho['NumProcesso']);
           $xml->addNode('DataProcesso', $empenho['DataProcesso']);
           $xml->addNode('NumContrato', $empenho['NumContrato']);
           $xml->addNode('DataContrato', $empenho['DataContrato']);
           $xml->addNode('NumConvenio', $empenho['NumConvenio']);
           $xml->addNode('DataConvenio', $empenho['DataConvenio']);
           $xml->addNode('NumObra', $empenho['NumObra']);
           $xml->addNode('Historico', $empenho['Historico']);
         }
         if ($this->uri->segment(3) == "sagres") {
           $xml->addNode('Ano', $empenho['ano']);
           $xml->addNode('CodUnidadeOrcamentaria', $empenho['cod_unid_orc']);
           $xml->addNode('Funcao', $empenho['funcao']);
           $xml->addNode('SubFuncao', $empenho['subfuncao']);
           $xml->addNode('Programa', $empenho['programa']);
           $xml->addNode('Acao', $empenho['acao']);
           $xml->addNode('IdAcao', $empenho['id_acao']);
           $xml->addNode('CodigoCategoriaEconomica', $empenho['cod_cat_econ']);
           $xml->addNode('CodigoNaturezaDespesa', $empenho['cod_nat_desp']);
           $xml->addNode('ModalidadeAplicacao', $empenho['modalidade_aplic']);
           $xml->addNode('CodigoElementoDespesaDotacao', $empenho['cod_elem_desp_dot']);
           $xml->addNode('SubElementoDespesa', $empenho['sub_elem_desp']);
           $xml->addNode('ModalidadeLicitacao', $empenho['modalidade_licit']);
           $xml->addNode('NumeroEmpenho', $empenho['num_empenho']);
           $xml->addNode('TipoEmpenho', $empenho['tipo_empenho']);
           $xml->addNode('DataEmissaoEmpenho', $empenho['data_emissao_empenho']);
           $xml->addNode('ValorEmpenhado', $empenho['valor_empenhado']);
           $xml->addNode('Historico', $empenho['historico']);
           $xml->addNode('CpfCnpjFornecedor', $empenho['cpf_cnpj_credor']);
           $xml->addNode('NumeroProcedimento', $empenho['num_procedimento']);
           $xml->addNode('FonteDeRecurso', $empenho['font_recurso']);
           $xml->addNode('CpfOrdenador', $empenho['cpf_ordenador']);
           $xml->addNode('CodigoElementoDespesa', $empenho['cod_elem_desp']);
         }

         $xml->endBranch();
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/empenhos/xml', $data);
  }

  public function importsxls()
  {
    if ($this->uri->segment(3) == "tc") {
      $model = $this->imports_model;
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->sicaps_model;
    }
    if ($this->uri->segment(3) == "sagres") {
      $model = $this->sagres_model;
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $model->select_search($this->input->get('search'));
      } else {
        $empenhos = $model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      $empenhos = $model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      if ($this->uri->segment(3) == "tc") {
        $titles = array(
          'id',
          'Exercicio',
          'Numero',
          'Processo',
          'Data',
          'AcaoCodigo',
          'FonteCodigo',
          'RubricaCodigo',
          'CPF',
          'CNPJ',
          'ValorEmpAteAgora',
          'ValorLiqAteAgora',
          'ValorPagAteAgora'
        );
      }
      if ($this->uri->segment(3) == "sicap") {
        $titles = array(
          'id',
          'CodUndGestora',
          'CodigoUA',
          'Bimestre',
          'Exercicio',
          'NumEmpenho',
          'DataEmpenho',
          'Valor',
          'Sinal',
          'Tipo',
          'CodOrgao',
          'CodUndOrcamentaria',
          'CodFuncao',
          'CodSubFuncao',
          'CodPrograma',
          'CodProjAtividade',
          'CodContaDespesa',
          'CodContaContabil',
          'CodRecVinculado',
          'ContraPartida',
          'CodCredor',
          'ModalLicita',
          'RegistroDePreco',
          'ReferenciaLegal',
          'NumProcesso',
          'DataProcesso',
          'NumContrato',
          'DataContrato',
          'NumConvenio',
          'DataConvenio',
          'NumObra',
          'Historico'
        );
      }
      if ($this->uri->segment(3) == "sagres") {
        $titles = array(
          'ID',
          'Ano',
          'CodUnidadeOrcamentaria',
          'Funcao',
          'SubFuncao',
          'Programa',
          'Acao',
          'IdAcao',
          'CodigoCategoriaEconomica',
          'CodigoNaturezaDespesa',
          'ModalidadeAplicacao',
          'CodigoElementoDespesaDotacao',
          'SubElementoDespesa',
          'ModalidadeLicitacao',
          'NumeroEmpenho',
          'TipoEmpenho',
          'DataEmissaoEmpenho',
          'ValorEmpenhado',
          'Historico',
          'CpfCnpjFornecedor',
          'NumeroProcedimento',
          'FonteDeRecurso',
          'CpfOrdenador',
          'CodigoElementoDespesa'
        );
      }
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_empenhos';
    	$this->excel->make_from_array($titles, $empenhos);
  }

  public function receitas()
  {
    if ($this->uri->segment(3) == "") {
      $model = $this->receitas_model;
      $view = "receitas";
      $field_emp = "id";
    }
    if ($this->uri->segment(3) == "tc") {
      $model = $this->tcreceitas_model;
      $view = "receitas2";
      $field_emp = "id";
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->screceitas_model;
      $view = "receitas3";
      $field_emp = "NumEmpenho";
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $receitas = $model->select_search($this->input->get('search'));
      } else {
        $receitas = $model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $receitas = $model->select($year_key = date('Y'),$month_key = date('n'));
      $receitas = $model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'receitas' => $receitas,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site("pages/{$view}", $dados);
  }

  public function impservidores()
  {
    $model = $this->sservidores_model;
    $view = "impservidores";
    $field_emp = "Matricula";


    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $model->select_search_not_delete($this->input->get('search'));
      } else {
        $servidores = $model->select_not_delete($year_key = $this->input->get('year'));
      }

    } else {
      // $receitas = $model->select($year_key = date('Y'),$month_key = date('n'));
      $servidores = $model->select_all_not_delete();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $menu = "servidores";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'servidores' => $servidores,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site("pages/{$view}", $dados);
  }

  public function receitasxml()
  {
    if ($this->uri->segment(3) == "") {
      $model = $this->receitas_model;
    }
    if ($this->uri->segment(3) == "tc") {
      $model = $this->tcreceitas_model;
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->screceitas_model;
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $receitas = $model->select_search($this->input->get('search'));
      } else {
        $receitas = $model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $receitas = $model->select($year_key = date('Y'),$month_key = date('n'));
      $receitas = $model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
      $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Receitas');

       foreach ($receitas as $key => $receita) {
         $xml->startBranch('Receita', array('id' => $receita['id']));
         if ($this->uri->segment(3) == "") {
           $xml->addNode('Codigo', $receita['codigo']);
           $xml->addNode('Descricao', $receita['descricao']);
           $xml->addNode('Data', $receita['data']);
           $xml->addNode('Valor', 'R$ '.number_format($receita['valor'],2,',','.'));
         }
         if ($this->uri->segment(3) == "tc") {
           $xml->addNode('exercicio', $receita['exercicio']);
           $xml->addNode('rubricacodigo', $receita['rubricacodigo']);
           $xml->addNode('valororcado', $receita['valororcado']);
           $xml->addNode('valorarrecadado', $receita['valorarrecadado']);
         }
         if ($this->uri->segment(3) == "sicap") {
           $xml->addNode('CodUndGestora', $receita['CodUndGestora']);
           $xml->addNode('CodigoUA', $receita['CodigoUA']);
           $xml->addNode('Bimestre', $receita['Bimestre']);
           $xml->addNode('Exercicio', $receita['Exercicio']);
           $xml->addNode('CodOrgao', $receita['CodOrgao']);
           $xml->addNode('CodUndOrcamentaria', $receita['CodUndOrcamentaria']);
           $xml->addNode('CodContaReceita', $receita['CodContaReceita']);
           $xml->addNode('CodBanco', $receita['CodBanco']);
           $xml->addNode('CodAgencia', $receita['CodAgencia']);
           $xml->addNode('NumConta', $receita['NumConta']);
           $xml->addNode('CodContaAtivo', $receita['CodContaAtivo']);
           $xml->addNode('DataArrecadacao', $receita['DataArrecadacao']);
           $xml->addNode('Valor', $receita['Valor']);
           $xml->addNode('CodRecVinculado', $receita['CodRecVinculado']);
           $xml->addNode('FormaArrecadacao', $receita['FormaArrecadacao']);
           $xml->addNode('DataRegistro', $receita['DataRegistro']);
         }

         $xml->endBranch();
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/receitas/xml', $data);
  }

  public function receitasxls()
  {
    if ($this->uri->segment(3) == "") {
      $model = $this->receitas_model;
    }
    if ($this->uri->segment(3) == "tc") {
      $model = $this->tcreceitas_model;
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->screceitas_model;
    }

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $receitas = $model->select_search($this->input->get('search'));
      } else {
        $receitas = $model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      $receitas = $model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');
      if ($this->uri->segment(3) == "") {
        $titles = array(
          'id', 'Codigo', 'Descricao','Data','Valor'
        );
      }
      if ($this->uri->segment(3) == "tc") {
        $titles = array(
          'id', 'exercicio', 'rubricacodigo', 'valororcado', 'valorarrecadado'
        );
      }
      if ($this->uri->segment(3) == "sicap") {
        $titles = array(
          'id',
          'CodUndGestora',
          'CodigoUA',
          'Bimestre',
          'Exercicio',
          'CodOrgao',
          'CodUndOrcamentaria',
          'CodContaReceita',
          'CodBanco',
          'CodAgencia',
          'NumConta',
          'CodContaAtivo',
          'DataArrecadacao',
          'Valor',
          'CodRecVinculado',
          'FormaArrecadacao',
          'DataRegistro'
        );
      }
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_receitas';
    	$this->excel->make_from_array($titles, $receitas);
  }

  public function licitacoes()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $licitacoes = $this->licitacoes_model->select_search($this->input->get('search'));
      } else {
        $licitacoes = $this->licitacoes_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $licitacoes = $this->licitacoes_model->select($year_key = date('Y'));
      $licitacoes = $this->licitacoes_model->select();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'licitacoes' => $licitacoes,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/licitacoes', $dados);
  }

  public function licitacoesxml()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $licitacoes = $this->licitacoes_model->select_search($this->input->get('search'));
      } else {
        $licitacoes = $this->licitacoes_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $licitacoes = $this->licitacoes_model->select($year_key = date('Y'));
      $licitacoes = $this->licitacoes_model->select();
    }

    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Licitacoes');

       foreach ($licitacoes as $key => $licitacao) {
         // Set children for branch 'cars'
         if ($licitacao['tipo'] == 2) {
           $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento' ;

           $xml->startBranch('Licitacao', array('id' => $licitacao['id']));
           $xml->addNode('Modalidade', $licitacao['modalidade']);
           $xml->addNode('DataHora', date('d/m/Y H:i:s', strtotime($licitacao['data'])));
           $xml->addNode('Objeto', $licitacao['objeto']);
           $xml->addNode('Preco', 'R$ '.number_format($licitacao['valor'],2,',','.'));
           $xml->addNode('Status', $status);
           $xml->addNode('Vencedor', $licitacao['vencedor']);
           $xml->endBranch();
         }
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/licitacoes/xmll', $data);
  }

  public function licitacoesxls()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $licitacoes = $copys = $this->licitacoes_model->select_search_xlsl($this->input->get('search'));
        foreach ($licitacoes as $key => $licitacao) {
          $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento';
          $copys[$key] = $licitacao;
          $copys[$key]['status'] = $status;
        }
        $licitacoes = $copys;
      } else {
        $licitacoes = $copys = $this->licitacoes_model->select_filter_xlsl($year_key = $this->input->get('year'));
        foreach ($licitacoes as $key => $licitacao) {
          $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento';
          $copys[$key] = $licitacao;
          $copys[$key]['status'] = $status;
        }
        $licitacoes = $copys;
      }

    } else {
      // $licitacoes = $this->licitacoes_model->select($year_key = date('Y'));
      $licitacoes = $copys = $this->licitacoes_model->select_xlsl();
      foreach ($licitacoes as $key => $licitacao) {
        $status = ($licitacao['status'] == true) ? 'Concluído' : 'Em Andamento';
        $copys[$key] = $licitacao;
        $copys[$key]['status'] = $status;
      }
      $licitacoes = $copys;
    }

    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
    		'id', 'Órgão', 'Modalidade','Data/Hora','Objeto','Preço','Status','Vencedor'
    	);
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_licitacoes';
    	$this->excel->make_from_array($titles, $licitacoes);
  }

  public function contratos()
  {
    if ($this->input->get()) {
      if ($this->input->get('search') != "") {
        $licitacoes = $this->licitacoes_model->select_search($this->input->get('search'));
      } else if ($this->input->get('year') != "") {
        $licitacoes = $this->licitacoes_model->select_filter($year_key = $this->input->get('year'));
      } else {
        $licitacoes = $this->licitacoes_model->select();
      }

    } else {
      // $licitacoes = $this->licitacoes_model->select($year_key = date('Y'));
      $licitacoes = $this->licitacoes_model->select();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'licitacoes' => $licitacoes,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/contratos', $dados);
  }

  public function contratosxml()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $contratos = $this->licitacoes_model->select_search($this->input->get('search'));
      } else {
        $contratos = $this->licitacoes_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $contratos = $this->licitacoes_model->select($year_key = date('Y'));
      $contratos = $this->licitacoes_model->select();
    }

    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Contratos');

       foreach ($contratos as $key => $contrato) {
         // Set children for branch 'cars'
         if ($contrato['tipo'] == 1) {
           $status = ($contrato['status'] == true) ? 'Concluído' : 'Em Andamento' ;

           $xml->startBranch('Contrato', array('id' => $contrato['id']));
           $xml->addNode('Modalidade', $contrato['modalidade']);
           $xml->addNode('DataHora', date('d/m/Y H:i:s', strtotime($contrato['data'])));
           $xml->addNode('Objeto', $contrato['objeto']);
           $xml->addNode('Preco', 'R$ '.number_format($contrato['valor'],2,',','.'));
           $xml->addNode('Status', $status);
           $xml->addNode('Vencedor', $contrato['vencedor']);
           $xml->endBranch();
         }
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/licitacoes/xmlc', $data);
  }

  public function contratosxls()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $contratos = $copys = $this->licitacoes_model->select_search_xlsc($this->input->get('search'));
        foreach ($contratos as $key => $contrato) {
          $status = ($contrato['status'] == true) ? 'Concluído' : 'Em Andamento';
          $copys[$key] = $contrato;
          $copys[$key]['status'] = $status;
        }
        $contratos = $copys;
      } else {
        $contratos = $copys = $this->licitacoes_model->select_filter_xlsc($year_key = $this->input->get('year'));
        foreach ($contratos as $key => $contrato) {
          $status = ($contrato['status'] == true) ? 'Concluído' : 'Em Andamento';
          $copys[$key] = $contrato;
          $copys[$key]['status'] = $status;
        }
        $contratos = $copys;
      }

    } else {
      // $contratos = $this->licitacoes_model->select($year_key = date('Y'));
      $contratos = $copys = $this->licitacoes_model->select_xlsc();
      foreach ($contratos as $key => $contrato) {
        $status = ($contrato['status'] == true) ? 'Concluído' : 'Em Andamento';
        $copys[$key] = $contrato;
        $copys[$key]['status'] = $status;
      }
      $contratos = $copys;
    }

    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
        'id', 'Órgão', 'Modalidade','Data/Hora','Objeto','Preço','Status','Vencedor'
      );
      $array = array();
      for ($i = 0; $i <= 100; $i++) {
        $array[] = array($i, $i+1, $i+2);
      }
      $this->excel->filename = $municipio.'_contratos';
      $this->excel->make_from_array($titles, $contratos);
  }

  public function diarias()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $diarias = $this->diarias_model->select_search($this->input->get('search'));
      } else {
        $diarias = $this->diarias_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $diarias = $this->diarias_model->select($year_key = date('Y'));
      $diarias = $this->diarias_model->select();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $menu = "diarias";

    $dados = array(
      'page_title' => 'Diárias',
      'page_subtitle' => 'Lista',
      'diarias' => $diarias,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/diarias', $dados);
  }

  public function diariasxml()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $diarias = $this->diarias_model->select_search($this->input->get('search'));
      } else {
        $diarias = $this->diarias_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      $diarias = $this->diarias_model->select();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Diarias');

       foreach ($diarias as $key => $diaria) {
         $cargo = $this->cargos_model->select_id($diaria['cargo_id']);
         // Set children for branch 'cars'
         $xml->startBranch('Diaria', array('id' => $diaria['id']));
         $xml->addNode('Favorecido', $diaria['favorecido']);
         $xml->addNode('Cargo', $cargo['name']);
         $xml->addNode('Data', date('d/m/Y', strtotime($diaria['data'])));
         $xml->addNode('Destino', $diaria['destino']);
         $xml->addNode('Motivo', $diaria['motivo']);
         $xml->addNode('Valor', 'R$ '.number_format($diaria['valor'],2,',','.'));
         $xml->endBranch();
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/diarias/xml', $data);
  }

  public function diariasxls()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $diarias = $copyd = $this->diarias_model->select_search($this->input->get('search'));
        foreach ($diarias as $key => $diaria) {
          $cargo = $this->cargos_model->select_id($diaria['cargo_id']);
          $copyd[$key] = $diaria;
          $copyd[$key]['cargo_id'] = $cargo['name'];
        }
        $diarias = $copyd;
      } else {
        $diarias = $copyd = $this->diarias_model->select_filter($year_key = $this->input->get('year'));
        foreach ($diarias as $key => $diaria) {
          $cargo = $this->cargos_model->select_id($diaria['cargo_id']);
          $copyd[$key] = $diaria;
          $copyd[$key]['cargo_id'] = $cargo['name'];
        }
        $diarias = $copyd;
      }

    } else {
      $diarias = $copyd = $this->diarias_model->select();
      foreach ($diarias as $key => $diaria) {
        $cargo = $this->cargos_model->select_id($diaria['cargo_id']);
        $copyd[$key] = $diaria;
        $copyd[$key]['cargo_id'] = $cargo['name'];
      }
      $diarias = $copyd;

    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
    		'id', 'Favorecido', 'Cargo','Data','Destino','Motivo','Valor'
    	);
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_servidores';
    	$this->excel->make_from_array($titles, $diarias);
  }

  public function servidores()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $this->servidores_model->select_search($this->input->get('search'));
      } else {
        $servidores = $this->servidores_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $servidores = $this->servidores_model->select($year_key = date('Y'),$month_key = date('n'));
      $servidores = $this->servidores_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $menu = "home";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'servidores' => $servidores,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/servidores', $dados);
  }

  public function servidoresxml()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $this->servidores_model->select_search($this->input->get('search'));
      } else {
        $servidores = $this->servidores_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      $servidores = $this->servidores_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('Servidores');

       foreach ($servidores as $key => $servidor) {
         // Set children for branch 'cars'
         $mess1 = explode('/', $servidor['mes_ref']);
         $mess2 = $this->retorna_mes($mess1[0]);
         $mes_refs = $mess2."/".$mess1[1];

         $xml->startBranch('Servidor', array('id' => $servidor['id']));
         $xml->addNode('Nome', $servidor['nome']);
         $xml->addNode('ValorBruto', 'R$ '.number_format($servidor['valor_bruto'],2,',','.'));
         $xml->addNode('Desconto', 'R$ '.number_format($servidor['descontos'],2,',','.'));
         $xml->addNode('valorLiquido', 'R$ '.number_format($servidor['valor_liquido'],2,',','.'));
         $xml->addNode('mesReferente', $mes_refs);
         $xml->endBranch();
       }

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/servidores/xml', $data);
  }

  public function servidoresxls()
  {

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $copys = $this->servidores_model->select_search($this->input->get('search'));
        foreach ($servidores as $key => $servidor) {
          $mess1 = explode('/', $servidor['mes_ref']);
          $mess2 = $this->retorna_mes($mess1[0]);
          $mes_refs = $mess2."/".$mess1[1];
          $copys[$key] = $servidor;
          $copys[$key]['mes_ref'] = $mes_refs;
        }
        $servidores = $copys;
      } else {
        $servidores = $copys = $this->servidores_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
        foreach ($servidores as $key => $servidor) {
          $mess1 = explode('/', $servidor['mes_ref']);
          $mess2 = $this->retorna_mes($mess1[0]);
          $mes_refs = $mess2."/".$mess1[1];
          $copys[$key] = $servidor;
          $copys[$key]['mes_ref'] = $mes_refs;
        }
        $servidores = $copys;
      }

    } else {
      $servidores = $copys = $this->servidores_model->select_all();
      foreach ($servidores as $key => $servidor) {
        $mess1 = explode('/', $servidor['mes_ref']);
        $mess2 = $this->retorna_mes($mess1[0]);
        $mes_refs = $mess2."/".$mess1[1];
        $copys[$key] = $servidor;
        $copys[$key]['mes_ref'] = $mes_refs;
      }
      $servidores = $copys;
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
    		'id', 'Nome', 'Valor Bruto','Descontos','Valor Líquido','Mês de Referência','Data de Criação'
    	);
    	$array = array();
    	for ($i = 0; $i <= 100; $i++) {
    		$array[] = array($i, $i+1, $i+2);
    	}
      $this->excel->filename = $municipio.'_servidores';
    	$this->excel->make_from_array($titles, $servidores);
  }

  public function list_for_category($categoria)
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $reports = $this->reports_model->select_search($this->input->get('search'));
      } else {
        $reports = $this->reports_model->select($year_key = $this->input->get('year'),$category_key = $categoria,$subcategory_key = $this->input->get('subcategory'));
      }

    } else {
      // $reports = $this->reports_model->select($year_key = date('Y'),$category_key = $categoria);
      $reports = $this->reports_model->select_all($category_key = $categoria);
    }
    $categories = $this->categories_model->select_id($categoria);
    $subcategories = $this->subcategories_model->select_for_category($categoria);
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  
    $year = range(2030,1956);
  
    $menu = "home";

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'reports' => $reports,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'menu' => $menu,
      'settings' => $settings,
    );

    $this->load->view_site('pages/list',$dados);
  }

  public function esic()
  {
    $settings = $this->settings_model->select_id();
    // $requests = $this->requests_model->select();
    $typerequests = $this->typerequests_model->select_all();
    $menu = "esic";
    $dados = array(
      'menu' => $menu,
      'settings' => $settings,
      'typerequests' => $typerequests,
    );
    $this->load->view_site('pages/esic', $dados);
  }

  public function estatisticas($value='')
  {
    $settings = $this->settings_model->select_id();
    // $requests = $this->requests_model->select();
    $typerequests = $this->typerequests_model->select_all();
    $menu = "estatisticas";
    // echo $this->input->get('ano');
    if($this->input->get('ano')) {
      $estatisticas = array(
        // Abertas
        'total_jan_a' => count($this->requests_model->est($this->input->get('ano'),1,0)),
        'total_fev_a' => count($this->requests_model->est($this->input->get('ano'),2,0)),
        'total_mar_a' => count($this->requests_model->est($this->input->get('ano'),3,0)),
        'total_abr_a' => count($this->requests_model->est($this->input->get('ano'),4,0)),
        'total_mai_a' => count($this->requests_model->est($this->input->get('ano'),5,0)),
        'total_jun_a' => count($this->requests_model->est($this->input->get('ano'),6,0)),
        'total_jul_a' => count($this->requests_model->est($this->input->get('ano'),7,0)),
        'total_ago_a' => count($this->requests_model->est($this->input->get('ano'),8,0)),
        'total_set_a' => count($this->requests_model->est($this->input->get('ano'),9,0)),
        'total_out_a' => count($this->requests_model->est($this->input->get('ano'),10,0)),
        'total_nov_a' => count($this->requests_model->est($this->input->get('ano'),11,0)),
        'total_dez_a' => count($this->requests_model->est($this->input->get('ano'),12,0)),
        'total_a' => count($this->requests_model->est($this->input->get('ano'),null,0)),

        // Em andamento
        'total_jan_e' => count($this->requests_model->est($this->input->get('ano'),1,1)),
        'total_fev_e' => count($this->requests_model->est($this->input->get('ano'),2,1)),
        'total_mar_e' => count($this->requests_model->est($this->input->get('ano'),3,1)),
        'total_abr_e' => count($this->requests_model->est($this->input->get('ano'),4,1)),
        'total_mai_e' => count($this->requests_model->est($this->input->get('ano'),5,1)),
        'total_jun_e' => count($this->requests_model->est($this->input->get('ano'),6,1)),
        'total_jul_e' => count($this->requests_model->est($this->input->get('ano'),7,1)),
        'total_ago_e' => count($this->requests_model->est($this->input->get('ano'),8,1)),
        'total_set_e' => count($this->requests_model->est($this->input->get('ano'),9,1)),
        'total_out_e' => count($this->requests_model->est($this->input->get('ano'),10,1)),
        'total_nov_e' => count($this->requests_model->est($this->input->get('ano'),11,1)),
        'total_dez_e' => count($this->requests_model->est($this->input->get('ano'),12,1)),
        'total_e' => count($this->requests_model->est($this->input->get('ano'),null,1)),

        // Fechado
        'total_jan_f' => count($this->requests_model->est($this->input->get('ano'),1,2)),
        'total_fev_f' => count($this->requests_model->est($this->input->get('ano'),2,2)),
        'total_mar_f' => count($this->requests_model->est($this->input->get('ano'),3,2)),
        'total_abr_f' => count($this->requests_model->est($this->input->get('ano'),4,2)),
        'total_mai_f' => count($this->requests_model->est($this->input->get('ano'),5,2)),
        'total_jun_f' => count($this->requests_model->est($this->input->get('ano'),6,2)),
        'total_jul_f' => count($this->requests_model->est($this->input->get('ano'),7,2)),
        'total_ago_f' => count($this->requests_model->est($this->input->get('ano'),8,2)),
        'total_set_f' => count($this->requests_model->est($this->input->get('ano'),9,2)),
        'total_out_f' => count($this->requests_model->est($this->input->get('ano'),10,2)),
        'total_nov_f' => count($this->requests_model->est($this->input->get('ano'),11,2)),
        'total_dez_f' => count($this->requests_model->est($this->input->get('ano'),12,2)),
        'total_f' => count($this->requests_model->est($this->input->get('ano'),null,2))
      );

    } else {

      $estatisticas = array(
        // Abertas
        'total_jan_a' => count($this->requests_model->est(date('Y'),1,0)),
        'total_fev_a' => count($this->requests_model->est(date('Y'),2,0)),
        'total_mar_a' => count($this->requests_model->est(date('Y'),3,0)),
        'total_abr_a' => count($this->requests_model->est(date('Y'),4,0)),
        'total_mai_a' => count($this->requests_model->est(date('Y'),5,0)),
        'total_jun_a' => count($this->requests_model->est(date('Y'),6,0)),
        'total_jul_a' => count($this->requests_model->est(date('Y'),7,0)),
        'total_ago_a' => count($this->requests_model->est(date('Y'),8,0)),
        'total_set_a' => count($this->requests_model->est(date('Y'),9,0)),
        'total_out_a' => count($this->requests_model->est(date('Y'),10,0)),
        'total_nov_a' => count($this->requests_model->est(date('Y'),11,0)),
        'total_dez_a' => count($this->requests_model->est(date('Y'),12,0)),
        'total_a' => count($this->requests_model->est(date('Y'),null,0)),

        // Em andamento
        'total_jan_e' => count($this->requests_model->est(date('Y'),1,1)),
        'total_fev_e' => count($this->requests_model->est(date('Y'),2,1)),
        'total_mar_e' => count($this->requests_model->est(date('Y'),3,1)),
        'total_abr_e' => count($this->requests_model->est(date('Y'),4,1)),
        'total_mai_e' => count($this->requests_model->est(date('Y'),5,1)),
        'total_jun_e' => count($this->requests_model->est(date('Y'),6,1)),
        'total_jul_e' => count($this->requests_model->est(date('Y'),7,1)),
        'total_ago_e' => count($this->requests_model->est(date('Y'),8,1)),
        'total_set_e' => count($this->requests_model->est(date('Y'),9,1)),
        'total_out_e' => count($this->requests_model->est(date('Y'),10,1)),
        'total_nov_e' => count($this->requests_model->est(date('Y'),11,1)),
        'total_dez_e' => count($this->requests_model->est(date('Y'),12,1)),
        'total_e' => count($this->requests_model->est(date('Y'),null,1)),

        // Fechado
        'total_jan_f' => count($this->requests_model->est(date('Y'),1,2)),
        'total_fev_f' => count($this->requests_model->est(date('Y'),2,2)),
        'total_mar_f' => count($this->requests_model->est(date('Y'),3,2)),
        'total_abr_f' => count($this->requests_model->est(date('Y'),4,2)),
        'total_mai_f' => count($this->requests_model->est(date('Y'),5,2)),
        'total_jun_f' => count($this->requests_model->est(date('Y'),6,2)),
        'total_jul_f' => count($this->requests_model->est(date('Y'),7,2)),
        'total_ago_f' => count($this->requests_model->est(date('Y'),8,2)),
        'total_set_f' => count($this->requests_model->est(date('Y'),9,2)),
        'total_out_f' => count($this->requests_model->est(date('Y'),10,2)),
        'total_nov_f' => count($this->requests_model->est(date('Y'),11,2)),
        'total_dez_f' => count($this->requests_model->est(date('Y'),12,2)),
        'total_f' => count($this->requests_model->est(date('Y'),null,2))
      );
    }
    $year = range(2030,1956);
    // var_dump($estatisticas['total_f']);
    $dados = array(
      'menu' => $menu,
      'settings' => $settings,
      'typerequests' => $typerequests,
      'estatisticas' => $estatisticas,
      'year' => $year,
    );
    $this->load->view_site('pages/estatisticas', $dados);
  }

  public function relatorio()
  {
    $settings = $this->settings_model->select_id();
    if ($this->input->get()) {
      $de = $this->input->get('de');
      $ate = $this->input->get('ate');
      $requests_0 = $this->requests_model->select_by_status(0,$this->input->get('de'),$this->input->get('ate'));
      $requests_1 = $this->requests_model->select_by_status(1,$this->input->get('de'),$this->input->get('ate'));
      $requests_2 = $this->requests_model->select_by_status(2,$this->input->get('de'),$this->input->get('ate'));
      $requests_3 = $this->requests_model->select_by_status(3,$this->input->get('de'),$this->input->get('ate'));
    } else {
      $de = null;
      $ate = null;
      $requests_0 = $this->requests_model->select_by_status(0,null,null);
      $requests_1 = $this->requests_model->select_by_status(1,null,null);
      $requests_2 = $this->requests_model->select_by_status(2,null,null);
      $requests_3 = $this->requests_model->select_by_status(3,null,null);
    }
    $typerequests = $this->typerequests_model->select_all();
    $menu = "relatorio";

    $dados = array(
      'menu' => $menu,
      'settings' => $settings,
      'typerequests' => $typerequests,
      'requests_0' => $requests_0,
      'requests_1' => $requests_1,
      'requests_2' => $requests_2,
      'requests_3' => $requests_3,
      'de' => $de,
      'ate' => $ate
    );
    $this->load->view('pages/relatorio', $dados);
  }

  public function relatorioxml()
  {

    if ($this->input->get()) {
      $de = $this->input->get('de');
      $ate = $this->input->get('ate');
      $requests_0 = $this->requests_model->select_by_status(0,$this->input->get('de'),$this->input->get('ate'));
      $requests_1 = $this->requests_model->select_by_status(1,$this->input->get('de'),$this->input->get('ate'));
      $requests_2 = $this->requests_model->select_by_status(2,$this->input->get('de'),$this->input->get('ate'));
      $requests_3 = $this->requests_model->select_by_status(3,$this->input->get('de'),$this->input->post('ate'));
      $requests_total = count($requests_0) + count($requests_1) + count($requests_2) + count($requests_3);
    } else {
      $de = null;
      $ate = null;
      $requests_0 = $this->requests_model->select_by_status(0,null,null);
      $requests_1 = $this->requests_model->select_by_status(1,null,null);
      $requests_2 = $this->requests_model->select_by_status(2,null,null);
      $requests_3 = $this->requests_model->select_by_status(3,null,null);
      $requests_total = count($requests_0) + count($requests_1) + count($requests_2) + count($requests_3);
    }

    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

    // Load XML writer library
       $this->load->library('xml_writer');

       // Initiate class
       $xml = new Xml_writer();
       $xml->setRootName($municipio);
       $xml->initiate();

       // Start branch 'cars'
       $xml->startBranch('RelatorioDeAtendimento');

         // Set children for branch 'cars'

         $xml->startBranch('Pedidos');
         $xml->addNode('Respondidos', count($requests_1));
         $xml->addNode('Abertos', count($requests_0));
         $xml->addNode('EmTramitacao', count($requests_2));
         $xml->addNode('Fechado', count($requests_3));
         $xml->addNode('Total', $requests_total);
         if ($de != null && $ate != null) {
           $xml->addNode('DataInicial', $de);
           $xml->addNode('DataFinal', $ate);
         }
         $xml->endBranch();

       // End branch 'cars'
       $xml->endBranch();

       // Pass the XML to the view
       $data = array();
       $data['xml'] = $xml->getXml(FALSE);
       $data['municipio'] = $municipio;
       $this->load->view('admin/requests/xml', $data);
  }

  public function relatorioxls()
  {

    if ($this->input->get('de')) {
    $requests_0 = ($count_0 = count($this->requests_model->select_by_status(0,$this->input->get('de'),$this->input->get('ate'))) > 0) ? count($this->requests_model->select_by_status(0,$this->input->get('de'),$this->input->get('ate'))):"Nenhum";
    $requests_1 = ($count_1 = count($this->requests_model->select_by_status(1,$this->input->get('de'),$this->input->get('ate'))) > 0) ? count($this->requests_model->select_by_status(1,$this->input->get('de'),$this->input->get('ate'))):"Nenhum";
    $requests_2 = ($count_2 = count($this->requests_model->select_by_status(2,$this->input->get('de'),$this->input->get('ate'))) > 0) ? count($this->requests_model->select_by_status(2,$this->input->get('de'),$this->input->get('ate'))):"Nenhum";
    $requests_3 = ($count_3 = count($this->requests_model->select_by_status(3,$this->input->get('de'),$this->input->get('ate'))) > 0) ? count($this->requests_model->select_by_status(3,$this->input->get('de'),$this->input->get('ate'))):"Nenhum";
    $requests_total = $count_0 + $count_1 + $count_2 + $count_3;
      $de = $this->input->get('de');
      $ate = $this->input->get('ate');
      $relatorio = array(
        0 => array(
          'requests_1' => $requests_1,
          'requests_0' => $requests_0,
          'requests_2' => $requests_2,
          'requests_3' => $requests_3,
          'requests_total' => ($requests_total > 0) ? $requests_total:"Nenhum",
          'de' => $de,
          'ate' => $ate
        )
      );

    } else {
      $requests_1 = ($count_0 = count($this->requests_model->select_by_status(1,null,null)) > 0) ? count($this->requests_model->select_by_status(1,null,null)):"Nenhum";
      $requests_0 = ($count_1 = count($this->requests_model->select_by_status(0,null,null)) > 0) ? count($this->requests_model->select_by_status(0,null,null)):"Nenhum";
      $requests_2 = ($count_2 = count($this->requests_model->select_by_status(2,null,null)) > 0) ? count($this->requests_model->select_by_status(2,null,null)):"Nenhum";
      $requests_3 = ($count_3 = count($this->requests_model->select_by_status(3,null,null)) > 0) ? count($this->requests_model->select_by_status(3,null,null)):"Nenhum";
      $requests_total = $count_0 + $count_1 + $count_2 + $count_3;
      $de = null;
      $ate = null;
      $relatorio = array(
        0 => array(
          'requests_1' => $requests_1,
          'requests_0' => $requests_0,
          'requests_2' => $requests_2,
          'requests_3' => $requests_3,
          'requests_total' => ($requests_total > 0) ? $requests_total:"Nenhum",
          'de' => $de,
          'ate' => $ate
        )
      );
      // echo "<pre>";
      // var_dump($relatorio);
      // exit;
    }
    $settings = $this->settings_model->select_id();
    $municipio = str_replace(' ', '', ucwords($settings['name']));
    $municipio = str_replace(
      array("D'",'â','á','ã','Â','Á','Ã','ê','é','Ê','É','í','Í','ô','ó','õ','Ô','Ó','Õ','ú','Ú','ç'),
      array("D",'a','a','a','A','A','A','e','e','E','E','i','I','o','o','o','O','O','O','u','U','c'),
        $municipio);

      $this->load->library('excel');

      $titles = array(
    		'Respondidos', 'Abertos', 'Em Tramitação','Fechados','Total','Data Inicial','Data Final'
    	);
    	// $array = array();
    	// for ($i = 0; $i <= 100; $i++) {
    	// 	$array[] = array($i, $i+1, $i+2);
    	// }
      $this->excel->filename = $municipio.'_relatorio_de_atendimento';
    	$this->excel->make_from_array($titles, $relatorio);
  }

  public function retorna_mes($mes)
  {
    switch ($mes) {
      case 1:
        $mes = "Janeiro";
        break;
      case 2:
        $mes = "Fevereiro";
        break;
      case 3:
        $mes = "Março";
        break;
      case 4:
        $mes = "Abril";
        break;
      case 5:
        $mes = "Maio";
        break;
      case 6:
        $mes = "Junho";
        break;
      case 7:
        $mes = "Julho";
        break;
      case 8:
        $mes = "Agosto";
        break;
      case 9:
        $mes = "Setembro";
        break;
      case 10:
        $mes = "Outubro";
        break;
      case 11:
        $mes = "Novembro";
        break;
      case 12:
        $mes = "Dezembro";
        break;

    }

    return $mes;
  }

  public function verbas()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $verbas = $this->verbas_model->select_search($this->input->get('search'));
      } else {
        $verbas = $this->verbas_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $verbas = $this->verbas_model->select($year_key = date('Y'),$month_key = date('n'));
      $verbas = $this->verbas_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'verbas' => $verbas,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_site('pages/verbas', $dados);
  }
}
