<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargos extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('cargos_model');
        $this->load->model('reports_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    $settings = $this->settings_model->select_id();
    $cargos = $this->cargos_model->select();
    $dados = array(
      'page_title' => 'Cargos',
      'page_subtitle' => 'Lista',
      'cargos' => $cargos,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/cargos/index', $dados);
  }
  public function add()
  {
    $cargo = array(
      'name' => $this->input->post('name'),
    );
    $this->cargos_model->insert($cargo);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Cargo criado com sucesso!"));
    redirect('admin/cargos');
  }

  public function remove($id)
  {
    $this->cargos_model->delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Cargo removido com sucesso!"));
    redirect('admin/cargos');
  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }

}
