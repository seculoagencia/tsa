<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Licitacoes extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('licitacoes_model');
        $this->load->model('licitacoesanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $licitacoes = $this->licitacoes_model->select_search($this->input->get('search'));
      } else {
        $licitacoes = $this->licitacoes_model->select_filter($year_key = $this->input->get('year'));
      }

    } else {
      // $licitacoes = $this->licitacoes_model->select($year_key = date('Y'));
      $licitacoes = $this->licitacoes_model->select();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

  $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'licitacoes' => $licitacoes,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/licitacoes/index', $dados);

  }
  public function add()
  {
    if ($this->input->post('status') == "") {
      $status = null;
    } else {
      $status = ($this->input->post('status') == 0) ? false : true ;
    }

      $licitacao = array(
        'orgao' => $this->input->post('orgao'),
        'modalidade' => $this->input->post('modalidade'),
        // 'data' => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'data' => $this->input->post('data'),
        'objeto' => $this->input->post('objeto'),
        'valor' => $this->input->post('valor'),
        'status' => $status,
        'vencedor' => $this->input->post('vencedor'),
        'tipo' => $this->input->post('tipo'),
        'created' => date('Y-m-d H:i:s'),
      );
      $this->licitacoes_model->insert($licitacao);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Licitação criada com sucesso!"));
      redirect('admin/licitacoes');

  }
  public function add_anexo($id)
  {
    // $report = $this->licitacoesanexos_model->select_id($id);
    // if (empty($_FILES['anexo']['name'])) {
    //     $anexo = $report['anexo'];
    // } else {
    //     $anexo = $this->do_upload_anexo();
    // }
    $anexo = $this->do_upload_anexo();
    $report = array(
      'report_id' => $id,
      'anexo' => $anexo['path'],
      'name' => $anexo['original_name'],
    );
    $this->licitacoesanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('licitacoes');
  }
  public function remove_anexo($id)
  {
    $report = $this->licitacoesanexos_model->select_id($id);
    $this->licitacoesanexos_model->delete($id);
    // if (file_exists("./{$licitacao['anexo']}")) {
    //   unlink("./{$report['anexo']}");
    // }
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('licitacoes');
  }
  public function edit($id)
  {
    $licitacao = $this->licitacoes_model->select_num($id);
    
    if ($this->input->post()) {
      // var_dump($this->input->post());
      // exit;
      if (empty($_FILES['anexo']['name'])) {
        $anexo = $licitacao['anexo'];
      } else {
        $anexo = $this->do_upload_anexo();
      }
      if ($this->input->post('status') == "") {
        $status = null;
      } else {
        $status = ($this->input->post('status') == 0) ? false : true ;
      }
      
      $licitacao_up = array(
        'orgao' => $this->input->post('orgao'),
        'modalidade' => $this->input->post('modalidade'),
        // 'data' => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'data' => $this->input->post('data'),
        'objeto' => $this->input->post('objeto'),
        'valor' => $this->input->post('valor'),
        'status' => $status,
        'vencedor' => $this->input->post('vencedor'),
        'tipo' => $this->input->post('tipo'),
        // 'anexo' => $anexo,
      );
      $this->licitacoes_model->update($licitacao_up, $id);
      $this->last_update();
      // if (file_exists("./{$licitacao['anexo']}")) {
      //   unlink("./{$licitacao['anexo']}");
      // }
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/licitacoes');
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'licitacoes',
      'page_subtitle' => 'Editar',
      'licitacao' => $licitacao,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/licitacoes/edit', $dados);
  }
  public function details($num)
  {
    $licitacao = $this->licitacoes_model->select_id($num);
    $liquidacoes = $this->licitacoes_model->liquidacoes_select($licitacao['id']);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'licitacoes',
      'page_subtitle' => 'Detalhes',
      'licitacao' => $licitacao,
      'liquidacoes' => $liquidacoes,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/licitacoes/details', $dados);
  }
  //
  public function remove($id)
  {
    $licitacao = $this->licitacoes_model->select_id($id);
    $this->licitacoes_model->delete($id);
    // if (file_exists("./{$licitacao['anexo']}")) {
    //   unlink("./{$licitacao['anexo']}");
    // }
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Receita removida com sucesso!"));
    redirect('licitacoes');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("licitacoes");
    }

    $data = array(
      'path' => 'uploads/anexos/'.$this->upload->file_name,
      'original_name' => $this->upload->client_name
    );

    return $data;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
