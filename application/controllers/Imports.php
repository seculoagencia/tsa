<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imports extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('imports_model');
        $this->load->model('sicaps_model');
        $this->load->model('empenhosanexos_model');
        $this->load->model('settings_model');
        $this->load->model('sagres_model');
        $this->load->library("pagination");
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->imports_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->imports_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->imports_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->imports_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();

    

    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = range(2030,1956);
    
    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/index', $dados);
  }

  public function index2()
  {
    $config = array();
    $config["base_url"] = base_url() . "/imports/index2/pagina";
    $config["per_page"] = 100;
    $config["uri_segment"] = 4;
    $config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = "<ul class='pagination'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tagl_close'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tagl_close'] = "</li>";
    $config['first_tag_open'] = "<li>";
    $config['first_tagl_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tagl_close'] = "</li>";
    $config['first_link'] = 'Primeiro';
    $config['last_link'] = 'Último';
    $config['next_link'] = '»';
    $config['prev_link'] = '«';

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->sicaps_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->sicaps_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->imports_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->sicaps_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();

    $page = ($this->uri->segment(4) > 0) ? $config["per_page"]*($this->uri->segment(4) - 1) : 0;

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        if ($page > 0) {
          $config["base_url"] = base_url() . "/imports/index2?search=".$this->input->get('search');
        }
        $config["total_rows"] = $this->sicaps_model->select_search_page_record_count($this->input->get('search'));
        $config['suffix'] = "?search=".$this->input->get('search');
        $empenhos = $this->sicaps_model->select_search_page($this->input->get('search'), $config["per_page"], $page);
      } else {
        if ($page > 0) {
          $config["base_url"] = base_url() . "/imports/index2/pagina?year=".$this->input->get('year').'&month='.$this->input->get('month');
        }
        $config["total_rows"] = $this->sicaps_model->select_page_record_count($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
        $config['suffix'] = "?year=".$this->input->get('year').'&month='.$this->input->get('month');
        $empenhos = $this->sicaps_model->select_page($year_key = $this->input->get('year'),$month_key = $this->input->get('month'),$config["per_page"], $page);
      }

    } else {
      $config["total_rows"] = $this->sicaps_model->select_all_page_record_count();
      // $empenhos = $model->select($year_key = date('Y'),$month_key = date('n'));
      // echo $config["total_rows"] - ($config["per_page"] * ($page - 1));
      // exit;
      $empenhos = $this->sicaps_model->select_all_page($config["per_page"], $page);
    }
    $this->pagination->initialize($config);
    // $config["total_rows"] = count($empenhos);

    $pagination = $this->pagination->create_links();

    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = range(2030,1956);
    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'pagination' => $pagination,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/index2', $dados);
  }

  public function index3()
  {
    $config = array();
    $config["base_url"] = base_url() . "/imports/index3/pagina";
    $config["per_page"] = 100;
    $config["uri_segment"] = 4;
    $config['use_page_numbers'] = TRUE;
    $config['full_tag_open'] = "<ul class='pagination'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tagl_close'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tagl_close'] = "</li>";
    $config['first_tag_open'] = "<li>";
    $config['first_tagl_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tagl_close'] = "</li>";
    $config['first_link'] = 'Primeiro';
    $config['last_link'] = 'Último';
    $config['next_link'] = '»';
    $config['prev_link'] = '«';

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $empenhos = $this->sagres_model->select_search($this->input->get('search'));
      } else {
        $empenhos = $this->sagres_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $empenhos = $this->imports_model->select($year_key = date('Y'),$month_key = date('n'));
      $empenhos = $this->sagres_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();

    $page = ($this->uri->segment(4) > 0) ? $config["per_page"]*($this->uri->segment(4) - 1) : 0;

    if ($this->input->get()) {
      if ($this->input->get('search')) {
        if ($page > 0) {
          $config["base_url"] = base_url() . "/imports/index2?search=".$this->input->get('search');
        }
        $config["total_rows"] = $this->sagres_model->select_search_page_record_count($this->input->get('search'));
        $config['suffix'] = "?search=".$this->input->get('search');
        $empenhos = $this->sagres_model->select_search_page($this->input->get('search'), $config["per_page"], $page);
      } else {
        if ($page > 0) {
          $config["base_url"] = base_url() . "/imports/index2/pagina?year=".$this->input->get('year').'&month='.$this->input->get('month');
        }
        $config["total_rows"] = $this->sagres_model->select_page_record_count($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
        $config['suffix'] = "?year=".$this->input->get('year').'&month='.$this->input->get('month');
        $empenhos = $this->sagres_model->select_page($year_key = $this->input->get('year'),$month_key = $this->input->get('month'),$config["per_page"], $page);
      }

    } else {
      $config["total_rows"] = $this->sagres_model->select_all_page_record_count();
      // $empenhos = $model->select($year_key = date('Y'),$month_key = date('n'));
      // echo $config["total_rows"] - ($config["per_page"] * ($page - 1));
      // exit;
      $empenhos = $this->sagres_model->select_all_page($config["per_page"], $page);
    }
    $this->pagination->initialize($config);
    // $config["total_rows"] = count($empenhos);

    $pagination = $this->pagination->create_links();

    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );
    $year = range(2030,1956);
    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'empenhos' => $empenhos,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'pagination' => $pagination,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/sagres', $dados);
  }

  public function add()
  {
    if ($this->input->post()) {
      if ($this->input->post('type') == "tc") {
        if ($_FILES['import']['name']) {
          for ($i=0; $i < count($_FILES['import']['name']); $i++) {
            $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
          }
          foreach ($_FILES['import']['name'] as $key => $file) {
            $table = "tc_".explode(".",$file)[0];
            if ($file == 'empenho.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );

              foreach( $items as $key => $xml )
              {
                $item = array(
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'Numero' => $xml->getElementsByTagName( "Numero" )->item(0)->nodeValue,
                  'Processo' => $xml->getElementsByTagName( "Processo" )->item(0)->nodeValue,
                  'Data' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue))),
                  'AcaoCodigo' => $xml->getElementsByTagName( "AcaoCodigo" )->item(0)->nodeValue,
                  'FonteCodigo' => $xml->getElementsByTagName( "FonteCodigo" )->item(0)->nodeValue,
                  'RubricaCodigo' => $xml->getElementsByTagName( "RubricaCodigo" )->item(0)->nodeValue,
                  'CPF' => $xml->getElementsByTagName( "CPF" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                  'ValorEmpAteAgora' => $xml->getElementsByTagName( "ValorEmpAteAgora" )->item(0)->nodeValue,
                  'ValorLiqAteAgora' => $xml->getElementsByTagName( "ValorLiqAteAgora" )->item(0)->nodeValue,
                  'ValorPagAteAgora' => $xml->getElementsByTagName( "ValorPagAteAgora" )->item(0)->nodeValue,
                );


                if (count($this->imports_model->select_id($item['Numero'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'acao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                  'funcaocodigo' => $xml->getElementsByTagName( "funcaocodigo" )->item(0)->nodeValue,
                  'subfuncaocodigo' => $xml->getElementsByTagName( "subfuncaocodigo" )->item(0)->nodeValue,
                  'programacodigo' => $xml->getElementsByTagName( "programacodigo" )->item(0)->nodeValue,
                  'unidadecodigo' => $xml->getElementsByTagName( "unidadecodigo" )->item(0)->nodeValue,
                  'metafinanceiraano01' => $xml->getElementsByTagName( "metafinanceiraano01" )->item(0)->nodeValue,
                  'metafinanceiraano02' => $xml->getElementsByTagName( "metafinanceiraano02" )->item(0)->nodeValue,
                  'metafinanceiraano03' => $xml->getElementsByTagName( "metafinanceiraano03" )->item(0)->nodeValue,
                  'metafinanceiraano04' => $xml->getElementsByTagName( "metafinanceiraano04" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->acao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'funcao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->funcao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'subfuncao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );

                if (count($this->imports_model->subfuncao($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'programa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->programa($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'unidade.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->unidade($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'fonte.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->fonte($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'rubrica.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'codigo' => $xml->getElementsByTagName( "codigo" )->item(0)->nodeValue,
                  'descricao' => $xml->getElementsByTagName( "descricao" )->item(0)->nodeValue,
                  'tipo' => $xml->getElementsByTagName( "tipo" )->item(0)->nodeValue,
                  'analiticasintetica' => $xml->getElementsByTagName( "analiticasintetica" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->rubrica($item['codigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'credor.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CPF' => $xml->getElementsByTagName( "CPF" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->credor($item['CPF'], $item['CNPJ'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'dotacao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'acaocodigo' => $xml->getElementsByTagName( "acaocodigo" )->item(0)->nodeValue,
                  'fontecodigo' => $xml->getElementsByTagName( "fontecodigo" )->item(0)->nodeValue,
                  'rubricacodigo' => $xml->getElementsByTagName( "rubricacodigo" )->item(0)->nodeValue,
                  'valororcado' => $xml->getElementsByTagName( "valororcado" )->item(0)->nodeValue,
                  'valoratualizado' => $xml->getElementsByTagName( "valoratualizado" )->item(0)->nodeValue,
                  'valorempateagora' => $xml->getElementsByTagName( "valorempateagora" )->item(0)->nodeValue,
                  'valorliqateagora' => $xml->getElementsByTagName( "valorliqateagora" )->item(0)->nodeValue,
                  'valorpagateagora' => $xml->getElementsByTagName( "valorpagateagora" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->dotacao($item['rubricacodigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'receitaclassificada.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'exercicio' => $xml->getElementsByTagName( "exercicio" )->item(0)->nodeValue,
                  'rubricacodigo' => $xml->getElementsByTagName( "rubricacodigo" )->item(0)->nodeValue,
                  'valororcado' => $xml->getElementsByTagName( "valororcado" )->item(0)->nodeValue,
                  'valorarrecadado' => $xml->getElementsByTagName( "valorarrecadado" )->item(0)->nodeValue,
                );
                if (count($this->imports_model->receitaclassificada($item['rubricacodigo'])) == 0) {
                  $this->imports_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'receitalancada.xml') {
              unlink( "./uploads/".$file );
            }
          }
        }
      }
      if ($this->input->post('type') == "sicap") {
        if ($_FILES['import']['name']) {
          for ($i=0; $i < count($_FILES['import']['name']); $i++) {
            $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
          }
          foreach ($_FILES['import']['name'] as $key => $file) {
            $table = "sicap_".explode(".",$file)[0];
            if ($file == 'Credor.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );

              foreach( $items as $key => $xml )
              {
                if (count($xml->getElementsByTagName( "NumerodoRegistro" )["length"]) > 0) {
                  $numerodoRegistro = $xml->getElementsByTagName( "NumerodoRegistro" )->item(0)->nodeValue;
                } else {
                  if (count($xml->getElementsByTagName( "Registro" )["length"]) > 0) {
                    $numerodoRegistro = $xml->getElementsByTagName( "Registro" )->item(0)->nodeValue;
                  } else {
                    if (count($xml->getElementsByTagName( "NumeroDoRegistro" )["length"]) > 0) {
                      $numerodoRegistro = $xml->getElementsByTagName( "NumeroDoRegistro" )->item(0)->nodeValue;
                    } else {
                      $numerodoRegistro = null;
                    }
                  }
                }

                if (count($xml->getElementsByTagName( "TipoRegistro" )["length"]) > 0) {
                  $tipoRegistro = $xml->getElementsByTagName( "TipoRegistro" )->item(0)->nodeValue;
                } else {
                  $tipoRegistro = null;
                }

                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'InscricaoEstadual' => $xml->getElementsByTagName( "InscricaoEstadual" )->item(0)->nodeValue,
                  'InscricaoMunicipal' => $xml->getElementsByTagName( "InscricaoMunicipal" )->item(0)->nodeValue,
                  'Endereco' => $xml->getElementsByTagName( "Endereco" )->item(0)->nodeValue,
                  'Cidade' => $xml->getElementsByTagName( "Cidade" )->item(0)->nodeValue,
                  'UF' => $xml->getElementsByTagName( "UF" )->item(0)->nodeValue,
                  'Cep' => $xml->getElementsByTagName( "Cep" )->item(0)->nodeValue,
                  'Fone' => $xml->getElementsByTagName( "Fone" )->item(0)->nodeValue,
                  'Fax' => $xml->getElementsByTagName( "Fax" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                  'NumerodoRegistro' => $numerodoRegistro,
                  'TipoRegistro' => $tipoRegistro,
                );
                if (count($this->sicaps_model->getVerifica($table, 'CodCredor', $item['CodCredor'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Empenho.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if ($xml->getElementsByTagName( "DataEmpenho" )->length != 0) {
                  $dataEmpenho = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataEmpenho" )->item(0)->nodeValue)));
                } else {
                  if ($xml->getElementsByTagName( "Data" )->length != 0) {
                    $dataEmpenho = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue)));
                  } else {
                    $dataEmpenho = null;
                  }
                }
                if ($xml->getElementsByTagName( "CodContaDespesa" )->length != 0) {
                  $codContaDespesa = $xml->getElementsByTagName( "CodContaDespesa" )->item(0)->nodeValue;
                } else {
                  $codContaDespesa = null;
                }
                if ($xml->getElementsByTagName( "CodContaContabil" )->length != 0) {
                  $codContaContabil = $xml->getElementsByTagName( "CodContaContabil" )->item(0)->nodeValue;
                } else {
                  $codContaContabil = null;
                }
                if ($xml->getElementsByTagName( "RegistroDePreco" )->length != 0) {
                  $registroDePreco = $xml->getElementsByTagName( "RegistroDePreco" )->item(0)->nodeValue;
                } else {
                  $registroDePreco = null;
                }
                if ($xml->getElementsByTagName( "ReferenciaLegal" )->length != 0) {
                  $referenciaLegal = $xml->getElementsByTagName( "ReferenciaLegal" )->item(0)->nodeValue;
                } else {
                  $referenciaLegal = null;
                }
                if ($xml->getElementsByTagName( "DataProcesso" )->length != 0) {
                  $dataProcesso = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataProcesso" )->item(0)->nodeValue)));
                } else {
                  $dataProcesso = null;
                }
                if ($xml->getElementsByTagName( "DataConvenio" )->length != 0) {
                  $dataConvenio = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataConvenio" )->item(0)->nodeValue)));
                } else {
                  $dataConvenio = null;
                }
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'DataEmpenho' => $dataEmpenho,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                  'CodSubFuncao' => $xml->getElementsByTagName( "CodSubFuncao" )->item(0)->nodeValue,
                  'CodPrograma' => $xml->getElementsByTagName( "CodPrograma" )->item(0)->nodeValue,
                  'CodProjAtividade' => $xml->getElementsByTagName( "CodProjAtividade" )->item(0)->nodeValue,
                  'CodContaDespesa' => $codContaDespesa,
                  'CodContaContabil' => $codContaContabil,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'ContraPartida' => $xml->getElementsByTagName( "ContraPartida" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'ModalLicita' => $xml->getElementsByTagName( "ModalLicita" )->item(0)->nodeValue,
                  'RegistroDePreco' => $registroDePreco,
                  'ReferenciaLegal' => $referenciaLegal,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                  'DataProcesso' => $dataProcesso,
                  'NumContrato' => $xml->getElementsByTagName( "NumContrato" )->item(0)->nodeValue,
                  'DataContrato' => date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataContrato" )->item(0)->nodeValue))),
                  'NumConvenio' => $xml->getElementsByTagName( "NumConvenio" )->item(0)->nodeValue,
                  'DataConvenio' => $dataConvenio,
                  'NumObra' => $xml->getElementsByTagName( "NumObra" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                );
                if (count($this->sicaps_model->getVerifica($table, 'NumEmpenho', $item['NumEmpenho'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
                // if (count($this->sicaps_model->select_id($item['NumEmpenho'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Funcao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here
              $items = $doc->getElementsByTagName( "funcao" );

              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodFuncao' => $xml->getElementsByTagName( "CodFuncao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );
                if (count($this->sicaps_model->getVerifica($table, 'CodFuncao', $item['CodFuncao'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }
                // if (count($this->sicaps_model->getVerifica($table, 'CodFuncao', $item['CodFuncao'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Liquidacao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if ($xml->getElementsByTagName( "DataLiquidacao" )->length != 0) {
                  $dataLiquidacao = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataLiquidacao" )->item(0)->nodeValue)));
                } else {
                  if ($xml->getElementsByTagName( "Data" )->length != 0) {
                    $dataLiquidacao = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue)));
                  } else {
                    $dataLiquidacao = null;
                  }
                }
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'DataLiquidacao' => $dataLiquidacao,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'CodOperacao' => $xml->getElementsByTagName( "CodOperacao" )->item(0)->nodeValue,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                  'CodCredor' => $xml->getElementsByTagName( "CodCredor" )->item(0)->nodeValue,
                  'Referencia' => $xml->getElementsByTagName( "Referencia" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumLiquidacao', $item['NumLiquidacao'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'NumLiquidacao', $item['NumLiquidacao'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Pagamento.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if ($xml->getElementsByTagName( "DataPagamento" )->length != 0) {
                  $dataPagamento = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataPagamento" )->item(0)->nodeValue)));
                } else {
                  if ($xml->getElementsByTagName( "Data" )->length != 0) {
                    $dataPagamento = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue)));
                  } else {
                    $dataPagamento = null;
                  }
                }

                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumPagamento' => $dataPagamento,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'DataPagamento' => $dataPagamento,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'Historico' => $xml->getElementsByTagName( "Historico" )->item(0)->nodeValue,
                  'CodOperacao' => $xml->getElementsByTagName( "CodOperacao" )->item(0)->nodeValue,
                  'NumProcesso' => $xml->getElementsByTagName( "NumProcesso" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'PagamentoFinanceiro.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if ($xml->getElementsByTagName( "NumDocumento" )->length != 0) {
                  $numDocumento = $xml->getElementsByTagName( "NumDocumento" )->item(0)->nodeValue;
                } else {
                  $numDocumento = null;
                }
                if ($xml->getElementsByTagName( "TipoConta" )->length != 0) {
                  $tipoConta = $xml->getElementsByTagName( "TipoConta" )->item(0)->nodeValue;
                } else {
                  $tipoConta = null;
                }
                if ($xml->getElementsByTagName( "NumContaBancaria" )->length != 0) {
                  $numContaBancaria = $xml->getElementsByTagName( "NumContaBancaria" )->item(0)->nodeValue;
                } else {
                  if ($xml->getElementsByTagName( "NumContaCorrente" )->length != 0) {
                    $numContaBancaria = $xml->getElementsByTagName( "NumContaCorrente" )->item(0)->nodeValue;
                  } else {
                    $numContaBancaria = null;
                  }
                }
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumEmpenho' => $xml->getElementsByTagName( "NumEmpenho" )->item(0)->nodeValue,
                  'NumLiquidacao' => $xml->getElementsByTagName( "NumLiquidacao" )->item(0)->nodeValue,
                  'NumPagamento' => $xml->getElementsByTagName( "NumPagamento" )->item(0)->nodeValue,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'Sinal' => $xml->getElementsByTagName( "Sinal" )->item(0)->nodeValue,
                  'TipoPagamento' => $xml->getElementsByTagName( "TipoPagamento" )->item(0)->nodeValue,
                  'NumDocumento' => $numDocumento,
                  'CodContaBalancete' => $xml->getElementsByTagName( "CodContaBalancete" )->item(0)->nodeValue,
                  'CodBanco' => $xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue,
                  'CodAgenciaBanco' => $xml->getElementsByTagName( "CodAgenciaBanco" )->item(0)->nodeValue,
                  'NumContaBancaria' => $numContaBancaria,
                  'TipoConta' => $tipoConta,
                );

                if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'NumPagamento', $item['NumPagamento'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'Programa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodPrograma' => $xml->getElementsByTagName( "CodPrograma" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Objetivo' => $xml->getElementsByTagName( "Objetivo" )->item(0)->nodeValue,
                  'PublicoAlvo' => $xml->getElementsByTagName( "PublicoAlvo" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodPrograma', $item['CodPrograma'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodPrograma', $item['CodPrograma'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'ProjAtividade.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodProjAtividade' => $xml->getElementsByTagName( "CodProjAtividade" )->item(0)->nodeValue,
                  'Identificador' => $xml->getElementsByTagName( "Identificador" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodProjAtividade', $item['CodProjAtividade'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodProjAtividade', $item['CodProjAtividade'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RecursoVinculado.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Finalidade' => $xml->getElementsByTagName( "Finalidade" )->item(0)->nodeValue,
                  'Tipo' => $xml->getElementsByTagName( "Tipo" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodRecVinculado', $item['CodRecVinculado'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodRecVinculado', $item['CodRecVinculado'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RubricaDespesa.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodRubrica' => $xml->getElementsByTagName( "CodRubrica" )->item(0)->nodeValue,
                  'Especificacao' => $xml->getElementsByTagName( "Especificacao" )->item(0)->nodeValue,
                  'TipoNivelConta' => $xml->getElementsByTagName( "TipoNivelConta" )->item(0)->nodeValue,
                  'NumNivelConta' => $xml->getElementsByTagName( "NumNivelConta" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodRubrica', $item['CodRubrica'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodRubrica', $item['CodRubrica'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RubricasRecDesp.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodRubrica' => $xml->getElementsByTagName( "CodRubrica" )->item(0)->nodeValue,
                  'Especificacao' => $xml->getElementsByTagName( "Especificacao" )->item(0)->nodeValue,
                  'TipoNivelConta' => $xml->getElementsByTagName( "TipoNivelConta" )->item(0)->nodeValue,
                  'NumNivelConta' => $xml->getElementsByTagName( "NumNivelConta" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica('sicap_RubricaDespesa', 'CodRubrica', $item['CodRubrica'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, 'sicap_RubricaDespesa');
                }

                // if (count($this->sicaps_model->getVerifica('sicap_RubricaDespesa', 'CodRubrica', $item['CodRubrica'])) == 0) {
                //   $this->sicaps_model->insert($item, 'sicap_RubricaDespesa');
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'SubFuncao.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodSubFuncao' => $xml->getElementsByTagName( "CodSubFuncao" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodSubFuncao', $item['CodSubFuncao'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodSubFuncao', $item['CodSubFuncao'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'UndOrcamentaria.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach( $items as $key => $xml )
              {
                if ($xml->getElementsByTagName( "Descricao" )->length != 0) {
                  $descricao = $xml->getElementsByTagName( "Descricao" )->item(0)->nodeValue;
                } else {
                  $descricao = "Sem Descrição";
                }
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue,
                  'CodUndOrcamentaria' => $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue,
                  'CNPJ' => $xml->getElementsByTagName( "CNPJ" )->item(0)->nodeValue,
                  'Nome' => $xml->getElementsByTagName( "Nome" )->item(0)->nodeValue,
                  'Identificador' => $xml->getElementsByTagName( "Identificador" )->item(0)->nodeValue,
                  'Descricao' => $descricao,
                );

                if (count($this->sicaps_model->getVerifica($table, 'CodUndOrcamentaria', $item['CodUndOrcamentaria'], $item['CodUndGestora'])) == 0) {
                  $this->sicaps_model->insert($item, $table);
                }

                // if (count($this->sicaps_model->getVerifica($table, 'CodUndOrcamentaria', $item['CodUndOrcamentaria'])) == 0) {
                //   $this->sicaps_model->insert($item, $table);
                // }
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'ReceitaArrecadada.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here
              
              if($doc->getElementsByTagName( explode(".",$file)[0] )->length > 0) {
                $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              } else {
                $items = $doc->getElementsByTagName( $file );
              }
              
              foreach( $items as $key => $xml )
              {
                // echo "<pre>";var_dump($xml->getElementsByTagName( "Valor" )->item(0)->nodeValue." - ".$xml->getElementsByTagName( "CodUndOrcamentaria" )->length);echo "<hr>";
                if ($xml->getElementsByTagName( "CodContaAtivo" )->length != 0) {
                  $codContaAtivo = $xml->getElementsByTagName( "CodContaAtivo" )->item(0)->nodeValue;
                } else {
                  $codContaAtivo = null;
                }
                if ($xml->getElementsByTagName( "FormaArrecadacao" )->length != 0) {
                  $formaArrecadacao = $xml->getElementsByTagName( "FormaArrecadacao" )->item(0)->nodeValue;
                } else {
                  $formaArrecadacao = null;
                }
                if ($xml->getElementsByTagName( "DataRegistro" )->length != 0) {
                  $dataRegistro = $xml->getElementsByTagName( "DataRegistro" )->item(0)->nodeValue;
                } else {
                  if ($xml->getElementsByTagName( "Data" )->length != 0) {
                    $dataRegistro = $xml->getElementsByTagName( "Data" )->item(0)->nodeValue;
                  } else {
                    $dataRegistro = null;
                  }
                }
                if ($xml->getElementsByTagName( "DataArrecadacao" )->length != 0) {
                  $DataArrecadacao = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "DataArrecadacao" )->item(0)->nodeValue)));
                } else {
                  if ($xml->getElementsByTagName( "Data" )->length != 0) {
                    $DataArrecadacao = date('Y-m-d', strtotime(str_replace('/', '-',$xml->getElementsByTagName( "Data" )->item(0)->nodeValue)));
                  } else {
                    $DataArrecadacao = null;
                  }
                }
                if ($xml->getElementsByTagName( "CodUndOrcamentaria" )->length != 0) {
                  $CodUndOrcamentaria = $xml->getElementsByTagName( "CodUndOrcamentaria" )->item(0)->nodeValue;
                } else {
                  $CodUndOrcamentaria = null;
                }
                if ($xml->getElementsByTagName( "CodOrgao" )->length != 0) {
                  $CodOrgao = $xml->getElementsByTagName( "CodOrgao" )->item(0)->nodeValue;
                } else {
                  $CodOrgao = null;
                }


                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'CodOrgao' => $CodOrgao,
                  'CodUndOrcamentaria' => $CodUndOrcamentaria,
                  'CodContaReceita' => $xml->getElementsByTagName( "CodContaReceita" )->item(0)->nodeValue,
                  'CodBanco' => $xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue,
                  'CodAgencia' => $xml->getElementsByTagName( "CodAgencia" )->item(0)->nodeValue,
                  'NumConta' => $xml->getElementsByTagName( "NumConta" )->item(0)->nodeValue,
                  'CodContaAtivo' => $codContaAtivo,
                  'DataArrecadacao' => $DataArrecadacao,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'CodRecVinculado' => $xml->getElementsByTagName( "CodRecVinculado" )->item(0)->nodeValue,
                  'FormaArrecadacao' => $formaArrecadacao,
                  'DataRegistro' => $dataRegistro,
                );
                  $this->sicaps_model->insert($item, $table);
              }
              unlink( "./uploads/".$file );
            }
            if ($file == 'RecDespExtraOrcamentarias.xml') {
              $doc = new DOMDocument();
              $doc->load( "./uploads/".$file );//xml file loading here

              $items = $doc->getElementsByTagName( explode(".",$file)[0] );
              foreach ($items as $key => $xml) {
                $item = array(
                  'CodUndGestora' => $xml->getElementsByTagName( "CodUndGestora" )->item(0)->nodeValue,
                  'CodigoUA' => $xml->getElementsByTagName( "CodigoUA" )->item(0)->nodeValue,
                  'Bimestre' => $xml->getElementsByTagName( "Bimestre" )->item(0)->nodeValue,
                  'Exercicio' => $xml->getElementsByTagName( "Exercicio" )->item(0)->nodeValue,
                  'NumerodaExtraOrcamentario' => $xml->getElementsByTagName( "NumerodaExtraOrcamentario" )->item(0)->nodeValue,
                  'CodContaBalancete' => $xml->getElementsByTagName( "CodContaBalancete" )->item(0)->nodeValue,
                  'IdentificadorDC' => $xml->getElementsByTagName( "IdentificadorDC" )->item(0)->nodeValue,
                  'Valor' => $xml->getElementsByTagName( "Valor" )->item(0)->nodeValue,
                  'IdentificadorDR' => $xml->getElementsByTagName( "IdentificadorDR" )->item(0)->nodeValue,
                  'TipoMovimentacao' => $xml->getElementsByTagName( "TipoMovimentacao" )->item(0)->nodeValue,
                  'Classificacao' => $xml->getElementsByTagName( "Classificacao" )->item(0)->nodeValue,
                  'CodBanco' => $xml->getElementsByTagName( "CodBanco" )->item(0)->nodeValue,
                  'CodAgenciaBanco' => $xml->getElementsByTagName( "CodAgenciaBanco" )->item(0)->nodeValue,
                  'NumContaCorrente' => $xml->getElementsByTagName( "NumContaCorrente" )->item(0)->nodeValue,
                  'NumDocumento' => $xml->getElementsByTagName( "NumDocumento" )->item(0)->nodeValue,
                  'TipoPagamento' => $xml->getElementsByTagName( "TipoPagamento" )->item(0)->nodeValue,
                  'created' => date('Y-m-d H:i:s')
                );
                $this->sicaps_model->insert($item, $table);
              }
              unlink( "./uploads/".$file );
            }
          }
          // exit;
        }
      }
      if($this->input->post('type') == 'sagres_txt') {
        if ($_FILES['import']['name']) {
          for ($i=0; $i < count($_FILES['import']['name']); $i++) {
            // $this->do_upload_anexo($_FILES['import']['name'][$i],$_FILES['import']['tmp_name'][$i],$_FILES['import']['size'][$i]);
          }
          foreach ($_FILES['import']['name'] as $key => $file) {
            $file_name = substr($file, 12,-4);
            $table = "sagres_".$file_name;
            if ($file_name == 'Fornecedores') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    // 'zeros_tce_1' => (strlen(trim(substr($linha, 0, 6))) > 0) ? substr($linha, 0, 6) : null,
                    'cpf_cnpj' => (strlen(trim(substr($linha, 6, 14))) > 0) ? trim(substr($linha, 6, 14)) : null,
                    'nome' => (strlen(trim(substr($linha, 20, 80))) > 0) ? substr($linha, 20, 80) : null,
                    'tipo_credor' => (strlen(trim(substr($linha, 100, 1))) > 0) ? substr($linha, 100, 1) : null,
                    'sigla_uf' => (strlen(trim(substr($linha, 101, 2))) > 0) ? substr($linha, 101, 2) : null,
                    'municipio' => (strlen(trim(substr($linha, 103, 60))) > 0) ? substr($linha, 103, 60) : null,
                    // 'zeros_tce_2' => (strlen(trim(substr($linha, 163, 6))) > 0) ? substr($linha, 163, 6) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'cpf_cnpj', $item['cpf_cnpj'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
                
              }

            }
            if ($file_name == 'Empenhos') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $data_dia = substr(substr($linha, 61, 8), 0, 2);
                  $data_mes = substr(substr($linha, 61, 8), 2, 2);
                  $data_ano = substr(substr($linha, 61, 8), 4, 4);
           
                  $item = array(
                    // 'zeros_tce_1' => (strlen(trim(substr($linha, 0, 6))) > 0) ? substr($linha, 0, 6) : null,
                    'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                    'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null, // Unidade Orçamentária
                    'funcao' => (strlen(trim(substr($linha, 20, 2))) > 0) ? substr($linha, 20, 2) : null, // Função
                    'subfuncao' => (strlen(trim(substr($linha, 22, 3))) > 0) ? substr($linha, 22, 3) : null, // Subfunção
                    'programa' => (strlen(trim(substr($linha, 25, 4))) > 0) ? substr($linha, 25, 4) : null, // Programa
                    'acao' => (strlen(trim(substr($linha, 29, 6))) > 0) ? substr($linha, 29, 6) : null, // Projeto / Atividade
                    'id_acao' => (strlen(trim(substr($linha, 35, 1))) > 0) ? substr($linha, 35, 1) : null,
                    // 'zeros_tce_2' => (strlen(trim(substr($linha, 36, 6))) > 0) ? substr($linha, 36, 6) : null,
                    'cod_cat_econ' => (strlen(trim(substr($linha, 42, 1))) > 0) ? substr($linha, 42, 1) : null,
                    'cod_nat_desp' => (strlen(trim(substr($linha, 43, 1))) > 0) ? substr($linha, 43, 1) : null,
                    'modalidade_aplic' => (strlen(trim(substr($linha, 44, 2))) > 0) ? substr($linha, 44, 2) : null,
                    'cod_elem_desp_dot' => (strlen(trim(substr($linha, 46, 2))) > 0) ? substr($linha, 46, 2) : null,
                    'sub_elem_desp' => (strlen(trim(substr($linha, 48, 3))) > 0) ? substr($linha, 48, 3) : null,
                    'modalidade_licit' => (strlen(trim(substr($linha, 51, 2))) > 0) ? substr($linha, 51, 2) : null,
                    'num_empenho' => (strlen(trim(substr($linha, 53, 7))) > 0) ? substr($linha, 53, 7) : null, // Número
                    'tipo_empenho' => (strlen(trim(substr($linha, 60, 1))) > 0) ? substr($linha, 60, 1) : null,
                    'data_emissao_empenho' => (strlen(trim(substr($linha, 61, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null, // Data
                    'valor_empenhado' => (strlen(trim(substr($linha, 69, 16))) > 0) ? substr($linha, 69, 16) : null, // Valor
                    'historico' => (strlen(trim(substr($linha, 85, 510))) > 0) ? substr($linha, 85, 510) : null, // Histórico
                    'cpf_cnpj_credor' => (strlen(trim(substr($linha, 585, 49))) > 0) ? explode(' ', trim(substr($linha, 585, 49)))[0] : null, // Credor
                    'num_procedimento' => (strlen(trim(substr($linha, 609, 9))) > 0) ? substr($linha, 609, 9) : null, // Numero do Processo
                    'font_recurso' => (strlen(trim(substr($linha, 618, 6))) > 0) ? substr($linha, 618, 6) : null, // Fonte de Recurso
                    'cpf_ordenador' => (strlen(trim(substr($linha, 624, 11))) > 0) ? trim(substr($linha, 624, 11)) : null,
                    'cod_elem_desp' => (strlen(trim(substr($linha, 635, 2))) > 0) ? substr($linha, 635, 2) : null, // Elemento de Despesa
                    // 'zeros_tce_3' => (strlen(trim(substr($linha, 637, 6))) > 0) ? substr($linha, 637, 6) : null
                    'deleted' => false,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'num_empenho', $item['num_empenho'], $item['ano'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }

              }
            }
            if ($file_name == 'UnidadeOrcamentaria') {
              
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));
                if(trim($linha) != ''){
                  $item = array(
                    // 'zeros_tce_1' => (strlen(trim(substr($linha, 0, 6))) > 0) ? substr($linha, 0, 6) : null,
                    'codigo' => (strlen(trim(substr($linha, 6, 10))) > 0) ? substr($linha, 6, 10) : null,
                    'denominacao' => (strlen(trim(substr($linha, 16, 50))) > 0) ? substr($linha, 16, 50) : null,
                    'num_unid_jurisd' => (strlen(trim(substr($linha, 66, 6))) > 0) ? substr($linha, 66, 6) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'Funcoes') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    'codigo' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                    'nome' => (strlen(trim(substr($linha, 2, 30))) > 0) ? utf8_decode(substr($linha, 2, 30)) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'SubFuncoes') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    'cod_funcao' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                    'codigo' => (strlen(trim(substr($linha, 2, 3))) > 0) ? substr($linha, 2, 3) : null,
                    'nome' => (strlen(trim(substr($linha, 5, 68))) > 0) ? utf8_decode(substr($linha, 5, 68)) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
              
            }
            if ($file_name == 'Programas') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    'codigo' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                    'nome' => (strlen(trim(substr($linha, 10, 70))) > 0) ? substr($linha, 10, 70) : null,
                    'descricao' => (strlen(trim(substr($linha, 80, 150))) > 0) ? substr($linha, 80, 150) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'Acao') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    'codigo' => (strlen(trim(substr($linha, 6, 6))) > 0) ? substr($linha, 6, 6) : null,
                    'nome' => (strlen(trim(substr($linha, 12, 70))) > 0) ? substr($linha, 12, 70) : null,
                    'identificacao' => (strlen(trim(substr($linha, 82, 1))) > 0) ? substr($linha, 82, 1) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'ElementoDespesa') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $item = array(
                    'codigo' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                    'descricao' => (strlen(trim(substr($linha, 2, 87))) > 0) ? utf8_decode(substr($linha, 2, 87)) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerifica($table, 'codigo', $item['codigo'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'Liquidacao') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));
                
                if(trim($linha) != ''){
                  $data_dia = substr(substr($linha, 34, 8), 0, 2);
                  $data_mes = substr(substr($linha, 34, 8), 2, 2);
                  $data_ano = substr(substr($linha, 34, 8), 4, 4);

                  $item = array(
                    'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                    'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null,
                    'num_empenho' => (strlen(trim(substr($linha, 20, 7))) > 0) ? substr($linha, 20, 7) : null,
                    'num_liquidacao' => (strlen(trim(substr($linha, 27, 7))) > 0) ? substr($linha, 27, 7) : null,
                    'data' => (strlen(trim(substr($linha, 34, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null, // Data
                    'valor' => (strlen(trim(substr($linha, 42, 16))) > 0) ? substr($linha, 42, 16) : null, // Valor
                    'tipo_doc' => (strlen(trim(substr($linha, 58, 1))) > 0) ? substr($linha, 58, 1) : null, 
                    'num_chave' => (strlen(trim(substr($linha, 59, 44))) > 0) ? substr($linha, 59, 44) : null,
                    'historico' => (strlen(trim(substr($linha, 103, 510))) > 0) ? substr($linha, 103, 510) : null,
                    'cod_fonte_recurso' => (strlen(trim(substr($linha, 613, 6))) > 0) ? substr($linha, 613, 6) : null,
                    'created' => date('Y-m-d H:i:s')
                  );
                  // echo "<pre>";
                  // var_dump($item);
                  if (count($this->sagres_model->getVerificaLiquidacao($table, $item['num_liquidacao'], $item['num_empenho'], $item['ano'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
              // exit;
            }
            if ($file_name == 'Pagamentos') {
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $data_dia = substr(substr($linha, 34, 8), 0, 2);
                  $data_mes = substr(substr($linha, 34, 8), 2, 2);
                  $data_ano = substr(substr($linha, 34, 8), 4, 4);

                  $item = array(
                    'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                    'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null,
                    'num_empenho' => (strlen(trim(substr($linha, 20, 7))) > 0) ? substr($linha, 20, 7) : null,
                    'num_parc_pagamento' => (strlen(trim(substr($linha, 27, 7))) > 0) ? substr($linha, 27, 7) : null,
                    'data' => (strlen(trim(substr($linha, 34, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null, // Data
                    'created' => date('Y-m-d H:i:s')
                  );
                  if (count($this->sagres_model->getVerificaPagamento($table, $item['num_parc_pagamento'], $item['num_empenho'], $item['ano'])) == 0) {
                    $this->sagres_model->insert($item, $table);
                  }
                }
              }
            }
            if ($file_name == 'ItemPagamento') {
              $table = "sagres_Pagamentos";
              $file_txt = fopen ($_FILES['import']['tmp_name'][$key], "r");
              $items = array();
              while (!feof ($file_txt)) {
                $linha = utf8_encode(fgets($file_txt, 4096));

                if(trim($linha) != ''){
                  $ano = substr($linha, 6, 4);
                  $num_empenho = substr($linha, 20, 7);
                  $num_parc_pagamento = substr($linha, 27, 7);

                  $item = array(
                    'valor' => (strlen(trim(substr($linha, 34, 16))) > 0) ? substr($linha, 34, 16) : null,
                    'cod_banco' => (strlen(trim(substr($linha, 79, 3))) > 0) ? substr($linha, 79, 3) : null,
                    'num_agencia_bancaria' => (strlen(trim(substr($linha, 82, 6))) > 0) ? substr($linha, 82, 6) : null,
                    'num_conta_bancaria' => (strlen(trim(substr($linha, 88, 12))) > 0) ? substr($linha, 88, 12) : null,
                    'num_seq' => (strlen(trim(substr($linha, 107, 7))) > 0) ? substr($linha, 107, 7) : null
                  );
                  $pagamento = $this->sagres_model->getVerificaPagamento($table, $num_parc_pagamento, $num_empenho, $ano);
                  if ($pagamento) {
                    $this->sagres_model->update($item, $table, $pagamento['id']);
                  }
                }
              }
            }
          }
        }
      }
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Arquivos importados com sucesso!"));
      redirect('admin/imports/add');
    } else {
      $settings = $this->settings_model->select_id();
      $dados = array(
        'page_title' => 'Prestação de Contas',
        'page_subtitle' => 'Lista',
        'settings' => $settings,
      );

      $this->load->view_admin('admin/empenhos2/add', $dados);
    }

  }
  public function add_anexo($id)
  {
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'report_id' => $id,
      'anexo' => $this->do_upload_anexo()
    );
    $this->empenhosanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('empenhos');
  }
  public function remove_anexo($id)
  {
    $report = $this->empenhosanexos_model->select_id($id);
    $this->empenhosanexos_model->delete($id);
    $this->last_update();
    unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('empenhos');
  }
  public function liquidacao_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->imports_model->select_num($empenho_id);
      $liquidacao = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      $this->imports_model->liquidacao_insert($liquidacao);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação inserida com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function pagamento_add($empenho_id)
  {
    if ($this->input->post()) {
      $empenho = $this->imports_model->select_num($empenho_id);
      $pagamento = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'empenho_id' => $empenho_id,
        'created' => date('Y-m-d H:i:s')
      );
      // echo $empenho['numero'];
      // exit;
      $this->imports_model->pagamento_insert($pagamento);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Pagamento inserido com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function edit1($id)
  {
    $empenho = $this->imports_model->select_num($id);

    if ($this->input->post()) {
      if (empty($_FILES['anexo']['name'])) {
        $anexo = $empenho['anexo'];
      } else {
        $anexo = $this->do_upload_anexo();
      }
      $empenho_up = array(
        'numero' => $this->input->post('numero'),
        'credor' => $this->input->post('credor'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'unidade_orcamentaria' => $this->input->post('unidade_orcamentaria'),
        'funcao' => $this->input->post('funcao'),
        'subfuncao' => $this->input->post('subfuncao'),
        'programa' => $this->input->post('programa'),
        'acao' => $this->input->post('acao'),
        'fonte_de_recurso' => $this->input->post('fonte_de_recurso'),
        'elemento_de_despesa' => $this->input->post('elemento_de_despesa'),
        'cpf_cnpj' => $this->input->post('cpf_cnpj'),
        'numero_do_processo' => $this->input->post('numero_do_processo'),
        'historico' => $this->input->post('historico'),
        'anexo' => $anexo,
      );
      $this->imports_model->update($empenho_up, $id);
      if (!empty($_FILES['anexo']['name'])) {
        unlink("./{$empenho['anexo']}");
      }
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Editar',
      'empenho' => $empenho,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/empenhos2/edit', $dados);
  }
  public function liquidacao_edit($id)
  {
    if ($this->input->post()) {
      $liquidacao = $this->imports_model->liquidacoes_select_id($id);
      $empenho = $this->imports_model->select_num($liquidacao['empenho_id']);
      $liquidacaoUp = array(
        'numero' => $this->input->post('numero'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->imports_model->liquidacao_update($liquidacaoUp, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function pagamento_edit($id)
  {
    if ($this->input->post()) {
      $pagamento = $this->imports_model->pagamentos_select_id($id);
      $empenho = $this->imports_model->select_num($pagamento['empenho_id']);
      $pagamentoUp = array(
        'banco' => $this->input->post('banco'),
        'agencia' => $this->input->post('agencia'),
        'conta' => $this->input->post('conta'),
        'documento' => $this->input->post('documento'),
        'data' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('data')))),
        'valor' => $this->input->post('valor')
      );
      $this->imports_model->pagamento_update($pagamentoUp, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Liquidação alterada com sucesso!"));
      redirect('admin/empenhos2/details/'.$empenho['numero']);
    }
  }
  public function details($num)
  {
    if ($this->uri->segment(3) == "tc") {
      $model = $this->imports_model;
      $view = "details";
      $field_emp = "id";
    }
    if ($this->uri->segment(3) == "sicap") {
      $model = $this->sicaps_model;
      $view = "details2";
      $field_emp = "NumEmpenho";
    }
    if ($this->uri->segment(3) == "sagres") {
      $model = $this->sagres_model;
      $view = "sagres_details";
      $field_emp = "num_empenho";
    }
    $empenho = $model->select_num($num);
    $liquidacoes = $model->liquidacoes_select($empenho[$field_emp]);
    $pagamentos = $model->pagamentos_select($empenho[$field_emp]);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'Empenhos',
      'page_subtitle' => 'Detalhes',
      'empenho' => $empenho,
      'liquidacoes' => $liquidacoes,
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin("admin/empenhos2/{$view}", $dados);

  }

  public function remove($id)
  {
    $this->imports_model->delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho removido com sucesso!"));
    redirect('admin/imports/tc');
  }
  public function remove_sicap($id)
  {
    $empenho = array('deleted' => true);
    $this->sicaps_model->update($empenho,$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho enviado para lixeira!"));
    redirect('imports/index2');
  }
  public function remove_sagres($id)
  {
    $empenho = array('deleted' => true);
    $this->sagres_model->update($empenho,'sagres_Empenhos',$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho enviado para lixeira!"));
    redirect('imports/index3');
  }
  public function back_sicap($id)
  {
    $empenho = array('deleted' => false);
    $this->sicaps_model->update($empenho,$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho exibido com sucesso!"));
    redirect('imports/index2');
  }
  public function back_sagres($id)
  {
    $empenho = array('deleted' => false);
    $this->sagres_model->update($empenho,'sagres_Empenhos',$id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Empenho exibido com sucesso!"));
    redirect('imports/index3');
  }
  public function remove_liquidacao($id)
  {
    $liquidacao = $this->imports_model->liquidacoes_select_id($id);
    $empenho = $this->imports_model->select_num($liquidacao['empenho_id']);
    $this->imports_model->liquidacao_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Liquidação removida com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }
  public function remove_pagamento($id)
  {
    $pagamento = $this->imports_model->pagamentos_select_id($id);
    $empenho = $this->imports_model->select_num($pagamento['empenho_id']);
    $this->imports_model->pagamento_delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Pagamento removido com sucesso!"));
    redirect('admin/empenhos2/details/'.$empenho['numero']);
  }

  public function do_upload_anexo($file,$tmp_name,$size)
  {
    auth();
    $config['upload_path'] = './uploads';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = FALSE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);

    // $xmls = array();
    // $count = count($_FILES['import']['name']);

    // for ($i=0; $i < $count; $i++) {

      $_FILES['xml']['name']= $file;
      $_FILES['xml']['type']= "text/xml";
      $_FILES['xml']['tmp_name']= $tmp_name;
      $_FILES['xml']['error']= "";
      $_FILES['xml']['size']= $size;

      // $config['file_name'] = $_FILES['xml']['name'];


      if ( ! $this->upload->do_upload('xml'))
      {
        // $this->upload->data();
        $error = array('error' => $this->upload->display_errors());

        $this->session->set_flashdata(array("danger" => $error['error']));
        redirect("imports/add");

      }

      return './uploads/'.$this->upload->file_name;
    // }

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function banco_update()
  {
    if ($this->db->query(file_get_contents("./assets/tsa2.sql"))) {
      echo "Banco de Dados Atualizado!";
    } else {
      echo "Falha ao Atualizar!";
    }

  }

  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
