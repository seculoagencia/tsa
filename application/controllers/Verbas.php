<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verbas extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('verbas_model');
        $this->load->model('verbasanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $verbas = $this->verbas_model->select_search($this->input->get('search'));
      } else {
        $verbas = $this->verbas_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $verbas = $this->verbas_model->select($year_key = date('Y'),$month_key = date('n'));
      $verbas = $this->verbas_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'verbas' => $verbas,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/verbas/index', $dados);
  }

//   verbasanexos_model
  public function add()
  {

      $verba = array(
        'data' => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('data')))),
        'valor' => $this->input->post('valor'),
        'beneficiario' => $this->input->post('beneficiario'),
        'mes' => $this->input->post('mes'),
        'created' => date('Y-m-d H:i:s'),
      );
      $this->verbas_model->insert($verba);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "verba criada com sucesso!"));
      redirect('admin/verbas');

  }
  public function add_anexo($verba_id)
  {
    if (empty($_FILES['anexo']['name'])) {
        $anexo = $report['anexo'];
    } else {
        $anexo = $this->do_upload_anexo();
    }
    $report = array(
      'verba_id' => $verba_id,
      'anexo' => $this->do_upload_anexo('anexo')
    );
    $this->verbasanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para verbas criado com sucesso!"));
    redirect('verbas');
  }
  public function remove_anexo($id)
  {
    $report = $this->verbasanexos_model->select_id($id);
    $this->verbasanexos_model->delete($id);
    $this->last_update();
    // unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('verbas');
  }
  public function edit($id)
  {

    $verba_up = array(
      'data' => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('data')))),
      'valor' => $this->input->post('valor'),
      'beneficiario' => $this->input->post('beneficiario'),
      'mes' => $this->input->post('mes'),
    );
    $this->verbas_model->update($verba_up, $id);
    $this->last_update();

    $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
    redirect('admin/verbas');

  }
  public function details($num)
  {
    $verba = $this->verbas_model->select_id($num);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'verbas',
      'page_subtitle' => 'Detalhes',
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/verbas/details', $dados);
  }
  //
  public function remove($id)
  {
    $verba = $this->verbas_model->select_id($id);
    $this->verbas_model->delete($id);
    $this->last_update();
    // unlink("./{$verba['anexo']}");
    $this->session->set_flashdata(array("success" => "verba removida com sucesso!"));
    redirect('verbas');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("verbas");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function import()
  {
    if ($this->input->post()) {
      var_dump($_FILES['import']['name']);
      exit;
      if ($_FILES['import']['name']) {
        $this->do_upload_anexo($_FILES['import']['name'],$_FILES['import']['tmp_name'],$_FILES['import']['size']);
      }

      $file = $_FILES['import']['name'];
    }
  }
  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }

}
