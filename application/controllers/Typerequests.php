<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Typerequests extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('typerequests_model');
        $this->load->model('requests_model');
        $this->load->model('users_model');
        $this->load->model('settings_model');

    }
  public function index()
  {
    $settings = $this->settings_model->select_id();
    $typerequests = $this->typerequests_model->select_all();
    $dados = array(
      'page_title' => 'Tipos de Solicitações',
      'page_subtitle' => 'Lista',
      'typerequests' => $typerequests,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/typerequests/index', $dados);
  }
  public function add()
  {
    $typerequest = array(
      'name' => $this->input->post('name'),
    );
    $this->typerequests_model->insert($typerequest);
    $this->session->set_flashdata(array("success" => "Tipo de solicitação criado com sucesso!"));
    redirect('admin/typerequests');
  }
  public function edit($id)
  {
    if ($this->input->post()) {
      $typerequest = array(
        'name' => $this->input->post('name'),
      );
      $this->typerequests_model->update($typerequest, $id);
      $this->session->set_flashdata(array("success" => "Tipo de solicitação alterado com sucesso!"));
      redirect('admin/typerequests');
    }
    $settings = $this->settings_model->select_id();
    $typerequest = $this->typerequests_model->select($id);
    $dados = array(
      'page_title' => 'Tipos de Solicitações',
      'page_subtitle' => 'Detalhes',
      'settings' => $settings,
    );
    $this->load->view_admin('admin/typerequests/edit', $dados);
  }

  public function remove($id)
  {
    $this->typerequests_model->delete($id);
    $this->session->set_flashdata(array("success" => "Tipo de solicitação removido com sucesso!"));
    redirect('admin/typerequests');
  }
}
