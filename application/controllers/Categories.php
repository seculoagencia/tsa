<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('reports_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $dados = array(
      'page_title' => 'Categorias',
      'page_subtitle' => 'Lista',
      'categories' => $categories,
      'subcategories' => $subcategories,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/categories/index', $dados);
  }
  public function add_category()
  {
    $category = array(
      'name' => $this->input->post('name'),
    );
    $this->categories_model->insert($category);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Categoria <strong>".$this->input->post('name')."</strong> criada com sucesso!"));
    redirect('categories');
  }
  public function add_subcategory()
  {
    $subcategory = array(
      'name' => $this->input->post('name'),
      'category_id' => $this->input->post('category_id'),
    );
    $this->subcategories_model->insert($subcategory);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Sub-Categoria <strong>".$this->input->post('name')."</strong> criada com sucesso!"));
    redirect('admin/categories');
  }
  public function edit_category($id)
  {
    if ($this->input->post()) {
      $categoria = array(
        'name' => $this->input->post('name'),
      );
      $this->categories_model->update($categoria, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Categoria alterada com sucesso!"));
      redirect('admin/categories');
    }
  }
  public function edit_subcategory($id)
  {
    if ($this->input->post()) {
      $subcategoria = array(
        'name' => $this->input->post('name'),
        'category_id' => $this->input->post('category_id'),
      );
      $this->subcategories_model->update($subcategoria, $id);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "Sub-Categoria alterada com sucesso!"));
      redirect('admin/categories');
    }
  }

  public function remove_category($id)
  {
    // $category = $this->categories_model->select_id($id);
    $subcategories = $this->subcategories_model->select();
    foreach ($subcategories as $subcategory) {
      if ($subcategory['category_id'] == $id) {
        $this->subcategories_model->delete($subcategory['id']);
      }
    }
    $this->categories_model->delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Categoria removida com sucesso!"));
    redirect('admin/categories');
  }
  public function remove_subcategory($id)
  {
    $this->subcategories_model->delete($id);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Sub-Categoria removida com sucesso!"));
    redirect('admin/categories');
  }
  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
