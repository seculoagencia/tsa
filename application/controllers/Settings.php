<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->model('settings_model');
  }
  public function index()
  {
    $settings = $this->settings_model->select_id();
    $dados = array(
      'settings' => $settings,
    );
    $this->load->view_admin('admin/settings/index', $dados);
  }
  public function edit($id)
  {
    if ($this->input->post()) {
      $setting = array(
        'name' => $this->input->post('name'),
        'site_url' => $this->input->post('site_url'),
        'email' => $this->input->post('email'),
        'view_last_update' => ($this->input->post('view_last_update') == 'on') ? true:false,
        'recaptcha_site' => $this->input->post('recaptcha_site'),
        'recaptcha_secret' => $this->input->post('recaptcha_secret'),
      );
      $this->settings_model->update($setting, $id);
      $this->session->set_flashdata(array("success" => "Configurações alteradas com sucesso!"));
      redirect('admin/settings');
    }
  }
}
