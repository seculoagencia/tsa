<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servidores extends CI_Controller {
  function __construct()
    {
        parent::__construct();
        auth();
        $this->load->model('groups_model');
        $this->load->model('users_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->load->model('servidores_model');
        $this->load->model('servidoresanexos_model');
        $this->load->model('settings_model');
    }
  public function index()
  {
    if ($this->input->get()) {
      if ($this->input->get('search')) {
        $servidores = $this->servidores_model->select_search($this->input->get('search'));
      } else {
        $servidores = $this->servidores_model->select($year_key = $this->input->get('year'),$month_key = $this->input->get('month'));
      }

    } else {
      // $servidores = $this->servidores_model->select($year_key = date('Y'),$month_key = date('n'));
      $servidores = $this->servidores_model->select_all();
    }
    $settings = $this->settings_model->select_id();
    $categories = $this->categories_model->select();
    $subcategories = $this->subcategories_model->select();
    $month = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

    $year = range(2030,1956);

    $dados = array(
      'page_title' => 'Prestação de Contas',
      'page_subtitle' => 'Lista',
      'servidores' => $servidores,
      'categories' => $categories,
      'subcategories' => $subcategories,
      'month' => $month,
      'year' => $year,
      'settings' => $settings,
    );

    $this->load->view_admin('admin/servidores/index', $dados);
  }

//   servidoresanexos_model
  public function add()
  {

      $servidor = array(
        'nome' => $this->input->post('nome'),
        'valor_bruto' => $this->input->post('valor_bruto'),
        'descontos' => $this->input->post('descontos'),
        'valor_liquido' => $this->input->post('valor_liquido'),
        'mes_ref' => $this->input->post('mes')."/".$this->input->post('ano'),
        'created' => date('Y-m-d H:i:s'),
      );
      $this->servidores_model->insert($servidor);
      $this->last_update();
      $this->session->set_flashdata(array("success" => "servidor criada com sucesso!"));
      redirect('admin/servidores');

  }
  public function add_anexo()
  {
    // if (empty($_FILES['anexo']['name'])) {
    //     $anexo = $report['anexo'];
    // } else {
    //     $anexo = $this->do_upload_anexo();
    // }
    $report = array(
      'report_id' => $this->input->post('report_id'),
      'anexo' => $this->do_upload_anexo('anexo')
    );
    $this->servidoresanexos_model->insert($report);
    $this->last_update();
    $this->session->set_flashdata(array("success" => "Anexo para empenhos criado com sucesso!"));
    redirect('servidores');
  }
  public function remove_anexo($id)
  {
    $report = $this->servidoresanexos_model->select_id($id);
    $this->servidoresanexos_model->delete($id);
    $this->last_update();
    // unlink("./{$report['anexo']}");
    $this->session->set_flashdata(array("success" => "Anexo removido com sucesso!"));
    redirect('servidores');
  }
  public function edit($id)
  {
    $servidor = $this->servidores_model->select_id($id);
    if (empty($_FILES['anexo']['name'])) {
      $anexo = $servidor['anexo'];
    } else {
      $anexo = $this->do_upload_anexo();
    }
    $servidor_up = array(
      'nome' => $this->input->post('nome'),
      'valor_bruto' => $this->input->post('valor_bruto'),
      'descontos' => $this->input->post('descontos'),
      'valor_liquido' => $this->input->post('valor_liquido'),
      'mes_ref' => $this->input->post('mes')."/".$this->input->post('ano'),
      'anexo' => $anexo,
    );
    $this->servidores_model->update($servidor_up, $id);
    $this->last_update();
    // if (!empty($_FILES['anexo']['name'])) {
    //   unlink("./{$servidor['anexo']}");
    // }
    $this->session->set_flashdata(array("success" => "Alterações feitas com sucesso!"));
    redirect('admin/servidores');

  }
  public function details($num)
  {
    $servidor = $this->servidores_model->select_id($num);
    $settings = $this->settings_model->select_id();
    $dados = array(
      'page_title' => 'servidores',
      'page_subtitle' => 'Detalhes',
      'pagamentos' => $pagamentos,
      'settings' => $settings,
    );
    $this->load->view_admin('admin/servidores/details', $dados);
  }
  //
  public function remove($id)
  {
    $servidor = $this->servidores_model->select_id($id);
    $this->servidores_model->delete($id);
    $this->last_update();
    // unlink("./{$servidor['anexo']}");
    $this->session->set_flashdata(array("success" => "servidor removida com sucesso!"));
    redirect('servidores');
  }

  public function do_upload_anexo()
  {
    auth();
    $config['upload_path'] = './uploads/anexos/';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;
    $config['max_size'] = 140000;

    $this->upload->initialize($config);
    $this->load->library('upload', $config);


    if ( ! $this->upload->do_upload('anexo'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->session->set_flashdata(array("danger" => $error['error']));
      redirect("servidores");
    }

    return 'uploads/anexos/'.$this->upload->file_name;

  }

  public function consultar_subcategorias($category_id)
  {
    $subcategory = $this->subcategories_model->select_for_category($category_id);
    echo json_encode($subcategory);
  }

  public function import()
  {
    if ($this->input->post()) {
      var_dump($_FILES['import']['name']);
      exit;
      if ($_FILES['import']['name']) {
        $this->do_upload_anexo($_FILES['import']['name'],$_FILES['import']['tmp_name'],$_FILES['import']['size']);
      }

      $file = $_FILES['import']['name'];
    }
  }
  public function last_update()
  {
    $update = array(
      'last_update' => date('Y-m-d H:i:s')
    );
    $this->settings_model->update($update, 1);
  }
}
