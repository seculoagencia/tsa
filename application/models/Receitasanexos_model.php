<?php

class Receitasanexos_model extends CI_Model {
	public function select($report_id)
	{
		$this->db->where('report_id', $report_id);
		return $this->db->get("receitas_anexos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("receitas_anexos", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('receitas_anexos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('receitas_anexos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('receitas_anexos');
	}
}
