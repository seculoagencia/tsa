<?php

class Sicaps_model extends CI_Model {
	public function select($year_key, $month_key)
	{

		$this->db->order_by("DataEmpenho","desc");
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(DataEmpenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(DataEmpenho)",$month_key);
		}
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_page($year_key, $month_key,$limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by("DataEmpenho","desc");
		$this->db->where("deleted",false);
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(DataEmpenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(DataEmpenho)",$month_key);
		}
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_page_record_count($year_key, $month_key)
	{
		$this->db->order_by("DataEmpenho","desc");
		$this->db->where("deleted",false);
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(DataEmpenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(DataEmpenho)",$month_key);
		}
		// return $this->db->count_all("sicap_Empenho");
		return $this->db->get("sicap_Empenho")->num_rows();
	}
	public function select_all()
	{
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_all_no_deleted()
	{
		$this->db->where("deleted",false);
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_all_no_deleted_page($limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->where("deleted",false);
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_all_page($limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_all_no_deleted_page_record_count()
	{
		$this->db->where("deleted",false);
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->count_all("sicap_Empenho");
	}
	public function select_all_page_record_count()
	{
		$this->db->order_by("DataEmpenho","desc");
		return $this->db->count_all("sicap_Empenho");
	}
	public function liquidacoes_select($empenho_id)
	{
		return $this->db->get_where("sicap_Liquidacao", array('NumEmpenho' => $empenho_id))->result_array();
	}
	public function pagamentos_select($empenho_id)
	{
		return $this->db->get_where("sicap_Pagamento", array('NumEmpenho' => $empenho_id))->result_array();
	}
	public function pagamento_fin($empenho_id,$numPag)
	{
		return $this->db->get_where("sicap_PagamentoFinanceiro", array('NumEmpenho' => $empenho_id, 'NumPagamento'=>$numPag))->row_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("sicap_Empenho", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("sicap_Empenho", array('NumEmpenho'=>$id))->row_array();
	}
	public function getVerifica($table,$field,$cod, $uniGestora = null)
	{
		return $this->db->get_where($table, array($field => $cod,'CodUndGestora'=>$uniGestora))->row_array();
	}
	public function getDecretoAltOrcamentaria($cod)
	{
		return $this->db->get_where("sicap_DecretoAltOrcamentaria", array('NumAlteracao' => $cod))->row_array();
	}
	public function credor($cpf,$cnpj)
	{
		if (!empty($cpf)) {
			$this->db->where("CPF",$cpf);
		}
		if (!empty($cnpj)) {
			$this->db->where("CNPJ",$cnpj);
		}
		return $this->db->get("tc_credor")->row_array();
	}
	public function acao($cod)
	{
		return $this->db->get_where("tc_acao", array('codigo'=>$cod))->row_array();
	}
	public function fonte($cod)
	{
		return $this->db->get_where("tc_fonte", array('codigo'=>$cod))->row_array();
	}
	public function rubrica($cod)
	{
		return $this->db->get_where("tc_rubrica", array('codigo'=>$cod))->row_array();
	}
	public function unidade($cod)
	{
		return $this->db->get_where("tc_unidade", array('codigo'=>$cod))->row_array();
	}
	public function funcao($cod)
	{
		return $this->db->get_where("tc_funcao", array('codigo'=>$cod))->row_array();
	}
	public function subfuncao($cod)
	{
		return $this->db->get_where("tc_subfuncao", array('codigo'=>$cod))->row_array();
	}
	public function programa($cod)
	{
		return $this->db->get_where("tc_programa", array('codigo'=>$cod))->row_array();
	}
	public function dotacao($cod)
	{
		return $this->db->get_where("tc_dotacao", array('rubricacodigo'=>$cod))->row_array();
	}
	public function receitaclassificada($cod)
	{
		return $this->db->get_where("tc_receitaclassificada", array('rubricacodigo'=>$cod))->row_array();
	}
	public function liquidacoes_select_id($id)
	{
		return $this->db->get_where("liquidacoes", array('id'=>$id))->row_array();
	}
	public function pagamentos_select_id($id)
	{
		return $this->db->get_where("pagamentos", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_Empenho")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_Empenho")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("DataEmpenho","desc");
		$this->db->where("NumEmpenho LIKE '%".$search."%'");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_search_page($search,$limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by("DataEmpenho","desc");
		$this->db->where("NumEmpenho LIKE '%".$search."%'");
		return $this->db->get("sicap_Empenho")->result_array();
	}
	public function select_search_page_record_count($search)
	{
		$this->db->order_by("DataEmpenho","desc");
		$this->db->where("NumEmpenho LIKE '%".$search."%'");
		// return $this->db->count_all("sicap_Empenho");
		return $this->db->get("sicap_Empenho")->num_rows();
	}
	public function insert($item,$table)
	{
		$this->db->insert($table,$item);
	}
	public function liquidacao_insert($grupo)
	{
		$this->db->insert('liquidacoes',$grupo);
	}
	public function pagamento_insert($grupo)
	{
		$this->db->insert('pagamentos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('sicap_Empenho',$grupo);
	}
	public function liquidacao_update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('liquidacoes',$grupo);
	}
	public function pagamento_update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('pagamentos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('sicap_Empenho');
	}
	public function liquidacao_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('liquidacoes');
	}
	public function pagamento_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('pagamentos');
	}

	public function selectRecDespExtraOrcamentarias()
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_RecDespExtraOrcamentarias")->result_array();
	}
}
