<?php

class Servidoresanexos_model extends CI_Model {
	public function select($report_id)
	{
		$this->db->where('report_id', $report_id);
		return $this->db->get("servidores_anexos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("servidores_anexos", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('servidores_anexos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('servidores_anexos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('servidores_anexos');
	}
}
