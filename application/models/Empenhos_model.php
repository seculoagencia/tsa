<?php

class Empenhos_model extends CI_Model {
	public function select($year_key, $month_key)
	{

		$this->db->order_by("created","desc");
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(data)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(data)",$month_key);
		}
		$this->db->order_by("created","desc");
		return $this->db->get("empenhos")->result_array();
	}
	public function select_all()
	{
		$this->db->order_by("created","desc");
		return $this->db->get("empenhos")->result_array();
	}
	public function liquidacoes_select($empenho_id)
	{
		return $this->db->get_where("liquidacoes", array('empenho_id' => $empenho_id))->result_array();
	}
	public function pagamentos_select($empenho_id)
	{
		return $this->db->get_where("pagamentos", array('empenho_id' => $empenho_id))->result_array();
	}
	public function select_num($num)
	{
		return $this->db->get_where("empenhos", array('numero'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("empenhos", array('id'=>$id))->row_array();
	}
	public function liquidacoes_select_id($id)
	{
		return $this->db->get_where("liquidacoes", array('id'=>$id))->row_array();
	}
	public function pagamentos_select_id($id)
	{
		return $this->db->get_where("pagamentos", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("empenhos")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("empenhos")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("created","desc");
		$this->db->where("name LIKE '%".$search."%'");
		return $this->db->get("empenhos")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('empenhos',$grupo);
	}
	public function liquidacao_insert($grupo)
	{
		$this->db->insert('liquidacoes',$grupo);
	}
	public function pagamento_insert($grupo)
	{
		$this->db->insert('pagamentos',$grupo);
	}
	public function update($empenho, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('empenhos',$empenho);
	}
	public function liquidacao_update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('liquidacoes',$grupo);
	}
	public function pagamento_update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('pagamentos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('empenhos');
	}
	public function liquidacao_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('liquidacoes');
	}
	public function pagamento_delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('pagamentos');
	}
}
