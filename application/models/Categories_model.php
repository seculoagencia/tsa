<?php

class Categories_model extends CI_Model {
	public function select()
	{
		$this->db->order_by("id","desc");
		return $this->db->get("categories")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("categories", array('id'=>$id))->row_array();
	}
	public function insert($category)
	{
		$this->db->insert('categories',$category);
	}
	public function update($category, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('categories',$category);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('categories');
	}
}

class Subcategories_model extends CI_Model {
	public function select()
	{
		$this->db->order_by("id","desc");
		return $this->db->get("subcategories")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("subcategories", array('id'=>$id))->row_array();
	}
	public function select_for_category($category_id)
	{
		$this->db->order_by("id","desc");
		return $this->db->get_where("subcategories", array('category_id'=>$category_id))->result_array();
	}
	// public function select_category($id)
	// {
	// 	return $this->db->get_where("subcategories", array('category_id'=>$id))->row_array();
	// }
	public function insert($subcategory)
	{
		$this->db->insert('subcategories',$subcategory);
	}
	public function update($subcategory, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('subcategories',$subcategory);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('subcategories');
	}
}
