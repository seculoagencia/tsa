<?php

class Licitacoes_model extends CI_Model {
	public function select_filter($year_key)
	{

		$this->db->order_by("data","desc");
		$this->db->where(array("YEAR(data)" => $year_key));
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_filter_xlsl($year_key)
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo',2);
		$this->db->where(array("YEAR(data)" => $year_key));
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_filter_xlsc($year_key)
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo',2);
		$this->db->where(array("YEAR(data)" => $year_key));
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select()
	{
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_xlsl()
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo', 2);
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_xlsc()
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo', 2);
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("licitacoes", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("licitacoes", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("data","desc");
		return $this->db->get_where("licitacoes")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("data","desc");
		return $this->db->get_where("licitacoes")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("data","desc");
		$this->db->where("orgao LIKE '%".$search."%' or vencedor LIKE '%".$search."%'");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_search_xlsl($search)
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo', 2);
		$this->db->where("name LIKE '%".$search."%'");
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function select_search_xlsc($search)
	{
		$this->db->select('id,orgao,modalidade,data,objeto,valor,status,vencedor');
		$this->db->where('tipo', 2);
		$this->db->where("name LIKE '%".$search."%'");
		$this->db->order_by("data","desc");
		return $this->db->get("licitacoes")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('licitacoes',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('licitacoes',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('licitacoes');
	}

	public function get_date($date)
	{
		return date('Y-m-d', strtotime($date)).'T'.date('H:i:s', strtotime($date));
	}
}
