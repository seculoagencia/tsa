<?php

class Diariasanexos_model extends CI_Model {
	public function select($report_id)
	{
		$this->db->where('report_id', $report_id);
		return $this->db->get("diarias_anexos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("diarias_anexos", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('diarias_anexos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('diarias_anexos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('diarias_anexos');
	}
}
