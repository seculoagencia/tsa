<?php

class Verbas_model extends CI_Model {
	public function select($year_key,$month_key)
	{

		$this->db->order_by("data","desc");
		// $this->db->where(array("YEAR(created)" => $year_key,"MONTH(created)" => $month_key));
		if (!empty($year_key) && !empty($month_key)) {
			$this->db->where("MONTH(data)", $month_key);
			$this->db->where("YEAR(data)", $year_key);
		}
		if (!empty($year_key) && empty($month_key)) {
			$this->db->where("YEAR(data)", $year_key);
		}
		if (empty($year_key) && !empty($month_key)) {
			$this->db->where("MONTH(data)", $month_key);
		}

		return $this->db->get("verbas")->result_array();
	}
	public function select_all()
	{
		$this->db->order_by("data","desc");
		return $this->db->get("verbas")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("verbas", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("verbas", array('id'=>$id))->row_array();
	}

	public function select_search($search)
	{
		$this->db->order_by("data","desc");
		$this->db->where("beneficiario LIKE '%".$search."%'");
		return $this->db->get("verbas")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('verbas',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('verbas',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('verbas');
	}

	public function retornaMes($num)
  {
		$months = array(
      1 => 'Janeiro',
      2 => 'Fevereiro',
      3 => 'Março',
      4 => 'Abril',
      5 => 'Maio',
      6 => 'Junho',
      7 => 'Julho',
      8 => 'Agosto',
      9 => 'Setembro',
      10 => 'Outubro',
      11 => 'Novembro',
      12 => 'Dezembro',
    );

		return $months[$num];
  }
}
