<?php

class Diarias_model extends CI_Model {
	public function select_filter($year_key)
	{

		$this->db->order_by("created","desc");
		$this->db->where(array("YEAR(data)" => $year_key));
		return $this->db->get("diarias")->result_array();
	}
	public function select()
	{
		$this->db->order_by("created","desc");
		return $this->db->get("diarias")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("diarias", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("diarias", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("diarias")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("diarias")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("created","desc");
		$this->db->where("favorecido LIKE '%".$search."%'");
		return $this->db->get("diarias")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('diarias',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('diarias',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('diarias');
	}
}
