<?php

class Answers_model extends CI_Model {
	public function select($request_id)
	{
		// $this->db->order_by("created","desc");
		$this->db->where(array('request_id' => $request_id));
		return $this->db->get("answers")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("answers", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		return $this->db->get_where("answers", array('category_id'=>$id))->row_array();
	}
	public function select_for_subcategory($id)
	{
		return $this->db->get_where("answers", array('subcategory_id'=>$id))->row_array();
	}
	public function select_for_request($id)
	{
		return $this->db->get_where("answers", array('request_id'=>$id))->result_array();
	}
	public function select_search($search)
	{
		$this->db->where("name LIKE '%".$search."%'");
		return $this->db->get("answers")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('answers',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('answers',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('answers');
	}
}
