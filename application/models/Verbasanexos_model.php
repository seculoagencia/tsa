<?php

class Verbasanexos_model extends CI_Model {
	public function select($verba_id)
	{
		$this->db->where('verba_id', $verba_id);
		return $this->db->get("verbas_anexos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("verbas_anexos", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('verbas_anexos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('verbas_anexos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('verbas_anexos');
	}
}
