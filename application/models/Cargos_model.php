<?php

class Cargos_model extends CI_Model {
	public function select()
	{
		$this->db->order_by("id","desc");
		return $this->db->get("cargos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("cargos", array('id'=>$id))->row_array();
	}
	public function insert($category)
	{
		$this->db->insert('cargos',$category);
	}
	public function update($category, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('cargos',$category);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('cargos');
	}
}

class Servidorecargos_model extends CI_Model {
	public function select()
	{
		$this->db->order_by("id","desc");
		return $this->db->get("servidorcargos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("servidorcargos", array('id'=>$id))->row_array();
	}
	public function insert($category)
	{
		$this->db->insert('servidorcargos',$category);
	}
	public function update($category, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('servidorcargos',$category);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('servidorcargos');
	}
}
