<?php

class Reports_model extends CI_Model {
	public function select($year_key,$category_id = null,$subcategory_id = null)
	{
		if ($subcategory_id == null && $category_id != null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('year' => $year_key,'category_id'=>$category_id));
		} else if ($category_id == null && $subcategory_id != null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('year' => $year_key,'subcategory_id'=>$subcategory_id));
		} else if ($subcategory_id == null && $category_id == null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('year' => $year_key));
		} else {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('year' => $year_key,'category_id'=>$category_id,'subcategory_id'=>$subcategory_id));
		}
		return $this->db->get("reports")->result_array();
	}
	public function select_all($category_id = null,$subcategory_id = null)
	{
		if ($subcategory_id == null && $category_id != null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('category_id'=>$category_id));
		} else if ($category_id == null && $subcategory_id != null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('subcategory_id'=>$subcategory_id));
		} else if ($subcategory_id == null && $category_id == null) {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('year' => $year_key));
		} else {
			$this->db->order_by("year","desc");
			$this->db->order_by("month","desc");
			$this->db->where(array('category_id'=>$category_id,'subcategory_id'=>$subcategory_id));
		}
		return $this->db->get("reports")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("reports", array('id'=>$id))->row_array();
	}
	public function select_alll()
	{
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		return $this->db->get("reports")->result_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		return $this->db->get_where("reports", array("category_id"=>$id))->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		return $this->db->get_where("reports", array('subcategory_id'=>$id))->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		$this->db->where("name LIKE '%".$search."%'");
		return $this->db->get("reports")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('reports',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('reports',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('reports');
	}
}
