<?php

class Settings_model extends CI_Model {
  public function select_id()
  {
    $id = 1;
    return $this->db->get_where('settings', array('id',$id))->row_array();
  }
  public function update($setting, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('settings',$setting);
	}
}
