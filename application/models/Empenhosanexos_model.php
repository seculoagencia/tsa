<?php

class Empenhosanexos_model extends CI_Model {
	public function select($report_id)
	{
		$this->db->where('report_id', $report_id);
		return $this->db->get("empenhos_anexos")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("empenhos_anexos", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('empenhos_anexos',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('empenhos_anexos',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('empenhos_anexos');
	}
}
