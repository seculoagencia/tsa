<?php

class Sservidores_model extends CI_Model {
	public function select($year_key)
	{

		$this->db->order_by("Nome","asc");
		$this->db->where("Exercicio",$year_key);

		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function select_not_delete($year_key)
	{

		$this->db->order_by("Nome","asc");
		$this->db->where("Exercicio",$year_key);
		$this->db->where("deleted",false);

		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function select_all()
	{
		$this->db->order_by("Nome","asc");
		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function select_all_not_delete()
	{
		$this->db->where("deleted",false);
		$this->db->order_by("Nome","asc");
		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("sicap_Servidores", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("sicap_Servidores", array('codigo'=>$id))->row_array();
	}
	public function select_matricula($matricula)
	{
		return $this->db->get_where("sicap_Servidores", array('Matricula'=>$matricula))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_Servidores")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_Servidores")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("Nome","asc");
		$this->db->where("Nome LIKE '%".$search."%'");
		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function select_search_not_delete($search)
	{
		$this->db->order_by("Nome","asc");
		$this->db->where("deleted",false);
		$this->db->where("Nome LIKE '%".$search."%'");
		return $this->db->get("sicap_Servidores")->result_array();
	}
	public function insert($item, $table)
	{
		$this->db->insert($table,$item);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('sicap_Servidores',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('sicap_Servidores');
	}
}
