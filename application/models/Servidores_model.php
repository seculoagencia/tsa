<?php

class Servidores_model extends CI_Model {
	public function select($year_key,$month_key)
	{

		$this->db->select("*, CAST(SUBSTRING_INDEX(mes_ref, '/', -1) as UNSIGNED) as year, CAST(SUBSTRING_INDEX(mes_ref, '/', 1) as UNSIGNED) as month");
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		// $this->db->where(array("YEAR(created)" => $year_key,"MONTH(created)" => $month_key));
		if (!empty($year_key) && !empty($month_key)) {
			$this->db->where("mes_ref", $month_key."/".$year_key);
		}
		if (!empty($year_key) && empty($month_key)) {
			$this->db->where("mes_ref LIKE '%/".$year_key."'");
		}
		if (empty($year_key) && !empty($month_key)) {
			$this->db->where("mes_ref LIKE '".$month_key."/%'");
		}
		// 	var_dump("mes_ref LIKE '".$year_key."/%'");
		// 	$this->db->where("mes_ref LIKE '%/".$year_key);
		// 	$this->db->where("mes_ref LIKE '".$year_key."/%'");
		// 	// $this->db->where("YEAR(data)",$year_key);
		// }
		// if (!empty($month_key)) {
		// 	$this->db->where("mes_ref LIKE '%/".$month_key);
		// 	// $this->db->where("mes_ref LIKE '%".$month_key."'");
		// 	// $this->db->where("MONTH(data)",$month_key);
		// }
		// $this->db->where(array("mes_ref" => $month_key."/".$year_key));
		return $this->db->get("servidores")->result_array();
	}
	public function select_all()
	{
		$this->db->select("*, CAST(SUBSTRING_INDEX(mes_ref, '/', -1) as UNSIGNED) as year, CAST(SUBSTRING_INDEX(mes_ref, '/', 1) as UNSIGNED) as month");
		// $this->db->select("*, CAST(SUBSTRING_INDEX(mes_ref, '/', -1) as UNSIGNED) as year, CAST(SUBSTRING_INDEX(mes_ref, '/', 1) as UNSIGNED) as month");
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		return $this->db->get("servidores")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("servidores", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("servidores", array('id'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->select("*, CAST(SUBSTRING_INDEX(mes_ref, '/', -1) as UNSIGNED) as year, CAST(SUBSTRING_INDEX(mes_ref, '/', 1) as UNSIGNED) as month");
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		return $this->db->get_where("servidores")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("servidores")->row_array();
	}
	public function select_search($search)
	{
		$this->db->select("*, CAST(SUBSTRING_INDEX(mes_ref, '/', -1) as UNSIGNED) as year, CAST(SUBSTRING_INDEX(mes_ref, '/', 1) as UNSIGNED) as month");
		$this->db->order_by("year","desc");
		$this->db->order_by("month","desc");
		$this->db->where("nome LIKE '%".$search."%'");
		return $this->db->get("servidores")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('servidores',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('servidores',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('servidores');
	}
}
