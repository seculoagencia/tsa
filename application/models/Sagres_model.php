<?php

class Sagres_model extends CI_Model {
  public function getVerifica($table,$field,$cod, $ano = null)
	{
    if ($ano == null) {
      return $this->db->get_where($table, array($field => $cod))->row_array();
    } else {
      return $this->db->get_where($table, array($field => $cod,'ano'=>$ano))->row_array();
    }
    
	}

  public function getVerificaLiquidacao($table,$num_liquidacao,$num_empenho, $ano)
	{
    return $this->db->get_where($table, array(
      'num_liquidacao' => $num_liquidacao,
      'num_empenho' => $num_empenho,
      'ano' => $ano
    ))->row_array();
    
	}

  public function getVerificaPagamento($table,$num_parc_pagamento,$num_empenho, $ano)
	{
    return $this->db->get_where($table, array(
      'num_parc_pagamento' => $num_parc_pagamento,
      'num_empenho' => $num_empenho,
      'ano' => $ano
    ))->row_array();
    
	}

  public function insert($item,$table)
	{
		$this->db->insert($table,$item);
	}

  public function select_all()
	{
		$this->db->order_by("data_emissao_empenho","desc");
		return $this->db->get("sagres_Empenhos")->result_array();
	}

  public function select_all_page_record_count()
	{
		$this->db->order_by("data_emissao_empenho","desc");
		return $this->db->count_all("sagres_Empenhos");
	}

  public function select_all_page($limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by("data_emissao_empenho","desc");
		return $this->db->get("sagres_Empenhos")->result_array();
	}

  public function select_num($id)
	{
		return $this->db->get_where("sagres_Empenhos", array('id'=>$id))->row_array();
	}

  public function liquidacoes_select($empenho_id)
	{
		return $this->db->get_where("sagres_Liquidacao", array('num_empenho' => $empenho_id))->result_array();
	}

  public function pagamentos_select($empenho_id)
	{
		return $this->db->get_where("sagres_Pagamentos", array('num_empenho' => $empenho_id))->result_array();
	}

  public function update($item, $table, $id)
	{
		$this->db->where('id',$id);
		$this->db->update($table,$item);
	}

  public function select_all_no_deleted_page_record_count()
	{
		$this->db->where("deleted",false);
		$this->db->order_by("data_emissao_empenho","desc");
		return $this->db->count_all("sagres_Empenhos");
	}

  public function select_all_no_deleted_page($limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->where("deleted",false);
		$this->db->order_by("data_emissao_empenho","desc");
		return $this->db->get("sagres_Empenhos")->result_array();
	}

  public function select_page_record_count($year_key, $month_key)
	{
		$this->db->order_by("data_emissao_empenho","desc");
		$this->db->where("deleted",false);
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(data_emissao_empenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(data_emissao_empenho)",$month_key);
		}
		// return $this->db->count_all("sicap_Empenho");
		return $this->db->get("sagres_Empenhos")->num_rows();
	}

  public function select_page($year_key, $month_key,$limit, $start)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by("data_emissao_empenho","desc");
		$this->db->where("deleted",false);
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(data_emissao_empenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(data_emissao_empenho)",$month_key);
		}
		return $this->db->get("sagres_Empenhos")->result_array();
	}

  public function select($year_key, $month_key)
	{

		$this->db->order_by("data_emissao_empenho","desc");
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(data_emissao_empenho)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(data_emissao_empenho)",$month_key);
		}
		return $this->db->get("sagres_Empenhos")->result_array();
	}
}