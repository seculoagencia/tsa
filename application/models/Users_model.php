<?php

class Users_model extends CI_Model {
	public function insert($user)
	{
		$this->db->insert("users", $user);
	}
	public function select_all()
	{
		return $this->db->get('users')->result_array();
	}
	public function count_all()
	{
		return $this->db->count_all('users');
	}
	public function select($id)
	{
		return $this->db->get_where('users', array('id'=>$id))->row_array();
	}
	public function update($id, $user)
	{
		$this->db->where('id',$id);
		return $this->db->update('users', $user);
	}

	public function auth_user($user, $pass)
	{
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		return $this->db->get("users")->row_array();
	}
}
