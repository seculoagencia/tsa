<?php

class Groups_model extends CI_Model {
	public function select_all()
	{
		return $this->db->get("groups")->result_array();
	}
	public function select($id)
	{
		return $this->db->get_where("groups", array('id'=>$id))->row_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('groups',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('groups',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('groups');
	}
}
