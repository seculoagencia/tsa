<?php

class Typerequests_model extends CI_Model {
	public function select_all()
	{
		return $this->db->get("typerequests")->result_array();
	}
	
	public function select($id)
	{
		return $this->db->get_where("typerequests", array('id'=>$id))->row_array();
	}
	public function insert($typerequest)
	{
		$this->db->insert('typerequests',$typerequest);
	}
	public function update($typerequest, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('typerequests',$typerequest);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('typerequests');
	}
}
