<?php

class Requests_model extends CI_Model {
	public function select()
	{
		return $this->db->get("requests")->result_array();
	}
	public function select_by_status($status,$de,$ate)
	{
		if ($de != null && $ate != null) {
			$this->db->where('DATE(created) >=', date('Y-m-d', strtotime($de)));
			$this->db->where('DATE(created) <=', date('Y-m-d', strtotime($ate)));
		}
		$this->db->where('status', $status);
		return $this->db->get("requests")->result_array();
	}
	public function select_filter($year_key,$month_key,$typerequest_id = null,$priority_id = null)
	{
		if ($typerequest_id == null && $priority_id != null) {
			$this->db->where(array('YEAR(created)' => $year_key,'MONTH(created)' => $month_key,'priority'=>$priority_id));
		} else if ($priority_id == null && $typerequest_id != null) {
			$this->db->where(array('YEAR(created)' => $year_key,'MONTH(created)' => $month_key,'typerequest_id'=>$typerequest_id));
		} else if ($typerequest_id == null && $priority_id == null) {
			$this->db->where(array('YEAR(created)' => $year_key,'MONTH(created)' => $month_key));
		} else {
			$this->db->where(array('YEAR(created)' => $year_key,'MONTH(created)' => $month_key,'priority'=>$priority_id,'typerequest_id'=>$typerequest_id));
		}
		return $this->db->get("requests")->result_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("requests", array('id'=>$id))->row_array();
	}
	public function est($ano = null,$mes = null,$status = null)
	{
		if ($ano !== null) {
			$this->db->where('YEAR(created)',$ano);
		}
		if ($mes !== null) {
			$this->db->where('MONTH(created)',$mes);
		}
		if ($status !== null) {
			$this->db->where('status',$status);
		}
		return $this->db->get("requests")->result_array();
	}
	public function select_consult($cpf,$serial)
	{
		return $this->db->get_where("requests", array('cpf'=>$cpf,'serial'=>$serial))->row_array();
	}
	public function select_for_category($id)
	{
		return $this->db->get_where("requests", array("typerequest_id"=>$id))->result_array();
	}
	public function select_search($search)
	{
		$this->db->where("subject LIKE '%".$search."%'");
		return $this->db->get("requests")->result_array();
	}
	public function insert($request)
	{
		$this->db->insert('requests',$request);
	}
	public function update($request, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('requests',$request);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('requests');
	}
}
