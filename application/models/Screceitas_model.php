<?php

class Screceitas_model extends CI_Model {
	public function select($year_key,$month_key)
	{

		$this->db->order_by("DataArrecadacao","desc");
		// $this->db->where(array("YEAR(data)" => $year_key,"MONTH(data)" => $month_key));
		if (!empty($year_key)) {
			$this->db->where("YEAR(DataArrecadacao)",$year_key);
		}
		if (!empty($month_key)) {
			$this->db->where("MONTH(DataArrecadacao)",$month_key);
		}
		return $this->db->get("sicap_ReceitaArrecadada")->result_array();
	}
	public function select_all()
	{
		$this->db->order_by("DataArrecadacao","desc");
		return $this->db->get("sicap_ReceitaArrecadada")->result_array();
	}
	public function select_num($id)
	{
		return $this->db->get_where("sicap_ReceitaArrecadada", array('id'=>$id))->row_array();
	}
	public function select_id($id)
	{
		return $this->db->get_where("sicap_ReceitaArrecadada", array('codigo'=>$id))->row_array();
	}
	public function select_for_category($id)
	{
		$this->db->order_by("DataArrecadacao","desc");
		return $this->db->get_where("sicap_ReceitaArrecadada")->result_array();
	}
	public function select_for_subcategory($id)
	{
		$this->db->order_by("created","desc");
		return $this->db->get_where("sicap_ReceitaArrecadada")->row_array();
	}
	public function select_search($search)
	{
		$this->db->order_by("DataArrecadacao","desc");
		$this->db->where("name LIKE '%".$search."%'");
		return $this->db->get("sicap_ReceitaArrecadada")->result_array();
	}
	public function insert($grupo)
	{
		$this->db->insert('receitas',$grupo);
	}
	public function update($grupo, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('receitas',$grupo);
	}
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('receitas');
	}
}
