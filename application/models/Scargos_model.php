<?php

class Scargos_model extends CI_Model {

	public function select_id($id)
	{
		return $this->db->get_where("sicap_CargosServidores", array('CodCargo'=>$id))->row_array();
	}

	public function select_cod($cod)
	{
		return $this->db->get_where("sicap_CargosServidores", array('CodCargo'=>$cod))->row_array();
	}

	public function insert($item,$table)
	{
		$this->db->insert($table,$item);
	}

}
