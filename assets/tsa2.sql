CREATE TABLE IF NOT EXISTS `empenho` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Exercicio` INT NULL,
  `Numero` INT NULL,
  `Processo` INT NULL,
  `Data` DATE NULL,
  `AcaoCodigo` INT NULL,
  `FonteCodigo` INT NULL,
  `RubricaCodigo` INT NULL,
  `CPF` VARCHAR(45) NULL,
  `CNPJ` VARCHAR(45) NULL,
  `ValorEmpAteAgora` DECIMAL(10,2) NULL,
  `ValorLiqAteAgora` DECIMAL(10,2) NULL,
  `ValorPagAteAgora` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `acao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` INT NOT NULL,
  `descricao` TEXT NULL,
  `funcaocodigo` INT NULL,
  `subfuncaocodigo` INT NULL,
  `programacodigo` INT NULL,
  `unidadecodigo` INT NULL,
  `metafinanceiraano01` DECIMAL(10,2) NULL,
  `metafinanceiraano02` DECIMAL(10,2) NULL,
  `metafinanceiraano03` DECIMAL(10,2) NULL,
  `metafinanceiraano04` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `funcao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `subfuncao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `programa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `unidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `fonte` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `rubrica` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `codigo` VARCHAR(45) NOT NULL,
  `descricao` TEXT NULL,
  `tipo` INT NULL,
  `analiticasintetica` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `credor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Exercicio` INT NULL,
  `CPF` VARCHAR(45) NULL,
  `CNPJ` VARCHAR(45) NULL,
  `Nome` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `dotacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `acaocodigo` INT NULL,
  `fontecodigo` VARCHAR(45) NULL,
  `rubricacodigo` VARCHAR(45) NULL,
  `valororcado` DECIMAL(10,2) NULL,
  `valoratualizado` DECIMAL(10,2) NULL,
  `valorempateagora` DECIMAL(10,2) NULL,
  `valorliqateagora` DECIMAL(10,2) NULL,
  `valorpagateagora` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `receitaclassificada` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `exercicio` INT NULL,
  `rubricacodigo` VARCHAR(45) NULL,
  `valororcado` DECIMAL(10,2) NULL,
  `valorarrecadado` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
