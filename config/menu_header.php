<?php
// Header menu

try {
    return [

        'items' => [
            [],
            [
                'title' => env('APP_NAME'),
                'root' => true,
                'page' => env('APP_URL_SITE'),
                'new-tab' => true,
            ]
        ]

    ];
} catch (\Throwable $th) {
    dd($th->getMessage());
}
