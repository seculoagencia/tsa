<?php

return [
    'months' => [
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro'
    ],
    'licitacoes' => [
        'tipo' => [
            'A' => 'Atas de Registro de Preço', //ok
            'D' => 'Dispensa de Licitação', //
            'C' => "Contrato", // ok
            'R' => "Concorrência", // ok
            'W' => 'Carta Convite', //ok
            'F' => "Contrato Pessoa Física", //ok
            'J' => "Contrato Pessoa Jurídica",
            'I' => 'Inexigibilidade', // ok
            'L' => 'Licitações',
            "P" => "Pregão Presencial",//ok
            'O' => 'Pregão Eletrônico',//ok
            'T' => "Tomada de Preços",//ok
        ],
        'status' => [
            'C' => 'Concluído',
            'D' => 'Deserta',
            'A' => 'Em Andamento',
            'F' => 'Fracassada',
            'S' => 'Suspenso',
            'R' => 'Revogado'
        ]
    ],
    'sicap' => [
        'ModalLicita' => [
            '01' => 'Dispensa',
            '02' => 'Inexigibilidade',
            '03' => 'Convite',
            '04' => 'Tomada de Preço',
            '05' => 'Concorrência',
            '06' => 'Registro de Preço',
            '07' => 'Pregão Presencial',
            '08' => 'Pregão Eletrônico',
            '09' => 'Pregão Presencial Registro de Preço',
            '10' => 'Pregão Eletrônico Registro de Preço',
            '11' => 'Concurso',
            '12' => 'Leilão',
            '13' => 'Regras Internacionais',
            '99' => 'Não Aplicado'
        ],
        'RegistroDePreco' => [
            '1' => 'Sim',
            '2' => 'Não'
        ],
        'FormaArrecadacao' => [
            '1' => 'Caixa / Tesouraria',
            '2' => 'Rede Bancária'
        ],
        'CodVinculoEmpregaticio' => [
            '1' => 'Efetivo',
            '2' => 'Comissionado',
            '3' => 'Contratado', // (por tempo indeterminado)
            '4' => 'Aposentado',
            '5' => 'Pensionista',
            '6' => 'Eletivo',
            '7' => 'Temporário'
        ],
        'CodRegimePrevidenciario' => [
            '1' => 'RPPS',
            '2' => 'RGPS'
        ],
        'CodEscolaridade' => [
            '0' => 'Analfabeto(a)',
            '1' => 'Fundamental Completo',
            '2' => 'Fundamental Incompleto',
            '3' => 'Médio Completo',
            '4' => 'Médio Incompleto',
            '5' => 'Superior Completo',
            '6' => 'Superior Incompleto',
            '7' => 'Outros'
        ],
        'SobCessao' => [
            '0' => 'Não',
            '1' => 'Concedida',
            '2' => 'Recebida'
        ],

        // SIAP
        'TipoContratacao' => [
            '1' => 'Contratação Direta',
            '2' => 'Dispensa de Licitação',
            '3' => 'Licitação',
            '4' => 'Folha de Pagamento',
        ],
        'TipoFornecedor' => [
            '1' => 'Pessoa Física',
            '2' => 'Pessoa Jurídica',
        ],
        'TipoAcao' => [
            '1' => 'Projeto',
            '2' => 'Atividade',
            '3' => 'Operação Especial',
        ],
        'TipoDocumento' => [
            '1' => 'Nota Fiscal',
            '2' => 'Folha de pagamento',
            '3' => 'Recibo',
            '4' => 'Outros',
        ]
    ],
    'esic' => [
        'priority' => [
            '1' => 'Alta',
            '2' => 'Média',
            '3' => 'Baixa'
        ],
        'priority_class' => [
            '1' => 'danger',
            '2' => 'warning',
            '3' => 'success'
        ],
        'status' => [
            '0' => 'Aberto',
            '1' => 'Respondido',
            '2' => 'Em Tramitação',
            '3' => 'Negado'
        ]
    ]
];
