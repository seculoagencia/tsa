<?php
// Aside menu
return [

    'items' => [

        // Administração
        [
            'section' => 'Administração',
        ],
        [
            'title' => 'Segurança',
            'icon' => 'media/svg/icons/General/Lock.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'users-list',
            'submenu' => [
                [
                    'title' => 'Usuários',
                    'bullet' => 'dot',
                    'page' => 'adm/users',
                    'permission' => 'users-list'
                ],
                [
                    'title' => 'Papéis',
                    'bullet' => 'dot',
                    'page' => 'adm/roles',
                    'permission' => 'role-list'
                ],
                // [
                //     'title' => 'Permissões',
                //     'bullet' => 'dot',
                //     'page' => 'adm/permissions'
                // ],
            ]
        ],
        [
            'title' => 'Controle de Menu',
            'icon' => 'media/svg/icons/General/Settings-1.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/controle-menu',
            'permission' => 'app-settings-list'
        ],
        [
            'title' => 'Configurações',
            'icon' => 'media/svg/icons/General/Settings-1.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/app-settings',
            'permission' => 'app-settings-list'
        ],
        // Portal da Transparência
        [
            'section' => 'Portal da Transparência',
        ],
        [
            'title' => 'COVID-19',
            'icon' => 'media/svg/icons/Communication/Shield-user.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/covid-resumos',
            'permission' => 'empenhos-list'
        ],

        [
            'title' => 'Tipos de Documentos',
            'icon' => 'media/svg/icons/Files/File.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/type-documents',
            'permission' => 'type-documents-list'
        ],
        [
            'title' => 'Documentos',
            'icon' => 'media/svg/icons/Files/File.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/documents',
            'permission' => 'documents-list'
        ],
        [
            'title' => 'Verbas Indenizatórias',
            'icon' => 'media/svg/icons/Shopping/Dollar.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/verbas',
            'permission' => 'verbas-list'
        ],
        [
            'title' => 'Empenhos',
            'icon' => 'media/svg/icons/Shopping/Dollar.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/empenhos',
            'permission' => 'empenhos-list'
        ],
        [
            'title' => 'Receitas',
            'icon' => 'media/svg/icons/Shopping/Dollar.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/receitas',
            'permission' => 'receitas-list'
        ],
        [
            'title' => 'Cargos',
            'icon' => 'media/svg/icons/Code/Compiling.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/cargos',
            'permission' => 'cargos-list'
        ],
        [
            'title' => 'Servidores',
            'icon' => 'media/svg/icons/Communication/Group.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/servidores',
            'permission' => 'servidores-list'
        ],
        [
            'title' => 'Licitações e Contratos',
            'icon' => 'media/svg/icons/Shopping/Cart1.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/contratos',
            'permission' => 'contratos-list'
        ],
        [
            'title' => 'Diárias',
            'icon' => 'media/svg/icons/Map/Marker1.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/diarias',
            'permission' => 'diarias-list'
        ],
        [
            'title' => 'Plan. Orçamentário',
            'icon' => 'media/svg/icons/Shopping/Dollar.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'tipo-planejamento-orcamentarios-list',
            'submenu' => [
                [
                    'title' => 'Tipos',
                    'icon' => 'media/svg/icons/Shopping/Dollar.svg',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => 'adm/tipo-planejamento-orcamentarios',
                    'permission' => 'tipo-planejamento-orcamentarios-list'
                ],
                [
                    'title' => 'Arquivos',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => 'adm/arquivo-plan-orcamentarios',
                    'permission' => 'arquivo-plan-orcamentarios-list'
                ],
            ]
        ],
        [
            'title' => 'LRF',
            'icon' => 'media/svg/icons/Tools/Hummer2.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'responsabilidades-list',
            'submenu' => [
                [
                    'title' => 'Tipos',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => 'adm/responsabilidades',
                    'permission' => 'responsabilidades-list'
                ],
                [
                    'title' => 'Arquivos',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => 'adm/arquivo-responsabilidades',
                    'permission' => 'arquivo-responsabilidades-list'
                ],
            ]
        ],

        [
            'title' => 'Estrutura Organizacional',
            'icon' => 'media/svg/icons/Code/Git4.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/estruturas',
            'permission' => 'estruturas-list'
        ],

        // Integração
        [
            'section' => 'Integração',
        ],
        [
            'title' => 'SICAP',
            'icon' => 'media/svg/icons/Files/Import.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'sicap-empenho-list',
            'submenu' => [
                [
                    'title' => 'Empenhos',
                    'page' => 'adm/sicap/sicap-empenho',
                    'permission' => 'sicap-empenho-list'
                ],
                [
                    'title' => 'Receitas',
                    'page' => 'adm/sicap/sicap-receitas',
                    'permission' => 'sicap-receitas-list'
                ],
                [
                    'title' => 'Servidores',
                    'page' => 'adm/sicap/sicap-servidores',
                    'permission' => 'sicap-servidores-list'
                ],
                // [
                //     'title' => 'SAGRES',
                //     'page' => 'adm/sicap/sagres-empenho'
                // ],
                // [
                //     'title' => 'Credor',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Função',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Subfunção',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Liquidação',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Pagamento',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Pagamento Financeiro',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Programa',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Projeto Atividade',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Recurso Vinculado',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Unidade Orçamentária',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Receita Arrecadada',
                //     'page' => 'adm/imports/sicap'
                // ],
                // [
                //     'title' => 'Rubricas RecDesp',
                //     'page' => 'adm/imports/sicap'
                // ],
            ]
        ],
        [
            'title' => 'SIAP',
            'icon' => 'media/svg/icons/Files/Import.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'siap-empenho-list',
            'submenu' => [
                [
                    'title' => 'Empenhos',
                    'page' => 'adm/siap/siap-empenho',
                    'permission' => 'siap-empenho-list'
                ]
            ]
        ],
        [
            'title' => 'SAGRES',
            'icon' => 'media/svg/icons/Files/Import.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'sagres-empenhos-list',
            'submenu' => [
                [
                    'title' => 'Empenhos',
                    'page' => 'adm/sagres/sagres-empenhos',
                    'permission' => 'sagres-empenhos-list'
                ],
            ]
        ],
        // E-SIC
        [
            'section' => 'E-SIC',
        ],
        [
            'title' => 'Pedidos',
            'icon' => 'media/svg/icons/Communication/Group-chat.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/pedidos',
            'permission' => 'pedidos-list'
        ],
        [
            'title' => 'Tipos de Solicitações',
            'icon' => 'media/svg/icons/Communication/Chat6.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'adm/tipo-pedidos',
            'permission' => 'tipo-pedidos-list'
        ],
    ]
];
