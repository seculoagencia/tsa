ALTER TABLE `atalaia_trans2`.`programa` RENAME TO  `atalaia_trans2`.`tc_programa` ;
ALTER TABLE `atalaia_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `atalaia_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `atalaia_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `atalaia_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `capela_trans2`.`programa` RENAME TO  `capela_trans2`.`tc_programa` ;
ALTER TABLE `capela_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `capela_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `capela_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `capela_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `carneiros_trans2`.`programa` RENAME TO  `carneiros_trans2`.`tc_programa` ;
ALTER TABLE `carneiros_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `carneiros_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `carneiros_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `carneiros_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `doisriac_trans2`.`programa` RENAME TO  `doisriac_trans2`.`tc_programa` ;
ALTER TABLE `doisriac_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `doisriac_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `doisriac_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `doisriac_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `inhapi_trans2`.`programa` RENAME TO  `inhapi_trans2`.`tc_programa` ;
ALTER TABLE `inhapi_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `inhapi_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `inhapi_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `inhapi_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `jacare_trans2`.`programa` RENAME TO  `jacare_trans2`.`tc_programa` ;
ALTER TABLE `jacare_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `jacare_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `jacare_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `jacare_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `majorisi_trans2`.`programa` RENAME TO  `majorisi_trans2`.`tc_programa` ;
ALTER TABLE `majorisi_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `majorisi_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `majorisi_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `majorisi_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `maravilh_trans2`.`programa` RENAME TO  `maravilh_trans2`.`tc_programa` ;
ALTER TABLE `maravilh_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `maravilh_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `maravilh_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `maravilh_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `mata_trans2`.`programa` RENAME TO  `mata_trans2`.`tc_programa` ;
ALTER TABLE `mata_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `mata_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `mata_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `mata_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `novolino_trans2`.`programa` RENAME TO  `novolino_trans2`.`tc_programa` ;
ALTER TABLE `novolino_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `novolino_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `novolino_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `novolino_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `olhodagu_trans2`.`programa` RENAME TO  `olhodagu_trans2`.`tc_programa` ;
ALTER TABLE `olhodagu_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `olhodagu_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `olhodagu_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `olhodagu_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `olivenca_trans2`.`programa` RENAME TO  `olivenca_trans2`.`tc_programa` ;
ALTER TABLE `olivenca_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `olivenca_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `olivenca_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `olivenca_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `ourobranco_trans2`.`programa` RENAME TO  `ourobranco_trans2`.`tc_programa` ;
ALTER TABLE `ourobranco_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `ourobranco_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `ourobranco_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `ourobranco_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `palestina_trans2`.`programa` RENAME TO  `palestina_trans2`.`tc_programa` ;
ALTER TABLE `palestina_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `palestina_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `palestina_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `palestina_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `paodeacu_trans2`.`programa` RENAME TO  `paodeacu_trans2`.`tc_programa` ;
ALTER TABLE `paodeacu_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `paodeacu_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `paodeacu_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `paodeacu_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `pariconha_trans2`.`programa` RENAME TO  `pariconha_trans2`.`tc_programa` ;
ALTER TABLE `pariconha_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `pariconha_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `pariconha_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `pariconha_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `piacabuc_trans2`.`programa` RENAME TO  `piacabuc_trans2`.`tc_programa` ;
ALTER TABLE `piacabuc_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `piacabuc_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `piacabuc_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `piacabuc_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `piranhas_trans2`.`programa` RENAME TO  `piranhas_trans2`.`tc_programa` ;
ALTER TABLE `piranhas_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `piranhas_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `piranhas_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `piranhas_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `poco_trans2`.`programa` RENAME TO  `poco_trans2`.`tc_programa` ;
ALTER TABLE `poco_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `poco_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `poco_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `poco_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `santana_trans2`.`programa` RENAME TO  `santana_trans2`.`tc_programa` ;
ALTER TABLE `santana_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `santana_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `santana_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `santana_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `traipu_trans2`.`programa` RENAME TO  `traipu_trans2`.`tc_programa` ;
ALTER TABLE `traipu_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `traipu_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `traipu_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `traipu_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;

ALTER TABLE `uniao_trans2`.`programa` RENAME TO  `uniao_trans2`.`tc_programa` ;
ALTER TABLE `uniao_trans2`.`sicap_PagamentoFinanceiro` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `uniao_trans2`.`sicap_Pagamento` ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `uniao_trans2`.`sicap_InfoRemessa` ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `uniao_trans2`.`sicap_Empenho` ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;
