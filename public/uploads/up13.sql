CREATE TABLE IF NOT EXISTS `sagres_Fornecedores` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cpf_cnpj` VARCHAR(14) NULL,
  `nome` VARCHAR(80) NULL,
  `tipo_credor` INT NULL,
  `sigla_uf` CHAR(2) NULL,
  `municipio` VARCHAR(60) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Empenhos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ano` INT NULL,
  `cod_unid_orc` VARCHAR(10) NULL,
  `funcao` VARCHAR(2) NULL,
  `subfuncao` VARCHAR(3) NULL,
  `programa` VARCHAR(4) NULL,
  `acao` VARCHAR(6) NULL,
  `id_acao` INT NULL,
  `cod_cat_econ` INT NULL,
  `cod_nat_desp` INT NULL,
  `modalidade_aplic` INT NULL,
  `cod_elem_desp_dot` INT NULL,
  `sub_elem_desp` INT NULL,
  `modalidade_licit` INT NULL,
  `num_empenho` VARCHAR(7) NULL,
  `tipo_empenho` INT NULL,
  `data_emissao_empenho` DATE NULL,
  `valor_empenhado` DECIMAL(16,2) NULL,
  `historico` VARCHAR(510) NULL,
  `cpf_cnpj_credor` VARCHAR(14) NULL,
  `num_procedimento` VARCHAR(9) NULL,
  `font_recurso` VARCHAR(6) NULL,
  `cpf_ordenador` VARCHAR(11) NULL,
  `cod_elem_desp` VARCHAR(2) NULL,
  `deleted` TINYINT(1) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS  `sagres_UnidadeOrcamentaria` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NULL,
  `denominacao` VARCHAR(50) NULL,
  `num_unid_jurisd` CHAR(6) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Funcoes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` CHAR(2) NULL,
  `nome` VARCHAR(30) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_SubFuncoes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cod_funcao` CHAR(2) NULL,
  `codigo` CHAR(3) NULL,
  `nome` VARCHAR(70) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Programas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` CHAR(4) NULL,
  `nome` VARCHAR(70) NULL,
  `descricao` VARCHAR(150) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Acao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` CHAR(6) NULL,
  `nome` VARCHAR(70) NULL,
  `identificacao` INT NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_ElementoDespesa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` CHAR(2) NULL,
  `descricao` VARCHAR(90) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Liquidacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ano` INT NULL,
  `cod_unid_orc` VARCHAR(10) NULL,
  `num_empenho` CHAR(7) NULL,
  `num_liquidacao` CHAR(7) NULL,
  `data` DATE NULL,
  `valor` DECIMAL(16,2) NULL,
  `tipo_doc` INT NULL,
  `num_chave` VARCHAR(44) NULL,
  `historico` VARCHAR(510) NULL,
  `cod_fonte_recurso` CHAR(6) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `sagres_Pagamentos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ano` INT NULL,
  `cod_unid_orc` VARCHAR(10) NULL,
  `num_empenho` CHAR(7) NULL,
  `num_parc_pagamento` CHAR(7) NULL,
  `data` DATE NULL,
  `valor` DECIMAL(16,2) NULL,
  `cod_banco` CHAR(3) NULL,
  `num_agencia_bancaria` CHAR(6) NULL,
  `num_conta_bancaria` VARCHAR(12) NULL,
  `num_seq` CHAR(7) NULL,
  `created` DATETIME NULL,
  PRIMARY KEY (`id`));