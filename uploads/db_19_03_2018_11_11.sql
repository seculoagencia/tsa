-- MySQL dump 10.13  Distrib 5.5.58, for Linux (x86_64)
--
-- ------------------------------------------------------
-- Server version	5.5.58-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `request_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_user_id_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'Convênios'),(7,'Fornecedores'),(9,'Diárias'),(12,'Publicações Oficiais'),(11,'Folhas de Pagamentos');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diarias`
--

DROP TABLE IF EXISTS `diarias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diarias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `favorecido` varchar(255) DEFAULT NULL,
  `cargo_id` int(11) DEFAULT '2',
  `data` datetime DEFAULT NULL,
  `destino` varchar(255) DEFAULT NULL,
  `motivo` text,
  `valor` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diarias`
--

LOCK TABLES `diarias` WRITE;
/*!40000 ALTER TABLE `diarias` DISABLE KEYS */;
/*!40000 ALTER TABLE `diarias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diarias_anexos`
--

DROP TABLE IF EXISTS `diarias_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diarias_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diarias_anexos`
--

LOCK TABLES `diarias_anexos` WRITE;
/*!40000 ALTER TABLE `diarias_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `diarias_anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empenhos`
--

DROP TABLE IF EXISTS `empenhos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empenhos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `credor` varchar(45) NOT NULL,
  `data` date DEFAULT NULL,
  `unidade_orcamentaria` varchar(45) DEFAULT NULL,
  `funcao` varchar(45) DEFAULT NULL,
  `subfuncao` varchar(45) DEFAULT NULL,
  `programa` varchar(45) DEFAULT NULL,
  `acao` varchar(45) DEFAULT NULL,
  `fonte_de_recurso` varchar(45) DEFAULT NULL,
  `elemento_de_despesa` varchar(45) DEFAULT NULL,
  `cpf_cnpj` varchar(45) DEFAULT NULL,
  `numero_do_processo` int(11) DEFAULT NULL,
  `historico` text,
  `anexo` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empenhos`
--

LOCK TABLES `empenhos` WRITE;
/*!40000 ALTER TABLE `empenhos` DISABLE KEYS */;
/*!40000 ALTER TABLE `empenhos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empenhos_anexos`
--

DROP TABLE IF EXISTS `empenhos_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empenhos_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empenhos_anexos`
--

LOCK TABLES `empenhos_anexos` WRITE;
/*!40000 ALTER TABLE `empenhos_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `empenhos_anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Master','2017-01-12 17:56:22','2017-01-12 17:56:22'),(2,'Admin','2017-01-12 17:56:22','2017-01-12 17:56:22'),(3,'Normal','2017-01-12 18:13:46','2017-01-12 18:13:46');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licitacoes`
--

DROP TABLE IF EXISTS `licitacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licitacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgao` varchar(255) DEFAULT NULL,
  `tipo` int(11) DEFAULT '2',
  `modalidade` varchar(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `objeto` text,
  `valor` decimal(10,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `vencedor` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licitacoes`
--

LOCK TABLES `licitacoes` WRITE;
/*!40000 ALTER TABLE `licitacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licitacoes_anexos`
--

DROP TABLE IF EXISTS `licitacoes_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licitacoes_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licitacoes_anexos`
--

LOCK TABLES `licitacoes_anexos` WRITE;
/*!40000 ALTER TABLE `licitacoes_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitacoes_anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liquidacoes`
--

DROP TABLE IF EXISTS `liquidacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liquidacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `empenho_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_liquidacoes_1_idx` (`empenho_id`),
  CONSTRAINT `fk_liquidacoes_1` FOREIGN KEY (`empenho_id`) REFERENCES `empenhos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liquidacoes`
--

LOCK TABLES `liquidacoes` WRITE;
/*!40000 ALTER TABLE `liquidacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `liquidacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagamentos`
--

DROP TABLE IF EXISTS `pagamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banco` varchar(45) DEFAULT NULL,
  `agencia` varchar(45) DEFAULT NULL,
  `conta` varchar(45) DEFAULT NULL,
  `documento` varchar(45) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `empenho_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pagamentos_1_idx` (`empenho_id`),
  CONSTRAINT `fk_pagamentos_1` FOREIGN KEY (`empenho_id`) REFERENCES `empenhos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagamentos`
--

LOCK TABLES `pagamentos` WRITE;
/*!40000 ALTER TABLE `pagamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programa`
--

DROP TABLE IF EXISTS `programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programa`
--

LOCK TABLES `programa` WRITE;
/*!40000 ALTER TABLE `programa` DISABLE KEYS */;
/*!40000 ALTER TABLE `programa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receitas`
--

DROP TABLE IF EXISTS `receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) DEFAULT NULL,
  `descricao` text,
  `data` date DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receitas`
--

LOCK TABLES `receitas` WRITE;
/*!40000 ALTER TABLE `receitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receitas_anexos`
--

DROP TABLE IF EXISTS `receitas_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receitas_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receitas_anexos`
--

LOCK TABLES `receitas_anexos` WRITE;
/*!40000 ALTER TABLE `receitas_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `receitas_anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `name` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `anexo` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_anexos`
--

DROP TABLE IF EXISTS `reports_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `priority` int(11) NOT NULL,
  `cpf` char(14) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `location` text NOT NULL,
  `type_answer` varchar(50) NOT NULL,
  `typerequest_id` int(11) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `servidores`
--

DROP TABLE IF EXISTS `servidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servidores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `valor_bruto` decimal(10,2) DEFAULT NULL,
  `descontos` decimal(10,2) DEFAULT NULL,
  `valor_liquido` decimal(10,2) DEFAULT NULL,
  `mes_ref` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servidores`
--

LOCK TABLES `servidores` WRITE;
/*!40000 ALTER TABLE `servidores` DISABLE KEYS */;
/*!40000 ALTER TABLE `servidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servidores_anexos`
--

DROP TABLE IF EXISTS `servidores_anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servidores_anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anexo` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servidores_anexos`
--

LOCK TABLES `servidores_anexos` WRITE;
/*!40000 ALTER TABLE `servidores_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `servidores_anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'Câmara Municipal de ','www.al.leg.br','');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_ContaDisponibilidade`
--

DROP TABLE IF EXISTS `sicap_ContaDisponibilidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_ContaDisponibilidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodContaBalancete` varchar(45) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  `CodBanco` varchar(45) DEFAULT NULL,
  `CodAgenciaBanco` varchar(45) DEFAULT NULL,
  `NumContaCorrente` varchar(45) DEFAULT NULL,
  `Tipo` int(11) DEFAULT NULL,
  `Classificacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_ContaDisponibilidade`
--

LOCK TABLES `sicap_ContaDisponibilidade` WRITE;
/*!40000 ALTER TABLE `sicap_ContaDisponibilidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_ContaDisponibilidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Credor`
--

DROP TABLE IF EXISTS `sicap_Credor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Credor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodCredor` varchar(45) DEFAULT NULL,
  `Nome` varchar(155) DEFAULT NULL,
  `InscricaoEstadual` varchar(45) DEFAULT NULL,
  `InscricaoMunicipal` varchar(45) DEFAULT NULL,
  `Endereco` varchar(155) DEFAULT NULL,
  `Cidade` varchar(45) DEFAULT NULL,
  `UF` char(2) DEFAULT NULL,
  `Cep` varchar(45) DEFAULT NULL,
  `Fone` varchar(45) DEFAULT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `Tipo` int(11) DEFAULT NULL,
  `NumerodoRegistro` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Credor`
--

LOCK TABLES `sicap_Credor` WRITE;
/*!40000 ALTER TABLE `sicap_Credor` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Credor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_DecretoAltOrcamentaria`
--

DROP TABLE IF EXISTS `sicap_DecretoAltOrcamentaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_DecretoAltOrcamentaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodFuncao` varchar(45) DEFAULT NULL,
  `CodSubFuncao` varchar(45) DEFAULT NULL,
  `CodPrograma` varchar(45) DEFAULT NULL,
  `CodProjAtividade` int(11) DEFAULT NULL,
  `CodRubrica` varchar(45) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  `NumDocumento` int(11) DEFAULT NULL,
  `DataDocumento` date DEFAULT NULL,
  `DataAlteracao` date DEFAULT NULL,
  `NumAlteracao` int(11) DEFAULT NULL,
  `TipoAlteracao` varchar(45) DEFAULT NULL,
  `ValorAlteracao` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_DecretoAltOrcamentaria`
--

LOCK TABLES `sicap_DecretoAltOrcamentaria` WRITE;
/*!40000 ALTER TABLE `sicap_DecretoAltOrcamentaria` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_DecretoAltOrcamentaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Empenho`
--

DROP TABLE IF EXISTS `sicap_Empenho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Empenho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `NumEmpenho` varchar(45) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `Sinal` char(1) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodFuncao` varchar(45) DEFAULT NULL,
  `CodPrograma` varchar(45) DEFAULT NULL,
  `CodProjAtividade` varchar(45) DEFAULT NULL,
  `CodRubrica` varchar(45) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  `ContraPartida` varchar(45) DEFAULT NULL,
  `CodCredor` varchar(45) DEFAULT NULL,
  `Historico` text,
  `ModalLicita` varchar(45) DEFAULT NULL,
  `CaracPeculiar` varchar(45) DEFAULT NULL,
  `NumProcesso` varchar(45) DEFAULT NULL,
  `NumContrato` varchar(45) DEFAULT NULL,
  `DataContrato` date DEFAULT NULL,
  `NumConvenio` varchar(45) DEFAULT NULL,
  `NumObra` varchar(45) DEFAULT NULL,
  `Tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Empenho`
--

LOCK TABLES `sicap_Empenho` WRITE;
/*!40000 ALTER TABLE `sicap_Empenho` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Empenho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Funcao`
--

DROP TABLE IF EXISTS `sicap_Funcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Funcao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodFuncao` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Funcao`
--

LOCK TABLES `sicap_Funcao` WRITE;
/*!40000 ALTER TABLE `sicap_Funcao` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Funcao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_InfoRemessa`
--

DROP TABLE IF EXISTS `sicap_InfoRemessa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_InfoRemessa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `TipoEnvio` int(11) DEFAULT NULL,
  `DataInicio` date DEFAULT NULL,
  `DataFim` date DEFAULT NULL,
  `DataGeracao` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_InfoRemessa`
--

LOCK TABLES `sicap_InfoRemessa` WRITE;
/*!40000 ALTER TABLE `sicap_InfoRemessa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_InfoRemessa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Liquidacao`
--

DROP TABLE IF EXISTS `sicap_Liquidacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Liquidacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `NumEmpenho` varchar(45) DEFAULT NULL,
  `NumLiquidacao` varchar(45) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `Sinal` char(1) DEFAULT NULL,
  `Historico` text,
  `CodOperacao` varchar(45) DEFAULT NULL,
  `NumProcesso` varchar(45) DEFAULT NULL,
  `CodCredor` varchar(45) DEFAULT NULL,
  `Referencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Liquidacao`
--

LOCK TABLES `sicap_Liquidacao` WRITE;
/*!40000 ALTER TABLE `sicap_Liquidacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Liquidacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_LoaReceita`
--

DROP TABLE IF EXISTS `sicap_LoaReceita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_LoaReceita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  `CodContaReceita` varchar(45) DEFAULT NULL,
  `ValorReceitaOrcada` decimal(10,2) DEFAULT NULL,
  `Descricao` text,
  `TipoNivelConta` char(1) DEFAULT NULL,
  `NumNivelConta` int(11) DEFAULT NULL,
  `MetaArrecadacao1Bimestre` decimal(10,2) DEFAULT NULL,
  `MetaArrecadacao2Bimestre` decimal(10,2) DEFAULT NULL,
  `MetaArrecadacao3Bimestre` decimal(10,2) DEFAULT NULL,
  `MetaArrecadacao4Bimestre` decimal(10,2) DEFAULT NULL,
  `MetaArrecadacao5Bimestre` decimal(10,2) DEFAULT NULL,
  `MetaArrecadacao6Bimestre` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_LoaReceita`
--

LOCK TABLES `sicap_LoaReceita` WRITE;
/*!40000 ALTER TABLE `sicap_LoaReceita` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_LoaReceita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Orgao`
--

DROP TABLE IF EXISTS `sicap_Orgao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Orgao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Orgao`
--

LOCK TABLES `sicap_Orgao` WRITE;
/*!40000 ALTER TABLE `sicap_Orgao` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Orgao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Pagamento`
--

DROP TABLE IF EXISTS `sicap_Pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `NumEmpenho` varchar(45) DEFAULT NULL,
  `NumPagamento` varchar(45) DEFAULT NULL,
  `NumLiquidacao` varchar(45) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `Sinal` char(2) DEFAULT NULL,
  `Historico` text,
  `CodOperacao` varchar(45) DEFAULT NULL,
  `NumProcesso` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Pagamento`
--

LOCK TABLES `sicap_Pagamento` WRITE;
/*!40000 ALTER TABLE `sicap_Pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_PagamentoFinanceiro`
--

DROP TABLE IF EXISTS `sicap_PagamentoFinanceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_PagamentoFinanceiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `NumEmpenho` varchar(45) DEFAULT NULL,
  `NumPagamento` varchar(45) DEFAULT NULL,
  `CodContaBalancete` varchar(45) DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `CodBanco` varchar(45) DEFAULT NULL,
  `CodAgenciaBanco` varchar(45) DEFAULT NULL,
  `NumContaCorrente` varchar(45) DEFAULT NULL,
  `NumCheque` varchar(45) DEFAULT NULL,
  `NumLiquidacao` varchar(45) DEFAULT NULL,
  `Sinal` char(2) DEFAULT NULL,
  `TipoPagamento` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_PagamentoFinanceiro`
--

LOCK TABLES `sicap_PagamentoFinanceiro` WRITE;
/*!40000 ALTER TABLE `sicap_PagamentoFinanceiro` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_PagamentoFinanceiro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_Programa`
--

DROP TABLE IF EXISTS `sicap_Programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_Programa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodPrograma` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Objetivo` text,
  `PublicoAlvo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_Programa`
--

LOCK TABLES `sicap_Programa` WRITE;
/*!40000 ALTER TABLE `sicap_Programa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_Programa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_ProjAtividade`
--

DROP TABLE IF EXISTS `sicap_ProjAtividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_ProjAtividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodProjAtividade` varchar(45) DEFAULT NULL,
  `Identificador` varchar(45) DEFAULT NULL,
  `Nome` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_ProjAtividade`
--

LOCK TABLES `sicap_ProjAtividade` WRITE;
/*!40000 ALTER TABLE `sicap_ProjAtividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_ProjAtividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_ReceitaArrecadada`
--

DROP TABLE IF EXISTS `sicap_ReceitaArrecadada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_ReceitaArrecadada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodContaReceita` varchar(45) DEFAULT NULL,
  `CodBanco` varchar(45) DEFAULT NULL,
  `CodAgencia` varchar(45) DEFAULT NULL,
  `NumConta` varchar(45) DEFAULT NULL,
  `DataArrecadacao` date DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_ReceitaArrecadada`
--

LOCK TABLES `sicap_ReceitaArrecadada` WRITE;
/*!40000 ALTER TABLE `sicap_ReceitaArrecadada` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_ReceitaArrecadada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_RecursoVinculado`
--

DROP TABLE IF EXISTS `sicap_RecursoVinculado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_RecursoVinculado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodRecVinculado` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Finalidade` text,
  `Tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_RecursoVinculado`
--

LOCK TABLES `sicap_RecursoVinculado` WRITE;
/*!40000 ALTER TABLE `sicap_RecursoVinculado` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_RecursoVinculado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_RubricaDespesa`
--

DROP TABLE IF EXISTS `sicap_RubricaDespesa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_RubricaDespesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodRubrica` varchar(45) DEFAULT NULL,
  `Especificacao` varchar(255) DEFAULT NULL,
  `TipoNivelConta` char(1) DEFAULT NULL,
  `NumNivelConta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_RubricaDespesa`
--

LOCK TABLES `sicap_RubricaDespesa` WRITE;
/*!40000 ALTER TABLE `sicap_RubricaDespesa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_RubricaDespesa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_SubFuncao`
--

DROP TABLE IF EXISTS `sicap_SubFuncao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_SubFuncao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodSubFuncao` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_SubFuncao`
--

LOCK TABLES `sicap_SubFuncao` WRITE;
/*!40000 ALTER TABLE `sicap_SubFuncao` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_SubFuncao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sicap_UndOrcamentaria`
--

DROP TABLE IF EXISTS `sicap_UndOrcamentaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sicap_UndOrcamentaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CodUndGestora` varchar(45) DEFAULT NULL,
  `CodigoUA` varchar(45) DEFAULT NULL,
  `Bimestre` int(11) DEFAULT NULL,
  `Exercicio` int(11) DEFAULT NULL,
  `CodUndOrcamentaria` varchar(45) DEFAULT NULL,
  `CodOrgao` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Identificador` varchar(45) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sicap_UndOrcamentaria`
--

LOCK TABLES `sicap_UndOrcamentaria` WRITE;
/*!40000 ALTER TABLE `sicap_UndOrcamentaria` DISABLE KEYS */;
/*!40000 ALTER TABLE `sicap_UndOrcamentaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategories`
--

LOCK TABLES `subcategories` WRITE;
/*!40000 ALTER TABLE `subcategories` DISABLE KEYS */;
INSERT INTO `subcategories` VALUES (8,12,'Processo Seletivo','0000-00-00 00:00:00'),(9,12,'Editais','0000-00-00 00:00:00'),(10,12,'ConvocaÃ§Ãµes','0000-00-00 00:00:00'),(11,12,'Leis','0000-00-00 00:00:00'),(12,12,'Estatuto Dos Servidores','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `subcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_acao`
--

DROP TABLE IF EXISTS `tc_acao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_acao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` int(11) NOT NULL,
  `descricao` text,
  `funcaocodigo` int(11) DEFAULT NULL,
  `subfuncaocodigo` int(11) DEFAULT NULL,
  `programacodigo` int(11) DEFAULT NULL,
  `unidadecodigo` int(11) DEFAULT NULL,
  `metafinanceiraano01` decimal(10,2) DEFAULT NULL,
  `metafinanceiraano02` decimal(10,2) DEFAULT NULL,
  `metafinanceiraano03` decimal(10,2) DEFAULT NULL,
  `metafinanceiraano04` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_acao`
--

LOCK TABLES `tc_acao` WRITE;
/*!40000 ALTER TABLE `tc_acao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_acao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_credor`
--

DROP TABLE IF EXISTS `tc_credor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_credor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Exercicio` int(11) DEFAULT NULL,
  `CPF` varchar(45) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_credor`
--

LOCK TABLES `tc_credor` WRITE;
/*!40000 ALTER TABLE `tc_credor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_credor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_dotacao`
--

DROP TABLE IF EXISTS `tc_dotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_dotacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `acaocodigo` int(11) DEFAULT NULL,
  `fontecodigo` varchar(45) DEFAULT NULL,
  `rubricacodigo` varchar(45) DEFAULT NULL,
  `valororcado` decimal(10,2) DEFAULT NULL,
  `valoratualizado` decimal(10,2) DEFAULT NULL,
  `valorempateagora` decimal(10,2) DEFAULT NULL,
  `valorliqateagora` decimal(10,2) DEFAULT NULL,
  `valorpagateagora` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_dotacao`
--

LOCK TABLES `tc_dotacao` WRITE;
/*!40000 ALTER TABLE `tc_dotacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_dotacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_empenho`
--

DROP TABLE IF EXISTS `tc_empenho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_empenho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Exercicio` int(11) DEFAULT NULL,
  `Numero` varchar(255) DEFAULT NULL,
  `Processo` int(11) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `AcaoCodigo` varchar(255) DEFAULT NULL,
  `FonteCodigo` varchar(255) DEFAULT NULL,
  `RubricaCodigo` varchar(255) DEFAULT NULL,
  `CPF` varchar(45) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `ValorEmpAteAgora` decimal(10,2) DEFAULT NULL,
  `ValorLiqAteAgora` decimal(10,2) DEFAULT NULL,
  `ValorPagAteAgora` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_empenho`
--

LOCK TABLES `tc_empenho` WRITE;
/*!40000 ALTER TABLE `tc_empenho` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_empenho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_fonte`
--

DROP TABLE IF EXISTS `tc_fonte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_fonte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_fonte`
--

LOCK TABLES `tc_fonte` WRITE;
/*!40000 ALTER TABLE `tc_fonte` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_fonte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_funcao`
--

DROP TABLE IF EXISTS `tc_funcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_funcao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_funcao`
--

LOCK TABLES `tc_funcao` WRITE;
/*!40000 ALTER TABLE `tc_funcao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_funcao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_receitaclassificada`
--

DROP TABLE IF EXISTS `tc_receitaclassificada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_receitaclassificada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `rubricacodigo` varchar(45) DEFAULT NULL,
  `valororcado` decimal(10,2) DEFAULT NULL,
  `valorarrecadado` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_receitaclassificada`
--

LOCK TABLES `tc_receitaclassificada` WRITE;
/*!40000 ALTER TABLE `tc_receitaclassificada` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_receitaclassificada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_rubrica`
--

DROP TABLE IF EXISTS `tc_rubrica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_rubrica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` varchar(45) NOT NULL,
  `descricao` text,
  `tipo` int(11) DEFAULT NULL,
  `analiticasintetica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_rubrica`
--

LOCK TABLES `tc_rubrica` WRITE;
/*!40000 ALTER TABLE `tc_rubrica` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_rubrica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_subfuncao`
--

DROP TABLE IF EXISTS `tc_subfuncao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_subfuncao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_subfuncao`
--

LOCK TABLES `tc_subfuncao` WRITE;
/*!40000 ALTER TABLE `tc_subfuncao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_subfuncao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_unidade`
--

DROP TABLE IF EXISTS `tc_unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercicio` int(11) DEFAULT NULL,
  `codigo` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_unidade`
--

LOCK TABLES `tc_unidade` WRITE;
/*!40000 ALTER TABLE `tc_unidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typerequests`
--

DROP TABLE IF EXISTS `typerequests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typerequests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typerequests`
--

LOCK TABLES `typerequests` WRITE;
/*!40000 ALTER TABLE `typerequests` DISABLE KEYS */;
INSERT INTO `typerequests` VALUES (2,'Licitações'),(3,'Reclamações'),(4,'Sugestão'),(5,'Denuncia');
/*!40000 ALTER TABLE `typerequests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `avatar` varchar(150) DEFAULT 'avatar-default.jpg',
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) NOT NULL,
  `group_id` int(1) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'Administrador','admin','1406edf84d33cf8afca4cf2e1115540d','avatar-default.jpg','admin@admin.com.br','(99) 99999-9999',1,1,'2017-11-25 12:42:01','2017-11-27 14:58:09'),(5,'Master','master','eb0a191797624dd3a48fa681d3061212','avatar-default.jpg','gessik.danielly@gmail.com','(86) 99423-9793',1,1,'2017-05-02 08:51:36','2017-05-02 08:51:36'),(9,'Wellington','Well','21c8ed336e131c2d3ab45243c545666a','avatar-default.jpg','wellington0995@hotmail.com','(82) 98182-7714',3,1,'2017-11-28 20:07:19','2017-12-04 00:06:06'),(10,'Eraldo','eraldo','648ccfeeecdd575d6c605a8acc6249c7','avatar-default.jpg','camarainhapi2017@gmail.com','(82) 98164-6744',3,1,'2018-01-15 17:09:49','2018-01-15 17:09:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

ALTER TABLE `sicap_PagamentoFinanceiro`
ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `sicap_Pagamento`
ADD COLUMN `Exercicio` INT NULL AFTER `Bimestre`;
ALTER TABLE `sicap_InfoRemessa`
ADD COLUMN `CodigoUA` VARCHAR(45) NULL AFTER `CodUndGestora`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;
ALTER TABLE `programa`
RENAME TO  `tsa`.`tc_programa` ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-19 10:10:03
