ALTER TABLE `sicap_Credor`
ADD COLUMN `TipoRegistro` VARCHAR(45) NULL AFTER `NumerodoRegistro`;

ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `CodRubrica` `CodContaDespesa` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `CodRecVinculado` `DocumentoAlteracao` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `NumDocumento` `NumDocAlteracao` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `DataDocumento` `DataDocAlteracao` DATE NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `DataAlteracao` `DataPubDocAlteracao` DATE NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
CHANGE COLUMN `NumAlteracao` `NumLeiAutorizacao` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
ADD COLUMN `DataLeiAutorizacao` DATE NULL AFTER `NumLeiAutorizacao`;
ALTER TABLE `sicap_DecretoAltOrcamentaria`
ADD COLUMN `DataPubLeiAutorizacao` DATE NULL AFTER `DataLeiAutorizacao`;

ALTER TABLE `sicap_Empenho`
DROP COLUMN `CaracPeculiar`;
ALTER TABLE `sicap_Empenho`
CHANGE COLUMN `Tipo` `Tipo` VARCHAR(45) NULL DEFAULT NULL AFTER `Sinal`;
ALTER TABLE `sicap_Empenho`
CHANGE COLUMN `DataContrato` `DataProcesso` DATE NULL DEFAULT NULL AFTER `NumProcesso`;
ALTER TABLE `sicap_Empenho`
CHANGE COLUMN `Historico` `Historico` TEXT NULL DEFAULT NULL AFTER `NumObra`;
ALTER TABLE `sicap_Empenho`
CHANGE COLUMN `Data` `DataEmpenho` DATE NULL DEFAULT NULL ;
ALTER TABLE `sicap_Empenho`
CHANGE COLUMN `CodRubrica` `CodContaDespesa` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `CodSubFuncao` VARCHAR(45) NULL AFTER `CodFuncao`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `CodContaContabil` VARCHAR(45) NULL AFTER `CodContaDespesa`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `RegistroDePreco` VARCHAR(45) NULL AFTER `ModalLicita`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `ReferenciaLegal` VARCHAR(45) NULL AFTER `RegistroDePreco`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `DataConvenio` DATE NULL AFTER `NumConvenio`;
ALTER TABLE `sicap_Empenho`
ADD COLUMN `DataContrato` DATE NULL AFTER `NumContrato`;

ALTER TABLE `sicap_Liquidacao`
CHANGE COLUMN `Historico` `Historico` TEXT NULL DEFAULT NULL AFTER `Referencia`;
ALTER TABLE `sicap_Liquidacao`
CHANGE COLUMN `Data` `DataLiquidacao` DATE NULL DEFAULT NULL ;

ALTER TABLE `sicap_Pagamento`
CHANGE COLUMN `Data` `DataPagamento` DATE NULL DEFAULT NULL ;
ALTER TABLE `sicap_Pagamento`
ADD COLUMN `Exercicio` INT NULL AFTER `NumProcesso` ;

ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `NumLiquidacao` `NumLiquidacao` VARCHAR(45) NULL DEFAULT NULL AFTER `NumEmpenho`;
ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `Valor` `Valor` DECIMAL(10,2) NULL DEFAULT NULL AFTER `NumPagamento`;
ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `Sinal` `Sinal` CHAR(2) NULL DEFAULT NULL AFTER `Valor`;
ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `TipoPagamento` `TipoPagamento` VARCHAR(45) NULL DEFAULT NULL AFTER `Sinal`;
ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `NumCheque` `NumDocumento` VARCHAR(45) NULL DEFAULT NULL AFTER `TipoPagamento`;
ALTER TABLE `sicap_PagamentoFinanceiro`
CHANGE COLUMN `NumContaCorrente` `NumContaBancaria` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `sicap_PagamentoFinanceiro`
ADD COLUMN `TipoConta` INT NULL AFTER `NumContaBancaria`;
ALTER TABLE `sicap_PagamentoFinanceiro`
ADD COLUMN `Exercicio` INT NULL AFTER `TipoConta`;

ALTER TABLE `sicap_UndOrcamentaria`
ADD COLUMN `Descricao` TEXT NULL AFTER `Identificador`;
ALTER TABLE `sicap_UndOrcamentaria`
CHANGE COLUMN `CodOrgao` `CodOrgao` VARCHAR(45) NULL DEFAULT NULL AFTER `Exercicio`;
ALTER TABLE `sicap_UndOrcamentaria`
CHANGE COLUMN `CNPJ` `CNPJ` VARCHAR(45) NULL DEFAULT NULL AFTER `CodUndOrcamentaria`;

TRUNCATE `sicap_ContaDisponibilidade`;
TRUNCATE `sicap_Credor`;
TRUNCATE `sicap_DecretoAltOrcamentaria`;
TRUNCATE `sicap_Empenho`;
TRUNCATE `sicap_Funcao`;
TRUNCATE `sicap_InfoRemessa`;
TRUNCATE `sicap_Liquidacao`;
TRUNCATE `sicap_LoaReceita`;
TRUNCATE `sicap_Orgao`;
TRUNCATE `sicap_Pagamento`;
TRUNCATE `sicap_PagamentoFinanceiro`;
TRUNCATE `sicap_Programa`;
TRUNCATE `sicap_ProjAtividade`;
TRUNCATE `sicap_ReceitaArrecadada`;
TRUNCATE `sicap_RecursoVinculado`;
TRUNCATE `sicap_RubricaDespesa`;
TRUNCATE `sicap_SubFuncao`;
TRUNCATE `sicap_UndOrcamentaria`;
