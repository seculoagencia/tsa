<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empenhos', function (Blueprint $table) {
            $table->id();
            $table->string('numero', 45)->nullable();
            $table->string('credor', 100)->nullable();
            $table->date('data');
            $table->string('unidade_orcamentaria', 100)->nullable();
            $table->string('funcao', 100)->nullable();
            $table->string('subfuncao', 100)->nullable();
            $table->string('programa', 100)->nullable();
            $table->string('acao', 100)->nullable();
            $table->string('fonte_de_recurso', 100)->nullable();
            $table->string('elemento_de_despesa', 100)->nullable();
            $table->string('cpf_cnpj', 45)->nullable();
            $table->integer('numero_do_processo')->unsigned()->nullable();
            $table->text('historico')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empenhos');
    }
}
