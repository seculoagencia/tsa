<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSicapReceitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sicap_receitas', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodOrgao', 2)->nullable();
            $table->string('CodUndOrcamentaria', 4)->nullable();
            $table->string('CodContaReceita', 17)->nullable();
            $table->string('CodBanco', 5)->nullable();
            $table->string('CodAgencia', 5)->nullable();
            $table->string('NumConta', 20)->nullable();
            $table->string('CodContaAtivo', 17)->nullable();
            $table->date('DataArrecadacao')->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('CodRecVinculado', 9)->nullable();
            $table->string('FormaArrecadacao', 1)->nullable();
            $table->date('DataRegistro')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sicap_receitas');
    }
}
