<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCovidResumosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('covid_resumos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('casos_confirmados')->nullable();
            $table->bigInteger('casos_suspeitos')->nullable();
            $table->bigInteger('pessoas_recuperadas')->nullable();
            $table->bigInteger('obitos')->nullable();
            $table->string('arquivo', 255)->nullable();
            $table->string('nome_arquivo', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('covid_resumos');
    }
}
