<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)->nullable();
            $table->string('site_url', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->timestamp('last_update')->nullable();
            $table->boolean('view_last_update')->default(false);
            $table->string('recaptcha_site', 100)->nullable();
            $table->string('recaptcha_secret', 100)->nullable();
            $table->string('logo', 255)->nullable();
            $table->string('cover', 255)->nullable();
            $table->string('cor_principal', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_settings');
    }
}
