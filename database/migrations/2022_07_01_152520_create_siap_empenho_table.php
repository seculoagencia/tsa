<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSiapEmpenhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siap_empenho', function (Blueprint $table) {
            $table->id();
            $table->string('NumeroEmpenho', 13)->nullable();
            $table->string('Tipo', 1)->nullable();
            $table->integer('Ano')->nullable();
            $table->integer('Mes')->nullable();
            $table->string('CodigoUnidadeGestora', 10)->nullable();
            $table->string('CodigoUnidadeOrcamentaria', 4)->nullable();
            $table->string('FuncaoSubfuncao', 5)->nullable();
            $table->string('CodigoPrograma', 16)->nullable();
            $table->string('NumeroAcao', 16)->nullable();
            $table->string('ContaContabil', 9)->nullable();
            $table->string('NaturezaDespesa', 8)->nullable();
            $table->date('DataEmissao')->nullable();
            $table->integer('TipoContratacao')->nullable();
            $table->string('NumeroContratacao', 16)->nullable();
            $table->string('NumeroLicitacao', 16)->nullable();
            $table->string('NumeroContrato', 16)->nullable();
            $table->string('NumeroConvenio', 16)->nullable();
            $table->string('NumeroProcesso', 32)->nullable();
            $table->string('Credor', 14)->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('siap_credor', function (Blueprint $table) {
            $table->id();
            $table->string('Codigo', 14)->nullable();
            $table->string('Nome', 255)->nullable();
            $table->string('CNAE')->nullable();
            $table->integer('Tipo')->nullable();
            $table->timestamps();
        });

        Schema::create('siap_undorcamentaria', function (Blueprint $table) {
            $table->id();
            $table->string('Codigo', 10)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('siap_funcao', function (Blueprint $table) {
            $table->id();
            $table->string('Codigo', 2)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->timestamps();
        });

        $funcoes = json_decode(file_get_contents(public_path('siap/funcoes.json')), true);
        DB::table('siap_funcao')->insert($funcoes);

        Schema::create('siap_subfuncao', function (Blueprint $table) {
            $table->id();
            $table->string('Codigo', 3)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->timestamps();
        });

        $subfuncoes = json_decode(file_get_contents(public_path('siap/subfuncoes.json')), true);
        DB::table('siap_subfuncao')->insert($subfuncoes);

        Schema::create('siap_acao', function (Blueprint $table) {
            $table->id();
            $table->string('Numero', 16)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->integer('Tipo')->nullable();
            $table->string('CodigoUnidadeGestora', 10)->nullable();
            $table->string('CodigoUnidadeOrcamentaria', 10)->nullable();
            $table->string('CodigoFuncao', 2)->nullable();
            $table->string('CodigoSubfuncao', 3)->nullable();
            $table->string('CodigoPrograma', 10)->nullable();
            $table->integer('Exercicio')->nullable();
            $table->integer('Mes')->nullable();
            $table->timestamps();
        });

        Schema::create('siap_pagamento', function (Blueprint $table) {
            $table->id();
            $table->string('Numero', 16)->nullable();
            $table->string('NumeroEmpenho', 16)->nullable();
            $table->string('CodigoUnidadeOrcamentaria', 10)->nullable();
            $table->string('ContaContabil', 10)->nullable();
            $table->date('Data')->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->integer('Exercicio')->nullable();
            $table->integer('Mes')->nullable();
            $table->timestamps();
        });

        Schema::create('siap_liquidacao', function (Blueprint $table) {
            $table->id();
            $table->string('Numero', 16)->nullable();
            $table->string('NumeroEmpenho', 16)->nullable();
            $table->string('CodigoUnidadeOrcamentaria', 10)->nullable();
            $table->string('ContaContabil', 10)->nullable();
            $table->date('Data')->nullable();
            $table->integer('TipoDocumento')->nullable();
            $table->string('NumeroDocumentoFiscal', 8)->nullable();
            $table->string('ChaveAcesso', 44)->nullable();
            $table->string('SerieDocumentoFiscal', 4)->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->longText('Justificativa')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->integer('Mes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siap_empenho');
        Schema::dropIfExists('siap_credor');
        Schema::dropIfExists('siap_undorcamentaria');
        Schema::dropIfExists('siap_funcao');
        Schema::dropIfExists('siap_subfuncao');
        Schema::dropIfExists('siap_acao');
        Schema::dropIfExists('siap_pagamento');
        Schema::dropIfExists('siap_liquidacao');
    }
}
