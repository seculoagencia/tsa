<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSiapEmpenhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siap_empenho', function (Blueprint $table) {
            $table->string('CodigoUnidadeOrcamentaria', 10)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siap_empenho', function (Blueprint $table) {
            $table->string('CodigoUnidadeOrcamentaria', 4)->nullable()->change();
        });
    }
}
