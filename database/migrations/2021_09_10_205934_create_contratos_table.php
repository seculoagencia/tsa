<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->id();
            $table->string('orgao', 255)->nullable();
            $table->string('tipo', 1)->nullable();
            $table->string('modalidade', 255)->nullable();
            $table->dateTime('data_hora')->nullable();
            $table->longText('objeto')->nullable();
            $table->decimal('valor', 20, 2)->nullable();
            $table->string('status', 1)->nullable();
            $table->string('vencedor', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
