<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('email', 50);
            $table->integer('priority');
            $table->char('cpf', 25);
            $table->string('phone', 25);
            $table->text('location');
            $table->string('type_answer', 50);
            $table->integer('typerequest_id');
            $table->string('subject', 150);
            $table->text('message');
            $table->string('anexo', 255)->nullable();
            $table->integer('status')->default(0);
            $table->string('serial', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
