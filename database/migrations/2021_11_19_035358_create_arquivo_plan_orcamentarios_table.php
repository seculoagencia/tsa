<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArquivoPlanOrcamentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arquivo_plan_orcamentarios', function (Blueprint $table) {
            $table->id();
            $table->string('name', 45);
            $table->integer('attachment_id')->unsigned();
            $table->integer('tipo_planejamento_orcamentario_id')->unsigned();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arquivo_plan_orcamentarios');
    }
}
