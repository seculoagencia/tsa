<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSicapServidoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sicap_servidores', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('Cpf', 11)->nullable();
            $table->string('Matricula', 50)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->date('DataNascimento')->nullable();
            $table->string('NomeMae', 100)->nullable();
            $table->string('NomePai', 100)->nullable();
            $table->string('NomeConjuge', 100)->nullable();
            $table->string('PisPasep', 11)->nullable();
            $table->string('TituloEleitoral', 12)->nullable();
            $table->date('DataAdmissao')->nullable();
            $table->string('CodVinculoEmpregaticio', 1)->nullable();
            $table->string('CodRegimePrevidenciario', 1)->nullable();
            $table->string('CodEscolaridade', 1)->nullable();
            $table->string('DescricaoEscolaridade', 200)->nullable();
            $table->string('SobCessao', 1)->nullable();
            $table->string('CnpjEntidade', 14)->nullable();
            $table->string('NomeEntidade', 200)->nullable();
            $table->date('DataCessao')->nullable();
            $table->date('DataRetornoCessao')->nullable();
            $table->decimal('MargemConsignada', 10, 2)->nullable();
            $table->string('CBO', 6)->nullable();
            $table->string('CodCargo', 15)->nullable();
            $table->string('CodLotacao', 15)->nullable();
            $table->string('CodFuncao', 15)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sicap_cargos_servidores', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodCargo', 15)->nullable();
            $table->string('Descricao', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sicap_cargos_servidores');
        Schema::dropIfExists('sicap_servidores');
    }
}
