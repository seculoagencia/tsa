<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSagresEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sagres_empenhos', function (Blueprint $table) {
            $table->id();
            $table->integer('ano')->nullable();
            $table->string('cod_unid_orc', 10)->nullable();
            $table->string('funcao', 2)->nullable();
            $table->string('subfuncao', 3)->nullable();
            $table->string('programa', 4)->nullable();
            $table->string('acao', 6)->nullable();
            $table->integer('id_acao')->nullable();
            $table->integer('cod_cat_econ')->nullable();
            $table->integer('cod_nat_desp')->nullable();
            $table->integer('modalidade_aplic')->nullable();
            $table->integer('cod_elem_desp_dot')->nullable();
            $table->integer('sub_elem_desp')->nullable();
            $table->integer('modalidade_licit')->nullable();
            $table->string('num_empenho', 7)->nullable();
            $table->integer('tipo_empenho')->nullable();
            $table->date('data_emissao_empenho')->nullable();
            $table->decimal('valor_empenhado', 16, 2)->nullable();
            $table->string('historico', 510)->nullable();
            $table->string('cpf_cnpj_credor', 14)->nullable();
            $table->string('num_procedimento', 9)->nullable();
            $table->string('font_recurso', 6)->nullable();
            $table->string('cpf_ordenador', 11)->nullable();
            $table->string('cod_elem_desp', 2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sagres_credor', function (Blueprint $table) {
            $table->id();
            $table->string('cpf_cnpj', 14)->nullable();
            $table->string('nome', 80)->nullable();
            $table->integer('tipo_credor')->nullable();
            $table->char('sigla_uf', 2)->nullable();
            $table->string('municipio', 60)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_undorcamentaria', function (Blueprint $table) {
            $table->id();
            $table->string('codigo', 10)->nullable();
            $table->string('denominacao', 50)->nullable();
            $table->char('num_unid_jurisd', 6)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_funcao', function (Blueprint $table) {
            $table->id();
            $table->char('codigo', 2)->nullable();
            $table->string('nome', 30)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_subfuncao', function (Blueprint $table) {
            $table->id();
            $table->char('cod_funcao', 2)->nullable();
            $table->char('codigo', 3)->nullable();
            $table->string('nome', 70)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_programa', function (Blueprint $table) {
            $table->id();
            $table->char('codigo', 4)->nullable();
            $table->string('nome', 70)->nullable();
            $table->string('descricao', 150)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_acao', function (Blueprint $table) {
            $table->id();
            $table->char('codigo', 6)->nullable();
            $table->string('nome', 70)->nullable();
            $table->integer('identificacao')->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_elemento_despesa', function (Blueprint $table) {
            $table->id();
            $table->char('codigo', 2)->nullable();
            $table->string('descricao', 90)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_liquidacao', function (Blueprint $table) {
            $table->id();
            $table->integer('ano')->nullable();
            $table->string('cod_unid_orc', 10)->nullable();
            $table->char('num_empenho', 7)->nullable();
            $table->char('num_liquidacao', 7)->nullable();
            $table->date('data')->nullable();
            $table->decimal('valor', 16, 2)->nullable();
            $table->integer('tipo_doc')->nullable();
            $table->string('num_chave', 44)->nullable();
            $table->string('historico', 510)->nullable();
            $table->char('cod_fonte_recurso', 6)->nullable();
            $table->timestamps();
        });

        Schema::create('sagres_pagamentos', function (Blueprint $table) {
            $table->id();
            $table->integer('ano')->nullable();
            $table->string('cod_unid_orc', 10)->nullable();
            $table->char('num_empenho', 7)->nullable();
            $table->char('num_parc_pagamento', 7)->nullable();
            $table->date('data')->nullable();
            $table->decimal('valor', 16, 2)->nullable();
            $table->char('cod_banco', 3)->nullable();
            $table->char('num_agencia_bancaria', 6)->nullable();
            $table->string('num_conta_bancaria', 12)->nullable();
            $table->char('num_seq', 7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sagres_pagamentos');
        Schema::dropIfExists('sagres_liquidacao');
        Schema::dropIfExists('sagres_elemento_despesa');
        Schema::dropIfExists('sagres_acao');
        Schema::dropIfExists('sagres_programa');
        Schema::dropIfExists('sagres_subfuncao');
        Schema::dropIfExists('sagres_funcao');
        Schema::dropIfExists('sagres_undorcamentaria');
        Schema::dropIfExists('sagres_credor');
        Schema::dropIfExists('sagres_empenhos');
    }
}
