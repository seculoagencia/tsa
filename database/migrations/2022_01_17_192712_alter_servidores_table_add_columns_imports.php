<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterServidoresTableAddColumnsImports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servidores', function (Blueprint $table) {
            $table->string('cpf', 15)->nullable();
            $table->string('regime', 150)->nullable();
            $table->string('cargo', 150)->nullable();
            $table->string('secretaria', 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servidores', function (Blueprint $table) {
            $table->dropColumn('cpf');
            $table->dropColumn('regime');
            $table->dropColumn('cargo');
            $table->dropColumn('secretaria');
        });
    }
}
