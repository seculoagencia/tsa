<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagamentos', function (Blueprint $table) {
            $table->id();
            $table->string('banco', 45)->nullable();
            $table->string('agencia', 45)->nullable();
            $table->string('conta', 45)->nullable();
            $table->string('documento', 45)->nullable();
            $table->date('data')->nullable();
            $table->decimal('valor', 10, 2);
            $table->integer('empenho_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamentos');
    }
}
