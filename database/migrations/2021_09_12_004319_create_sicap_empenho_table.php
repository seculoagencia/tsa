<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSicapEmpenhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sicap_empenho', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('NumEmpenho', 13)->nullable();
            $table->date('DataEmpenho')->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('Sinal', 1)->nullable();
            $table->string('Tipo', 1)->nullable();
            $table->string('CodOrgao', 2)->nullable();
            $table->string('CodUndOrcamentaria', 4)->nullable();
            $table->string('CodFuncao', 2)->nullable();
            $table->string('CodSubFuncao', 3)->nullable();
            $table->string('CodPrograma', 4)->nullable();
            $table->string('CodProjAtividade', 4)->nullable();
            $table->string('CodContaDespesa', 17)->nullable();
            $table->string('CodRecVinculado', 9)->nullable();
            $table->string('ContraPartida', 9)->nullable();
            $table->string('CodCredor', 14)->nullable();
            $table->string('ModalLicita', 2)->nullable();
            $table->string('RegistroDePreco', 1)->nullable();
            $table->string('ReferenciaLegal', 50)->nullable();
            $table->string('NumProcesso', 15)->nullable();
            $table->date('DataProcesso')->nullable();
            $table->string('NumContrato', 15)->nullable();
            $table->date('DataContrato')->nullable();
            $table->string('NumConvenio', 15)->nullable();
            $table->date('DataConvenio')->nullable();
            $table->string('NumObra', 15)->nullable();
            $table->longText('Historico')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sicap_credor', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodCredor', 14)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->string('InscricaoEstadual', 15)->nullable();
            $table->string('InscricaoMunicipal', 15)->nullable();
            $table->text('Endereco')->nullable();
            $table->string('Cidade', 50)->nullable();
            $table->string('UF', 2)->nullable();
            $table->string('Cep', 8)->nullable();
            $table->string('Fone', 10)->nullable();
            $table->string('Fax', 10)->nullable();
            $table->integer('Tipo')->nullable();
            $table->string('NumeroDoRegistro', 20)->nullable();
            $table->string('TipoRegistro', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_orgao', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodOrgao', 2)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_undorcamentaria', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodOrgao', 2)->nullable();
            $table->string('CodUndOrcamentaria', 4)->nullable();
            $table->string('CNPJ', 14)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->string('Identificador', 2)->nullable();
            $table->string('Descricao', 200)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_funcao', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodFuncao', 2)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_subfuncao', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodSubFuncao', 3)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_programa', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodPrograma', 4)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->string('Objetivo', 255)->nullable();
            $table->string('PublicoAlvo', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_projatividade', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodProjAtividade', 4)->nullable();
            $table->string('Identificador', 2)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->string('TipoPrograma', 1)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_rubricadespesa', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodRubrica', 17)->nullable();
            $table->longText('Especificacao')->nullable();
            $table->string('TipoNivelConta', 1)->nullable();
            $table->string('NumNivelConta', 2)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_recursovinculado', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('CodRecVinculado', 9)->nullable();
            $table->string('Nome', 100)->nullable();
            $table->longText('Finalidade')->nullable();
            $table->string('Tipo', 1)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_pagamento', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('NumEmpenho', 13)->nullable();
            $table->string('NumPagamento', 13)->nullable();
            $table->string('NumLiquidacao', 13)->nullable();
            $table->date('DataPagamento')->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('Sinal', 1)->nullable();
            $table->longText('Historico')->nullable();
            $table->string('CodOperacao', 30)->nullable();
            $table->string('NumProcesso', 30)->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_liquidacao', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('NumEmpenho', 13)->nullable();
            $table->string('NumLiquidacao', 13)->nullable();
            $table->date('DataLiquidacao')->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('Sinal', 1)->nullable();
            $table->string('CodOperacao', 30)->nullable();
            $table->string('NumProcesso', 15)->nullable();
            $table->string('CodCredor', 14)->nullable();
            $table->string('Referencia', 7)->nullable();
            $table->longText('Historico')->nullable();
            $table->timestamps();
        });

        Schema::create('sicap_pagamentofinanceiro', function (Blueprint $table) {
            $table->id();
            $table->string('CodUndGestora', 14)->nullable();
            $table->string('CodigoUA', 4)->nullable();
            $table->integer('Bimestre')->nullable();
            $table->integer('Exercicio')->nullable();
            $table->string('NumEmpenho', 13)->nullable();
            $table->string('NumLiquidacao', 13)->nullable();
            $table->string('NumPagamento', 13)->nullable();
            $table->decimal('Valor', 10, 2)->nullable();
            $table->string('Sinal', 1)->nullable();
            $table->string('TipoPagamento', 45)->nullable();
            $table->string('NumDocumento', 45)->nullable();
            $table->string('CodContaBalancete', 45)->nullable();
            $table->string('CodBanco', 45)->nullable();
            $table->string('CodAgenciaBanco', 45)->nullable();
            $table->string('NumContaBancaria', 45)->nullable();
            $table->string('TipoConta', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sicap_empenho');
        Schema::dropIfExists('sicap_credor');
        Schema::dropIfExists('sicap_orgao');
        Schema::dropIfExists('sicap_undorcamentaria');
        Schema::dropIfExists('sicap_funcao');
        Schema::dropIfExists('sicap_subfuncao');
        Schema::dropIfExists('sicap_programa');
        Schema::dropIfExists('sicap_projatividade');
        Schema::dropIfExists('sicap_rubricadespesa');
        Schema::dropIfExists('sicap_recursovinculado');
        Schema::dropIfExists('sicap_pagamento');
        Schema::dropIfExists('sicap_liquidacao');
        Schema::dropIfExists('sicap_pagamentofinanceiro');
    }
}
