<?php

namespace Database\Seeders;

use App\Models\OptionSearch;
use App\Models\SubMenu;
use Illuminate\Database\Seeder;

class OptionSearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Menu opção
//        OptionSearch::create([
//            'name' => 'Orçamento',
//            'route' => 'orcamento',
//            'icon' => 'media/svg/icons/Shopping/Calculator.svg'
//        ]);
//
//        OptionSearch::create([
//            'name' => 'Despesas',
//            'route' => 'despesas',
//            'icon' => 'media/svg/icons/Shopping/Dollar.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Receitas',
//            'route' => 'receitas',
//            'icon' => 'media/svg/icons/Shopping/Dollar.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Servidores',
//            'route' => 'servidores',
//            'icon' => 'media/svg/icons/General/User.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Licitações',
//            'route' => 'licitacoes',
//            'icon' => 'media/svg/icons/Shopping/Cart1.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Contratos',
//            'route' => 'contratos',
//            'icon' => 'media/svg/icons/Files/Selected-file.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Diárias',
//            'route' => 'diarias',
//            'icon' => 'media/svg/icons/Map/Marker1.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Publicações Oficiais',
//            'route' => 'publicacoes-oficiais',
//            'icon' => 'media/svg/icons/Design/Pen-tool-vector.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Lesgislções',
//            'route' => 'legislacoes',
//            'icon' => 'media/svg/icons/Tools/Hummer2.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Estatistica',
//            'route' => 'estatisticas',
//            'icon' => 'media/svg/icons/Shopping/Chart-bar2.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Estrutura Organizacional',
//            'route' => 'estrutura-organizacional',
//            'icon' => 'media/svg/icons/Code/Git4.svg'
//        ]);
//        OptionSearch::create([
//            'name' => 'Responsabilidade Fiscal',
//            'route' => 'lrf',
//            'icon' => 'media/svg/icons/Tools/Hummer2.svg'
//        ]);
//
//        OptionSearch::create([
//            'name' => 'Verbas Idenizadoras',
//            'route' => 'verba',
//            'icon' => 'media/svg/icons/Shopping/Dollar.svg'
//        ]);
        //Sub Menus

        SubMenu::create([
            'name' => 'Atas de Registro de Pagamento',
            'id_menu' =>5,
            'route'=>'atas',
        ]);

        SubMenu::create([
            'name' => 'Carta Convite',
            'id_menu' =>5,
            'route'=>'carta',
        ]);
        SubMenu::create([
            'name' => 'Inexigibilidade',
            'id_menu' =>5,
            'route'=>'inexigibilidade',
        ]);
        SubMenu::create([
            'name' => 'Concorrência',
            'id_menu' =>5,
            'route' => 'concorrencia',
        ]);
        SubMenu::create([
            'name' => 'Pregão Eletrônico',
            'id_menu' =>5,
            'route' => 'pregao.eletronico',
        ]);
        SubMenu::create([
            'name' => 'Pregão Presencial',
            'id_menu' =>5,
            'route' => 'pregao.presencial',
        ]);
        SubMenu::create([
            'name' => 'Tomada de Preços',
            'id_menu' =>5,
            'route' => 'tomada.precos',
        ]);
        SubMenu::create([
            'name' => 'Dispensa de Licitação',
            'id_menu' =>6,
            'route' => 'dispensa',
        ]);
        SubMenu::create([
            'name' => 'Contrato Pessoa Física',
            'id_menu' =>6,
            'route' => 'contrato.fisica',
        ]);
        SubMenu::create([
            'name' => 'Contrato Pessoa Juridica',
            'id_menu' =>6,
            'route' => 'contrato.juridica',
        ]);


    }
}
