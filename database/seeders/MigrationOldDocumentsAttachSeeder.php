<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AppSetting;
use App\Models\TypeDocument;
use App\Models\Attachment;
use App\Models\Empenho;
use App\Models\Liquidacao;
use App\Models\Pagamento;
use App\Models\Receita;
use App\Models\Cargo;
use App\Models\Servidor;
use App\Models\Contrato;
use App\Models\Diaria;
use App\Models\Document;
use App\Models\TipoPedido;
use App\Models\Pedido;
use App\Models\Resposta;
use Illuminate\Support\Facades\DB;
use Schema;

class MigrationOldDocumentsAttachSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Migrar Document
        $documents = DB::connection('mysql_old')->table('reports')
            ->select('id', 'year', 'month', 'name', 'category_id as type_document_id', 'user_id')->orderBy('id', 'asc')->get();
        foreach ($documents as $key => $document) {
            $checkDocument = Document::where('year', $document->year)->where('month', $document->month)->where('name', $document->name)
                ->where('type_document_id', $document->type_document_id)->first();
            if (!$checkDocument) {
                $newDocument = Document::create((array) $document);
            } else {
                $newDocument = $checkDocument;
            }

            $anexos =  DB::connection('mysql_old')->table('reports_anexos')->where('report_id', $document->id)->get();

            foreach ($anexos as $key => $anexo) {
                $attachment = Attachment::create(['original_name' => str_replace('uploads/anexos/', '', $anexo->anexo), 'path' => $anexo->anexo]);
                $newDocument->attachments()->syncWithoutDetaching($attachment->id);
            }
        }
        echo "Document - Sucesso!\n";
    }
}
