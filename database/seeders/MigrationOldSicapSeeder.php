<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SicapEmpenho;
use App\Models\SicapCredor;
use App\Models\SicapUndOrcamentaria;
use App\Models\SicapFuncao;
use App\Models\SicapSubFuncao;
use App\Models\SicapPrograma;
use App\Models\SicapProjetoAtividade;
use App\Models\SicapRecursoVinculado;
use App\Models\SicapRubricaDespesa;
use App\Models\SicapLiquidacao;
use App\Models\SicapPagamento;
use App\Models\SicapPagamentoFinanceiro;
use App\Models\SicapOrgao;
use Schema;
use DB;

class MigrationOldSicapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Migrar Sicap Empenho
        $sicap_empenhos = DB::connection('mysql_old')->table('sicap_Empenho')
        ->select(
            'id', 
            'CodUndGestora', 
            'CodigoUA', 
            'Bimestre', 
            'Exercicio', 
            'NumEmpenho', 
            'DataEmpenho', 
            'Valor', 
            'Sinal', 
            'Tipo', 
            'CodOrgao', 
            'CodUndOrcamentaria', 
            'CodFuncao', 
            'CodSubFuncao', 
            'CodPrograma', 
            'CodProjAtividade', 
            'CodContaDespesa', 
            'CodRecVinculado', 
            'CodCredor', 
            'ModalLicita', 
            'RegistroDePreco', 
            'ReferenciaLegal', 
            'NumProcesso', 
            'DataProcesso', 
            'NumContrato', 
            'DataContrato', 
            'NumConvenio', 
            'DataConvenio', 
            'NumObra', 
            'Historico', 
            'deleted as deleted_at'
        )
        ->orderBy('id','asc')->get();
        foreach ($sicap_empenhos as $key => $sicap_empenho) {
            $sicap_empenho->deleted_at = $sicap_empenho->deleted_at ? date('Y-m-d H:i:s') : null;
            SicapEmpenho::create((array) $sicap_empenho);
        }
        echo "Sicap Empenho - Sucesso!\n";

        // Migrar Sicap Credor
        $sicap_credors = DB::connection('mysql_old')->table('sicap_Credor')
        ->orderBy('id','asc')->get();
        foreach ($sicap_credors as $key => $sicap_credor) {
            SicapCredor::create((array) $sicap_credor);
        }
        echo "Sicap Credor - Sucesso!\n";

        // Migrar Sicap UndOrcamentaria
        $sicap_undorcamentarias = DB::connection('mysql_old')->table('sicap_UndOrcamentaria')
        ->orderBy('id','asc')->get();
        foreach ($sicap_undorcamentarias as $key => $sicap_undorcamentaria) {
            SicapUndOrcamentaria::create((array) $sicap_undorcamentaria);
        }
        echo "Sicap UndOrcamentaria - Sucesso!\n";

        // Migrar Sicap Funcao
        $sicap_funcaos = DB::connection('mysql_old')->table('sicap_Funcao')
        ->orderBy('id','asc')->get();
        foreach ($sicap_funcaos as $key => $sicap_funcao) {
            SicapFuncao::create((array) $sicap_funcao);
        }
        echo "Sicap Funcao - Sucesso!\n";

        // Migrar Sicap SubFuncao
        $sicap_subfuncaos = DB::connection('mysql_old')->table('sicap_SubFuncao')
        ->orderBy('id','asc')->get();
        foreach ($sicap_subfuncaos as $key => $sicap_subfuncao) {
            SicapSubFuncao::create((array) $sicap_subfuncao);
        }
        echo "Sicap SubFuncao - Sucesso!\n";

        // Migrar Sicap Programa
        $sicap_programas = DB::connection('mysql_old')->table('sicap_Programa')
        ->orderBy('id','asc')->get();
        foreach ($sicap_programas as $key => $sicap_programa) {
            SicapPrograma::create((array) $sicap_programa);
        }
        echo "Sicap Programa - Sucesso!\n";

        // Migrar Sicap ProjetoAtividade
        $sicap_projatividades = DB::connection('mysql_old')->table('sicap_ProjAtividade')
        ->orderBy('id','asc')->get();
        foreach ($sicap_projatividades as $key => $sicap_projatividade) {
            SicapProjetoAtividade::create((array) $sicap_projatividade);
        }
        echo "Sicap ProjetoAtividade - Sucesso!\n";

        // Migrar Sicap RecursoVinculado
        $sicap_recursovinculados = DB::connection('mysql_old')->table('sicap_RecursoVinculado')
        ->orderBy('id','asc')->get();
        foreach ($sicap_recursovinculados as $key => $sicap_recursovinculado) {
            SicapRecursoVinculado::create((array) $sicap_recursovinculado);
        }
        echo "Sicap RecursoVinculado - Sucesso!\n";

        // Migrar Sicap RubricaDespesa
        $sicap_rubricadespesas = DB::connection('mysql_old')->table('sicap_RubricaDespesa')
        ->orderBy('id','asc')->get();
        foreach ($sicap_rubricadespesas as $key => $sicap_rubricadespesa) {
            SicapRubricaDespesa::create((array) $sicap_rubricadespesa);
        }
        echo "Sicap RubricaDespesa - Sucesso!\n";

        // Migrar Sicap Liquidacao
        $sicap_liquidacaos = DB::connection('mysql_old')->table('sicap_Liquidacao')
        ->orderBy('id','asc')->get();
        foreach ($sicap_liquidacaos as $key => $sicap_liquidacao) {
            SicapLiquidacao::create((array) $sicap_liquidacao);
        }
        echo "Sicap Liquidacao - Sucesso!\n";

        // Migrar Sicap Pagamento
        $sicap_pagamentos = DB::connection('mysql_old')->table('sicap_Pagamento')
        ->orderBy('id','asc')->get();
        foreach ($sicap_pagamentos as $key => $sicap_pagamento) {
            SicapPagamento::create((array) $sicap_pagamento);
        }
        echo "Sicap Pagamento - Sucesso!\n";

        // Migrar Sicap PagamentoFinanceiro
        $sicap_pagamentofinanceiros = DB::connection('mysql_old')->table('sicap_PagamentoFinanceiro')
        ->orderBy('id','asc')->get();
        foreach ($sicap_pagamentofinanceiros as $key => $sicap_pagamentofinanceiro) {
            SicapPagamentoFinanceiro::create((array) $sicap_pagamentofinanceiro);
        }
        echo "Sicap PagamentoFinanceiro - Sucesso!\n";

        // Migrar Sicap Orgao
        $sicap_orgaos = DB::connection('mysql_old')->table('sicap_Orgao')
        ->orderBy('id','asc')->get();
        foreach ($sicap_orgaos as $key => $sicap_orgao) {
            SicapOrgao::create((array) $sicap_orgao);
        }
        echo "Sicap Orgao - Sucesso!\n";
    }
}
