<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        $role = Role::create(['name' => 'Super Usuário']);
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);

        foreach ($users as $key => $user) {
            $user->assignRole([$role->id]);
        }
    }
}
