<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Master',
            'username' => 'master',
            'email' => 'master@email.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make('master'),
            'status' => true,
            'superuser' => true,
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@email.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make('Ferreira2013'),
            'status' => true,
            'superuser' => true,
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}