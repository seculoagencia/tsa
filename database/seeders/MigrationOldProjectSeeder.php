<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AppSetting;
use App\Models\TypeDocument;
use App\Models\Attachment;
use App\Models\Empenho;
use App\Models\Liquidacao;
use App\Models\Pagamento;
use App\Models\Receita;
use App\Models\Cargo;
use App\Models\Servidor;
use App\Models\Contrato;
use App\Models\Diaria;
use App\Models\Document;
use App\Models\TipoPedido;
use App\Models\Pedido;
use App\Models\Resposta;
use App\Models\Verba;
use Illuminate\Support\Facades\DB;
use Schema;

class MigrationOldProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Migrar AppSetting
        $settings = DB::connection('mysql_old')->table('settings')->first();
        if ($settings) {
            AppSetting::create((array) $settings);
        }
        echo "AppSetting - Sucesso!\n";

        // Migrar TypeDocument
        $categories = DB::connection('mysql_old')->table('categories')->orderBy('id', 'asc')->get();
        foreach ($categories as $key => $category) {
            TypeDocument::create((array) $category);
        }
        echo "TypeDocument - Sucesso!\n";

        // Migrar Document
        $documents = DB::connection('mysql_old')->table('reports')
            ->select('id', 'year', 'month', 'name', 'category_id as type_document_id', 'user_id')->orderBy('id', 'asc')->get();
        foreach ($documents as $key => $document) {
            Document::create((array) $document);
        }
        echo "Document - Sucesso!\n";

        // Migrar Verba
        $verbas = DB::connection('mysql_old')->table('verbas')
            ->select('id', 'mes as referencia', 'data', 'beneficiario', 'valor')->orderBy('id', 'asc')->get();
        foreach ($verbas as $key => $verba) {
            $verba->referencia = date('Y', strtotime($verba->data)) . '-' . $verba->referencia;
            Verba::create((array) $verba);
        }
        echo "Verba - Sucesso!\n";

        // Migrar Empenhos
        $empenhos = DB::connection('mysql_old')->table('empenhos')
            ->select(
                'id',
                'numero',
                'credor',
                'data',
                'unidade_orcamentaria',
                'funcao',
                'subfuncao',
                'programa',
                'acao',
                'fonte_de_recurso',
                'elemento_de_despesa',
                'cpf_cnpj',
                'numero_do_processo',
                'historico'
            )->orderBy('id', 'asc')->get();
        foreach ($empenhos as $key => $empenho) {
            Empenho::create((array) $empenho);
        }
        echo "Empenhos - Sucesso!\n";

        // Migrar Anexos de Empenhos
        $empenhos_anexos = DB::connection('mysql_old')->table('empenhos_anexos')->orderBy('id', 'asc')->get();
        foreach ($empenhos_anexos as $key => $anexo) {
            $empenho = Empenho::find($anexo->report_id);
            if ($empenho) {
                $attachment = Attachment::create(['path' => $anexo->anexo]);
                if ($attachment) {
                    $empenho->attachments()->syncWithoutDetaching($attachment->id);
                }
            }
        }
        echo "Anexos de Empenhos - Sucesso!\n";

        // Migrar Liquidacoes
        $liquidacaos = DB::connection('mysql_old')->table('liquidacoes')
            ->select('id', 'numero', 'created as created_at', 'empenho_id', 'data', 'valor')->orderBy('id', 'asc')->get();
        foreach ($liquidacaos as $key => $liquidacao) {
            if ($liquidacao->empenho_id) {
                $liquidacao->valor = number_format($liquidacao->valor, 2, ',', '.');
                Liquidacao::create((array) $liquidacao);
            }
        }
        echo "Liquidacoes - Sucesso!\n";

        // Migrar Pagamento
        $pagamentos = DB::connection('mysql_old')->table('pagamentos')
            ->select('id', 'banco', 'agencia', 'conta', 'documento', 'data', 'valor', 'created as created_at', 'empenho_id')->orderBy('id', 'asc')->get();
        foreach ($pagamentos as $key => $pagamento) {
            if ($pagamento->empenho_id) {
                $pagamento->valor = number_format($pagamento->valor, 2, ',', '.');
                Pagamento::create((array) $pagamento);
            }
        }
        echo "Pagamento - Sucesso!\n";

        // Migrar Receitas
        $receitas = DB::connection('mysql_old')->table('receitas')
            ->select('id', 'codigo', 'descricao', 'data', 'valor')->orderBy('id', 'asc')->get();
        foreach ($receitas as $key => $receita) {
            Receita::create((array) $receita);
        }
        echo "Receitas - Sucesso!\n";

        // Migrar Anexos de Receitas
        $receitas_anexos = DB::connection('mysql_old')->table('receitas_anexos')->orderBy('id', 'asc')->get();
        foreach ($receitas_anexos as $key => $anexo) {
            $receita = Receita::find($anexo->report_id);
            if($receita) {
                $attachment = Attachment::create(['path' => $anexo->anexo]);
                $receita->attachments()->syncWithoutDetaching($attachment->id);
            }
        }
        echo "Anexos de Receitas - Sucesso!\n";

        // Migrar Cargos
        $cargos = DB::connection('mysql_old')->table('cargos')
            ->select('id', 'name as nome')->orderBy('id', 'asc')->get();
        foreach ($cargos as $key => $cargo) {
            Cargo::create((array) $cargo);
        }
        echo "Cargos - Sucesso!\n";

        // Migrar Servidores
        $servidores = DB::connection('mysql_old')->table('servidores')
            ->select('id', 'nome', 'valor_bruto', 'descontos', 'valor_liquido', 'mes_ref as referencia')
            ->orderBy('id', 'asc')->get();
        foreach ($servidores as $key => $servidore) {
            Servidor::create((array) $servidore);
        }
        echo "Servidores - Sucesso!\n";

        // Migrar Anexos de Servidores
        $servidores_anexos = DB::connection('mysql_old')->table('servidores_anexos')->orderBy('id', 'asc')->get();
        foreach ($servidores_anexos as $key => $anexo) {
            $servidore = Servidor::find($anexo->report_id);
            if ($servidore) {
                $attachment = Attachment::create(['path' => $anexo->anexo]);
                $servidore->attachments()->syncWithoutDetaching($attachment->id);
            }
        }
        echo "Anexos de Servidores - Sucesso!\n";

        // Migrar Licitações e Contratos
        $contratos = DB::connection('mysql_old')->table('licitacoes')
            ->select('id', 'orgao', 'tipo', 'modalidade', 'data as data_hora', 'objeto', 'valor', 'status', 'vencedor')
            ->orderBy('id', 'asc')->get();
        foreach ($contratos as $key => $contrato) {
            $contrato->tipo = $contrato->tipo == 2 ? 'L' : 'C';
            switch ($contrato->status) {
                case '1':
                    $contrato->status = 'C';
                    break;
                case '2':
                    $contrato->status = 'A';
                    break;

                default:
                    $contrato->status = 'S';
                    break;
            }
            Contrato::create((array) $contrato);
        }
        echo "Licitações e Contratos - Sucesso!\n";

        // Migrar Anexos de Licitações e Contratos
        $licitacoes_anexos = DB::connection('mysql_old')->table('licitacoes_anexos')->orderBy('id', 'asc')->get();
        foreach ($licitacoes_anexos as $key => $anexo) {
            $contrato = Contrato::find($anexo->report_id);
            if($contrato) {
                $attachment = Attachment::create(['path' => $anexo->anexo]);
                if($attachment) {
                    $contrato->attachments()->syncWithoutDetaching($attachment->id);
                }
            }
        }
        echo "Anexos de Licitações e Contratos - Sucesso!\n";

        // Migrar Diarias
        $diarias = DB::connection('mysql_old')->table('diarias')
            ->select('id', 'favorecido', 'cargo_id', 'data', 'destino', 'motivo', 'valor')
            ->orderBy('id', 'asc')->get();
        foreach ($diarias as $key => $diaria) {
            Diaria::create((array) $diaria);
        }
        echo "Diarias - Sucesso!\n";

        // Migrar Anexos de Diarias
        $diarias_anexos = DB::connection('mysql_old')->table('diarias_anexos')->orderBy('id', 'asc')->get();
        foreach ($diarias_anexos as $key => $anexo) {
            $diaria = Diaria::find($anexo->report_id);
            if ($diaria) {
                $attachment = Attachment::create(['path' => $anexo->anexo]);
                $diaria->attachments()->syncWithoutDetaching($attachment->id);
            }
        }
        echo "Anexos de Diarias - Sucesso!\n";

        // Migrar TipoPedidos
        $tipoPedidos = DB::connection('mysql_old')->table('typerequests')->orderBy('id', 'asc')->get();
        foreach ($tipoPedidos as $key => $tipoPedido) {
            TipoPedido::create((array) $tipoPedido);
        }
        echo "TipoPedidos - Sucesso!\n";

        // Migrar Pedidos
        $pedidos = DB::connection('mysql_old')->table('requests')
            ->select(
                'id',
                'name',
                'email',
                'priority',
                'cpf',
                'phone',
                'location',
                'type_answer',
                'typerequest_id',
                'subject',
                'message',
                'anexo',
                'status',
                'serial',
                'created as created_at'
            )->orderBy('id', 'asc')->get();
        foreach ($pedidos as $key => $pedido) {
            Pedido::create((array) $pedido);
        }
        echo "Pedidos - Sucesso!\n";

        // Migrar Respostas
        $respostas = DB::connection('mysql_old')->table('answers')
            ->select('id', 'description', 'anexo', 'user_id', 'request_id as pedido_id', 'created as created_at')->orderBy('id', 'asc')->get();
        foreach ($respostas as $key => $resposta) {
            Resposta::create((array) $resposta);
        }
        echo "Respostas - Sucesso!\n";
    }
}
