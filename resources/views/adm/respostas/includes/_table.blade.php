<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Nome</th>
                <th>Descrição</th>
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($respostas as $resposta)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $resposta->id }}</span>
                </td>
                <td>
                    <a href='{{ route('respostas.edit', $resposta->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $resposta->name }}</a>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $resposta->description }}</span>
                </td>
                <td class='pr-0 text-right'>
                    <a href='{{ route('respostas.edit', $resposta->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$resposta->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['respostas.destroy', $resposta->id], 'method'=>'POST', 'id' => 'form_remove_'.$resposta->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
            </tr>
            @empty
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name='offset']').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
