<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('title', 'Título') !!}
            {!! Form::text('title', null, [
                'class'=>(!empty($errors->default->first('title'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('title') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('order', 'Ordem') !!}
            {!! Form::text('order', null, [
                'class'=>(!empty($errors->default->first('order'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('order') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('description', 'Descrição') !!}
            {!! Form::textarea('description', null, [
                // 'class'=>(!empty($errors->default->first('description'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid',
                'id' => "kt-ckeditor-1"
            ]) !!}
            <div class='invalid-feedback'>@error('description') {{$message}} @enderror</div>
        </div>
    </div>
</div>

@section('scripts')
<script src="{{asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js')}}"></script>
<script>
    // import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
    // var Alignment = require('@ckeditor/ckeditor5-alignment/src/alignment.js');
    var KTCkeditor = function () {
        
        // Private functions
        var demos = function () {
            // ClassicEditor.builtinPlugins = [Alignment]
            ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-1' ), {
                // alignment: {
                //     options: [
                //         { name: 'left', className: 'my-align-left' },
                //         { name: 'right', className: 'my-align-right' }
                //     ]
                // },
                // plugins: ['Alignment'],
                // toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                // shouldNotGroupWhenFull: true,
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Parágrafo', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Título 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Título 2', class: 'ck-heading_heading2' },
                        { model: 'heading3', view: 'h3', title: 'Título 3', class: 'ck-heading_heading3' }
                    ]
                }
            } )
            .catch( error => {
                console.log( error );
            } );
        //         .create( document.querySelector( '#kt-ckeditor-1' ) )
        //         .then( editor => {
        //             console.log( editor );
        //         } )
        //         .catch( error => {
        //             console.error( error );
        //         } );
        }

        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();

    // Initialization
    jQuery(document).ready(function() {
        KTCkeditor.init();
    });
</script>
@endsection
