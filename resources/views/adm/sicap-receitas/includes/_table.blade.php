<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Data de Arrecadação</th>
                <th>Exercício</th>
                <th>Unidade Orçamentária</th>
                <th>Código do Banco</th>
                <th>Agência</th>
                <th>Nº da Conta</th>
                <th>Recurso Vinculado</th>
                <th>Valor</th>
                @if (Route::currentRouteName() != 'receitas')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($sicapReceitas as $sicapReceita)
            <tr class="">
                <td class='pl-0'>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->id }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->DataArrecadacao }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->Exercicio }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ @$sicapReceita->undOrcamentaria->Nome }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->CodBanco }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->CodAgencia }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $sicapReceita->NumConta }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ @$sicapReceita->recursoVinculado->Nome }}</span>
                </td>
                <td>
                    <span class='{{ $sicapReceita->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>R$ {{ $sicapReceita->Valor }}</span>
                </td>
                @if (Route::currentRouteName() != 'receitas')
                <td class='pr-0 text-right'>
                    @if ($sicapReceita->deleted_at)
                        <a href='{{ route('sicap-receitas.restore', $sicapReceita->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                            {{ Metronic::getSVG('media/svg/icons/Communication/Reply.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                        </a>
                    @else
                        <a href='javascript:;' onclick='deleteItem({{$sicapReceita->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                            {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                        </a>
                        {!! Form::open(['route' => ['sicap-receitas.destroy', $sicapReceita->id], 'method'=>'POST', 'id' => 'form_remove_'.$sicapReceita->id]) !!}
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        {!! Form::close() !!}
                    @endif
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='9' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
