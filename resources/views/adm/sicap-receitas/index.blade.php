@extends('layout.default')

@section('page_toolbar')
<a href='{{ route('sicap-receitas.create') }}' class='btn btn-info font-weight-bolder font-size-sm mr-2'>CADASTRAR</a>
<a href='javascript:;' data-toggle='modal' data-target='#filter-sicap-receitas' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
@endsection

@section('content')
@include('adm.sicap-receitas.includes._filter')

@if (session('message_success'))
    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
@endif
@if (session('message_error'))
    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
@endif
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} de {{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @include('adm.sicap-receitas.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $sicapReceitas->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection
