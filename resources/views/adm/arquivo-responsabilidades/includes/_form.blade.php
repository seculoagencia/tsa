<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('responsabilidade_id', 'Tipo') !!}
            {!! Form::select('responsabilidade_id', $tipos->pluck('name','id'), null, [
                'class'=>(!empty($errors->default->first('responsabilidade_id'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'placeholder' => 'Selecione...'
            ]) !!}
            <div class='invalid-feedback'>@error('responsabilidade_id') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('file', 'Arquivo') !!} <br>
            {!! Form::file('file', []) !!}
            <div class='invalid-feedback'>@error('file') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('description', 'Descrição') !!}
            {!! Form::textarea('description', null, [
                'class'=>(!empty($errors->default->first('description'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('description') {{$message}} @enderror</div>
        </div>
    </div>
</div>
