<div class='row'>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('banco', 'Banco') !!}
            {!! Form::text('banco', null, [
                'class'=>(!empty($errors->default->first('banco'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('banco') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('agencia', 'Agência') !!}
            {!! Form::text('agencia', null, [
                'class'=>(!empty($errors->default->first('agencia'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('agencia') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('conta', 'Conta') !!}
            {!! Form::text('conta', null, [
                'class'=>(!empty($errors->default->first('conta'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('conta') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('documento', 'Documento') !!}
            {!! Form::text('documento', null, [
                'class'=>(!empty($errors->default->first('documento'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('documento') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('valor', 'Valor') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
</div>
