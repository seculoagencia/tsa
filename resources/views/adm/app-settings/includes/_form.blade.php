<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome da Instituição') !!}
            {!! Form::text('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('site_url', 'URL do Site') !!}
            {!! Form::text('site_url', null, [
                'class'=>(!empty($errors->default->first('site_url'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('site_url') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, [
                'class'=>(!empty($errors->default->first('email'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('email') {{$message}} @enderror</div>
        </div>
    </div>
    {{-- <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('recaptcha_site', 'ReCAPTCHA - Chave do Site') !!}
            {!! Form::email('recaptcha_site', null, [
                'class'=>(!empty($errors->default->first('recaptcha_site'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('recaptcha_site') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('recaptcha_secret', 'ReCAPTCHA - Chave Secreta') !!}
            {!! Form::email('recaptcha_secret', null, [
                'class'=>(!empty($errors->default->first('recaptcha_secret'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('recaptcha_secret') {{$message}} @enderror</div>
        </div>
    </div> --}}
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('logo', 'Logo') !!}<br>
            {!! Form::file('logo') !!} <br>
            @if (isset($appSetting) && $appSetting->logo)
            <div class="mt-5">
                <img src="{{ asset($appSetting->logo) }}" alt="Logo" style="width:100%; max-width:200px" class="img-responsive">
            </div>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cover', 'Foto de Topo') !!}<br>
            {!! Form::file('cover') !!} <br>
            @if (isset($appSetting) && $appSetting->cover)
            <div class="mt-5">
                <img src="{{ asset($appSetting->cover) }}" alt="Logo" style="width:100%; max-width:200px" class="img-responsive">
            </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! Form::label('view_last_update', 'Exibir Última Atualização', ['class'=>'font-weight-bold text-uppercase']) !!}
            <span class="switch switch-success switch-outline switch-icon">
                <label>
                    {!! Form::checkbox('view_last_update', null, (isset($appSetting) && $appSetting->view_last_update) ? true : false) !!}
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! Form::label('view_last_update', 'Exibir Menu COVID', ['class'=>'font-weight-bold text-uppercase']) !!}
            <span class="switch switch-success switch-outline switch-icon">
                <label>
                    {!! Form::checkbox('display_covid', null, (isset($appSetting) && $appSetting->display_covid) ? true : false) !!}
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('cor_principal', 'Cor Principal') !!}
            <input type="color" name="cor_principal" id="cor_principal" class="form-control" value="{{ isset($appSetting) ? $appSetting->cor_principal : '' }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('prioridade', 'Fonte de Dados - Despesas') !!}
            {!! Form::select('prioridade', [''=>'', 'manual'=>'Manual', 'sicap'=>'SICAP', 'sagres'=>'SAGRES', 'siap'=>'SIAP'], isset($appSetting) ? $appSetting->prioridade : null, ['class'=>'form-control select2']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('prioridade_receita', 'Fonte de Dados - Receitas') !!}
            {!! Form::select('prioridade_receita', [''=>'', 'manual'=>'Manual', 'sicap'=>'SICAP', 'sagres'=>'SAGRES'], isset($appSetting) ? $appSetting->prioridade_receita : null, ['class'=>'form-control select2']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('prioridade_servidores', 'Fonte de Dados - Servidores') !!}
            {!! Form::select('prioridade_servidores', [''=>'', 'manual'=>'Manual/XML', 'sicap'=>'SICAP', 'sagres'=>'SAGRES'], isset($appSetting) ? $appSetting->prioridade_servidores : null, ['class'=>'form-control select2']) !!}
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(".select2").select2({
            placeholder: 'Selecione...',
            allowClear: true
        });
    </script>
@endsection
