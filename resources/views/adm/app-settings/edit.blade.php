@extends('layout.default')

@section('page_toolbar')
<a href={{ route('app-settings.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@if (session('message_success'))
    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
@endif
@if (session('message_error'))
    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
@endif
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$appSetting->id}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {!! Form::model($appSetting, ['route'=>['app-settings.update', $appSetting->id], 'method'=>'PUT', 'files'=>true]) !!}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        @include('adm.app-settings.includes._form')
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <button type='submit' class='btn btn-success'>Salvar Alterações</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
