<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Número</th>
                <th>Valor</th>
                <th>Data</th>
                @if (Route::currentRouteName() != 'despesas')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($liquidacoes as $liquidacao)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $liquidacao->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $liquidacao->numero }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $liquidacao->valor }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $liquidacao->data_format }}</span>
                </td>
                @if (Route::currentRouteName() != 'despesas')
                <td class='pr-0 text-right'>
                    @can('update', $liquidacao)
                    <a href='{{ route('liquidacoes.edit', $liquidacao->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    @endcan

                    @can('delete', $liquidacao)
                    <a href='javascript:;' onclick='deleteItem({{$liquidacao->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['liquidacoes.destroy', $liquidacao->id], 'method'=>'POST', 'id' => 'form_remove_'.$liquidacao->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                    @endcan
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName(), $empenho->id), ['id'=>'route']) !!}
    <script>
        $('select[name='offset']').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
