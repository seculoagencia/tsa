<div class='row'>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('numero', 'Número') !!}
            {!! Form::text('numero', null, [
                'class'=>(!empty($errors->default->first('numero'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('numero') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('valor', 'Valor') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
</div>
