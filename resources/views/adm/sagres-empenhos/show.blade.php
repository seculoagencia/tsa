@extends('layout.default')

@section('page_toolbar')
<a href={{ route('sagres-empenhos.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@include('adm.sagres-empenhos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$sagresEmpenho->num_empenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Data</label><br>
                    {{ $sagresEmpenho->data_emissao_empenho }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Número</label><br>
                    {{ $sagresEmpenho->num_empenho }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Credor</label><br>
                    {{ $sagresEmpenho->credor ? $sagresEmpenho->credor->nome : $sagresEmpenho->cpf_cnpj_credor }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Unidade Orçamentária</label><br>
                    {{ @$sagresEmpenho->undOrcamentaria->denominacao }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Função</label><br>
                    {{ @$sagresEmpenho->funcao->nome }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Sub-Função</label><br>
                    {{ @$sagresEmpenho->subfuncao->nome }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Programa</label><br>
                    {{ @$sagresEmpenho->getPrograma->nome }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Ação</label><br>
                    {{ @$sagresEmpenho->getAcao->nome }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Elemento de Despesa</label><br>
                    {{ @$sagresEmpenho->rubricaDespesa->descricao }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Número do Processo</label><br>
                    {{ $sagresEmpenho->num_procedimento }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor</label><br>
                    R$ {{ $sagresEmpenho->valor_empenhado }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Liquidado</label><br>
                    R$ {{ $sagresEmpenho->totalLiquidado() }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Pago</label><br>
                    R$ {{ $sagresEmpenho->totalPago() }}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="font-weight-bolder">Histórico</label><br>
                    {{ $sagresEmpenho->historico }}
                </div>
            </div>
        </div>
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Pagamentos do {{ $page_title }}: #{{$sagresEmpenho->num_empenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Código do Banco</th>
                        <th>Agência</th>
                        <th>Número da Conta</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($sagresEmpenho->pagamentos as $pagamento)
                        <tr>
                            <td scope="row">{{ $pagamento->cod_banco }}</td>
                            <td>{{ $pagamento->num_agencia_bancaria }}</td>
                            <td>{{ $pagamento->num_conta_bancaria }}</td>
                            <td>{{ $pagamento->data }}</td>
                            <td>R$ {{ $pagamento->valor }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Liquidações do {{ $page_title }}: #{{$sagresEmpenho->NumEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Número</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($sagresEmpenho->liquidacoes as $liquidacao)
                        <tr>
                            <td scope="row">{{ $liquidacao->num_liquidacao }}</td>
                            <td>{{ $liquidacao->data }}</td>
                            <td>R$ {{ $liquidacao->valor }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>
@endsection
