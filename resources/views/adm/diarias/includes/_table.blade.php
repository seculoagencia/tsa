<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Servidor</th>
                <th>Cargo</th>
                <th>Data</th>
                <th>Destino</th>
                <th>Motivo da Viagem</th>
                <th>Valor</th>
                <th>Anexo</th>
                @if (Route::currentRouteName() != 'diarias')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($diarias as $diaria)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->favorecido }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->cargo ? $diaria->cargo->nome : '---' }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->data_format }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->destino }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $diaria->motivo }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $diaria->valor }}</span>
                </td>
                <td>
                    @if (count($diaria->attachments) > 0)
                    {{-- Button trigger modal --}}
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark" data-toggle="modal" data-target="#add-anexo-{{ $diaria->id }}">
                        VISUALIZAR
                    </button>

                    {{-- Modal --}}
                    <div class="modal fade" id="add-anexo-{{ $diaria->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $diaria->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="diaria">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($diaria->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </td>
                @if (Route::currentRouteName() != 'diarias')
                <td class='pr-0 text-right'>
                    <a href='{{ route('diarias.edit', $diaria->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$diaria->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['diarias.destroy', $diaria->id], 'method'=>'POST', 'id' => 'form_remove_'.$diaria->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='9' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
