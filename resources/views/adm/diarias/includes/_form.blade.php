<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('favorecido', 'Favorecido') !!}
            {!! Form::text('favorecido', null, [
                'class'=>(!empty($errors->default->first('favorecido'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('favorecido') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('cargo_id', 'Cargo') !!}
            {!! Form::select('cargo_id', $cargos->pluck('nome','id'), null, [
                'class'=>(!empty($errors->default->first('cargo_id'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'placeholder'=>'Selecione...'
            ]) !!}
            <div class='invalid-feedback'>@error('cargo_id') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('destino', 'Destino') !!}
            {!! Form::text('destino', null, [
                'class'=>(!empty($errors->default->first('destino'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('destino') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('valor', 'Valor') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('motivo', 'Motivo') !!}
            {!! Form::textarea('motivo', null, [
                'class'=>(!empty($errors->default->first('motivo'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('motivo') {{$message}} @enderror</div>
        </div>
    </div>
</div>
