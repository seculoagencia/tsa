<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('nome', 'Nome') !!}
            {!! Form::text('nome', null, [
                'class'=>(!empty($errors->default->first('nome'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('nome') {{$message}} @enderror</div>
        </div>
    </div>
</div>
