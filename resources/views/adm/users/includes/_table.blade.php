<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Perfil</th>
                <th class="text-center">Status</th>
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $user)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $user->id }}</span>
                </td>
                <td>
                    <a href='{{ route('users.edit', $user->name) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $user->name }}</a>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $user->email }}</span>
                    <span class="text-muted font-weight-bold">{{$user->username}}</span>
                </td>
                <td>
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $role)
                        <span class='label label-light-primary label-inline font-weight-bold mr-2'>{{ $role }}</span>
                        @endforeach
                    @endif
                </td>
                <td class="text-center">
                    <span class='label label-light-{{ $user->status ? 'success':'danger' }} label-inline font-weight-bold mr-2'>{{ $user->status ? 'Ativo':'Bloqueado' }}</span>
                </td>
                <td class='pr-0 text-right'>
                    <a href='{{ route('users.edit', $user->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$user->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method'=>'POST', 'id' => 'form_remove_'.$user->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
            </tr>
            @empty
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route('users.index'), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
