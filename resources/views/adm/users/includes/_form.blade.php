<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome', ['class'=>'font-weight-bold text-uppercase']) !!}
            {!! Form::text('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('username', 'Usuário', ['class'=>'font-weight-bold text-uppercase']) !!}
            {!! Form::text('username', null, [
                'class'=>(!empty($errors->default->first('username'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('username') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('email', 'Email', ['class'=>'font-weight-bold text-uppercase']) !!}
            {!! Form::email('email', null, [
                'class'=>(!empty($errors->default->first('email'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('email') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('superuser', 'Super Usuário', ['class'=>'font-weight-bold text-uppercase']) !!}
            <span class="switch switch-success switch-outline switch-icon">
                <label>
                    {!! Form::checkbox('superuser', null, false) !!}
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('password', 'Senha', ['class'=>'font-weight-bold text-uppercase']) !!}
            <input type="password" name="password" class="{{ (!empty($errors->default->first('password'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'}}">
            <div class='invalid-feedback'>@error('password') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('confirm-password', 'Confirmar Senha', ['class'=>'font-weight-bold text-uppercase']) !!}
            <input type="password" name="confirm-password" class="{{ (!empty($errors->default->first('confirm-password'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'}}">
            <div class='invalid-feedback'>@error('confirm-password') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('status', 'Status', ['class'=>'font-weight-bold text-uppercase']) !!}
            <span class="switch switch-success switch-outline switch-icon">
                <label>
                    {!! Form::checkbox('status', null, (isset($user) && $user->status) ? true : false) !!}
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('roles', 'Perfis', ['class'=>'font-weight-bold text-uppercase']) !!}
            {!! Form::select('roles[]', $roles, $userRole, [
                'class'=>(!empty($errors->default->first('roles'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'multiple'
            ]) !!}
        </div>
    </div>
</div>


