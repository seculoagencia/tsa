<div class="modal fade" id="modelId{{$empenho->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId{{$empenho->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <h5 class="modal-title">Detalhamento de Empenho</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Empenho #{{$empenho->numero}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            {{--begin::Form--}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Data</label><br>
                                        {{ $empenho->data }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Número</label><br>
                                        {{ $empenho->numero }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Credor</label><br>
                                        {{ $empenho->credor }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Unidade Orçamentária</label><br>
                                        {{ $empenho->unidade_orcamentaria }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Função</label><br>
                                        {{ @$empenho->funcao }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Sub-Função</label><br>
                                        {{ $empenho->subfuncao }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Programa</label><br>
                                        {{ $empenho->programa }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Fonte de Recurso</label><br>
                                        {{ $empenho->fonte_de_recurso }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Elemento de Despesa</label><br>
                                        {{ $empenho->elemento_de_despesa }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Número do Processo</label><br>
                                        {{ $empenho->numero_do_processo }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Valor Liquidado</label><br>
                                        R$ {{ $empenho->totalLiquidacoes() }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Valor Pago</label><br>
                                        R$ {{ $empenho->totalPagamentos() }}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Histórico</label><br>
                                        {{ $empenho->historico }}
                                    </div>
                                </div>
                            </div>
                            {{--end::Form--}}
                        </div>
                        {{--end::Body--}}
                    </div>
                    
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Pagamentos do {{ $page_title }}: #{{$empenho->NumEmpenho}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            @php($pagamentos = $empenho->pagamentos)
                            @include('adm.pagamentos.includes._table')
                        </div>
                        {{--end::Body--}}
                    </div>
                    
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Liquidações do {{ $page_title }}: #{{$empenho->NumEmpenho}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            @php($liquidacoes = $empenho->liquidacoes)
                            @include('adm.liquidacoes.includes._table')
                        </div>
                        {{--end::Body--}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>