<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>Listagem de Pagamentos</span>
        </h3>
        <div class='card-toolbar'>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add-pagamento">Cadastrar</button>
            {!! Form::open(['route'=>'pagamentos.store', 'files'=>true]) !!}
            
            <!-- Modal -->
            <div class="modal fade" id="add-pagamento" tabindex="-1" role="dialog" aria-labelledby="add-pagamento-title" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cadastrar Pagamentos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @include('adm.pagamentos.includes._form')
                            {!! Form::hidden('empenho_id', $empenho->id) !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        @if (session('message_pag_success'))
            <div class='alert alert-success font-bold'>{{ session('message_pag_success') }}</div>
        @endif
        @if (session('message_pag_error'))
            <div class='alert alert-danger font-bold'>{{ session('message_pag_error') }}</div>
        @endif
        {{--begin::Table--}}
        @include('adm.pagamentos.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
</div>