<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Número</th>
                <th>Credor</th>
                <th>Data</th>
                <th>Valor</th>
                @if (Route::currentRouteName() != 'despesas')
                <th>COVID-19</th>
                @endif
                <th>Anexos</th>
                @if (Route::currentRouteName() != 'despesas')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($empenhos as $empenho)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $empenho->id }}</span>
                </td>
                <td>
                    @can('update', $empenho)
                    <a href='{{ route('empenhos.edit', $empenho->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $empenho->numero }}</a>
                    @else
                    {{-- Button trigger modal --}}
                    <button type="button" class="btn btn-text-success font-weight-bold text-dark-75 d-block font-size-lg {{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }}" data-toggle="modal" data-target="#modelId{{$empenho->id}}">
                        {{ $empenho->numero }}
                    </button>

                    {{-- Modal --}}
                    @include('adm.empenhos.includes._modal')

                    {{-- <span class='text-dark-75 d-block font-size-lg'>{{ $empenho->numero }}</span> --}}
                    @endcan
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $empenho->credor }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $empenho->data_format }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ number_format($empenho->totalLiquidacoes(),2,',','.') }}</span>
                </td>
                @if (Route::currentRouteName() != 'despesas')
                <td>
                    <span class="font-weight-bolder text-{{$empenho->covid19 ? 'success':'danger'}}">{{$empenho->covid19 ? 'SIM':'NÃO'}}</span>
                </td>
                @endif
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark btn-lg" data-toggle="modal" data-target="#add-anexo-{{ $empenho->id }}">
                        VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $empenho->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $empenho->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($empenho->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                @if (Route::currentRouteName() != 'despesas')
                <td class='pr-0 text-right'>
                    @can('update', $empenho)
                    <a href='{{ route('empenhos.edit', $empenho->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    @endcan
                    @can('delete', $empenho)
                    <a href='javascript:;' onclick='deleteItem({{$empenho->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['empenhos.destroy', $empenho->id], 'method'=>'POST', 'id' => 'form_remove_'.$empenho->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                    @endcan
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
