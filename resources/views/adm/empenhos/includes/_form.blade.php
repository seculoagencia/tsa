<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('numero', 'Número') !!}
            {!! Form::text('numero', null, [
                'class'=>(!empty($errors->default->first('numero'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('numero') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('credor', 'Credor') !!}
            {!! Form::text('credor', null, [
                'class'=>(!empty($errors->default->first('credor'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('credor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('unidade_orcamentaria', 'Unidade Orçamentária') !!}
            {!! Form::text('unidade_orcamentaria', null, [
                'class'=>(!empty($errors->default->first('unidade_orcamentaria'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('unidade_orcamentaria') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('funcao', 'Função') !!}
            {!! Form::text('funcao', null, [
                'class'=>(!empty($errors->default->first('funcao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('funcao') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('subfuncao', 'Sub-Função') !!}
            {!! Form::text('subfuncao', null, [
                'class'=>(!empty($errors->default->first('subfuncao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('subfuncao') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('programa', 'Programa') !!}
            {!! Form::text('programa', null, [
                'class'=>(!empty($errors->default->first('programa'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('programa') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('acao', 'Ação') !!}
            {!! Form::text('acao', null, [
                'class'=>(!empty($errors->default->first('acao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('acao') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('fonte_de_recurso', 'Fonte de Recurso') !!}
            {!! Form::text('fonte_de_recurso', null, [
                'class'=>(!empty($errors->default->first('fonte_de_recurso'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('fonte_de_recurso') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('elemento_de_despesa', 'Elemento de Despesa') !!}
            {!! Form::text('elemento_de_despesa', null, [
                'class'=>(!empty($errors->default->first('elemento_de_despesa'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('elemento_de_despesa') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('cpf_cnpj', 'CPF/CNPJ') !!}
            {!! Form::text('cpf_cnpj', null, [
                'class'=>(!empty($errors->default->first('cpf_cnpj'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('cpf_cnpj') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('numero_do_processo', 'Número do Processo') !!}
            {!! Form::text('numero_do_processo', null, [
                'class'=>(!empty($errors->default->first('numero_do_processo'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('numero_do_processo') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            <label class="checkbox checkbox-success">
                {!! Form::checkbox('covid19', null) !!}
                <span class="mr-5">&nbsp;</span>
                COVID-19
            </label>
            <div class='invalid-feedback'>@error('covid19') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('historico', 'Histórico') !!}
            {!! Form::textarea('historico', null, [
                'class'=>(!empty($errors->default->first('historico'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('historico') {{$message}} @enderror</div>
        </div>
    </div>
</div>
