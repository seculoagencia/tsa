<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Nome</th>
                <th>Exercício</th>
                <th>Data de Admissão</th>
                {{-- <th>Valor Bruto</th>
                <th>Valor Líquido</th> --}}
                <th>Cargo</th>
                @if (Route::currentRouteName() != 'servidores')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($sicapServidores as $sicapServidor)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $sicapServidor->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $sicapServidor->Nome }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $sicapServidor->Exercicio }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $sicapServidor->DataAdmissao }}</span>
                </td>
                {{-- <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $sicapServidor->SalarioBruto }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $sicapServidor->Salarioliquido }}</span>
                </td> --}}
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $sicapServidor->cargo->Descricao }}</span>
                </td>
                @if (Route::currentRouteName() != 'servidores')
                <td class='pr-0 text-right'>
                    {{-- <a href='{{ route('sicap-servidores.edit', $sicapServidor->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a> --}}
                    <a href='javascript:;' onclick='deleteItem({{$sicapServidor->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['sicap-servidores.destroy', $sicapServidor->id], 'method'=>'POST', 'id' => 'form_remove_'.$sicapServidor->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='8' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
