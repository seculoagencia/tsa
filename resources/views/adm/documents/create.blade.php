@extends('layout.default')

@section('page_toolbar')
<a href={{ route('documents.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} {{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {!! Form::open(['route'=>'documents.store']) !!}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        @include('adm.documents.includes._form')
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <button type='submit' class='btn btn-primary'>Cadastrar</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
    <script>
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
