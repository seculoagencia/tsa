<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('month_year', 'Mês/Ano') !!}
            {!! Form::month('month_year', isset($document) ? $document->month_year : null, [
                'class'=>(!empty($errors->default->first('month_year'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('month_year') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('type_document_id', 'Tipo de Documento') !!}
            {!! Form::select('type_document_id',  $typeDocuments->pluck('name','id'), null, [
                'class'=>(!empty($errors->default->first('type_document_id'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'placeholder'=>'Selecione...'
            ]) !!}
            <div class='invalid-feedback'>@error('type_document_id') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('name', 'Descrição') !!}
            {!! Form::textarea('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
</div>
