{{-- Modal --}}
<div class='modal fade' id='filter-documents' tabindex='-1' role='dialog' aria-labelledby='filter-documents-title' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Filtrar documents</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['method'=>'GET']) !!}
            <div class='modal-body'>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('year', 'Ano') !!}
                                {!! Form::number('year', request('year'), ['class'=>'form-control form-control-solid form-solid']) !!}
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('month', 'Mês') !!}
                                {!! Form::select('month', Config::get('constants.months'), request('month'), [
                                    'class'=>'form-control select2 form-control-solid form-solid',
                                    'placeholder' => 'Selecione...'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class='form-group'>
                                {!! Form::label('type_document_id', 'Tipo de Documento') !!}
                                {!! Form::select('type_document_id', $typeDocuments->pluck('name','id'), request('type_document_id'), ['class'=>'form-control select2 form-control-solid form-solid','placeholder'=>'Selecione...']) !!}
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class='form-group'>
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', request('name'), ['class'=>'form-control form-control-solid form-solid']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <a href='{{ route('documents.index') }}' class='btn btn-info'>Limpar</a>
                <button type='submit' class='btn btn-primary'>Filtrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
