<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>Listagem de Anexos</span>
        </h3>
        <div class='card-toolbar'>
            {!! Form::open(['route'=>'attachment-documents.store', 'files'=>true]) !!}
            <div class="form-inline">
                <div class="form-group mr-3">
                    {!! Form::file('file', []) !!}
                    {!! Form::hidden('document_id', $document->id) !!}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        @if (session('message_success'))
            <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
        @endif
        @if (session('message_error'))
            <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
        @endif
        @if($errors->has('file'))
            @foreach ($errors->all() as $error)
            <div class='alert alert-danger font-bold'>{{ $error }}</div>
            @endforeach
        @endif
        {{--begin::Table--}}
        @include('adm.attachments.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $attachments->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>