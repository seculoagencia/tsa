<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center mb-0' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Mês/Ano</th>
                <th>Título</th>
                <th>Tipo de Documento</th>
                <th>Anexos</th>
                @if (Route::currentRouteName() != 'publicacoes-oficiais' && Route::currentRouteName() != 'legislacoes')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($documents as $document)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $document->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ str_pad($document->month, 2, "0", STR_PAD_LEFT) }}/{{ $document->year }}</span>
                </td>
                <td>
                    @if (Route::currentRouteName() != 'publicacoes-oficiais' && Route::currentRouteName() != 'legislacoes')
                    <a href='{{ route('documents.edit', $document->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $document->name }}</a>
                    @else
                    <span class='text-dark-75 d-block font-size-lg'>{{ $document->name }}</span>
                    @endif
                </td>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $document->type_document->name }}</span>
                </td>
                <td class='pl-0'>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark btn-lg" data-toggle="modal" data-target="#add-anexo-{{ $document->id }}">
                      VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $document->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $document->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($document->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                @if (Route::currentRouteName() != 'publicacoes-oficiais' && Route::currentRouteName() != 'legislacoes')
                <td class='pr-0 text-right'>
                    <a href='{{ route('documents.edit', $document->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$document->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['documents.destroy', $document->id], 'method'=>'POST', 'id' => 'form_remove_'.$document->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='5' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('#filter-documents').on('shown.bs.modal', function () {
            $('.select2').select2({
                placeholder: "Selecione...",
                allowClear: true
            });
        })
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
