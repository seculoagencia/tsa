@extends('layout.default')

@section('page_toolbar')
<a href={{ route('sicap-empenho.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
{{--
@include('adm.sicap-empenho.includes._filter') --}}
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$sicapEmpenho->id}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {!! Form::model($sicapEmpenho, ['route'=>['sicap-empenho.update', $sicapEmpenho->id], 'method'=>'PUT']) !!}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        @include('adm.sicap-empenho.includes._form')
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <button type='submit' class='btn btn-success'>Salvar Alterações</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
