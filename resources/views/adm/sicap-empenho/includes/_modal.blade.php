<div class="modal fade" id="modelId{{$empenho->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId{{$empenho->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <h5 class="modal-title">Detalhamento de Empenho</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Empenho #{{$empenho->NumEmpenho}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            {{--begin::Form--}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Data</label><br>
                                        {{ $empenho->DataEmpenho }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Número</label><br>
                                        {{ $empenho->NumEmpenho }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Credor</label><br>
                                        {{ @$empenho->credor->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Unidade Orçamentária</label><br>
                                        {{ @$empenho->undOrcamentaria->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Função</label><br>
                                        {{ @$empenho->funcao->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Sub-Função</label><br>
                                        {{ @$empenho->subfuncao->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Programa</label><br>
                                        {{ @$empenho->programa->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Projeto / Atividade</label><br>
                                        {{ @$empenho->projetoAtividade->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Fonte de Recurso</label><br>
                                        {{ @$empenho->recursoVinculado->Nome }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Elemento de Despesa</label><br>
                                        {{ @$empenho->rubricaDespesa->Especificacao }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Número do Processo</label><br>
                                        {{ $empenho->NumProcesso }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Valor</label><br>
                                        R$ {{ $empenho->Valor }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Valor Liquidado</label><br>
                                        R$ {{ $empenho->totalLiquidado() }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Valor Pago</label><br>
                                        R$ {{ $empenho->totalPago() }}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bolder">Histórico</label><br>
                                        {{ $empenho->Historico }}
                                    </div>
                                </div>
                            </div>
                            {{--end::Form--}}
                        </div>
                        {{--end::Body--}}
                    </div>
                    
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Pagamentos do {{ $page_title }}: #{{$empenho->NumEmpenho}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>Código do Banco</th>
                                            <th>Agência</th>
                                            <th>Número da Conta</th>
                                            <th>Número do Processo</th>
                                            <th>Data</th>
                                            <th>Valor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($empenho->pagamentos as $pagamento) 
                                            <tr>
                                                <td scope="row">{{ @$pagamento->empenho->pagamentoFinanceiro->CodBanco }}</td>
                                                <td>{{ @$pagamento->empenho->pagamentoFinanceiro->CodAgenciaBanco }}</td>
                                                <td>{{ @$pagamento->empenho->pagamentoFinanceiro->NumContaBancaria }}</td>
                                                <td>{{ @$pagamento->empenho->pagamentoFinanceiro->NumProcesso }}</td>
                                                <td>{{ $pagamento->DataPagamento }}</td>
                                                <td>R$ {{ number_format($pagamento->Valor,2,',','.') }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                </table>
                            </div>
                        </div>
                        {{--end::Body--}}
                    </div>
                    
                    <div class='card card-custom gutter-b'>
                        {{--begin::Header--}}
                        <div class='card-header border-0 py-5'>
                            <h3 class='card-title align-items-start flex-column'>
                                <span class='card-label font-weight-bolder text-dark'>
                                    Liquidações do {{ $page_title }}: #{{$empenho->NumEmpenho}}
                                </span>
                            </h3>
                            <div class='card-toolbar'>
                                
                            </div>
                        </div>
                        {{--end::Header--}}
                        {{--begin::Body--}}
                        <div class='card-body py-0'>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>Número</th>
                                            <th>Data</th>
                                            <th>Valor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($empenho->liquidacoes as $liquidacao) 
                                            <tr>
                                                <td scope="row">{{ $liquidacao->NumLiquidacao }}</td>
                                                <td>{{ $liquidacao->DataLiquidacao }}</td>
                                                <td>R$ {{ number_format($liquidacao->Valor,2,',','.') }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                </table>
                            </div>
                        </div>
                        {{--end::Body--}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>