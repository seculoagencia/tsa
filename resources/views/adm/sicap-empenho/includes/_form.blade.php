<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            <input type="file" name="sicap[]" id="sicap" multiple>
            <div class='invalid-feedback'>@error('sicap') {{$message}} @enderror</div>
        </div>
    </div>
</div>
