@extends('layout.default')

@section('page_toolbar')
<a href={{ route('sicap-empenho.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@include('adm.sicap-empenho.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$sicapEmpenho->NumEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Data</label><br>
                    {{ $sicapEmpenho->DataEmpenho }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Número</label><br>
                    {{ $sicapEmpenho->NumEmpenho }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Credor</label><br>
                    {{ $sicapEmpenho->credor ? $sicapEmpenho->credor->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Unidade Orçamentária</label><br>
                    {{ $sicapEmpenho->undOrcamentaria ? $sicapEmpenho->undOrcamentaria->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Função</label><br>
                    {{ $sicapEmpenho->funcao ? $sicapEmpenho->funcao->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Sub-Função</label><br>
                    {{  $sicapEmpenho->subfuncao ? $sicapEmpenho->subfuncao->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Programa</label><br>
                    {{ $sicapEmpenho->programa ? $sicapEmpenho->programa->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Projeto / Atividade</label><br>
                    {{ $sicapEmpenho->projetoAtividade ? $sicapEmpenho->projetoAtividade->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Fonte de Recurso</label><br>
                    {{ $sicapEmpenho->recursoVinculado ? $sicapEmpenho->recursoVinculado->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Elemento de Despesa</label><br>
                    {{ $sicapEmpenho->rubricaDespesa ? $sicapEmpenho->rubricaDespesa->Especificacao : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Número do Processo</label><br>
                    {{ $sicapEmpenho->NumProcesso }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor</label><br>
                    R$ {{ $sicapEmpenho->Valor }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Liquidado</label><br>
                    R$ {{ $sicapEmpenho->totalLiquidado() }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Pago</label><br>
                    R$ {{ $sicapEmpenho->totalPago() }}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="font-weight-bolder">Histórico</label><br>
                    {{ $sicapEmpenho->Historico }}
                </div>
            </div>
        </div>
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Pagamentos do {{ $page_title }}: #{{$sicapEmpenho->NumEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Código do Banco</th>
                        <th>Agência</th>
                        <th>Número da Conta</th>
                        <th>Número do Processo</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($sicapEmpenho->pagamentos as $pagamento) 
                        <tr>
                            <td scope="row">{{ $pagamento->empenho->pagamentoFinanceiro->CodBanco }}</td>
                            <td>{{ $pagamento->empenho->pagamentoFinanceiro->CodAgenciaBanco }}</td>
                            <td>{{ $pagamento->empenho->pagamentoFinanceiro->NumContaBancaria }}</td>
                            <td>{{ $pagamento->empenho->pagamentoFinanceiro->NumProcesso }}</td>
                            <td>{{ $pagamento->DataPagamento }}</td>
                            <td>R$ {{ number_format($pagamento->Valor,2,',','.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Liquidações do {{ $page_title }}: #{{$sicapEmpenho->NumEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Número</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($sicapEmpenho->liquidacoes as $liquidacao) 
                        <tr>
                            <td scope="row">{{ $liquidacao->NumLiquidacao }}</td>
                            <td>{{ $liquidacao->DataLiquidacao }}</td>
                            <td>R$ {{ number_format($liquidacao->Valor,2,',','.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>
@endsection
