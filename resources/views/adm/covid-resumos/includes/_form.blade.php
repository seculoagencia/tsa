<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('casos_confirmados', 'Casos Confirmados') !!}
            {!! Form::number('casos_confirmados', null, [
                'class'=>(!empty($errors->default->first('casos_confirmados'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('casos_confirmados') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('casos_suspeitos', 'Casos Suspeitos') !!}
            {!! Form::number('casos_suspeitos', null, [
                'class'=>(!empty($errors->default->first('casos_suspeitos'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('casos_suspeitos') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('pessoas_recuperadas', 'Pessoas Recuperadas') !!}
            {!! Form::number('pessoas_recuperadas', null, [
                'class'=>(!empty($errors->default->first('pessoas_recuperadas'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('pessoas_recuperadas') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('obitos', 'Óbitos') !!}
            {!! Form::number('obitos', null, [
                'class'=>(!empty($errors->default->first('obitos'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('obitos') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('nome_arquivo', 'Nome do Arquivo') !!}
            {!! Form::text('nome_arquivo', null, [
                'class'=>(!empty($errors->default->first('nome_arquivo'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('nome_arquivo') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('arquivo', 'Arquivo') !!} <br>
            {!! Form::file('arquivo', null) !!}
            <div class='invalid-feedback'>@error('arquivo') {{$message}} @enderror</div>
        </div>
    </div>
    @if (isset($covidResumo->arquivo))
    <div class='col-md-3'>
        <div class='form-group'>
            <label>&nbsp;</label><br>
            <a href="{{ asset($covidResumo->arquivo) }}" class="btn btn-dark" target="_blank">
                <i class="fas fa-download"></i>
                {{ $covidResumo->nome_arquivo }}
            </a>
        </div>
    </div>
    @endif
</div>
