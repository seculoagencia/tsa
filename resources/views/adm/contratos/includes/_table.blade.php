<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Tipo</th>
                <th>Órgão</th>
                <th>Modalidade</th>
                <th>Data/Hora</th>
                <th>Objeto</th>
                <th>Preço</th>
                <th>Status</th>
                <th>Vencedor</th>
                @if (Route::currentRouteName() != 'licitacoes'
                    && Route::currentRouteName() != 'contratos'
                    && Route::currentRouteName() != 'dispensa'
                    && Route::currentRouteName() != 'inexigibilidade'
                    && Route::currentRouteName() != 'atas'
                    && Route::currentRouteName() != 'carta'
                    && Route::currentRouteName() != 'concorrencia'
                    && Route::currentRouteName() != 'pregao.eletronico'
                    && Route::currentRouteName() != 'pregao.presencial'
                    && Route::currentRouteName() != 'tomada.precos'
                    && Route::currentRouteName() != 'contrato.fisica'
                    && Route::currentRouteName() != 'contrato.juridica'
                         )
                <th>COVID19</th>
                @endif
                <th>Anexo</th>
                @if (Route::currentRouteName() != 'licitacoes'
                    && Route::currentRouteName() != 'contratos'
                    && Route::currentRouteName() != 'dispensa'
                    && Route::currentRouteName() != 'inexigibilidade'
                    && Route::currentRouteName() != 'atas'
                    && Route::currentRouteName() != 'carta'
                    && Route::currentRouteName() != 'concorrencia'
                    && Route::currentRouteName() != 'pregao.eletronico'
                    && Route::currentRouteName() != 'pregao.presencial'
                    && Route::currentRouteName() != 'tomada.precos'
                    && Route::currentRouteName() != 'contrato.fisica'
                    && Route::currentRouteName() != 'contrato.juridica'
                         )
                    <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($contratos as $contrato)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->tipo ? Config::get('constants.licitacoes.tipo')[$contrato->tipo] : '' }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->orgao }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->modalidade }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->data_hora_format }}</span>
                </td>
                <td>
                    <button type="button" class="btn btn-sm font-weight-bolder btn-primary text-uppercase" data-toggle="popover" data-trigger="click" data-placement="right" title="Objeto" data-content="{{ $contrato->objeto }}">
                        Visualizar/Ocultar
                    </button>
                    {{-- <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->objeto }}</span> --}}
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->valor }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{$contrato->status ? Config::get('constants.licitacoes.status')[$contrato->status] : ''}}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $contrato->vencedor }}</span>
                </td>
                @if (Route::currentRouteName() != 'licitacoes'
                    && Route::currentRouteName() != 'contratos'
                    && Route::currentRouteName() != 'dispensa'
                    && Route::currentRouteName() != 'inexigibilidade'
                    && Route::currentRouteName() != 'atas'
                    && Route::currentRouteName() != 'carta'
                    && Route::currentRouteName() != 'concorrencia'
                    && Route::currentRouteName() != 'pregao.eletronico'
                    && Route::currentRouteName() != 'pregao.presencial'
                    && Route::currentRouteName() != 'tomada.precos'
                    && Route::currentRouteName() != 'contrato.fisica'
                    && Route::currentRouteName() != 'contrato.juridica'
                         )
                <td>
                    <span class="font-weight-bolder text-{{$contrato->covid19 ? 'success':'danger'}}">{{$contrato->covid19 ? 'SIM':'NÃO'}}</span>
                </td>
                @endif
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark" data-toggle="modal" data-target="#add-anexo-{{ $contrato->id }}">
                        VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $contrato->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $contrato->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="contrato">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($contrato->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                @if (Route::currentRouteName() != 'licitacoes'
                    && Route::currentRouteName() != 'contratos'
                    && Route::currentRouteName() != 'dispensa'
                    && Route::currentRouteName() != 'inexigibilidade'
                    && Route::currentRouteName() != 'atas'
                    && Route::currentRouteName() != 'carta'
                    && Route::currentRouteName() != 'concorrencia'
                    && Route::currentRouteName() != 'pregao.eletronico'
                    && Route::currentRouteName() != 'pregao.presencial'
                    && Route::currentRouteName() != 'tomada.precos'
                    && Route::currentRouteName() != 'contrato.fisica'
                    && Route::currentRouteName() != 'contrato.juridica'
                         )
                <td class='pr-0 text-right'>
                    <a href='{{ route('contratos.edit', $contrato->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$contrato->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['contratos.destroy', $contrato->id], 'method'=>'POST', 'id' => 'form_remove_'.$contrato->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='11' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
