{{-- Modal --}}
<div class='modal fade' id='filter-contratos' tabindex='-1' role='dialog' aria-labelledby='filter-contratos-title' aria-hidden='true'>
    <div class='modal-dialog modal-lg' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Filtrar contratos</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['method'=>'GET']) !!}
            <div class='modal-body'>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('tipo', 'Tipo') !!}
                                {!! Form::select('tipo', Config::get('constants.licitacoes.tipo'), null, [
                                    'class'=>(!empty($errors->default->first('tipo'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                                    'placeholder' => 'Selecione...',
                                    'style' => 'width:100%'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('orgao', 'Órgão') !!}
                                {!! Form::text('orgao', null, [
                                    'class'=>(!empty($errors->default->first('orgao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('modalidade', 'Modalidade') !!}
                                {!! Form::text('modalidade', null, [
                                    'class'=>(!empty($errors->default->first('modalidade'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('data_hora', 'Data/Hora') !!}
                                <input type="datetime-local" name="data_hora" value="{{ isset($contrato) ? $contrato->data_hora : null }}" class="{{ (!empty($errors->default->first('data_hora'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid' }}">
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('valor', 'Preço') !!}
                                {!! Form::text('valor', null, [
                                    'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('status', 'Status') !!}
                                {!! Form::select('status', Config::get('constants.licitacoes.status'), null, [
                                    'class'=>(!empty($errors->default->first('status'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                                    'placeholder' => 'Selecione...',
                                    'style' => 'width:100%'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('vencedor', 'Vencedor') !!}
                                {!! Form::text('vencedor', null, [
                                    'class'=>(!empty($errors->default->first('vencedor'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        {{-- <div class='col-md-4'>
                            <div class='form-group'>
                                <label>&nbsp;</label><br>
                                <label class="checkbox checkbox-success">
                                    {!! Form::checkbox('covid19', null) !!}
                                    <span class="mr-5">&nbsp;</span>
                                    COVID-19
                                </label>
                            </div>
                        </div> --}}
                        <div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('objeto', 'Objeto') !!}
                                {!! Form::text('objeto', null, [
                                    'class'=>(!empty($errors->default->first('objeto'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <a href='{{ route('contratos.index') }}' class='btn btn-info'>Limpar</a>
                <button type='submit' class='btn btn-primary'>Filtrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
