<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('tipo', 'Tipo') !!}
            {!! Form::select('tipo', Config::get('constants.licitacoes.tipo'), null, [
                'class'=>(!empty($errors->default->first('tipo'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'placeholder' => 'Selecione...'
            ]) !!}
            <div class='invalid-feedback'>@error('tipo') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('orgao', 'Órgão') !!}
            {!! Form::text('orgao', null, [
                'class'=>(!empty($errors->default->first('orgao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('orgao') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('modalidade', 'Modalidade') !!}
            {!! Form::text('modalidade', null, [
                'class'=>(!empty($errors->default->first('modalidade'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('modalidade') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('data_hora', 'Data/Hora') !!}
            <input type="datetime-local" name="data_hora" value="{{ isset($contrato) ? $contrato->data_hora : null }}" class="{{ (!empty($errors->default->first('data_hora'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid' }}">
            <div class='invalid-feedback'>@error('data_hora') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('valor', 'Preço') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('status', 'Status') !!}
            {!! Form::select('status', Config::get('constants.licitacoes.status'), null, [
                'class'=>(!empty($errors->default->first('status'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                'placeholder' => 'Selecione...'
            ]) !!}
            <div class='invalid-feedback'>@error('status') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-6'>
        <div class='form-group'>
            {!! Form::label('vencedor', 'Vencedor') !!}
            {!! Form::text('vencedor', null, [
                'class'=>(!empty($errors->default->first('vencedor'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('vencedor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            <label class="checkbox checkbox-success">
                {!! Form::checkbox('covid19', null) !!}
                <span class="mr-5">&nbsp;</span>
                COVID-19
            </label>
            <div class='invalid-feedback'>@error('covid19') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('objeto', 'Objeto') !!}
            {!! Form::textarea('objeto', null, [
                'class'=>(!empty($errors->default->first('objeto'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('objeto') {{$message}} @enderror</div>
        </div>
    </div>
</div>
