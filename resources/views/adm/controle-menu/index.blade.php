@extends('layout.default')

@section('page_toolbar')
<a href='javascript:;' data-toggle='modal' data-target='#filter-pedidos' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
@endsection

@section('content')
@include('adm.pedidos.includes._filter')

@if (session('message_success'))
    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
@endif
@if (session('message_error'))
    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
@endif
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} de {{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @include('adm.controle-menu.includes._table')
        {{--end::Table--}}
    </div>


    {{--end::Body--}}
    {{-- <div class='card-footer text-right'>
        {{ $pedidos->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div> --}}
</div>
@if(isset($subOptions))

    <div class='card card-custom gutter-b'>
        {{--begin::Header--}}
        <div class='card-header border-0 py-5'>
            <h3 class='card-title align-items-start flex-column'>
                <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} de SubMenu </span>
            </h3>
            <div class='card-toolbar'>

            </div>
        </div>
        {{--end::Header--}}
        {{--begin::Body--}}
        <div class='card-body py-0'>
            {{--begin::Table--}}
            @include('adm.controle-menu.includes._form_sub_menu',['subMenuOptions' => $subOptions,'options' =>$options ])
            {{--end::Table--}}
        </div>


        {{--end::Body--}}
        {{-- <div class='card-footer text-right'>
            {{ $pedidos->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
        </div> --}}
    </div>
@endif
@endsection
