@extends('layout.default')

@section('page_toolbar')
<a href={{ route('pedidos.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@foreach ($options as $option)
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}

    @if (isset($error))
    <div class="col-md-12 bg-danger py-5 text-center text-white" id="error" style="font-weight: bold">{{$error}}</div>
    <script>
        setTimeout(() => {
            let component = document.querySelector('#error')
            component.style = 'display : none'
        },2000)
    </script>
    @endif

    @if (isset($success))
    <div class="col-md-12 text-white py-5 text-center" id="success" style="font-weight: bold; background-color:#1e1e2d">{{$success}}</div>
    <script>
        setTimeout(() => {
            let component = document.querySelector('#success')
            component.style = 'display : none'
        },2000)
    </script>

    @endif
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>

            {{ $page_description }} de {{ $page_title }} : #{{$option->id}}


            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>

    {{--end::Header--}}
    {!! Form::model($options, ['route'=>['submenu.update', $option->id], 'method'=>'PUT']) !!}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        @include('adm.controle-menu.includes._form')
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <button type='submit' class='btn btn-success'>Salvar Alterações</button>
    </div>
    {!! Form::close() !!}
</div>
@endforeach
@endsection
