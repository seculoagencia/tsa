<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
        <tr class='text-left'>
            <th>Nome</th>
            <th>Menu Vinculado</th>
            <th>Rota</th>
            <th>Status</th>
            <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
        </tr>
        </thead>
        <tbody>

        @forelse ($subMenuOptions as $option)
            <tr>

                <td class='pl-0'>
                    {{$option->name}}
                </td>
                <td class='pl-0'>
                    {{$option->getMenuFather["name"]}}
                </td>
                <td>
                    {{$option->route}}
                </td>
                <td>
                    {{$option->visible == 1 ? 'Ativado' : 'Desativado' }}
                </td>
                <td class="d-flex align-items-center justify-content-center">
                    <a href='{{route('submenu.edit',$option->id)}}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    <a href='javascript:;' onclick='deleteItem({{$option->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='9' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
        @endforelse
        {{-- @forelse ($pedidos as $pedido)
        <tr>
            <td class='pl-0'>
                <span class="label label-dot label-xl label-{{ Config::get('constants.esic.priority_class')[$pedido->priority] }}"></span>
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->id }}</span>
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->created_at }}</span>
            </td>
            <td>
                @if (Route::currentRouteName() != 'estatisticas')
                <a href='{{ route('pedidos.show', $pedido->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $pedido->serial }}</a>
                @else
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->serial }}</span>
                @endif
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->name }}</span>
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->subject }}</span>
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->tipoPedido ? $pedido->tipoPedido->name : '---' }}</span>
            </td>
            <td>
                <span class='text-dark-75 d-block font-size-lg'>{{ $pedido->status_extenso }}</span>
            </td>
            @if (Route::currentRouteName() != 'estatisticas')
            <td class='pr-0 text-right'>
                <a href='{{ route('pedidos.show', $pedido->id) }}' class='btn btn-icon btn-light btn-hover-primary btn-sm mx-3'>
                    {{ Metronic::getSVG('media/svg/icons/General/Search.svg', 'svg-icon svg-icon-md svg-icon-primary') }}
                </a>
                <a href='javascript:;' onclick='deleteItem({{$pedido->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                    {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                </a>
                {!! Form::open(['route' => ['pedidos.destroy', $pedido->id], 'method'=>'POST', 'id' => 'form_remove_'.$pedido->id]) !!}
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                {!! Form::close() !!}
            </td>
            @endif
        </tr>
        @empty
        <tr>
            <td colspan='9' class='text-center'><i>Nenhum registro encontrado</i></td>
        </tr>
        @endforelse --}}
        </tbody>
    </table>
</div>

@section('scripts')
    {{-- {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!} --}}
    <script>
        // $('select[name="offset"]').change(function () {
        //     var route_val = $('#route').val();
        //     $(location).attr('href', route_val+'?offset='+$(this).val());
        // })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                // if (result.value) {
                //     $('#form_remove_'+item_id).submit();
                // }
            })
        }
    </script>
@endsection
