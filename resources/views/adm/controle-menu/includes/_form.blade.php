<div class='row'>
@foreach ($options as $option)
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', $option->name, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-8 '>
        <div class='form-group'>
            {!! Form::label('route', 'Route') !!}
            {!! Form::text('route', $option->route, [
                'class'=>(!empty($errors->default->first('route'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('route') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        {!! Form::label('visible', 'Visibilidade', ['class'=>'font-weight-bold text-uppercase']) !!}
        <span class="switch switch-success switch-outline switch-icon">

            <label>
                {!! Form::checkbox('visible', $option->visible , ($option->visible) == 1 ? 1 : 0) !!}
                <span></span>
            </label>
        </span>
    </div>


    <div class='col-md-8 ' >
        <div class='form-group'>
            {{-- {!! Form::label('id', 'id') !!} --}}
            {!! Form::hidden('id', $option->id, [
                'class'=>(!empty($errors->default->first('id'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('id') {{$message}} @enderror</div>
        </div>
    </div>
    @endforeach


</div>
