@extends('layout.default')

@section('page_toolbar')
<a href={{ route('pedidos.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@include('adm.pedidos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$pedido->id}}
            </span>
        </h3>
        <div class='card-toolbar'>
            
        </div>
    </div>
    {{--end::Header--}}
    {!! Form::model($pedido, ['route'=>['pedidos.update', $pedido->id], 'method'=>'PUT']) !!}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        <div class="row">
            <div class="col-md-4">
              <p><strong>Nome</strong><br> <span>{{ $pedido->name }}</span></p>
              <p><strong>Email</strong><br> <span>{{ $pedido->email }}</span></p>
              <p><strong>CPF/CNPJ</strong><br> <span>{{ $pedido->cpf }}</span></p>
              <p><strong>Telefone</strong><br> <span>{{ $pedido->phone }}</span></p>
            </div>
            <div class="col-md-4">
              <p><strong>Prioridade</strong><br> <span>{{ $pedido->priority_extenso }}</span></p>
              <p><strong>Endereço</strong><br> <span>{{ $pedido->location }}</span></p>
              <p><strong>Como deseja receber sua resposta?</strong><br> <span>{{ $pedido->type_answer }}</span></p>
              <p><strong>Tipo de Solicitação</strong><br> <span>{{ $pedido->tipoPedido->name }}</span></p>
            </div>
            <div class="col-md-4">
              <p><strong>Assunto</strong><br> <span>{{ $pedido->subject }}</span></p>
              <p><strong>Nº de Protocolo</strong><br> <span>{{ $pedido->serial }}</span></p>
              <p><strong>Data de Registro</strong><br> <span>{{ $pedido->created_at }}</span></p>
              <p><strong>Status</strong><br> <span class="text-success">{{ $pedido->status_extenso }}</span></p>
            </div>

        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="12">
                <p><strong>Mensagem</strong></p>
                <p>{{ $pedido->message }}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="12">
                    <p><strong>Anexo</strong></p>
                    <p>
                        @if ($pedido->anexo)
                        <a href="{{ asset($pedido->anexo) }}" target="_blank">Visualizar</a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <div class="form-inline">
            {!! Form::select('status', \Config::get('constants.esic.status'), $pedido->status_real, [
                'class'=>'form-control select2',
                'placeholder' => 'Selecione...'
            ]) !!}
            <button type='submit' class='btn btn-success ml-5'>Salvar Alterações</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Respostas
            </span>
        </h3>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    {!! Form::open(['route'=>'respostas.store']) !!}
    <div class='card-body py-0'>
        @if (session('message_resp_success'))
            <div class="form-group mt-5">
                <div class='alert alert-success font-bold'>{!! session('message_resp_success') !!}</div>
            </div>
        @endif
        @if (session('message_resp_error'))
            <div class="form-group">
                <div class='alert alert-danger font-bold'>{!! session('message_resp_error') !!}</div>
            </div>
        @endif
        <div class="messages">
            @if (count($pedido->respostas) > 0)
                @foreach ($pedido->respostas as $resposta)
                    @if ($resposta->user_id)
                    <div class="d-flex flex-column mb-5 align-items-end">
                        <div class="d-flex align-items-center">
                            <div>
                                <span class="text-muted font-size-sm mr-5">{{ $resposta->created_at }}</span>
                                <span class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $appSettingName }}</span>
                            </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-600px">
                            {{ $resposta->description }}
                        </div>
                    </div>
                    @else
                    <div class="d-flex flex-column mb-5 align-items-start">
                        <div class="d-flex align-items-center">
                            <div>
                                <span class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 mr-5">
                                    {{ $pedido->name }}
                                </span>
                                <span class="text-muted font-size-sm">{{ $resposta->created_at }}</span>
                            </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-600px">
                            {{ $resposta->description }}
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
        <div class="form-group">
            @csrf
            <label>Mensagem</label>
            <textarea name="description" rows="8" class="form-control" required></textarea>
            {!! Form::hidden('pedido_id', $pedido->id) !!}
            {!! Form::hidden('user_id', Auth::user()->id) !!}
        </div>
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        <button type='submit' class='btn btn-primary ml-5'>Enviar Resposta</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
