<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('nome', 'Nome') !!}
            {!! Form::text('nome', null, [
                'class'=>(!empty($errors->default->first('nome'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('nome') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('valor_bruto', 'Valor Bruto') !!}
            {!! Form::text('valor_bruto', null, [
                'class'=>(!empty($errors->default->first('valor_bruto'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor_bruto') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('descontos', 'Descontos') !!}
            {!! Form::text('descontos', null, [
                'class'=>(!empty($errors->default->first('descontos'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('descontos') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('valor_liquido', 'Valor Líquido') !!}
            {!! Form::text('valor_liquido', null, [
                'class'=>(!empty($errors->default->first('valor_liquido'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor_liquido') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('referencia', 'Referência') !!}
            {!! Form::month('referencia', null, [
                'class'=>(!empty($errors->default->first('referencia'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('referencia') {{$message}} @enderror</div>
        </div>
    </div>
</div>
