<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Nome</th>
                @if ($countCpf > 0)
                    <th>CPF</th>
                    <th>Regime</th>
                    <th>Secretaria</th>
                @endif
                @if ($countAdmissao > 0)
                    <th>Admissão</th>
                @endif
                @if ($countCpf > 0 or $countAdmissao > 0)
                    <th>Cargo</th>
                @endif
                <th>Valor Bruto</th>
                <th>Desconto</th>
                <th>Valor Líquido</th>
                <th>Referência</th>
                @if (!($countCpf > 0))
                <th>Anexo</th>
                @endif
                @if (Route::currentRouteName() != 'servidores')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($servidores as $servidor)
            <tr>
                <td class='pl-0'>
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->id }}</span>
                </td>
                <td>
                    @can('update', $servidor)
                    <a href='{{ route('servidores.edit', $servidor->id) }}' class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} font-weight-bolder text-hover-primary font-size-lg'>{{ $servidor->nome }}</a>
                    @else
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->nome }}</span>
                    @endcan
                </td>
                @if ($countCpf > 0)
                    <td style="min-width:120px">{{ $servidor->cpf_format }}</td>
                    <td>{{ $servidor->regime }}</td>
                    <td>{{ $servidor->secretaria }}</td>
                @endif
                @if ($countAdmissao > 0)
                    <td>{{ date('d/m/Y', strtotime($servidor->admissao)) }}</td>
                @endif
                @if ($countCpf > 0 or $countAdmissao > 0)
                    <td>{{ $servidor->cargo }}</td>
                @endif
                <td>
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->valor_bruto }}</span>
                </td>
                <td>
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->descontos }}</span>
                </td>
                <td>
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->valor_liquido }}</span>
                </td>
                <td>
                    <span class='{{ $servidor->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $servidor->referencia_format }}</span>
                </td>
                @if (!($countCpf > 0))
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark btn-lg" data-toggle="modal" data-target="#add-anexo-{{ $servidor->id }}">
                        VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $servidor->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $servidor->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="servidor">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($servidor->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                @endif
                @if (Route::currentRouteName() != 'servidores')
                <td class='pr-0 text-right'>
                    @if ($servidor->deleted_at)
                        <a href='{{ route('servidores.restore', $servidor->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                            {{ Metronic::getSVG('media/svg/icons/Communication/Reply.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                        </a>
                    @else
                        @if (empty($servidor->cpf))
                            @can('update', $servidor)
                            <a href='{{ route('servidores.edit', $servidor->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                                {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                            </a>
                            @endcan
                        @endif
                        @can('delete', $servidor)
                        <a href='javascript:;' onclick='deleteItem({{$servidor->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                            {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                        </a>
                        {!! Form::open(['route' => ['servidores.destroy', $servidor->id], 'method'=>'POST', 'id' => 'form_remove_'.$servidor->id]) !!}
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        {!! Form::close() !!}
                        @endcan
                    @endif
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='7' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
