{{-- Modal --}}
<div class='modal fade' id='import-servidores' tabindex='-1' role='dialog' aria-labelledby='import-servidores-title' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Importar servidores</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>'servidores.store','files'=>true]) !!}
            <div class='modal-body'>
                <div class='form-group'>
                    {!! Form::select('tipo', [
                        'import' => 'XML',
                        'import_excel' => 'Excel',
                    ], null, ['class'=>'form-control']) !!}
                    {!! Form::hidden('nome', 'rack') !!}
                    {!! Form::hidden('referencia', 'rack') !!}
                    <div class='invalid-feedback'>@error('tipo') {{$message}} @enderror</div>
                </div>
                <div class="form-group">
                <input type="file" name="servidores" id="servidores">
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <button type='submit' class='btn btn-primary'>Importar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
