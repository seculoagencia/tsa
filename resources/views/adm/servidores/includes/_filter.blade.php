{{-- Modal --}}
<div class='modal fade' id='filter-servidores' tabindex='-1' role='dialog' aria-labelledby='filter-servidores-title' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Filtrar servidores</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['method'=>'GET']) !!}
            <div class='modal-body'>
                <div class='row'>
                    <div class='col-md-12'>
                        <div class='form-group'>
                            {!! Form::label('nome', 'Nome') !!}
                            {!! Form::text('nome', request('nome'), ['class'=>'form-control form-control-solid form-solid']) !!}
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='form-group'>
                            {!! Form::label('referencia', 'Referência') !!}
                            {!! Form::month('referencia', request('referencia'), ['class'=>'form-control form-control-solid form-solid']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <a href='{{ route('servidores.index') }}' class='btn btn-info'>Limpar</a>
                <button type='submit' class='btn btn-primary'>Filtrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
