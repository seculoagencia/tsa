@extends('layout.default')

@section('page_toolbar')
<a href='{{ route('servidores.create') }}' class='btn btn-info font-weight-bolder font-size-sm mr-2'>CADASTRO MANUAL</a>
<a href='javascript:;' data-toggle='modal' data-target='#import-servidores' class='btn btn-secondary font-weight-bolder mr-2 font-size-sm'>IMPORTAÇÃO</a>
<a href='javascript:;' data-toggle='modal' data-target='#filter-servidores' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
@endsection

@section('content')
@include('adm.servidores.includes._filter')
@include('adm.servidores.includes._import')

@if (session('message_success'))
    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
@endif
@if (session('message_error'))
    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
@endif
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} de {{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @include('adm.servidores.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $servidores->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection
