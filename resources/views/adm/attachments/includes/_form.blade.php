<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-8'>
        <div class='form-group'>
            {!! Form::label('description', 'Descrição') !!}
            {!! Form::text('description', null, [
                'class'=>(!empty($errors->default->first('description'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('description') {{$message}} @enderror</div>
        </div>
    </div>
</div>
