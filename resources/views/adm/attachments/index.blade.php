@extends('layout.default')

@section('page_toolbar')
<a href='{{ route('attachments.create') }}' class='btn btn-info font-weight-bolder font-size-sm mr-2'>CADASTRAR</a>
<a href='javascript:;' data-toggle='modal' data-target='#filter-attachments' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
@endsection

@section('content')
@include('adm.attachments.includes._filter')

@if (session('message_success'))
    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>
@endif
@if (session('message_error'))
    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>
@endif
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_description }} de {{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @include('adm.attachments.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $attachments->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name='offset']').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
