<div class='row'>
    <div class='col-md-3'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-5'>
        <div class='form-group'>
            {!! Form::label('beneficiario', 'Beneficiário') !!}
            {!! Form::text('beneficiario', null, [
                'class'=>(!empty($errors->default->first('beneficiario'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('beneficiario') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! Form::label('valor', 'Valor') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! Form::label('referencia', 'Referência') !!}
            {!! Form::month('referencia', null, [
                'class'=>(!empty($errors->default->first('referencia'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('referencia') {{$message}} @enderror</div>
        </div>
    </div>
</div>
