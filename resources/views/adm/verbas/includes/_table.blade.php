<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Data</th>
                <th>Beneficiário</th>
                <th>Valor</th>
                <th>Referência</th>
                <th>Anexo</th>
                <!-- <th class='pr-0 text-right' style='min-width: 160px'>Ações</th> -->
            </tr>
        </thead>
        <tbody>
            @forelse ($verbas as $verba)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $verba->id }}</span>
                </td>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $verba->data_format }}</span>
                </td>
                <td>
	            <a href='{{ route('verbas.edit', $verba->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $verba->beneficiario }}</a>    
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $verba->valor }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $verba->referencia_format }}</span>
                </td>
                <td class='pl-0'>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark btn-lg" data-toggle="modal" data-target="#add-anexo-{{ $verba->id }}">
                      VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $verba->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $verba->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($verba->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr> 
            @empty 
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('#filter-verbas').on('shown.bs.modal', function () {
            $('.select2').select2({
                placeholder: "Selecione...",
                allowClear: true
            });
        })
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
