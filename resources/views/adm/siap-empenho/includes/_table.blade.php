<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Número</th>
                <th>Credor</th>
                <th>Data</th>
                <th>Valor</th>
                @if (Route::currentRouteName() != 'despesas')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($siapEmpenho as $empenho)
            <tr>
                <td class="pl-0">
                    <span class='{{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $empenho->id }}</span>
                </td>
                <td>
                    @can('view', $empenho)
                    <a href='{{ route('siap-empenho.show', $empenho->id) }}' class='{{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }} font-weight-bolder text-hover-primary font-size-lg'>{{ $empenho->NumeroEmpenho }}</a>
                    @else
                    {{-- Button trigger modal --}}
                    <button type="button" class="btn btn-text-success font-weight-bold text-dark-75 d-block font-size-lg {{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }}" data-toggle="modal" data-target="#modelId{{$empenho->id}}">
                        {{ $empenho->NumeroEmpenho }}
                    </button>

                    {{-- Modal --}}
                    @include('adm.siap-empenho.includes._modal')
                    @endcan
                </td>
                <td>
                    <span class='{{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $empenho->credor ? $empenho->credor->Nome : '' }}</span>
                </td>
                <td>
                    <span class='{{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>{{ $empenho->DataEmissao }}</span>
                </td>
                <td>
                    <span class='{{ $empenho->deleted_at ? 'text-danger' : 'text-dark-75' }} d-block font-size-lg'>R$ {{ $empenho->Valor }}</span>
                </td>
                @if (Route::currentRouteName() != 'despesas')
                <td class='pr-0 text-right'>
                    @if ($empenho->deleted_at)
                        <a href='{{ route('siap-empenho.restore', $empenho->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                            {{ Metronic::getSVG('media/svg/icons/Communication/Reply.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                        </a>
                    @else
                        <a href='{{ route('siap-empenho.show', $empenho->id) }}' class='btn btn-icon btn-light btn-hover-primary btn-sm mx-3'>
                            {{ Metronic::getSVG('media/svg/icons/General/Search.svg', 'svg-icon svg-icon-md svg-icon-primary') }}
                        </a>
                        <a href='javascript:;' onclick='deleteItem({{$empenho->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                            {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                        </a>
                        {!! Form::open(['route' => ['siap-empenho.destroy', $empenho->id], 'method'=>'POST', 'id' => 'form_remove_'.$empenho->id]) !!}
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        {!! Form::close() !!}
                    @endif
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='6' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
