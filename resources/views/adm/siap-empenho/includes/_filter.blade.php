{{-- Modal --}}
<div class='modal fade' id='filter-siap-empenho' tabindex='-1' role='dialog' aria-labelledby='filter-siap-empenho-title' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Filtrar SicapEmpenho</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['method'=>'GET']) !!}
            <div class='modal-body'>
                <div class='row'>
                    <div class='col-md-6'>
                        <div class='form-group'>
                            {!! Form::label('year', 'Ano') !!}
                            {!! Form::number('year', request('year'), ['class'=>'form-control form-control-solid form-solid']) !!}
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class='form-group'>
                            {!! Form::label('month', 'Mês') !!}
                            {!! Form::select('month', Config::get('constants.months'), request('month'), [
                                'class'=>'form-control select2 form-control-solid form-solid',
                                'style'=>'width:100%','placeholder'=>'Selecione...'
                            ]) !!}
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='form-group'>
                            {!! Form::label('numero', 'Número') !!}
                            {!! Form::text('numero', request('numero'), ['class'=>'form-control form-control-solid form-solid']) !!}
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='form-group'>
                            {!! Form::label('credor', 'Credor') !!}
                            {!! Form::text('credor', request('credor'), ['class'=>'form-control form-control-solid form-solid']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <a href='{{ route('siap-empenho.index') }}' class='btn btn-info'>Limpar</a>
                <button type='submit' class='btn btn-primary'>Filtrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
