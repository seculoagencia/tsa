@extends('layout.default')

@section('page_toolbar')
<a href={{ route('siap-empenho.index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>
@endsection

@section('content')
@include('adm.siap-empenho.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
            {{ $page_description }} de {{ $page_title }}: #{{$siapEmpenho->NumeroEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Form--}}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Data</label><br>
                    {{ $siapEmpenho->DataEmissao }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Número</label><br>
                    {{ $siapEmpenho->NumeroEmpenho }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Credor</label><br>
                    {{ $siapEmpenho->credor ? $siapEmpenho->credor->Nome : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Unidade Orçamentária</label><br>
                    {{ $siapEmpenho->undOrcamentaria ? $siapEmpenho->undOrcamentaria->Descricao : $siapEmpenho->CodigoUnidadeOrcamentaria }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Função</label><br>
                    {{ $siapEmpenho->funcao() ? $siapEmpenho->funcao()->Descricao : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Sub-Função</label><br>
                    {{  $siapEmpenho->subfuncao() ? $siapEmpenho->subfuncao()->Descricao : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Acao</label><br>
                    {{ $siapEmpenho->acao ? $siapEmpenho->acao->Descricao : '---' }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor</label><br>
                    R$ {{ $siapEmpenho->Valor }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Liquidado</label><br>
                    R$ {{ $siapEmpenho->totalLiquidado() }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="font-weight-bolder">Valor Pago</label><br>
                    R$ {{ $siapEmpenho->totalPago() }}
                </div>
            </div>
        </div>
        {{--end::Form--}}
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Pagamentos do {{ $page_title }}: #{{$siapEmpenho->NumeroEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Número</th>
                        <th>Conta Contábil</th>
                        <th>Descrição</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($siapEmpenho->pagamentos as $pagamento)
                    <tr>
                        <td scope="row">{{ $pagamento->Numero }}</td>
                        <td>{{ $pagamento->ContaContabil }}</td>
                        <td>{{ $pagamento->Descricao }}</td>
                        <td>{{ $pagamento->Data }}</td>
                        <td style="min-width: 120px">R$ {{ number_format($pagamento->Valor,2,',','.') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>
                Liquidações do {{ $page_title }}: #{{$siapEmpenho->NumeroEmpenho}}
            </span>
        </h3>
        <div class='card-toolbar'>

        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>Número</th>
                        <th>Conta Contábil</th>
                        <th>Justificativa</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($siapEmpenho->liquidacoes as $liquidacao)
                    <tr>
                        <td scope="row">{{ $liquidacao->Numero }}</td>
                        <td>{{ $liquidacao->ContaContabil }}</td>
                        <td>{{ $liquidacao->Justificativa }}</td>
                        <td>{{ $liquidacao->Data }}</td>
                        <td style="min-width: 120px">R$ {{ number_format($liquidacao->Valor,2,',','.') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--end::Body--}}
</div>
@endsection
