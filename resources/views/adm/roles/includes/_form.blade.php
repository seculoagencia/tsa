<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', null, [
                'class'=>(!empty($errors->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('name') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('permissions', 'Permissões') !!}
            <div class="row">
                @foreach($permissions as $value)
                <div class="col-md-3 mb-3">
                    <span class="switch switch-success switch-outline switch-icon">
                        <label class="mr-3">
                            {!! Form::checkbox('permissions[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, false) !!}
                            <span></span>
                        </label>
                        {{ $value->name }}
                    </span>
                </div>
                @endforeach
            </div>
            <div class='invalid-feedback'>@error('permissions') {{$message}} @enderror</div>
        </div>
    </div>
</div>
