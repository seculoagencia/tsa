<div class='table-responsive'>
    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
        <thead>
            <tr class='text-left'>
                <th class='pl-0'>#ID</th>
                <th>Código</th>
                <th>Descrição</th>
                <th>Valor</th>
                <th>Data</th>
                @if (Route::currentRouteName() != 'receitas')
                <th>COVID-19</th>
                @endif
                <th>Anexo</th>
                @if (Route::currentRouteName() != 'receitas')
                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($receitas as $receita)
            <tr>
                <td class='pl-0'>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $receita->id }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $receita->codigo }}</span>
                </td>
                <td>
                    @can('update', $receita)
                    <a href='{{ route('receitas.edit', $receita->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $receita->descricao }}</a>
                    @else
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $receita->descricao }}</span>
                    @endcan
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>R$ {{ $receita->valor_format }}</span>
                </td>
                <td>
                    <span class='text-dark-75 d-block font-size-lg'>{{ $receita->data_format }}</span>
                </td>
                @if (Route::currentRouteName() != 'receitas')
                <td>
                    <span class="font-weight-bolder text-{{$receita->covid19 ? 'success':'danger'}}">{{$receita->covid19 ? 'SIM':'NÃO'}}</span>
                </td>
                @endif
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm font-weight-bolder btn-dark btn-lg" data-toggle="modal" data-target="#add-anexo-{{ $receita->id }}">
                        VISUALIZAR
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="add-anexo-{{ $receita->id }}" tabindex="-1" role="dialog" aria-labelledby="add-anexo-{{ $receita->id }}-title" aria-hidden="true">
                        <div class="modal-dialog" role="receita">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Visualizar Anexos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($receita->attachments as $attachment)
                                    <div class="d-flex align-items-center mb-5">
                                        <i class="fas fa-file-upload mr-5"></i>
                                        <a href='{{ asset($attachment->path) }}' target="_blank" class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>
                                            {{ $attachment->original_name ? $attachment->original_name : 'Arquivo_'.date('YmdHis', strtotime($attachment->updated_at)).''.$attachment->id  }}
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                @if (Route::currentRouteName() != 'receitas')
                <td class='pr-0 text-right'>
                    @can('update', $receita)
                    <a href='{{ route('receitas.edit', $receita->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>
                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}
                    </a>
                    @endcan
                    @can('delete', $receita)
                    <a href='javascript:;' onclick='deleteItem({{$receita->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>
                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}
                    </a>
                    {!! Form::open(['route' => ['receitas.destroy', $receita->id], 'method'=>'POST', 'id' => 'form_remove_'.$receita->id]) !!}
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    {!! Form::close() !!}
                    @endcan
                </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@section('scripts')
    {!! Form::hidden('route', route(Route::currentRouteName()), ['id'=>'route']) !!}
    <script>
        $('select[name="offset"]').change(function () {
            var route_val = $('#route').val();
            $(location).attr('href', route_val+'?offset='+$(this).val());
        })
        $('#filter-receitas').on('shown.bs.modal', function () {
            $('.select2').select2({
                placeholder: "Selecione...",
                allowClear: true
            });
        })
        function deleteItem(item_id) {
            Swal.fire({
                title: 'Tem certeza disso?',
                text: 'O item #'+item_id+' será removido',
                icon: 'question',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sim, remover!',
                customClass: {
                    confirmButton: 'btn-danger',
                }
            }).then(function (result) {
                if (result.value) {
                    $('#form_remove_'+item_id).submit();
                }
            })
        }
    </script>
@endsection
