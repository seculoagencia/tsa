<div class='row'>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('codigo', 'Código') !!}
            {!! Form::text('codigo', null, [
                'class'=>(!empty($errors->default->first('codigo'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('codigo') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('valor', 'Valor') !!}
            {!! Form::text('valor', null, [
                'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('valor') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            {!! Form::label('data', 'Data') !!}
            {!! Form::date('data', null, [
                'class'=>(!empty($errors->default->first('data'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('data') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-4'>
        <div class='form-group'>
            <label class="checkbox checkbox-success">
                {!! Form::checkbox('covid19', null) !!}
                <span class="mr-5">&nbsp;</span>
                COVID-19
            </label>
            <div class='invalid-feedback'>@error('covid19') {{$message}} @enderror</div>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::textarea('descricao', null, [
                'class'=>(!empty($errors->default->first('descricao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
            ]) !!}
            <div class='invalid-feedback'>@error('descricao') {{$message}} @enderror</div>
        </div>
    </div>
</div>
