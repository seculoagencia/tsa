<div class="d-flex justify-content-between align-items-center flex-wrap">
    <div class="d-flex flex-wrap py-2 mr-3">
        @if ($paginator->hasPages())
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <span class="btn btn-icon btn-sm btn-light mr-2 my-1 disabled">
                        <i class="ki ki-bold-arrow-back icon-xs"></i>
                    </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="btn btn-icon btn-sm btn-light mr-2 my-1">
                    <i class="ki ki-bold-arrow-back icon-xs"></i>
                </a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <span class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1 disabled">{{ $element }}</span>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span class="btn btn-icon btn-sm border-0 btn-light btn-hover-primary active mr-2 my-1">{{ $page }}</span>
                        @else
                            <a href="{{ $url }}" class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="btn btn-icon btn-sm btn-light mr-2 my-1">
                    <i class="ki ki-bold-arrow-next icon-xs"></i>
                </a>
            @else
                <span class="btn btn-icon btn-sm btn-light mr-2 my-1 disabled">
                        <i class="ki ki-bold-arrow-next icon-xs"></i>
                    </span>
            @endif
        @endif
    </div>
    <div class="d-flex align-items-center py-3">
        <select name="offset" class="form-control form-control-sm font-weight-bold mr-4 border-0 bg-light" style="width: 75px;">
            <option value="10" {{ $paginator->perPage() == 10 ? 'selected':'' }}>10</option>
            <option value="20" {{ $paginator->perPage() == 20 ? 'selected':'' }}>20</option>
            <option value="30" {{ $paginator->perPage() == 30 ? 'selected':'' }}>30</option>
            <option value="50" {{ $paginator->perPage() == 50 ? 'selected':'' }}>50</option>
            <option value="100" {{ $paginator->perPage() == 100 ? 'selected':'' }}>100</option>
        </select>
        <span class="text-muted">Exibindo {{$paginator->perPage()}} de {{$paginator->total()}} registros</span>
    </div>
</div>
