@extends('layout.default')

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Empenhos</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\Empenho::count() > 0
                        ? date('Y', strtotime(\App\Models\Empenho::orderBy('data','desc')->first()->data))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">Último Mês:
                    <span class="text-success">{{
                        \App\Models\Empenho::count() > 0
                        ? date('m', strtotime(\App\Models\Empenho::orderBy('data','desc')->first()->data))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\Empenho::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Receitas</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\Receita::count() > 0
                        ? date('Y', strtotime(\App\Models\Receita::orderBy('data','desc')->first()->data))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">Último Mês:
                    <span class="text-success">{{
                        \App\Models\Receita::count() > 0
                        ? date('m', strtotime(\App\Models\Receita::orderBy('data','desc')->first()->data))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\Receita::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Licitações e Contratos</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\Contrato::count() > 0
                        ? date('Y', strtotime(\App\Models\Contrato::orderBy('data_hora','desc')->first()->data_hora))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">
                    Último Mês:
                    <span class="text-success">{{
                        \App\Models\Contrato::count() > 0
                        ? date('m', strtotime(\App\Models\Contrato::orderBy('data_hora','desc')->first()->data_hora))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\Contrato::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Pedido de Informação</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Pedidos Abertos:
                    <span class="text-success">{{ \App\Models\Pedido::where('status','0')->count() }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">
                    Pedidos Respondidos:
                    <span class="text-success">{{ \App\Models\Pedido::where('status','1')->count() }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\Pedido::count() }} solicitações
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Empenhos - SICAP</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\SicapEmpenho::count() > 0
                        ? date('Y', strtotime(\App\Models\SicapEmpenho::orderBy('DataEmpenho','desc')->first()->DataEmpenho))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">
                    Último Mês:
                    <span class="text-success">{{
                        \App\Models\SicapEmpenho::count() > 0
                        ? date('m', strtotime(\App\Models\SicapEmpenho::orderBy('DataEmpenho','desc')->first()->DataEmpenho))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\SicapEmpenho::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Receitas - SICAP</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\SicapReceita::count() > 0
                        ? date('Y', strtotime(\App\Models\SicapReceita::orderBy('DataArrecadacao','desc')->first()->DataArrecadacao))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">Último Mês:
                    <span class="text-success">{{
                        \App\Models\SicapReceita::count() > 0
                        ? date('m', strtotime(\App\Models\SicapReceita::orderBy('DataArrecadacao','desc')->first()->DataArrecadacao))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\SicapReceita::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-4.svg)">
            <div class="card-body">
                <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">Empenhos - SAGRES</a>
                <div class="font-weight-bold mt-9 mb-0">
                    Último Exercício:
                    <span class="text-success">{{
                        \App\Models\SagresEmpenho::count() > 0
                        ? date('Y', strtotime(\App\Models\SagresEmpenho::orderBy('data_emissao_empenho','desc')->first()->data_emissao_empenho))
                        : '---'
                    }}</span>
                </div>
                <div class="font-weight-bold mt-0 mb-5">
                    Último Mês:
                    <span class="text-success">{{
                        \App\Models\SagresEmpenho::count() > 0
                        ? date('m', strtotime(\App\Models\SagresEmpenho::orderBy('data_emissao_empenho','desc')->first()->data_emissao_empenho))
                        : '---'
                    }}</span>
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">
                    Total de {{ \App\Models\SagresEmpenho::count() }} cadastros
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
