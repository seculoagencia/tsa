<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }}
    {{ Metronic::printClasses('html') }}>

<head>
    <meta charset="utf-8" />

    {{-- Title Section --}}
    <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>
    <link href="{{ asset('/css/modal.css') }}" rel="stylesheet">
    {{-- Meta Data --}}
    <meta name="description" content="@yield('page_description', $page_description ?? '')" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <script src="https://kit.fontawesome.com/70cc1b4b0d.js" crossorigin="anonymous"></script>
    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('media/logos/favicon.png') }}" />

    {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach (config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}"
            rel="stylesheet" type="text/css" />
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}"
            rel="stylesheet" type="text/css" />
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}"
            rel="stylesheet" type="text/css" />
    @endforeach




    <style>
        a.text-hover-primary:hover,
        .text-hover-primary:hover {
            color: {{ $appSetting && $appSetting->cor_principal ? $appSetting->cor_principal : '#00B0A8' }} !important;
        }

        .svg-icon.svg-icon-success svg g [fill] {
            fill: {{ $appSetting && $appSetting->cor_principal ? $appSetting->cor_principal : '#00B0A8' }} !important;
        }
    </style>

    {{-- Includable CSS --}}
    @yield('styles')

</head>

<body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }}>

    @if (config('layout.page-loader.type') != '')
        @include('layout.partials._page-loader')
    @endif

    <div class="d-flex flex-column flex-root w-100" >
        <div class="site-header w-100  py-5 " style="background: {{ $appSetting && $appSetting->cor_principal ? $appSetting->cor_principal : '#00B0A8' }} ">

            <div class=""  style="width: 100%; ">
                <div class="column d-flex align-items-center justify-content-center   flex-wrap  containerTwo" style="width: 100%;" >


                    <div class="d-flex  align-items-center flex-wrap  menu-total" style="width: 100%; " >

                        <div class="mr-5" >
                        <a href="{{ url('/') }}"><img src="{{ $appSetting ? asset($appSetting->logo) : '' }}" alt="Logo" class="img-logo" ></a>
                        </div>


                        <div class="dropdown mobile-menu dropleft ">
                            <a class="mr-5 dropdown-toggle text-white" href="javascript:;" id="dropdownMenuButtons"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa-solid fa-bars" style="color:white; font-size:40px"></i>
                            </a>

                            <div class="dropdown-menu dropMenus" aria-labelledby="dropdownMenuButtons">
                                <a class="mr-5 dropdown-item text-dark "  id="dropdownMenu"
                                data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false">
                                <hr>Opções<hr>
                                </a>
                                <a href="{{ url('/') }}"
                                class="mr-5 {{ request()->route()->getName() == 'home'? 'active': null }} dropdown-item text-dark">Portal
                                da Transparência</a>

                                <a href="{{ url('/pedidos-esic') }} "
                                class=" dropdown-item text-dark {{ request()->segment(1) == 'pedidos-esic' ? 'active' : null }}">Pedidos
                                e-SIC</a>


                                @if ($appSetting && $appSetting->display_covid)
                                    <a href="{{ url('/covid19') }} "
                                    class="mr-5 dropdown-item text-dark {{ request()->segment(1) == 'covid19' ? 'active' : null }}">COVID-19</a>
                                @endif

                                <a class="mr-5 dropdown-item text-dark "  id="dropdownMenu"
                                data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false">
                                <hr>Consulta<hr>
                                </a>
                                @foreach ($optionsCompleted as $optionss )

                                <a class="dropdown-item text-dark" href="{{ route($optionss['route']) }}">{{$optionss['name']}}</a>
                                @endforeach

                                <a class="mr-5 dropdown-item text-dark "  id="dropdownMenu"
                                data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false">
                                <hr>
                                </a>

                            </div>
                        </div>

                        <div class="dialog-responsive">
                            <div class="d-flex text-right public-menu d-flex align-items-center">

                                <div>
                                    <a href="{{ url('/') }}"
                                    class="mr-5 {{ request()->route()->getName() == 'home'? 'active': null }}">Portal
                                    da Transparência</a>
                                </div>
                                <div class="dropdown">
                                    <a class="mr-5 dropdown-toggle" href="javascript:;" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Consultas
                                    </a>

                                    <div class="dropdown-menu dropMenus" aria-labelledby="dropdownMenuButton">
                                        @foreach ($optionsCompleted as $optionss )

                                        <a class="dropdown-item text-dark" href="{{ route($optionss['route']) }}">{{$optionss['name']}}</a>
                                        @endforeach
                                    </div>
                                </div>

                                    @if ($appSetting && $appSetting->display_covid)

                                    <div>
                                    <a href="{{ url('/covid19') }} "
                                    class="mr-5 {{ request()->segment(1) == 'covid19' ? 'active' : null }}">COVID-19</a>
                                    </div>
                                    @endif

                                <div>
                                    <a href="{{ url('/pedidos-esic') }} "
                                    class="{{ request()->segment(1) == 'pedidos-esic' ? 'active' : null }}">Pedidos
                                    e-SIC</a>
                                </div>


                            </div>

                            </div>
                            <div class="input-group  mx-6 search-input  " >
                                <form class="d-flex form-item" method="POST" action="{{url('/search-options')}}">
                                @method('POST')
                                @csrf
                                <input type="search" name="search" class="form-control rounded input-item" placeholder="Buscar no portal" aria-label="Search" aria-describedby="search-addon" />
                                <button type="submit"  class="btn btn-primary"><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                    </div>

                </div>

            </div>
        </div>
        {{-- https://www.japaratinga.al.gov.br/portal/wp-content/uploads/2021/03/Praia-de-Japaratinga.jpg --}}
        <div class="site-cover" style="background-image: url({{ $appSetting ? asset($appSetting->cover) : '' }}); ">
            <h1 class="text-center  portalDaTransferencia ">
                <small>PORTAL DA <br> <strong>TRANSPARÊNCIA</strong></small>
            </h1>
        </div>
        <div class="container mt-10">
            @yield('content')
        </div>
        <div class="footer bg-dark py-4 mt-22 d-flex flex-lg-column " id="kt_footer">
            <div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-center">
                <div class="text-dark order-2 order-md-1">
                    <span class="text-muted font-weight-bold mr-2">2021 ©</span>
                    <a href="https://seculoagencia.com.br" target="_blank" class="text-white text-hover-primary">Alfa
                        Sistemas</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        var HOST_URL = "{{ route('home') }}";
    </script>

    {{-- Global Config (global config for global JS scripts) --}}
    <script>
        var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) !!};
    </script>

    {{-- Global Theme JS Bundle (used by all pages)  --}}
    @foreach (config('layout.resources.js') as $script)
        <script src="{{ asset($script) }}" type="text/javascript"></script>
    @endforeach

    <script>
        $('.cpf').mask('000.000.000-00', {
            reverse: true
        });
        $('.phone').mask('(00) 0 0000-0000');
        $('.select2').select2({
            placeholder: "Selecione...",
            allowClear: true
        });
    </script>

    {{-- Includable JS --}}
    @yield('scripts')

    <div vw class="enabled">
      <div vw-access-button class="active"></div>
      <div vw-plugin-wrapper>
        <div class="vw-plugin-top-wrapper"></div>
      </div>
    </div>
    <script src="https://vlibras.gov.br/app/vlibras-plugin.js"></script>
    <script>
      new window.VLibras.Widget('https://vlibras.gov.br/app');
    </script>

</body>

</html>
