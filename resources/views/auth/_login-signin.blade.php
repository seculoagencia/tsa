{{-- Signin --}}
<div class="login-form login-signin">
    {{-- Form --}}
    <form class="form text-left" novalidate="novalidate" id="kt_login_signin_form"  method="POST" action="{{ route('login') }}">
        <div class="pb-13 pt-lg-0 pt-5">
            <h3 class="font-weight-bolder text-center text-dark font-size-h4 font-size-h3-lg">Acesso Restrito</h3>
        </div>

        {{-- <form method="POST" action="{{ route('login') }}"> --}}
        @csrf

        @error('username')
        <div class="mb-5 text-center">
            <span class="text-danger" role="alert">
                <strong><i class="fa fa-exclamation text-danger">&nbsp;</i> {{ $message }}</strong>
            </span>
        </div>
        @enderror

        <div class="form-group">
            <label class="font-size-h6 font-weight-bolder text-dark">Usuário ou Email</label>
            <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="text" name="username" autocomplete="off" />
        </div>

        {{-- <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div> --}}

        <div class="form-group">
            <div class="d-flex justify-content-between mt-n5">
                <label class="font-size-h6 font-weight-bolder text-dark pt-5">Senha</label>
                <a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Esqueceu a Senha ?</a>
            </div>
            <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg @error('password') is-invalid @enderror" type="password" name="password" autocomplete="off" />
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        {{-- <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div> --}}
        <div class="pb-lg-0 pb-5">
            <button type="button" id="kt_login_signin_submit" class="btn btn-block btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Acessar</button>
        </div>
        {{-- <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </div>
        </div> --}}
    </form>
</div>
