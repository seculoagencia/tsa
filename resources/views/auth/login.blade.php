@extends('layout.auth')

@section('content')

@include('auth._login-signin')
@include('auth._login-signup')
@include('auth._login-forgot')

@endsection
