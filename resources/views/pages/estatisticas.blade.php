@extends('layout.public')

@section('content')
@include('adm.empenhos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        <form action="{{ route('estatisticas') }}" method="get">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="data_inicial">Data Inicial</label>
                        {!! Form::date('data_inicial', request('data_inicial'), ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="data_final">Data Inicial</label>
                        {!! Form::date('data_final', request('data_final'), ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label><br>
                    <button type="submit" class="btn btn-dark">Gerar relatório</button>
                </div>
            </div>
        </form>
        {{--begin::Table--}}
        <div class='table-responsive'>
            <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>
                <thead>
                    <tr class='text-left'>
                        <th class='pl-0'>Respondido</th>
                        <th>Aberto</th>
                        <th>Em Tramitação</th>
                        <th>Negado</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $respondidos }}</td>
                    <td>{{ $abertos }}</td>
                    <td>{{ $tramitacao }}</td>
                    <td>{{ $negados }}</td>
                    <td>{{ $respondidos + $abertos + $tramitacao + $negados }}</td>
                </tbody>
            </table>
        </div>
        {{--end::Table--}}
    </div>
    {{--end::Body--}}

</div>

<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>Pedidos de Informação</span>
        </h3>
    </div>
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @include('adm.pedidos.includes._table')
        {{--end::Table--}}
        {{--end::Body--}}
        <div class='card-footer text-right'>
            {{ $pedidos->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
        </div>
        {{--end::Table--}}
    </div>
    {{--end::Header--}}
</div>
@endsection
