@extends('layout.public')

@section('content')
@include('adm.empenhos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            <div class="btn-group btn-group-lg" role="group" aria-label="Large button group">
                <a href="{{ route(Route::currentRouteName(), ['export'=>'xml', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">XML</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'csv', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">CSV</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'json', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">JSON</a>
            </div>
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
        @endif
        @if (count($tipos) > 0)
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @foreach ($tipos as $key => $tipo)
                <li class="nav-item" role="presentation">
                <a class="nav-link {{ ($key == 0) ? 'active':'' }}" id="tab{{$key}}" data-toggle="tab" href="#content{{$key}}" role="tab" aria-controls="content{{$key}}" aria-selected="true">{{ $tipo->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach ($tipos as $key => $tipo)
                <div class="tab-pane fade show {{ ($key == 0) ? 'active':'' }}" id="content{{$key}}" role="tabpanel" aria-labelledby="content{{$key}}-tab">
                    <div class="col-md-12">
                        <p class="my-10">{{ $tipo->description }}</p>
                        @foreach ($tipo->arquivos as $arquivo)
                        <p class="my-10">
                            <a href="{{ asset($arquivo->attachment->path) }}" class="btn btn-primary btn-sm" target="_blank">{{ $arquivo->name }}</a> - {{ $arquivo->description }}
                        </p>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        @endif
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
</div>
@endsection
