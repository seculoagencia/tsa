@extends('layout.public')

@section('content')
@include('adm.receitas.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            <a href='javascript:;' data-toggle='modal' data-target='#filter-receitas' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
        @endif
        {{--begin::Table--}}
        {{-- {!! Form::hidden('route', route('receitas'), ['id'=>'route']) !!} --}}
        @include('adm.receitas.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $receitas->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection
