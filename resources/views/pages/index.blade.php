@extends('layout.public')

@section('content')
<div class="row">
    @if(! isset($options))
    <style>
        .option{

            height: 200px;
        }

    </style>
    @foreach ($optionsCompleted as $option)
     @if($option["name"] == 'Contratos')
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-7 ">
            <a href="{{ route($option['route']) }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
            <div class="card card-custom mb-8 mb-lg-0 d-flex align-items-center justify-content-center option ">
                <div class="card-body  d-flex align-items-center justify-content-center  ">
                    <div class="d-flex flex-column align-items-center p-5">
                        <div class="text-center mb-3">
                            <span class="svg-icon svg-icon-4x">
                                {{ Metronic::getSVG($option['icon'], "svg-icon-success") }}
                            </span>
                        </div>
                        <style>
                            @media(max-width:600px){
                                .dropMenus2{
                                    width: 200px;
                                }
                            }
                        </style>
                        <div class="dropdown">
                            <a class="ml-2 text-dark text-hover-primary font-weight-bold font-size-h5  dropdown-toggle" href="javascript:;" id="dropdownMenuButton3"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$option["name"]}}
                            </a>

                            <div class="dropdown-menu dropMenus2" aria-labelledby="dropdownMenuButton3">

                                <a class="dropdown-item text-dark" href="{{route($option['route'])}}" >{{$option['name']}}</a>
                                @for($i = 0; $i < count($option->getSubMenus('6')); $i++ )
                                <a class="dropdown-item text-dark" href="{{route($option->getSubMenus('6')[$i]["route"])}}" >{{$option->getSubMenus('6')[$i]["name"]}}</a>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>
    @elseif($option["name"] == 'Licitações')
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-7 ">
            <a href="{{ route($option['route']) }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
                <div class="card card-custom mb-8 mb-lg-0 d-flex align-items-center justify-content-center option ">
                    <div class="card-body  d-flex align-items-center justify-content-center  ">
                        <div class="d-flex flex-column align-items-center p-5">
                            <div class="text-center mb-3">
                <span class="svg-icon svg-icon-4x">
                    {{ Metronic::getSVG($option['icon'], "svg-icon-success") }}
                </span>
                            </div>
                            <style>
                                @media(max-width:600px){
                                    .dropMenus2{
                                        width: 200px;
                                    }
                                }
                            </style>
                            <div class="dropdown">
                                <a class="ml-2 text-dark text-hover-primary font-weight-bold font-size-h5  dropdown-toggle" href="javascript:;" id="dropdownMenuButton3"
                                   data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    {{$option["name"]}}
                                </a>


                                <div class="dropdown-menu dropMenus2" aria-labelledby="dropdownMenuButton3">
                                    <a class="dropdown-item text-dark" href="{{route($option['route'])}}" >{{$option['name']}}</a>
                                @for($i = 0; $i < count($option->getSubMenus('5')); $i++ )
                                    <a class="dropdown-item text-dark" href="{{route($option->getSubMenus('5')[$i]["route"])}}">{{$option->getSubMenus('5')[$i]["name"]}}</a>
                                @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    @else

    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-7 ">
        <a href="{{ route($option['route']) }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0 d-flex align-items-center justify-content-center option ">
            <div class="card-body  d-flex align-items-center justify-content-center  ">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG($option['icon'], "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        {{$option['name']}}
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
     @endif


    @endforeach
</div>


    @else
    <div class="w-100 ml-5 pb-5 d-flex align-items-center justify-content-start">
        <div class="bg-dark p-3 px-5 text-white text-start teste" >
            <div>
                <strong> Sua busca foi:</strong> {{$resultSearch['searchItem']}}
            </div>
            <div>
                <strong> Resultado Encontrado:</strong> {{$resultSearch['qtdSearchFind']}}
            </div>
        </div>
    </div>
    @foreach ($options as $option)

    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-7">
        <a href="{{ route($option['route']) }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG($option['icon'], "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        {{$option['name']}}
                    </div>
                </div>
            </div>
        </div>
        </a>
   </div>

    @endforeach
    @endif
</div>
@endsection
