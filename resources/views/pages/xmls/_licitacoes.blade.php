@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

<Licitacoes>
    @foreach ($collection as $collect)
        <licitacao>
           <tipo>{{ $collect->tipo ? Config::get('constants.licitacoes.tipo')[$collect->tipo] : '' }}</tipo>
           <orgao>{{ $collect->orgao }}</orgao>
           <modalidade>{{ $collect->modalidade }}</modalidade>
           <dataHora>{{ $collect->data_hora }}</dataHora>
           <valor>{{ $collect->valor }}</valor>
           <status>{{ $collect->status ? Config::get('constants.licitacoes.status')[$collect->status] : '' }}</status>
           <vencedor>{{ $collect->vencedor }}</vencedor>
           <anexos>
           @foreach ($collect->attachments as $attachment)
               <anexo>
                   <nome>{{ $attachment->original_name }}</nome>
                   <path>{{ $attachment->path }}</path>
               </anexo>
           @endforeach
            </anexos>
        </licitacao>
    @endforeach
</Licitacoes>
