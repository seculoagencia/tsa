@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

<planejamentoOrcamentario>
    @foreach ($collection as $collect)
        <orcamento>
           <tipo>{{ $collect->name }}</tipo>
           <descricao>{{ $collect->description }}</descricao>
           @foreach ($collect->arquivos as $file)
               <arquivo>
                   <nome>{{ $file->name }}</nome>
                   <descricao>{{ $file->description }}</descricao>
                   <path>{{ $file->attachment->path }}</path>
               </arquivo>
           @endforeach
        </orcamento>
    @endforeach
</planejamentoOrcamentario>