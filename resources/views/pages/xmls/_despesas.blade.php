@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

@if ($appSetting->prioridade == 'manual')
<Empenhos>
    @foreach ($collection as $collect)
        <empenho>
           <data>{{ $collect->data }}</data>
           <numero>{{ $collect->numero }}</numero>
           <credor>{{ $collect->credor }}</credor>
           <unidadeOrcamentaria>{{ $collect->unidade_orcamentaria }}</unidadeOrcamentaria>
           <funcao>{{ $collect->funcao }}</funcao>
           <subfuncao>{{ $collect->subfuncao }}</subfuncao>
           <programa>{{ $collect->programa }}</programa>
           <fonteDeRecurso>{{ $collect->fonte_de_recurso }}</fonteDeRecurso>
           <elementoDeDespesa>{{ $collect->elemento_de_despesa }}</elementoDeDespesa>
           <numeroDoProcesso>{{ $collect->numero_do_processo }}</numeroDoProcesso>
           <valorLiquidado>{{ $collect->totalLiquidacoes() }}</valorLiquidado>
           <valorPago>{{ $collect->totalPagamentos() }}</valorPago>
           <historico>{{ $collect->historico }}</historico>
           <pagamentos>
           @foreach ($collect->pagamentos as $pagamento)
               <pagamento>
                   <banco>{{ $pagamento->banco }}</banco>
                   <agencia>{{ $pagamento->agencia }}</agencia>
                   <conta>{{ $pagamento->conta }}</conta>
                   <documento>{{ $pagamento->documento }}</documento>
                   <valor>{{ $pagamento->valor }}</valor>
                   <data>{{ $pagamento->data }}</data>
               </pagamento>
           @endforeach
            </pagamentos>
           <liquidacoes>
           @foreach ($collect->liquidacoes as $liquidacao)
               <liquidacao>
                   <numero>{{ $liquidacao->numero }}</numero>
                   <valor>{{ $liquidacao->valor }}</valor>
                   <data>{{ $liquidacao->data }}</data>
               </liquidacao>
           @endforeach
            </liquidacoes>
        </empenho>
    @endforeach
</Empenhos>
@endif

@if ($appSetting->prioridade == 'sicap')
<Empenhos>
    @foreach ($collection as $collect)
        <empenho>
           <data>{{ $collect->DataEmpenho }}</data>
           <numero>{{ $collect->NumEmpenho }}</numero>
           <credor>{{ @$collect->credor->Nome }}</credor>
           <unidadeOrcamentaria>{{ @$collect->undOrcamentaria->Nome }}</unidadeOrcamentaria>
           <funcao>{{ @$collect->funcao->Nome }}</funcao>
           <subfuncao>{{ @$collect->subfuncao->Nome }}</subfuncao>
           <programa>{{ @$collect->programa->Nome }}</programa>
           <fonteDeRecurso>{{ @$collect->recursoVinculado->Nome }}</fonteDeRecurso>
           <elementoDeDespesa>{{ @$collect->rubricaDespesa->Especificacao }}</elementoDeDespesa>
           <numeroDoProcesso>{{ $collect->NumProcesso }}</numeroDoProcesso>
           <valorLiquidado>{{ $collect->totalLiquidado() }}</valorLiquidado>
           <valorPago>{{ $collect->totalPago() }}</valorPago>
           <historico>{{ $collect->Historico }}</historico>
           <pagamentos>
           @foreach ($collect->pagamentos as $pagamento)
               <pagamento>
                   <banco>{{ @$pagamento->empenho->pagamentoFinanceiro->CodBanco }}</banco>
                   <agencia>{{ @$pagamento->empenho->pagamentoFinanceiro->CodAgenciaBanco }}</agencia>
                   <conta>{{ $pagamento->empenho->pagamentoFinanceiro->NumContaBancaria }}</conta>
                   <documento>{{ $pagamento->empenho->pagamentoFinanceiro->NumProcesso }}</documento>
                   <valor>{{ $pagamento->Valor }}</valor>
                   <data>{{ $pagamento->DataPagamento }}</data>
               </pagamento>
           @endforeach
            </pagamentos>
           <liquidacoes>
           @foreach ($collect->liquidacoes as $liquidacao)
               <liquidacao>
                   <numero>{{ $liquidacao->NumLiquidacao }}</numero>
                   <valor>{{ $liquidacao->Valor }}</valor>
                   <data>{{ $liquidacao->DataLiquidacao }}</data>
               </liquidacao>
           @endforeach
            </liquidacoes>
        </empenho>
    @endforeach
</Empenhos>
@endif

@if ($appSetting->prioridade == 'siap')
<Empenhos>
    @foreach ($collection as $collect)
        <empenho>
           <data>{{ $collect->DataEmissao }}</data>
           <numero>{{ $collect->NumeroEmpenho }}</numero>
           <credor>{{ @$collect->credor->Nome }}</credor>
           <unidadeOrcamentaria>{{ @$collect->undOrcamentaria->Descricao }}</unidadeOrcamentaria>
           <funcao>{{ @$collect->funcao()->Descricao }}</funcao>
           <subfuncao>{{ @$collect->subfuncao()->Descricao }}</subfuncao>
           <programa>{{ @$collect->acao->Descricao }}</programa>
           <valorLiquidado>{{ $collect->totalLiquidado() }}</valorLiquidado>
           <valorPago>{{ $collect->totalPago() }}</valorPago>
           <pagamentos>
           @foreach ($collect->pagamentos as $pagamento)
               <pagamento>
               </pagamento>
           @endforeach
            </pagamentos>
           <liquidacoes>
           @foreach ($collect->liquidacoes as $liquidacao)
               <liquidacao>
               </liquidacao>
           @endforeach
            </liquidacoes>
        </empenho>
    @endforeach
</Empenhos>
@endif

@if ($appSetting->prioridade == 'sagres')
<Empenhos>
    @foreach ($collection as $collect)
        <empenho>
           <data>{{ $collect->data_emissao_empenho }}</data>
           <numero>{{ $collect->num_empenho }}</numero>
           <credor>{{ $collect->credor ? $collect->credor->nome : $collect->cpf_cnpj_credor }}</credor>
           <unidadeOrcamentaria>{{ @$collect->undOrcamentaria->denominacao }}</unidadeOrcamentaria>
           <funcao>{{ @$collect->funcao->Nome }}</funcao>
           <subfuncao>{{ @$collect->subfuncao->Nome }}</subfuncao>
           <programa>{{ @$collect->getPrograma->nome }}</programa>
           <elementoDeDespesa>{{ @$collect->rubricaDespesa->descricao }}</elementoDeDespesa>
           <numeroDoProcesso>{{ $collect->num_procedimento }}</numeroDoProcesso>
           <valorLiquidado>{{ $collect->totalLiquidado() }}</valorLiquidado>
           <valorPago>{{ $collect->totalPago() }}</valorPago>
           <historico>{{ $collect->historico }}</historico>
           <pagamentos>
           @foreach ($collect->pagamentos as $pagamento)
               <pagamento>
                   <banco>{{ @$pagamento->cod_banco }}</banco>
                   <agencia>{{ @$pagamento->num_agencia_bancaria }}</agencia>
                   <conta>{{ $pagamento->num_conta_bancaria }}</conta>
                   <valor>{{ $pagamento->valor }}</valor>
                   <data>{{ $pagamento->data }}</data>
               </pagamento>
           @endforeach
            </pagamentos>
           <liquidacoes>
           @foreach ($collect->liquidacoes as $liquidacao)
               <liquidacao>
                   <numero>{{ $liquidacao->num_liquidacao }}</numero>
                   <valor>{{ $liquidacao->valor }}</valor>
                   <data>{{ $liquidacao->data }}</data>
               </liquidacao>
           @endforeach
            </liquidacoes>
        </empenho>
    @endforeach
</Empenhos>
@endif
