@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

@if ($appSetting->prioridade == 'manual')
<Receitas>
    @foreach ($collection as $collect)
        <receita>
           <codigo>{{ $collect->codigo }}</codigo>
           <descricao>{{ $collect->descricao }}</descricao>
           <valor>{{ $collect->valor }}</valor>
           <data>{{ $collect->data }}</data>
           <anexos>
           @foreach ($collect->attachments as $attachment)
               <anexo>
                   <nome>{{ $attachment->original_name }}</nome>
                   <path>{{ $attachment->path }}</path>
               </anexo>
           @endforeach
            </anexos>
        </receita>
    @endforeach
</Receitas>
@endif

@if ($appSetting->prioridade == 'sicap')
<Receitas>
    @foreach ($collection as $collect)
        <receita>
           <DataArrecadacao>{{ $collect->DataArrecadacao }}</DataArrecadacao>
           <Exercicio>{{ $collect->Exercicio }}</Exercicio>
           <unidadeOrcamentaria>{{ @$collect->undOrcamentaria->Nome }}</unidadeOrcamentaria>
           <CodBanco>{{ $collect->CodBanco }}</CodBanco>
           <CodAgencia>{{ $collect->CodAgencia }}</CodAgencia>
           <NumConta>{{ $collect->NumConta }}</NumConta>
           <RecursoVinculado>{{ $collect->recursoVinculado->Nome }}</RecursoVinculado>
           <Valor>{{ $collect->Valor }}</Valor>
        </receita>
    @endforeach
</Receitas>
@endif
