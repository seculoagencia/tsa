@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

@if ($appSetting->prioridade == 'manual')
<Servidores>
    @foreach ($collection as $collect)
        <servidor>
           <nome>{{ $collect->nome }}</nome>
           <valorBruto>{{ $collect->valor_bruto }}</valorBruto>
           <descontos>{{ $collect->descontos }}</descontos>
           <valorLiquido>{{ $collect->valor_liquido }}</valorLiquido>
           <referencia>{{ $collect->referencia }}</referencia>
           <anexos>
           @foreach ($collect->attachments as $attachment)
               <anexo>
                   <nome>{{ $attachment->original_name }}</nome>
                   <path>{{ $attachment->path }}</path>
               </anexo>
           @endforeach
            </anexos>
        </servidor>
    @endforeach
</Servidores>
@endif

@if ($appSetting->prioridade == 'sicap')
<Servidores>
    @foreach ($collection as $collect)
        <servidor>
           <Nome>{{ $collect->Nome }}</Nome>
           <Exercicio>{{ $collect->Exercicio }}</Exercicio>
           <DataAdmissao>{{ $collect->DataAdmissao }}</DataAdmissao>
           <Descricao>{{ $collect->Descricao }}</Descricao>
        </servidor>
    @endforeach
</Servidores>
@endif
