@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

<Diarias>
    @foreach ($collection as $collect)
        <diaria>
           <favorecido>{{ $collect->favorecido }}</favorecido>
           <cargo>{{ $collect->cargo->nome }}</cargo>
           <data>{{ $collect->data }}</data>
           <destino>{{ $collect->destino }}</destino>
           <motivo>{{ $collect->motivo }}</motivo>
           <valor>{{ $collect->valor }}</valor>
           <anexos>
           @foreach ($collect->attachments as $attachment)
               <anexo>
                   <nome>{{ $attachment->original_name }}</nome>
                   <path>{{ $attachment->path }}</path>
               </anexo>
           @endforeach
            </anexos>
        </diaria>
    @endforeach
</Diarias>
