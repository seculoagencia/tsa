@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
@endphp

<PublicacoesOficiais>
    @foreach ($collection as $collect)
        <publicacao>
           <mesAno>{{ str_pad($collect->month, 2, "0", STR_PAD_LEFT) }}/{{ $collect->year }}</mesAno>
           <titulo>{{ $collect->name }}</titulo>
           <tipo>{{ $collect->type_document->name }}</tipo>
           <anexos>
           @foreach ($collect->attachments as $attachment)
               <anexo>
                   <nome>{{ $attachment->original_name }}</nome>
                   <path>{{ $attachment->path }}</path>
               </anexo>
           @endforeach
            </anexos>
        </publicacao>
    @endforeach
</PublicacoesOficiais>
