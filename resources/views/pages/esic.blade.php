@extends('layout.public')

@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h4 class="card-title mb-0">Pedidos e-SIC</h4>
        <div class="card-toolbar">
            <a href="{{ route('consultarPedido') }}" target="_blank" class="btn btn-info">Consultar</a>
        </div>
    </div>
    {!! Form::open(['route'=>'salvarPedido', 'method'=>'post']) !!}
    <div class="card-body">
        @csrf
        <div class="row">
            {{-- @if ($errors->any())
            <div class="col-md-12">
                @foreach ($errors->all() as $error)
                <div class="form-group">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
                @endforeach
            </div>
            @endif --}}
            @if (session('message_success'))
                <div class="col-md-12">
                    <div class="form-group">
                        <div class='alert alert-success font-bold'>{!! session('message_success') !!}</div>
                    </div>
                </div>
            @endif
            @if (session('message_error'))
                <div class="col-md-12">
                    <div class="form-group">
                        <div class='alert alert-danger font-bold'>{!! session('message_error') !!}</div>
                    </div>
                </div>
            @endif
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Form::text('name', null, ['class'=> !empty($errors->default->first('name')) ? 'form-control is-invalid' : 'form-control']) !!}
                    @error('name')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', null, ['class'=> !empty($errors->default->first('email')) ? 'form-control is-invalid' : 'form-control']) !!}
                    @error('email')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('tipo_pessoa', 'Tipo de Pessoa') !!}
                    {!! Form::select('tipo_pessoa', ['f'=>'Física','j'=>'Jurídica'], null, [
                        'class'=>!empty($errors->default->first('tipo_pessoa')) ? 'form-control select2 is-invalid' : 'form-control select2',
                        'placeholder'=>'Selecione...'
                    ]) !!}
                    @error('tipo_pessoa')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('cpf', 'CPF') !!}
                    {!! Form::text('cpf', null, ['class'=>!empty($errors->default->first('cpf')) ? 'form-control cpf is-invalid' : 'form-control cpf']) !!}
                    @error('cpf')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('phone', 'Telefone') !!}
                    {!! Form::text('phone', null, ['class'=>!empty($errors->default->first('phone')) ? 'form-control phone is-invalid' : 'form-control phone']) !!}
                    @error('phone')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('priority', 'Prioridade') !!}
                    {!! Form::select('priority', ['3'=>'Baixa','2'=>'Média','1'=>'Alta'], null, [
                        'class'=>!empty($errors->default->first('priority')) ? 'form-control select2 is-invalid' : 'form-control select2',
                        'placeholder'=>'Selecione...'
                    ]) !!}
                    @error('priority')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('type_answer', 'Como deseja receber sua resposta?') !!}
                    {!! Form::select('type_answer', ['Email'=>'Email','Telefone'=>'Telefone','Correios'=>'Correios'], null, [
                        'class'=>!empty($errors->default->first('type_answer')) ? 'form-control select2 is-invalid' : 'form-control select2',
                        'placeholder'=>'Selecione...'
                    ]) !!}
                    @error('type_answer')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('typerequest_id', 'Tipo de Solicitação') !!}
                    {!! Form::select('typerequest_id', ['2'=>'Licitações','3'=>'Reclamações','4'=>'Sugestão'], null, [
                        'class'=>!empty($errors->default->first('typerequest_id')) ? 'form-control select2 is-invalid' : 'form-control select2',
                        'placeholder'=>'Selecione...'
                    ]) !!}
                    @error('typerequest_id')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('location', 'Endereço') !!}
                    {!! Form::text('location', null, ['class'=>!empty($errors->default->first('location')) ? 'form-control is-invalid' : 'form-control']) !!}
                    @error('location')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('subject', 'Assunto') !!}
                    {!! Form::text('subject', null, ['class'=>!empty($errors->default->first('subject')) ? 'form-control is-invalid' : 'form-control']) !!}
                    @error('subject')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
                <div class="form-group">
                    {!! Form::label('message', 'Mensagem') !!}
                    {!! Form::textarea('message', null, [
                        'class'=>!empty($errors->default->first('message')) ? 'form-control is-invalid' : 'form-control',
                        'rows'=>'6','cols'=>"80"
                    ]) !!}
                    @error('message')<div class='invalid-feedback'>{{$message}}</div>@enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Anexo</label> <br>
                    <input type="file" name="anexo">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 text-right">
                        {!! captcha_img('flat') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('captcha', null, ['class'=>!empty($errors->default->first('message')) ? 'form-control is-invalid' : 'form-control']) !!}
                        @error('captcha')<div class='invalid-feedback'>{{$message}}</div>@enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary btn-fill">Gerar Pedido</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
