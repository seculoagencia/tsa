@extends('layout.public')

@section('content')
@include('adm.receitas.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            <div class="btn-group btn-group-lg mr-5" role="group" aria-label="Large button group">
                <a href="{{ route(Route::currentRouteName(), ['export'=>'xml', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">XML</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'csv', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">CSV</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'json', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">JSON</a>
            </div>
            <a href='javascript:;' data-toggle='modal' data-target='#filter-receitas' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
        @endif
        {{--begin::Table--}}
        {{-- {!! Form::hidden('route', route('receitas'), ['id'=>'route']) !!} --}}
        @switch($appSetting->prioridade_receita)
            @case('manual')
                @include('adm.receitas.includes._table')
                @break
            @case('sicap')
                @php($sicapReceitas = $receitas)
                @include('adm.sicap-receitas.includes._table')
                @break
            @default
                @include('adm.receitas.includes._table')
        @endswitch
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $receitas->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection
