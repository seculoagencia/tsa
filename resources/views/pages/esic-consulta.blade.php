@extends('layout.public')

@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h4 class="card-title mb-0">Consultar Pedidos e-SIC</h4>
        <div class="card-toolbar">
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            @if (session('message_error'))
                <div class="col-md-12">
                    <div class="form-group">
                        <div class='alert alert-danger font-bold'>{!! session('message_error') !!}</div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                {!! Form::open(['route'=>'consultarPedido', 'method'=>'get']) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::text('email', request()->has('email') ? request('email') : null, ['class'=> !empty($errors->default->first('email')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('email')<div class='invalid-feedback'>{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('serial', 'Nº de Protocolo') !!}
                            {!! Form::text('serial', request()->has('serial') ? request('serial') : null, ['class'=> !empty($errors->default->first('serial')) ? 'form-control is-invalid' : 'form-control']) !!}
                            @error('serial')<div class='invalid-feedback'>{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label> <br>
                        <button type="submit" class="btn btn-info">Consultar</button>
                    </div>
                </div>
                {!! Form::close() !!}
                <hr>
                @if ($pedido)
                <div class="row">
                    <div class="col-md-4">
                      <p><strong>Nome</strong><br> <span>{{ $pedido->name }}</span></p>
                      <p><strong>Email</strong><br> <span>{{ $pedido->email }}</span></p>
                      <p><strong>CPF/CNPJ</strong><br> <span>{{ $pedido->cpf }}</span></p>
                      <p><strong>Telefone</strong><br> <span>{{ $pedido->phone }}</span></p>
                    </div>
                    <div class="col-md-4">
                      <p><strong>Prioridade</strong><br> <span>{{ $pedido->priority_extenso }}</span></p>
                      <p><strong>Endereço</strong><br> <span>{{ $pedido->location }}</span></p>
                      <p><strong>Como deseja receber sua resposta?</strong><br> <span>{{ $pedido->type_answer }}</span></p>
                      <p><strong>Tipo de Solicitação</strong><br> <span>{{ $pedido->tipoPedido->name }}</span></p>
                    </div>
                    <div class="col-md-4">
                      <p><strong>Assunto</strong><br> <span>{{ $pedido->subject }}</span></p>
                      <p><strong>Nº de Protocolo</strong><br> <span>{{ $pedido->serial }}</span></p>
                      <p><strong>Data de Registro</strong><br> <span>{{ $pedido->created_at }}</span></p>
                      <p><strong>Status</strong><br> <span class="text-success">{{ $pedido->status_extenso }}</span></p>
                    </div>
    
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="12">
                        <p><strong>Mensagem</strong></p>
                        <p>{{ $pedido->message }}</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="12">
                            <p><strong>Anexo</strong></p>
                            <p>
                                @if ($pedido->anexo)
                                <a href="{{ asset($pedido->anexo) }}" target="_blank">Visualizar</a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12" id="answer_box">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title mb-10 mt-5">Respostas</h3>
                            </div>
                            {!! Form::open(['route'=>['responderPedido'], 'method'=>'post']) !!}
                            <div class="panel-body">
                                @if (session('message_resp_success'))
                                    <div class="form-group mt-5">
                                        <div class='alert alert-success font-bold'>{!! session('message_resp_success') !!}</div>
                                    </div>
                                @endif
                                @if (session('message_resp_error'))
                                    <div class="form-group">
                                        <div class='alert alert-danger font-bold'>{!! session('message_resp_error') !!}</div>
                                    </div>
                                @endif
                                <div class="messages">
                                    @if (count($pedido->respostas) > 0)
                                        @foreach ($pedido->respostas as $resposta)
                                            @if ($resposta->user_id)
                                            <div class="d-flex flex-column mb-5 align-items-end">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <span class="text-muted font-size-sm mr-5">{{ $resposta->created_at }}</span>
                                                        <span class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $appSettingName }}</span>
                                                    </div>
                                                </div>
                                                <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-600px">
                                                    {{ $resposta->description }}
                                                </div>
                                            </div>
                                            @else
                                            <div class="d-flex flex-column mb-5 align-items-start">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <span class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 mr-5">
                                                            {{ $pedido->name }}
                                                        </span>
                                                        <span class="text-muted font-size-sm">{{ $resposta->created_at }}</span>
                                                    </div>
                                                </div>
                                                <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-600px">
                                                    {{ $resposta->description }}
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="form-group">
                                    @csrf
                                    <label>Mensagem</label>
                                    <textarea name="description" rows="8" class="form-control" required></textarea>
                                    {!! Form::hidden('pedido_id', $pedido->id) !!}
                                    {!! Form::hidden('email', $pedido->email) !!}
                                    {!! Form::hidden('serial', $pedido->serial) !!}
                                </div>
                                <div class="form-group text-right">
                                    {{-- <button type="submit" class="btn btn-fill btn-success" name="fechar_pedido">Fechar Pedido (Resolvido)</button> --}}
                                    <button type="submit" class="btn btn-primary btn-fill">Responder</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    
</div>
@endsection
