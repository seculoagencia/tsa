@extends('layout.public')

@section('content')
@include('adm.empenhos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }} - Órgãos e Horários de Funcionamento</span>
        </h3>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @if (count($estruturas) > 0)
        <div class="row">
            @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
            @endif
            <div class="col-3">
                <ul class="nav flex-column nav-pills">
                    @foreach ($estruturas as $key => $estrutura)
                    <li class="nav-item" role="presentation">
                        <a class="nav-link {{ ($key == 0) ? 'active':'' }}" id="tab{{$key}}" data-toggle="tab" href="#content{{$key}}" role="tab" aria-controls="content{{$key}}" aria-selected="true">
                            {{ $estrutura->order }}. {{ $estrutura->title }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-9">
                <div class="tab-content" id="myTabContent">
                    @foreach ($estruturas as $key => $estrutura)
                    <div class="tab-pane fade show {{ ($key == 0) ? 'active':'' }}" id="content{{$key}}" role="tabpanel" aria-labelledby="content{{$key}}-tab">
                        <div class="col-md-12 mb-10">
                            {!! $estrutura->description !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
            {{-- <ul class="nav flex-column nav-tabs" id="myTab" role="tablist">
                @foreach ($estruturas as $key => $estrutura)
                <li class="nav-item" role="presentation">
                <a class="nav-link {{ ($key == 0) ? 'active':'' }}" id="tab{{$key}}" data-toggle="tab" href="#content{{$key}}" role="tab" aria-controls="content{{$key}}" aria-selected="true">{{ $estrutura->title }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach ($estruturas as $key => $estrutura)
                <div class="tab-pane fade show {{ ($key == 0) ? 'active':'' }}" id="content{{$key}}" role="tabpanel" aria-labelledby="content{{$key}}-tab">
                    <div class="col-md-12">
                        {!! $estrutura->description !!}
                    </div>
                </div>
                @endforeach
            </div> --}}
        @endif
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
</div>
@endsection
