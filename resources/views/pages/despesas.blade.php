@extends('layout.public')

@section('content')
@include('adm.empenhos.includes._filter')
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            <div class="btn-group btn-group-lg mr-5" role="group" aria-label="Large button group">
                <a href="{{ route(Route::currentRouteName(), ['export'=>'xml', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">XML</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'csv', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">CSV</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'json', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">JSON</a>
            </div>
            <a href='javascript:;' data-toggle='modal' data-target='#filter-empenhos' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        {{--begin::Table--}}
        @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
        @endif
        {{-- {!! Form::hidden('route', route('despesas'), ['id'=>'route']) !!} --}}
        @switch($appSetting->prioridade)
            @case('manual')
                @include('adm.empenhos.includes._table')
                @break
            @case('sicap')
                @php($sicapEmpenho = $empenhos)
                @include('adm.sicap-empenho.includes._table')
                @break
            @case('siap')
                @php($siapEmpenho = $empenhos)
                @include('adm.siap-empenho.includes._table')
                @break
            @case('sagres')
                @php($sagresEmpenhos = $empenhos)
                @include('adm.sagres-empenhos.includes._table')
                @break
            @default
                @include('adm.empenhos.includes._table')
        @endswitch
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right'>
        {{ $empenhos->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}
    </div>
</div>
@endsection
