@extends('layout.public')

@section('content')
@if ($covidResumo)
<section id="inicio" style="background-image: url({{asset('img/bg1.jpg')}}); background-position: 50% 0px; height:300px">
    <div class="container pt-10">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
          <div class="funfact text-center">
            <i class="pe-7s-smile mt-5 text-white"></i>
            <h2 class=" text-white font-weight-900 mt-0 mb-0" style="line-height:60px;font-size:60px">{{number_format($covidResumo->casos_confirmados,0,'','.')}}</h2>
            <h5 class="mt-5 pt-0 text-white text-uppercase font-weight-900">CASOS<br>CONFIRMADOS</h5>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
          <div class="funfact text-center">
            <i class="pe-7s-rocket mt-5 text-white"></i>
            <h2 class="  text-white font-60 font-weight-900 mt-0 mb-0" style="line-height:60px;font-size:60px">{{number_format($covidResumo->casos_suspeitos,0,'','.')}}</h2>
            <h5 class="mt-5 pt-0 text-white text-uppercase font-weight-900">CASOS<br>SUSPEITOS</h5>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
          <div class="funfact text-center">
            <i class="pe-7s-add-user mt-5 text-white"></i>
            <h2 class=" text-white font-60 font-weight-900 mt-0 mb-0" style="line-height:60px;font-size:60px">{{number_format($covidResumo->pessoas_recuperadas,0,'','.')}}</h2>
            <h5 class="mt-5 pt-0 text-white text-uppercase font-weight-900">PESSOAS<br>RECUPERADAS</h5>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
          <div class="funfact text-center">
            <i class="pe-7s-global mt-5 text-white"></i>
            <h2 class=" text-white font-60 font-weight-900 mt-0 mb-0" style="line-height:60px;font-size:60px">{{number_format($covidResumo->obitos,0,'','.')}}</h2>
            <h5 class="mt-5 pt-0 text-white text-uppercase font-weight-900">ÓBITOS</h5>
          </div>
        </div>
        
        <div class="col-md-12 text-center font-14 text-white mt-10">
            Atualizado em {{date('d/m/Y H:i:s', strtotime($covidResumo->updated_at))}}
        </div>
        <div class="col-md-12 text-center font-14 text-white mt-5">
            <a href="" target="_blank" class="btn btn-dark btn-lg"><i class="fas fa-download"></i> BOLETIM EPIDEMIOLÓGICO</a>
        </div>
         
      </div>
    </div>
</section>

<div class="row">
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 my-7">
        <a href="{{ route('covid19-despesas') }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG("media/svg/icons/Shopping/Dollar.svg", "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        Despesas
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 my-7">
        <a href="{{ route('covid19-receitas') }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG("media/svg/icons/Shopping/Dollar.svg", "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        Receitas
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 my-7">
        <a href="{{ route('covid19-licitacoes') }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG("media/svg/icons/Shopping/Cart1.svg", "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        Licitações
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 my-7">
        <a href="{{ route('covid19-contratos') }}" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3">
        <div class="card card-custom mb-8 mb-lg-0">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center p-5">
                    <div class="text-center mb-3">
                        <span class="svg-icon svg-icon-4x">
                            {{ Metronic::getSVG("media/svg/icons/Files/Selected-file.svg", "svg-icon-success") }}
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        Contratos
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
</div>
@endif
@endsection