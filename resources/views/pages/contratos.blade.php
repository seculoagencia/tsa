@extends('layout.public')

@section('content')
{{-- @include('adm.contratos.includes._filter') --}}
<div class='card card-custom gutter-b'>
    {{--begin::Header--}}
    <div class='card-header border-0 py-5'>
        <h3 class='card-title align-items-start flex-column'>
            <span class='card-label font-weight-bolder text-dark'>{{ $page_title }}</span>
        </h3>
        <div class='card-toolbar'>
            <div class="btn-group btn-group-lg mr-5" role="group" aria-label="Large button group">
                <a href="{{ route(Route::currentRouteName(), ['export'=>'xml', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">XML</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'csv', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">CSV</a>
                <a href="{{ route(Route::currentRouteName(), ['export'=>'json', 'datetime'=>date('YmdHis')]) }}" target="_blank" class="btn btn-outline-secondary">JSON</a>
            </div>
            <a href='javascript:;' data-toggle='modal' data-target='#filter-contratos' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>
        </div>
    </div>
    {{--end::Header--}}
    {{--begin::Body--}}
    <div class='card-body py-0'>
        @if ($appSetting->view_last_update)
            <div class="col-12">
                <p class="text-center">
                    <small>Ultima Atualização: <strong>{{ $last_update ? date('d/m/Y H:i:s', strtotime($last_update->created_at)) : '' }}</strong></small>
                </p>
            </div>
        @endif
        {{--begin::Table--}}

        @include('adm.contratos.includes._table')
        {{--end::Table--}}
    </div>
    {{--end::Body--}}
    <div class='card-footer text-right border d-flex flex-row  align-items-center justify-content-end'>
        {{ $contratos->appends(request()->except(['page']))->links('vendor.pagination.metronic')}}
    </div>
</div>

<div class='modal fade' id='filter-contratos' tabindex='-1' role='dialog' aria-labelledby='filter-contratos-title' aria-hidden='true'>
    <div class='modal-dialog modal-lg' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h5 class='modal-title'>Filtrar contratos</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            {!! Form::open(['method'=>'GET']) !!}
            <div class='modal-body'>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('orgao', 'Órgão') !!}
                                {!! Form::text('orgao', request('orgao'), [
                                    'class'=>(!empty($errors->default->first('orgao'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-8'>
                            <div class='form-group'>
                                {!! Form::label('modalidade', 'Modalidade') !!}
                                {!! Form::text('modalidade', request('modalidade'), [
                                    'class'=>(!empty($errors->default->first('modalidade'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('data_hora', 'Data/Hora') !!}
                                <input type="datetime-local" name="data_hora" value="{{ request('data_hora') }}" class="{{ (!empty($errors->default->first('data_hora'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid' }}">
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('valor', 'Preço') !!}
                                {!! Form::text('valor', request('valor'), [
                                    'class'=>(!empty($errors->default->first('valor'))) ? 'form-control decimal form-control-solid is-invalid':'form-control decimal form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class='form-group'>
                                {!! Form::label('status', 'Status') !!}
                                {!! Form::select('status', Config::get('constants.licitacoes.status'), request('status'), [
                                    'class'=>(!empty($errors->default->first('status'))) ? 'form-control select2 form-control-solid is-invalid':'form-control select2 form-control-solid',
                                    'placeholder' => 'Selecione...',
                                    'style' => 'width:100%'
                                ]) !!}
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('vencedor', 'Vencedor') !!}
                                {!! Form::text('vencedor', request('vencedor'), [
                                    'class'=>(!empty($errors->default->first('vencedor'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div><div class='col-md-6'>
                            <div class='form-group'>
                                {!! Form::label('objeto', 'Objeto') !!}
                                {!! Form::text('objeto', request('objeto'), [
                                    'class'=>(!empty($errors->default->first('objeto'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
                <a href='{{ route('contratos.index') }}' class='btn btn-info'>Limpar</a>
                <button type='submit' class='btn btn-primary'>Filtrar</button>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
