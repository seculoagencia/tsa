<?php

namespace App\Policies;

use App\Models\SicapServidor;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SicapServidorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('sicap-servidores-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapServidor  $sicapServidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, SicapServidor $sicapServidor)
    {
        return $user->hasPermissionTo('sicap-servidores-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('sicap-servidores-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapServidor  $sicapServidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, SicapServidor $sicapServidor)
    {
        return $user->hasPermissionTo('sicap-servidores-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapServidor  $sicapServidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, SicapServidor $sicapServidor)
    {
        return $user->hasPermissionTo('sicap-servidores-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapServidor  $sicapServidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, SicapServidor $sicapServidor)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapServidor  $sicapServidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, SicapServidor $sicapServidor)
    {
        return false;
    }
}
