<?php

namespace App\Policies;

use App\Models\Servidor;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServidorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('servidores-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Servidor  $servidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Servidor $servidor)
    {
        return $user->hasPermissionTo('servidores-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('servidores-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Servidor  $servidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Servidor $servidor)
    {
        return $user->hasPermissionTo('servidores-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Servidor  $servidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Servidor $servidor)
    {
        return $user->hasPermissionTo('servidores-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Servidor  $servidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Servidor $servidor)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Servidor  $servidor
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Servidor $servidor)
    {
        return false;
    }
}
