<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Verba;
use Illuminate\Auth\Access\HandlesAuthorization;

class VerbaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('verbas-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Verba  $verba
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Verba $verba)
    {
        return $user->hasPermissionTo('verbas-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('verbas-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Verba  $verba
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Verba $verba)
    {
        return $user->hasPermissionTo('verbas-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Verba  $verba
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Verba $verba)
    {
        return $user->hasPermissionTo('verbas-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Verba  $verba
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Verba $verba)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Verba  $verba
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Verba $verba)
    {
        return false;
    }
}
