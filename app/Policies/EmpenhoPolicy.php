<?php

namespace App\Policies;

use App\Models\Empenho;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmpenhoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('empenhos-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Empenho  $empenho
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Empenho $empenho)
    {
        return $user->hasPermissionTo('empenhos-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('empenhos-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Empenho  $empenho
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Empenho $empenho)
    {
        return $user->hasPermissionTo('empenhos-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Empenho  $empenho
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Empenho $empenho)
    {
        return $user->hasPermissionTo('empenhos-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Empenho  $empenho
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Empenho $empenho)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Empenho  $empenho
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Empenho $empenho)
    {
        return false;
    }
}
