<?php

namespace App\Policies;

use App\Models\Receita;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReceitaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('receitas-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Receita $receita)
    {
        return $user->hasPermissionTo('receitas-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('receitas-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Receita $receita)
    {
        return $user->hasPermissionTo('receitas-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Receita $receita)
    {
        return $user->hasPermissionTo('receitas-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Receita $receita)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Receita $receita)
    {
        return false;
    }
}
