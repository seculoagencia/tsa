<?php

namespace App\Policies;

use App\Models\SicapReceita;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SicapReceitaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('sicap-receitas-list');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapReceita  $sicapReceita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, SicapReceita $sicapReceita)
    {
        return $user->hasPermissionTo('sicap-receitas-list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('sicap-receitas-create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapReceita  $sicapReceita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, SicapReceita $sicapReceita)
    {
        return $user->hasPermissionTo('sicap-receitas-edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapReceita  $sicapReceita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, SicapReceita $sicapReceita)
    {
        return $user->hasPermissionTo('sicap-receitas-delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapReceita  $sicapReceita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, SicapReceita $sicapReceita)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\SicapReceita  $sicapReceita
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, SicapReceita $sicapReceita)
    {
        return false;
    }
}
