<?php

namespace App\Imports;

use App\Models\Servidor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ServidorImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        try {
            $admissao = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($row[1]));
        } catch (\Throwable $th) {
            $admissao = date('Y-m-d', strtotime(str_replace('/','-', trim($row[1]))));
        }
        return new Servidor([
            'nome' => $row[0],
            'admissao' => $admissao,
            'cargo' => $row[2],
            'valor_bruto' => $row[3],
            'descontos' => $row[4],
            'valor_liquido' => $row[5],
            'referencia' => $row[7].'-'.$row[6]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
