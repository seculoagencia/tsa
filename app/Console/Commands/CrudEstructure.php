<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;
use Storage;

class CrudEstructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gsa:crud {name} {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creare a new crud files. (php artisan make:crud CrudSample crud_samples crudSamples)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $name = $this->argument('name');
            $table = $this->argument('table');
            $view = $table;
            $varPlural = $table;
            $pageTitle = ucfirst($table);
            $checkCompost = explode('_', $table);
            if (count($checkCompost) > 1) {
                $view = str_replace('_','-',$table);
                $pageTitle = ucwords(str_replace('_',' ',$table));
                $varPlural = "";
                for ($i=0; $i < count($checkCompost); $i++) { 
                    $varPlural .= ucfirst($checkCompost[$i]);
                }
            }

            $this->_createMigrationFile($table);
            $this->_createModelFile($name, $table);
            $this->_createControllerFile($name, $table, $view, $varPlural, $pageTitle);
            $this->_createRequestFile($name);
            $this->_createViewIndexFile($view, $varPlural);
            $this->_createViewCreateFile($view);
            $this->_createViewEditFile($name, $view);
            $this->_createViewFormFile($view);
            $this->_createViewTableFile($name, $view, $varPlural);
            $this->_createViewFilterFile($view, $varPlural);
            $this->info('::::::::::::::::');
            $this->info('GSA Crud Finish');
            $this->info('::::::::::::::::');
            $this->info("web.php: Route::resource('".$view."', ".$name."Controller::class);");

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Create migration file
     * 
     * @param mixed $table 
     * @return void 
     */
    private function _createMigrationFile($table)
    {
        Artisan::call('make:migration', [
            'name'=>'create_'.$table.'_table',
            '--create' => $table
        ]);

        $this->info('Migration created successfully.');
    }

    /**
     * Create model file
     * 
     * @param mixed $name, $table
     * @return void 
     */
    private function _createModelFile($name, $table)
    {
        $file = "";
        $file .= "<?php\n";
        $file .= "\n";
        $file .= "namespace App\\Models;\n";
        $file .= "\n";
        $file .= "use Illuminate\\Database\\Eloquent\\Model;\n";
        $file .= "\n";
        $file .= "class {$name} extends Model\n";
        $file .= "{\n";
        $file .= "    /** \n";
        $file .= "     * The attributes that are mass assignable\n";
        $file .= "     * \n";
        $file .= "     * @var array \n";
        $file .= "     */\n";
        $file .= "     protected ".'$table'." = '{$table}';\n";
        $file .= "     protected ".'$fillable'." = [];\n";
        $file .= "\n";
        $file .= "}";

        $fp = \fopen("{$this->laravel->basePath()}/app/Models/{$name}.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('Model created successfully.');
    }

    /**
     * Create controller file
     * 
     * @param mixed $name, $table, $view, $varPlural, $pageTitle
     * @return void 
     */
    private function _createControllerFile($name, $table, $view, $varPlural, $pageTitle)
    {
        $file = "";
        $file .= "<?php\n";
        $file .= "\n";
        $file .= "namespace App\\Http\\Controllers;\n";
        $file .= "\n";
        $file .= "use App\\Models\\{$name};\n";
        $file .= "use App\\Http\\Requests\\{$name}Request;\n";
        $file .= "\n";
        $file .= "class {$name}Controller extends Controller\n";
        $file .= "{\n";
        $file .= "    public function __construct() {\n";
        $file .= "        ".'$page_title'." = '".$pageTitle."';\n";
        $file .= "        view()->share('page_title', ".'$page_title'.");\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Display a listing of the resource.\n";
        $file .= "     *\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function index()\n";
        $file .= "    {\n";
        $file .= "        ".'$page_description'." = 'Listagem';\n";
        $file .= "        ".'$offset'." = request()->has('offset') ? request('offset') : 10;\n";
        $file .= "        $".lcfirst($varPlural)." = {$name}::where(function (".'$q'.") {\n";
        $file .= "            if (request()->has('name') && request('name') != '') {\n";
        $file .= "                ".'$q'."->where('name','like','%'.request('name').'%');\n";
        $file .= "            }\n";
        $file .= "        })->orderBy('id','desc')->paginate(".'$offset'.");\n";
        $file .= "\n";
        $file .= "        return view('adm.".$view.".index', compact('page_description', 'offset', '".lcfirst($varPlural)."'));\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Show the form for creating a new resource.\n";
        $file .= "     *\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function create()\n";
        $file .= "    {\n";
        $file .= "        ".'$page_description'." = 'Cadastrar';\n";
        $file .= "        return view('adm.".$view.".create', compact('page_description'));\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Store a newly created resource in storage.\n";
        $file .= "     *\n";
        $file .= "     * @param  \\App\\Http\\Requests\\{$name}Request  ".'$request'."\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function store({$name}Request ".'$request'.")\n";
        $file .= "    {\n";
        $file .= "        try {\n";
        $file .= "            $".lcfirst($name)." = {$name}::create(".'$request'."->except('_token'));\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_success', 'Cadastro feito com sucesso!');\n";
        $file .= "        } catch (\Throwable ".'$th'.") {\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_error', ".'$th'."->getMessage());\n";
        $file .= "        }\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Display the specified resource.\n";
        $file .= "     *\n";
        $file .= "     * @param  \\App\\Models\\{$name} $".lcfirst($name)."\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function show({$name} $".lcfirst($name).")\n";
        $file .= "    {\n";
        $file .= "        ".'$page_description'." = 'Detalhes';\n";
        $file .= "        return view('adm.".$view.".show', compact('page_description', '".lcfirst($name)."'));\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Show the form for editing the specified resource.\n";
        $file .= "     *\n";
        $file .= "     * @param  \\App\\Models\\{$name} $".lcfirst($name)."\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function edit({$name} $".lcfirst($name).")\n";
        $file .= "    {\n";
        $file .= "        ".'$page_description'." = 'Edição';\n";
        $file .= "        return view('adm.".$view.".edit', compact('page_description', '".lcfirst($name)."'));\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Update the specified resource in storage.\n";
        $file .= "     *\n";
        $file .= "     * @param  \\App\\Http\\Requests\\{$name}Request  ".'$request'."\n";
        $file .= "     * @param  \\App\\Models\\{$name} $".lcfirst($name)."\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function update({$name}Request ".'$request'.", {$name} $".lcfirst($name).")\n";
        $file .= "    {\n";
        $file .= "        try {\n";
        $file .= "            $".lcfirst($name)."->update(".'$request'."->except('_token'));\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_success', 'Registro editado com sucesso!');\n";
        $file .= "        } catch (\Throwable ".'$th'.") {\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_error', ".'$th'."->getMessage());\n";
        $file .= "        }\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Remove the specified resource from storage.\n";
        $file .= "     *\n";
        $file .= "     * @param  \\App\\Models\\{$name} $".lcfirst($name)."\n";
        $file .= "     * @return \\Illuminate\\Http\\Response\n";
        $file .= "    */\n";
        $file .= "    public function destroy({$name} $".lcfirst($name).")\n";
        $file .= "    {\n";
        $file .= "        try {\n";
        $file .= "            $".lcfirst($name)."->delete();\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_success', 'Registro removido com sucesso!');\n";
        $file .= "        } catch (\Throwable ".'$th'.") {\n";
        $file .= "            return redirect()->route('".$view.".index')->with('message_error', ".'$th'."->getMessage());\n";
        $file .= "        }\n";
        $file .= "    }\n";
        $file .= "}\n";

        $fp = \fopen("{$this->laravel->basePath()}/app/Http/Controllers/{$name}Controller.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('Controller created successfully.');
    }

    /**
     * Create request file
     * 
     * @param mixed $name 
     * @return void 
     */
    public function _createRequestFile($name)
    {
        $file = "";
        $file .= "<?php\n";
        $file .= "\n";
        $file .= "namespace App\\Http\\Requests;\n";
        $file .= "\n";
        $file .= "use Illuminate\\Foundation\\Http\\FormRequest;\n";
        $file .= "\n";
        $file .= "class {$name}Request extends FormRequest\n";
        $file .= "{\n";
        $file .= "    /**\n";
        $file .= "     * Determine if the user is authorized to make this request.\n";
        $file .= "     *\n";
        $file .= "     * @return bool\n";
        $file .= "     */\n";
        $file .= "    public function authorize()\n";
        $file .= "    {\n";
        $file .= "        return true;\n";
        $file .= "    }\n";
        $file .= "\n";
        $file .= "    /**\n";
        $file .= "     * Get the validation rules that apply to the request.\n";
        $file .= "     *\n";
        $file .= "     * @return array\n";
        $file .= "     */\n";
        $file .= "    public function rules()\n";
        $file .= "    {\n";
        $file .= "        return [\n";
        $file .= "            //\n";
        $file .= "        ];\n";
        $file .= "    }\n";
        $file .= "}\n";
        
        $fp = \fopen("{$this->laravel->basePath()}/app/Http/Requests/{$name}Request.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);
    }

    /**
     * Create view index file
     * 
     * @param mixed $view
     * @return void 
     */
    public function _createViewIndexFile($view, $varPlural)
    {
        mkdir("{$this->laravel->basePath()}/resources/views/adm/{$view}");

        $file = "";
        $file .= "@extends('layout.default')\n";
        $file .= "\n";
        $file .= "@section('page_toolbar')\n";
        $file .= "<a href='{{ route('".$view.".create') }}' class='btn btn-info font-weight-bolder font-size-sm mr-2'>CADASTRAR</a>\n";
        $file .= "<a href='javascript:;' data-toggle='modal' data-target='#filter-".$view."' class='btn btn-primary font-weight-bolder font-size-sm'>FILTRAR</a>\n";
        $file .= "@endsection\n";
        $file .= "\n";
        $file .= "@section('content')\n";
        $file .= "@include('adm.".$view.".includes._filter')\n";
        $file .= "\n";
        $file .= "@if (session('message_success'))\n";
        $file .= "    <div class='alert alert-success font-bold'>{{ session('message_success') }}</div>\n";
        $file .= "@endif\n";
        $file .= "@if (session('message_error'))\n";
        $file .= "    <div class='alert alert-danger font-bold'>{{ session('message_error') }}</div>\n";
        $file .= "@endif\n";
        $file .= "<div class='card card-custom gutter-b'>\n";
        $file .= "    {{--begin::Header--}}\n";
        $file .= "    <div class='card-header border-0 py-5'>\n";
        $file .= "        <h3 class='card-title align-items-start flex-column'>\n";
        $file .= "            <span class='card-label font-weight-bolder text-dark'>{{ ".'$page_description'." }} de {{ ".'$page_title'." }}</span>\n";
        $file .= "        </h3>\n";
        $file .= "        <div class='card-toolbar'>\n";
        $file .= "            \n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Header--}}\n";
        $file .= "    {{--begin::Body--}}\n";
        $file .= "    <div class='card-body py-0'>\n";
        $file .= "        {{--begin::Table--}}\n";
        $file .= "        @include('adm.".$view.".includes._table')\n";
        $file .= "        {{--end::Table--}}\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Body--}}\n";
        $file .= "    <div class='card-footer text-right'>\n";
        $file .= "        {{ ".'$'.lcfirst($varPlural)."->appends(request()->except(['page']))->links('vendor.pagination.metronic') }}\n";
        $file .= "    </div>\n";
        $file .= "</div>\n";
        $file .= "@endsection\n";

        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/index.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Index created successfully.');
    }

    /**
     * Create view create file
     * 
     * @param mixed $view
     * @return void 
     */
    public function _createViewCreateFile($view)
    {
        $file = "";
        $file .= "@extends('layout.default')\n";
        $file .= "\n";
        $file .= "@section('page_toolbar')\n";
        $file .= "<a href={{ route('".$view.".index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>\n";
        $file .= "@endsection\n";
        $file .= "\n";
        $file .= "@section('content')\n";
        $file .= "@include('adm.".$view.".includes._filter')\n";
        $file .= "<div class='card card-custom gutter-b'>\n";
        $file .= "    {{--begin::Header--}}\n";
        $file .= "    <div class='card-header border-0 py-5'>\n";
        $file .= "        <h3 class='card-title align-items-start flex-column'>\n";
        $file .= "            <span class='card-label font-weight-bolder text-dark'>{{ $"."page_description }} {{ $"."page_title }}</span>\n";
        $file .= "        </h3>\n";
        $file .= "        <div class='card-toolbar'>\n";
        $file .= "            \n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Header--}}\n";
        $file .= "    {!! Form::open(['route'=>'".$view.".store']) !!}\n";
        $file .= "    {{--begin::Body--}}\n";
        $file .= "    <div class='card-body py-0'>\n";
        $file .= "        {{--begin::Form--}}\n";
        $file .= "        @include('adm.".$view.".includes._form')\n";
        $file .= "        {{--end::Form--}}\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Body--}}\n";
        $file .= "    <div class='card-footer text-right'>\n";
        $file .= "        <button type='submit' class='btn btn-primary'>Cadastrar</button>\n";
        $file .= "    </div>\n";
        $file .= "    {!! Form::close() !!}\n";
        $file .= "</div>\n";
        $file .= "@endsection\n";

        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/create.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Create created successfully.');
    }

    /**
     * Create view edit file
     * 
     * @param mixed $name, $view
     * @return void 
     */
    public function _createViewEditFile($name, $view)
    {
        $file = "";

        $file .= "@extends('layout.default')\n";
        $file .= "\n";
        $file .= "@section('page_toolbar')\n";
        $file .= "<a href={{ route('".$view.".index') }} class='btn btn-info font-weight-bolder font-size-sm mr-2'>LISTAGEM</a>\n";
        $file .= "@endsection\n";
        $file .= "\n";
        $file .= "@section('content')\n";
        $file .= "@include('adm.".$view.".includes._filter')\n";
        $file .= "<div class='card card-custom gutter-b'>\n";
        $file .= "    {{--begin::Header--}}\n";
        $file .= "    <div class='card-header border-0 py-5'>\n";
        $file .= "        <h3 class='card-title align-items-start flex-column'>\n";
        $file .= "            <span class='card-label font-weight-bolder text-dark'>\n";
        $file .= "            {{ ".'$page_description'." }} de {{ ".'$page_title'." }}: #{{".'$'."".lcfirst($name)."->id}}\n";
        $file .= "            </span>\n";
        $file .= "        </h3>\n";
        $file .= "        <div class='card-toolbar'>\n";
        $file .= "            \n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Header--}}\n";
        $file .= "    {!! Form::model($".lcfirst($name).", ['route'=>['".$view.".update', $".lcfirst($name)."->id], 'method'=>'PUT']) !!}\n";
        $file .= "    {{--begin::Body--}}\n";
        $file .= "    <div class='card-body py-0'>\n";
        $file .= "        {{--begin::Form--}}\n";
        $file .= "        @include('adm.".$view.".includes._form')\n";
        $file .= "        {{--end::Form--}}\n";
        $file .= "    </div>\n";
        $file .= "    {{--end::Body--}}\n";
        $file .= "    <div class='card-footer text-right'>\n";
        $file .= "        <button type='submit' class='btn btn-success'>Salvar Alterações</button>\n";
        $file .= "    </div>\n";
        $file .= "    {!! Form::close() !!}\n";
        $file .= "</div>\n";
        $file .= "@endsection\n";


        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/edit.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Edit created successfully.');
    }

    /**
     * Create view form file
     * 
     * @param mixed $view
     * @return void 
     */
    public function _createViewFormFile($view)
    {
        mkdir("{$this->laravel->basePath()}/resources/views/adm/{$view}/includes");
        $file = "";

        $file .= "<div class='row'>\n";
        $file .= "    <div class='col-md-4'>\n";
        $file .= "        <div class='form-group'>\n";
        $file .= "            {!! Form::label('name', 'Nome') !!}\n";
        $file .= "            {!! Form::text('name', null, [\n";
        $file .= "                'class'=>(!empty(".'$errors'."->default->first('name'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'\n";
        $file .= "            ]) !!}\n";
        $file .= "            <div class='invalid-feedback'>@error('name') {{".'$message'."}} @enderror</div>\n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "    <div class='col-md-8'>\n";
        $file .= "        <div class='form-group'>\n";
        $file .= "            {!! Form::label('description', 'Descrição') !!}\n";
        $file .= "            {!! Form::text('description', null, [\n";
        $file .= "                'class'=>(!empty(".'$errors'."->default->first('description'))) ? 'form-control form-control-solid is-invalid':'form-control form-control-solid'\n";
        $file .= "            ]) !!}\n";
        $file .= "            <div class='invalid-feedback'>@error('description') {{".'$message'."}} @enderror</div>\n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "</div>\n";

        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/includes/_form.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Form created successfully.');
    }

    /**
     * Create view table file
     * 
     * @param mixed $name, $view, $varPlural
     * @return void 
     */
    public function _createViewTableFile($name, $view, $varPlural)
    {
        $file = "";

        $file .= "<div class='table-responsive'>\n";
        $file .= "    <table class='table table-head-custom table-vertical-center' id='kt_advance_table_widget_4'>\n";
        $file .= "        <thead>\n";
        $file .= "            <tr class='text-left'>\n";
        $file .= "                <th class='pl-0'>#ID</th>\n";
        $file .= "                <th>Name</th>\n";
        $file .= "                <th>Descrição</th>\n";
        $file .= "                <th class='pr-0 text-right' style='min-width: 160px'>Ações</th>\n";
        $file .= "            </tr>\n";
        $file .= "        </thead>\n";
        $file .= "        <tbody>\n";
        $file .= "            @forelse ($".lcfirst($varPlural)." as $".lcfirst($name).")\n";
        $file .= "            <tr>\n";
        $file .= "                <td class='pl-0'>\n";
        $file .= "                    <span class='text-dark-75 d-block font-size-lg'>{{ $".lcfirst($name)."->id }}</span>\n";
        $file .= "                </td>\n";
        $file .= "                <td>\n";
        $file .= "                    <a href='{{ route('".$view.".edit', $".lcfirst($name)."->id) }}' class='text-dark-75 font-weight-bolder text-hover-primary font-size-lg'>{{ $".lcfirst($name)."->name }}</a>\n";
        $file .= "                </td>\n";
        $file .= "                <td>\n";
        $file .= "                    <span class='text-dark-75 d-block font-size-lg'>{{ $".lcfirst($name)."->description }}</span>\n";
        $file .= "                </td>\n";
        $file .= "                <td class='pr-0 text-right'>\n";
        $file .= "                    <a href='{{ route('".$view.".edit', $".lcfirst($name)."->id) }}' class='btn btn-icon btn-light btn-hover-success btn-sm mx-3'>\n";
        $file .= "                        {{ Metronic::getSVG('media/svg/icons/Communication/Write.svg', 'svg-icon svg-icon-md svg-icon-success') }}\n";
        $file .= "                    </a>\n";
        $file .= "                    <a href='javascript:;' onclick='deleteItem({{".'$'."".lcfirst($name)."->id}})' class='btn btn-icon btn-light btn-hover-danger btn-sm'>\n";
        $file .= "                        {{ Metronic::getSVG('media/svg/icons/General/Trash.svg', 'svg-icon svg-icon-md svg-icon-danger') }}\n";
        $file .= "                    </a>\n";
        $file .= "                    {!! Form::open(['route' => ['".$view.".destroy', $".lcfirst($name)."->id], 'method'=>'POST', 'id' => 'form_remove_'.$".lcfirst($name)."->id]) !!}\n";
        $file .= "                        {{ csrf_field() }}\n";
        $file .= "                        {{ method_field('DELETE') }}\n";
        $file .= "                    {!! Form::close() !!}\n";
        $file .= "                </td>\n";
        $file .= "            </tr>\n";
        $file .= "            @empty\n";
        $file .= "            <tr>\n";
        $file .= "                <td colspan='4' class='text-center'><i>Nenhum registro encontrado</i></td>\n";
        $file .= "            </tr>\n";
        $file .= "            @endforelse\n";
        $file .= "        </tbody>\n";
        $file .= "    </table>\n";
        $file .= "</div>\n";
        $file .= "\n";
        $file .= "@section('scripts')\n";
        $file .= "    {!! Form::hidden('route', route('".$view.".index'), ['id'=>'route']) !!}\n";
        $file .= "    <script>\n";
        $file .= "        $('select[name='offset']').change(function () {\n";
        $file .= "            var route_val = $('#route').val();\n";
        $file .= "            $(location).attr('href', route_val+'?offset='+$(this).val());\n";
        $file .= "        })\n";
        $file .= "        function deleteItem(item_id) {\n";
        $file .= "            Swal.fire({\n";
        $file .= "                title: 'Tem certeza disso?',\n";
        $file .= "                text: 'O item #'+item_id+' será removido',\n";
        $file .= "                icon: 'question',\n";
        $file .= "                showConfirmButton: true,\n";
        $file .= "                showCancelButton: true,\n";
        $file .= "                confirmButtonText: 'Sim, remover!',\n";
        $file .= "                customClass: {\n";
        $file .= "                    confirmButton: 'btn-danger',\n";
        $file .= "                }\n";
        $file .= "            }).then(function (result) {\n";
        $file .= "                if (result.value) {\n";
        $file .= "                    $('#form_remove_'+item_id).submit();\n";
        $file .= "                }\n";
        $file .= "            })\n";
        $file .= "        }\n";
        $file .= "    </script>\n";
        $file .= "@endsection\n";

        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/includes/_table.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Table created successfully.');
    }

    /**
     * Create view filter file
     * 
     * @param mixed $name, $table, $view, $varPlural, $pageTitle
     * @return void 
     */
    public function _createViewFilterFile($view, $varPlural)
    {
        $file = "";

        $file .= "{{-- Modal --}}\n";
        $file .= "<div class='modal fade' id='filter-".$view."' tabindex='-1' role='dialog' aria-labelledby='filter-".$view."-title' aria-hidden='true'>\n";
        $file .= "    <div class='modal-dialog' role='document'>\n";
        $file .= "        <div class='modal-content'>\n";
        $file .= "            <div class='modal-header'>\n";
        $file .= "                <h5 class='modal-title'>Filtrar ".$varPlural."</h5>\n";
        $file .= "                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>\n";
        $file .= "                    <span aria-hidden='true'>&times;</span>\n";
        $file .= "                </button>\n";
        $file .= "            </div>\n";
        $file .= "            {!! Form::open(['method'=>'GET']) !!}\n";
        $file .= "            <div class='modal-body'>\n";
        $file .= "                <div class='container-fluid'>\n";
        $file .= "                    <div class='row'>\n";
        $file .= "                        <div class='col-md-12'>\n";
        $file .= "                            <div class='form-group'>\n";
        $file .= "                                {!! Form::label('name', 'Nome') !!}\n";
        $file .= "                                {!! Form::text('name', request('name'), ['class'=>'form-control form-control-solid form-solid']) !!}\n";
        $file .= "                            </div>\n";
        $file .= "                        </div>\n";
        $file .= "                    </div>\n";
        $file .= "                </div>\n";
        $file .= "            </div>\n";
        $file .= "            <div class='modal-footer'>\n";
        $file .= "                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>\n";
        $file .= "                <a href='{{ route('".lcfirst($view).".index') }}' class='btn btn-info'>Limpar</a>\n";
        $file .= "                <button type='submit' class='btn btn-primary'>Filtrar</button>\n";
        $file .= "            </div>\n";
        $file .= "            {!! Form::close() !!}\n";
        $file .= "        </div>\n";
        $file .= "    </div>\n";
        $file .= "</div>\n";

        $fp = \fopen("{$this->laravel->basePath()}/resources/views/adm/{$view}/includes/_filter.blade.php", "w+");
        \fwrite($fp, $file);
        \fclose($fp);

        $this->info('View Filter created successfully.');
    }
}
