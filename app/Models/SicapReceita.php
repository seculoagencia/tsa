<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SicapReceita extends Model
{
    use SoftDeletes;
    
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'sicap_receitas';
     protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'CodOrgao',
        'CodUndOrcamentaria',
        'CodContaReceita',
        'CodBanco',
        'CodAgencia',
        'NumConta',
        'CodContaAtivo',
        'DataArrecadacao',
        'Valor',
        'CodRecVinculado',
        'FormaArrecadacao',
        'DataRegistro'
     ];

    /**
     * Get the undOrcamentaria that owns the SicapReceita
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function undOrcamentaria(): BelongsTo
    {
        return $this->belongsTo(SicapUndOrcamentaria::class, 'CodUndOrcamentaria', 'CodUndOrcamentaria');
    }

    /**
     * Get the recursoVinculado that owns the SicapReceita
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recursoVinculado(): BelongsTo
    {
        return $this->belongsTo(SicapRecursoVinculado::class, 'CodRecVinculado', 'CodRecVinculado');
    }

    public function getDataArrecadacaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}