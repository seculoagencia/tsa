<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $table = 'app_settings';
    protected $fillable = [
        'name',
        'site_url',
        'email',
        'view_last_update',
        'recaptcha_site',
        'recaptcha_secret',
        'logo',
        'cover',
        'cor_principal',
        'display_covid',
        'prioridade',
        'prioridade_receita',
        'prioridade_servidores'
    ];

    public function setViewLastUpdateAttribute($value)
    {
        $this->attributes['view_last_update'] = $value == 'on' ? true : false;
    }

    public function setDisplayCovidAttribute($value)
    {
        $this->attributes['display_covid'] = $value == 'on' ? true : false;
    }
}
