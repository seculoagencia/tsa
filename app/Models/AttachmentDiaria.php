<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentDiaria extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_diarias';
     protected $fillable = ['attachment_id','diaria_id'];

}