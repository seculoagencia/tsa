<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Liquidacao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'liquidacoes';
     protected $fillable = ['numero','valor','data','empenho_id'];

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }

    public function getDataFormatAttribute($value)
    {
        return date('d/m/Y', strtotime($this->data));
    }

    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = str_replace(['.',','],['','.'],$value);
    }

}