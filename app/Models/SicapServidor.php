<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SicapServidor extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'sicap_servidores';
     protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'Cpf',
        'Nome',
        'DataNascimento',
        'NomeMae',
        'NomePai',
        'PisPasep',
        'TituloEleitoral',
        'DataAdmissao',
        'CodVinculoEmpregaticio',
        'CodRegimePrevidenciario',
        'CodEscolaridade',
        'SobCessao',
        'CnpjEntidade',
        'NomeEntidade',
        'DataCessao',
        'DataRetornoCessao',
        'SalarioBruto',
        'Salarioliquido',
        'MargemConsignada',
        'CBO',
        'CodCargo',
        'CodLotacao',
        'CodFuncao',
        'Matricula'
     ];
    
    /**
     * Get the cargo that owns the SicapEmpenho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargo(): BelongsTo
    {
        return $this->belongsTo(SicapCargosServidores::class, 'CodCargo', 'CodCargo');
    }
    
    public function getDataNascimentoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
    
    public function getDataAdmissaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
    
    public function getDataCessaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
    
    public function getDataRetornoCessaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
    
    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['DataNascimento'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }
    
    public function setDataAdmissaoAttribute($value)
    {
        $this->attributes['DataAdmissao'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }
    
    public function setDataCessaoAttribute($value)
    {
        $this->attributes['DataCessao'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }
    
    public function setDataRetornoCessaoAttribute($value)
    {
        $this->attributes['DataRetornoCessao'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function getSalarioBrutoAttribute($value)
    {
        return number_format($value,2,',','.');
    }

    public function getSalarioliquidoAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}