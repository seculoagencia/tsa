<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentDocument extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_documents';
     protected $fillable = ['attachment_id','document_id'];

}