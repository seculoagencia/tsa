<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresLiquidacao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_liquidacao';
   protected $fillable = [
      'ano',
      'cod_unid_orc',
      'num_empenho',
      'num_liquidacao',
      'data',
      'valor',
      'tipo_doc',
      'num_chave',
      'historico',
      'cod_fonte_recurso'
   ];

   public function getValorAttribute($value)
   {
      return number_format($value,2,',','.');
   }

   public function getDataAttribute($value)
   {
      return date('d/m/Y', strtotime($value));
   }
}