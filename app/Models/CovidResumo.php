<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CovidResumo extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'covid_resumos';
     protected $fillable = [
        'casos_confirmados',
        'casos_suspeitos',
        'pessoas_recuperadas',
        'obitos',
        'arquivo',
        'nome_arquivo'
     ];

}