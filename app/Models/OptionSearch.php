<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionSearch extends Model
{
    use HasFactory;
    public $table = "option_search";
    protected $fillable = ['name','icon','route'];

    public function getSubMenus($id){
        return SubMenu::where('id_menu',$id)->where('visible',1)->get();
//        return $this->hasMany(SubMenu::class,'id_menu','id');
    }
}
