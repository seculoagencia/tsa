<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
    protected $table = 'documents';
    protected $fillable = ['name','month','year','type_document_id','user_id'];

    /**
     * Get the type_document that owns the Document
     */
    public function type_document()
    {
        return $this->belongsTo(TypeDocument::class);
    }

    /**
     * The attachments that belong to the Document
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_documents');
    }

    public function getMonthYearAttribute($value)
    {
        return $this->year.'-'.str_pad($this->month,2,"0",STR_PAD_LEFT);
    }
}