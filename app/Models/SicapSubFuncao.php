<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapSubFuncao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_subfuncao';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodSubFuncao',
      'Nome'
   ];

}