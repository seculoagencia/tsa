<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
   /** 
    * The attributes that are mass assignable
    * 
    * @var array 
   */
   protected $table = 'pagamentos';
   protected $fillable = [
      'banco',
      'agencia',
      'conta',
      'documento',
      'data',
      'valor',
      'empenho_id'
   ];
   
   public function getValorAttribute($value)
   {
      return number_format($value,2,',','.');
   }

   public function getDataFormatAttribute($value)
   {
      return date('d/m/Y', strtotime($this->data));
   }

   public function setValorAttribute($value)
   {
      $this->attributes['valor'] = str_replace(['.',','],['','.'],$value);
   }
}