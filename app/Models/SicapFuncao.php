<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapFuncao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_funcao';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodFuncao',
      'Nome'
   ];

}