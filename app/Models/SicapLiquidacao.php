<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapLiquidacao extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $table = 'sicap_liquidacao';
    protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'NumEmpenho',
        'NumLiquidacao',
        'DataLiquidacao',
        'Valor',
        'Sinal',
        'CodOperacao',
        'NumProcesso',
        'CodCredor',
        'Referencia',
        'Historico'
    ];

   public function getDataLiquidacaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

}
