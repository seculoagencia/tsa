<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapCargosServidores extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'sicap_cargos_servidores';
     protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'CodCargo',
        'Descricao'
     ];

}