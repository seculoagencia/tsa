<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresCredor extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
   */
   protected $table = 'sagres_credor';
   protected $fillable = [
      'cpf_cnpj',
      'nome',
      'tipo_credor',
      'sigla_uf',
      'municipio'
   ];

}