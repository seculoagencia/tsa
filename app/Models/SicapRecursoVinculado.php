<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapRecursoVinculado extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_recursovinculado';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodRecVinculado',
      'Nome',
      'Finalidade',
      'Tipo'
   ];

}