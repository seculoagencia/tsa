<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SiapPagamento extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
   protected $table = 'siap_pagamento';
   protected $fillable = [
    'Numero',
    'NumeroEmpenho',
    'CodigoUnidadeOrcamentaria',
    'ContaContabil',
    'Data',
    'Valor',
    'Descricao',
    'Exercicio',
    'Mes',
   ];

   /**
    * Get the empenho that owns the SiapPagamento
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function empenho(): BelongsTo
   {
       return $this->belongsTo(SiapEmpenho::class, 'NumeroEmpenho', 'NumeroEmpenho');
   }

   public function getDataAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

}
