<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
     protected $table = 'contratos';
     protected $fillable = [
        'orgao',
        'tipo',
        'modalidade',
        'data_hora',
        'objeto',
        'valor',
        'status',
        'vencedor',
        'covid19'
     ];

    /**
     * The attachments that belong to the Document
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_contratos');
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }

    public function getDataHoraFormatAttribute($value)
    {
        return date('d/m/Y H:i', strtotime($this->data_hora));
    }

    public function getDataHoraAttribute($value)
    {
        return date('Y-m-d\TH:i', strtotime($value));
    }

    public function setDataHoraAttribute($value)
    {
        $this->attributes['data_hora'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = $value ? str_replace(['.',','],['','.'],$value) : null;
    }
}
