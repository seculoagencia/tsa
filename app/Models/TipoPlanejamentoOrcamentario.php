<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TipoPlanejamentoOrcamentario extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'tipo_planejamento_orcamentarios';
     protected $fillable = ['name','description'];

    /**
     * Get all of the arquivos for the TipoPlanejamentoOrcamentario
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function arquivos(): HasMany
    {
        return $this->hasMany(ArquivoPlanOrcamentario::class, 'tipo_planejamento_orcamentario_id');
    }

}