<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'cargos';
     protected $fillable = ['nome'];

}