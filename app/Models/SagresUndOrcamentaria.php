<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SagresUndOrcamentaria extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'sagres_undorcamentaria';
     protected $fillable = [
        'codigo',
        'denominacao',
        'num_unid_jurisd'
     ];
    
    /**
     * Get the credor that owns the SagresEmpenho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credor(): BelongsTo
    {
        return $this->belongsTo(SagresCredor::class, 'cpf_cnpj_credor', 'cpf_cnpj');
    }

    public function getDataEmissaoEmpenhoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValorEmpenhadoAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}