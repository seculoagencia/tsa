<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresPrograma extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_programa';
   protected $fillable = [
      'codigo',
      'nome',
      'descricao'
   ];

}