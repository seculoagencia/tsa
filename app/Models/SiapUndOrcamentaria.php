<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiapUndOrcamentaria extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
   protected $table = 'siap_undorcamentaria';
   protected $fillable = [
      'Codigo',
      'Descricao'
   ];

}
