<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapCredor extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'sicap_credor';
     protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'CodCredor',
        'Nome',
        'InscricaoEstadual',
        'InscricaoMunicipal',
        'Endereco',
        'Cidade',
        'UF',
        'Cep',
        'Fone',
        'Fax',
        'Tipo',
        'NumeroDoRegistro',
        'TipoRegistro'
     ];

}