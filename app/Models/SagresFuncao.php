<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresFuncao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_funcao';
   protected $fillable = [
      'codigo',
      'nome'
   ];

}