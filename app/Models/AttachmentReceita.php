<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentReceita extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_receitas';
     protected $fillable = ['attachment_id','receita_id'];

}