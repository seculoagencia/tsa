<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentVerba extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_verbas';
     protected $fillable = ['attachment_id','verba_id'];

}