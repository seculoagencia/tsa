<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapProjetoAtividade extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_projatividade';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodProjAtividade',
      'Identificador',
      'Nome',
      'TipoPrograma'
   ];

}