<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapRubricaDespesa extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_rubricadespesa';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodRubrica',
      'Especificacao',
      'TipoNivelConta',
      'NumNivelConta'
   ];

}