<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresAcao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_acao';
   protected $fillable = [
      'codigo',
      'nome',
      'identificacao'
   ];

}