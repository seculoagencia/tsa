<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SagresEmpenho extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
     protected $table = 'sagres_empenhos';
     protected $fillable = [
        'ano',
        'cod_unid_orc',
        'funcao',
        'subfuncao',
        'programa',
        'acao',
        'id_acao',
        'cod_cat_econ',
        'cod_nat_desp',
        'modalidade_aplic',
        'cod_elem_desp_dot',
        'sub_elem_desp',
        'modalidade_licit',
        'num_empenho',
        'tipo_empenho',
        'data_emissao_empenho',
        'valor_empenhado',
        'historico',
        'cpf_cnpj_credor',
        'num_procedimento',
        'font_recurso',
        'cpf_ordenador',
        'cod_elem_desp'
     ];

    /**
     * Get the credor that owns the SagresEmpenho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credor(): BelongsTo
    {
        return $this->belongsTo(SagresCredor::class, 'cpf_cnpj_credor', 'cpf_cnpj');
    }

    /**
     * Get the funcao that owns the SagresFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function funcao(): BelongsTo
    {
        return $this->belongsTo(SagresFuncao::class, 'funcao', 'codigo');
    }

    /**
     * Get the subFuncao that owns the SagresSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subfuncao(): BelongsTo
    {
        return $this->belongsTo(SagresSubFuncao::class, 'subfuncao', 'codigo');
    }

    /**
     * Get the subFuncao that owns the SagresSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function undOrcamentaria(): BelongsTo
    {
        return $this->belongsTo(SagresUndOrcamentaria::class, 'cod_unid_orc', 'codigo');
    }

    /**
     * Get the subFuncao that owns the SagresSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getPrograma(): BelongsTo
    {
        return $this->belongsTo(SagresPrograma::class, 'programa', 'codigo');
    }

    /**
     * Get the subFuncao that owns the Acao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getAcao(): BelongsTo
    {
        return $this->belongsTo(SagresAcao::class, 'acao', 'codigo');
    }

    /**
     * Get the subFuncao that owns the RecursoVinculado
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rubricaDespesa(): BelongsTo
    {
        return $this->belongsTo(SagresElementoDespesa::class, 'cod_elem_desp', 'codigo');
    }

    /**
     * Get all of the pagamentos for the SagresPagamento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagamentos(): HasMany
    {
        return $this->hasMany(SagresPagamento::class, 'num_empenho', 'num_empenho')->where('ano', $this->ano);
    }

    /**
     * Get all of the liquidacoes for the SagresLiquidacao
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function liquidacoes(): HasMany
    {
        return $this->hasMany(SagresLiquidacao::class, 'num_empenho', 'num_empenho')->where('ano', $this->ano);
    }

    public function totalLiquidado()
    {
        $total = SagresLiquidacao::where('num_empenho', $this->num_empenho)
        ->where('ano', $this->ano)
        ->sum('valor');
        return number_format($total,2,',','.');
    }

    public function totalPago()
    {
        $total = SagresPagamento::where('num_empenho', $this->num_empenho)->where('ano', $this->ano)->sum('valor');
        return number_format($total,2,',','.');
    }

    public function getDataEmissaoEmpenhoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValorEmpenhadoAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}
