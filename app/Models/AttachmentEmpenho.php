<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentEmpenho extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_empenhos';
     protected $fillable = ['attachment_id','empenho_id'];

}