<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapUndOrcamentaria extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_undorcamentaria';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodOrgao',
      'CodUndOrcamentaria',
      'CNPJ',
      'Nome',
      'Identificador',
      'Descricao'
   ];

}