<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPedido extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'tipo_pedidos';
     protected $fillable = ['name'];

}