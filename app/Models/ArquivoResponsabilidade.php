<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArquivoResponsabilidade extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'arquivo_responsabilidades';
     protected $fillable = ['name','attachment_id','responsabilidade_id','description'];

    /**
     * Get the tipo that owns the ArquivoPlanOrcamentario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo(): BelongsTo
    {
        return $this->belongsTo(Responsabilidade::class, 'responsabilidade_id');
    }

    /**
     * Get the attachment that owns the ArquivoPlanOrcamentario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attachment(): BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }

}