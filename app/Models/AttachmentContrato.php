<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentContrato extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_contratos';
     protected $fillable = ['attachment_id','contrato_id'];

}