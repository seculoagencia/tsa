<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArquivoPlanOrcamentario extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'arquivo_plan_orcamentarios';
     protected $fillable = ['name','attachment_id','tipo_planejamento_orcamentario_id','description'];

    /**
     * Get the tipo that owns the ArquivoPlanOrcamentario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo(): BelongsTo
    {
        return $this->belongsTo(TipoPlanejamentoOrcamentario::class, 'tipo_planejamento_orcamentario_id');
    }

    /**
     * Get the attachment that owns the ArquivoPlanOrcamentario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attachment(): BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }

}