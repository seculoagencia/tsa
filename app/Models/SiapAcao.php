<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiapAcao extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
   protected $table = 'siap_acao';
   protected $fillable = [
    'Numero',
    'Descricao',
    'Tipo',
    'CodigoUnidadeGestora',
    'CodigoUnidadeOrcamentaria',
    'CodigoFuncao',
    'CodigoSubfuncao',
    'CodigoPrograma',
    'Exercicio',
    'Mes',
   ];

}
