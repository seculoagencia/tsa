<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapPagamentoFinanceiro extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
    protected $table = 'sicap_pagamentofinanceiro';
    protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'NumEmpenho',
        'NumPagamento',
        'CodContaBalancete',
        'Valor',
        'CodBanco',
        'CodAgenciaBanco',
        'NumContaBancaria',
        'NumDocumento',
        'NumLiquidacao',
        'Sinal',
        'TipoPagamento',
        'TipoConta'
    ];

   public function getDataPagamentoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

}