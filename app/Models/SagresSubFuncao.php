<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresSubFuncao extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_subfuncao';
   protected $fillable = [
      'cod_funcao',
      'codigo',
      'nome'
   ];

}