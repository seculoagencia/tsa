<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiapCredor extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
     protected $table = 'siap_credor';
     protected $fillable = [
        'Codigo',
        'Nome',
        'CNAE',
        'Tipo'
     ];

}
