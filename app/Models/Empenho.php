<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empenho extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'empenhos';
     protected $fillable = [
        'numero',
        'credor',
        'data',
        'unidade_orcamentaria',
        'funcao',
        'subfuncao',
        'programa',
        'acao',
        'fonte_de_recurso',
        'elemento_de_despesa',
        'cpf_cnpj',
        'numero_do_processo',
        'historico',
        'covid19'
     ];

    /**
     * Get the pagamentos that owns the Empenho
    */
    public function pagamentos()
    {
        return $this->hasMany(Pagamento::class, 'empenho_id');
    }

    /**
     * Get the liquidacoes that owns the Empenho
    */
    public function liquidacoes()
    {
        return $this->hasMany(Liquidacao::class, 'empenho_id');
    }

    /**
     * The attachments that belong to the Empenho
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_empenhos');
    }

    public function totalPagamentos()
    {
        return Pagamento::where('empenho_id', $this->id)->sum('valor');
    }

    public function totalLiquidacoes()
    {
        return Liquidacao::where('empenho_id', $this->id)->sum('valor');
    }

    public function getDataFormatAttribute($value)
    {
        return date('d/m/Y', strtotime($this->data));
    }
}