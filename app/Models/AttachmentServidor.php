<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentServidor extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'attachment_servidores';
     protected $fillable = ['attachment_id','servidor_id'];

}