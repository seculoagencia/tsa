<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresElementoDespesa extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_elemento_despesa';
   protected $fillable = [
      'codigo',
      'descricao'
   ];

}