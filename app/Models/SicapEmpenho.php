<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SicapEmpenho extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
     protected $table = 'sicap_empenho';
     protected $fillable = [
        'CodUndGestora',
        'CodigoUA',
        'Bimestre',
        'Exercicio',
        'NumEmpenho',
        'DataEmpenho',
        'Valor',
        'Sinal',
        'Tipo',
        'CodOrgao',
        'CodUndOrcamentaria',
        'CodFuncao',
        'CodSubFuncao',
        'CodPrograma',
        'CodProjAtividade',
        'CodContaDespesa',
        'CodRecVinculado',
        'ContraPartida',
        'CodCredor',
        'ModalLicita',
        'RegistroDePreco',
        'ReferenciaLegal',
        'NumProcesso',
        'DataProcesso',
        'NumContrato',
        'DataContrato',
        'NumConvenio',
        'DataConvenio',
        'NumObra',
        'Historico'
     ];

    /**
     * Get the credor that owns the SicapEmpenho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credor(): BelongsTo
    {
        return $this->belongsTo(SicapCredor::class, 'CodCredor', 'CodCredor');
    }

    /**
     * Get the funcao that owns the SicapFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function funcao(): BelongsTo
    {
        return $this->belongsTo(SicapFuncao::class, 'CodFuncao', 'CodFuncao');
    }

    /**
     * Get the subFuncao that owns the SicapSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subfuncao(): BelongsTo
    {
        return $this->belongsTo(SicapSubFuncao::class, 'CodSubFuncao', 'CodSubFuncao');
    }

    /**
     * Get the subFuncao that owns the SicapSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function undOrcamentaria(): BelongsTo
    {
        return $this->belongsTo(SicapUndOrcamentaria::class, 'CodUndOrcamentaria', 'CodUndOrcamentaria');
    }

    /**
     * Get the subFuncao that owns the SicapSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function programa(): BelongsTo
    {
        return $this->belongsTo(SicapPrograma::class, 'CodPrograma', 'CodPrograma');
    }

    /**
     * Get the subFuncao that owns the ProjetoAtividade
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projetoAtividade(): BelongsTo
    {
        return $this->belongsTo(SicapProjetoAtividade::class, 'CodProjAtividade', 'CodProjAtividade');
    }

    /**
     * Get the subFuncao that owns the RecursoVinculado
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recursoVinculado(): BelongsTo
    {
        return $this->belongsTo(SicapRecursoVinculado::class, 'CodRecVinculado', 'CodRecVinculado');
    }

    /**
     * Get the subFuncao that owns the RecursoVinculado
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rubricaDespesa(): BelongsTo
    {
        return $this->belongsTo(SicapRubricaDespesa::class, 'CodContaDespesa', 'CodRubrica');
    }

    /**
     * Get all of the pagamentos for the SicapPagamento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagamentos(): HasMany
    {
        return $this->hasMany(SicapPagamento::class, 'NumEmpenho', 'NumEmpenho');
    }

    /**
    * Get the pagamentoFinanceiro that owns the SicapPagamento
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function pagamentoFinanceiro(): BelongsTo
   {
       return $this->belongsTo(SicapPagamentoFinanceiro::class, 'NumEmpenho', 'NumEmpenho');
   }

    /**
     * Get all of the liquidacoes for the SicapLiquidacao
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function liquidacoes(): HasMany
    {
        return $this->hasMany(SicapLiquidacao::class, 'NumEmpenho', 'NumEmpenho');
    }

    public function totalLiquidado()
    {
        $total = SicapLiquidacao::where('NumEmpenho', $this->NumEmpenho)->sum('Valor');
        return number_format($total,2,',','.');
    }

    public function totalPago()
    {
        $total = SicapPagamento::where('NumEmpenho', $this->NumEmpenho)->sum('Valor');
        return number_format($total,2,',','.');
    }

    public function getDataEmpenhoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}
