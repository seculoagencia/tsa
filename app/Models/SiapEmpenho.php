<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiapEmpenho extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
     public $table = 'siap_empenho';
     protected $fillable = [
        'NumeroEmpenho',
        'Tipo',
        'Ano',
        'CodigoUnidadeGestora',
        'CodigoUnidadeOrcamentaria',
        'FuncaoSubfuncao',
        'CodigoPrograma',
        'NumeroAcao',
        'ContaContabil',
        'NaturezaDespesa',
        'DataEmissao',
        'TipoContratacao',
        'NumeroContratacao',
        'NumeroLicitacao',
        'NumeroContrato',
        'NumeroConvenio',
        'NumeroProcesso',
        'Credor',
        'Valor',
        'Mes',
     ];

    /**
     * Get the credor that owns the SiapEmpenho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credor(): BelongsTo
    {
        return $this->belongsTo(SiapCredor::class, 'Credor', 'Codigo');
    }

    /**
     * Get the funcao that owns the SiapFuncao
     */
    public function funcao()
    {
        return SiapFuncao::where('Codigo', substr($this->FuncaoSubfuncao, -5, 2))->first();
    }

    /**
     * Get the subFuncao that owns the SiapSubFuncao
     */
    public function subfuncao()
    {
        return SiapSubFuncao::where('Codigo', substr($this->FuncaoSubfuncao, 2))->first();
    }

    /**
     * Get the subFuncao that owns the SiapSubFuncao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function undOrcamentaria(): BelongsTo
    {
        return $this->belongsTo(SiapUndOrcamentaria::class, 'CodigoUnidadeOrcamentaria', 'Codigo');
    }

    /**
     * Get the subFuncao that owns the Acao
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function acao(): BelongsTo
    {
        return $this->belongsTo(SiapAcao::class, 'NumeroAcao', 'Numero');
    }

    /**
     * Get all of the pagamentos for the SiapPagamento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagamentos(): HasMany
    {
        return $this->hasMany(SiapPagamento::class, 'NumeroEmpenho', 'NumeroEmpenho');
    }

    /**
     * Get all of the liquidacoes for the SiapLiquidacao
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function liquidacoes(): HasMany
    {
        return $this->hasMany(SiapLiquidacao::class, 'NumeroEmpenho', 'NumeroEmpenho');
    }

    public function totalLiquidado()
    {
        $total = SiapLiquidacao::where('NumeroEmpenho', $this->NumeroEmpenho)->sum('Valor');
        return number_format($total,2,',','.');
    }

    public function totalPago()
    {
        $total = SiapPagamento::where('NumeroEmpenho', $this->NumeroEmpenho)->sum('Valor');
        return number_format($total,2,',','.');
    }

    public function getDataEmissaoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }
}
