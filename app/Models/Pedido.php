<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pedido extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'pedidos';
     protected $fillable = [
        'name',
        'email',
        'priority',
        'cpf',
        'phone',
        'location',
        'type_answer',
        'typerequest_id',
        'subject',
        'message',
        'anexo',
        'status',
        'serial'
     ];

    /**
     * Get all of the respostas for the Pedido
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function respostas(): HasMany
    {
        return $this->hasMany(Resposta::class);
    }

    /**
     * Get the tipoPedido that owns the Pedido
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPedido(): BelongsTo
    {
        return $this->belongsTo(TipoPedido::class, 'typerequest_id', 'id');
    }

    public function getPriorityExtensoAttribute($value)
    {
        return \Config::get('constants.esic.priority')[$this->priority];
    }

    public function getStatusExtensoAttribute($value)
    {
        return \Config::get('constants.esic.status')[$this->status];
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y H:i:s', strtotime($value));
    }

}