<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    use HasFactory;
    public $table = 'sub_menu';

    protected  $fillable = [
        'name',
        'id_menu',
        'route',
        'visible'
    ];

    public function getMenuFather(){
        return $this->belongsTo(OptionSearch::class,'id_menu','id');
    }
}
