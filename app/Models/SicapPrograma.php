<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SicapPrograma extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_programa';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'CodPrograma',
      'Nome',
      'Objetivo',
      'PublicoAlvo'
   ];

}