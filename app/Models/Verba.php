<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verba extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'verbas';
     protected $fillable = ['data','beneficiario','valor','referencia'];

    /**
     * The attachments that belong to the Document
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_verbas');
    }

    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = str_replace(['.',','],['','.'], $value);
    }

    public function getDataFormatAttribute($value)
    {
        return date('d/m/Y', strtotime($this->data));
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }

    public function getReferenciaFormatAttribute($value)
    {
        return explode('-', $this->referencia)[1].'/'.explode('-', $this->referencia)[0];
    }
}