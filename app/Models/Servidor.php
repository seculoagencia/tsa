<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servidor extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $table = 'servidores';
    protected $fillable = [
        'nome',
        'cpf',
        'regime',
        'admissao',
        'cargo',
        'secretaria',
        'valor_bruto',
        'valor_liquido',
        'descontos',
        'referencia'
    ];

    /**
     * The attachments that belong to the Servidor
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_servidores');
    }

    public function getValorBrutoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setValorBrutoAttribute($value)
    {
        $checkValue = explode(',',$value);
        if (count($checkValue) > 1) {
            $this->attributes['valor_bruto'] = $value ? str_replace(['.', ','], ['', '.'], $value) : null;
        } else {
            $this->attributes['valor_bruto'] = $value;
        }
    }

    public function getValorLiquidoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setValorLiquidoAttribute($value)
    {
        $checkValue = explode(',',$value);
        if (count($checkValue) > 1) {
            $this->attributes['valor_liquido'] = $value ? str_replace(['.', ','], ['', '.'], $value) : null;
        } else {
            $this->attributes['valor_liquido'] = $value;
        }

    }

    public function getDescontosAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setDescontosAttribute($value)
    {
        $checkValue = explode(',',$value);
        if (count($checkValue) > 1) {
            $this->attributes['descontos'] = $value ? str_replace(['.', ','], ['', '.'], $value) : null;
        } else {
            $this->attributes['descontos'] = $value;
        }
    }

    public function getReferenciaFormatAttribute($value)
    {
        if (count(explode('-', $this->referencia)) > 1) {
            return explode('-', $this->referencia)[1] . '/' . explode('-', $this->referencia)[0];
        } else {
            return $this->referencia;
        }
    }

    public function getCpfFormatAttribute($value)
    {
        $str_split = str_split($this->cpf, 3);
        $cpf = '***.'.$str_split[1].'.***-**';
        return $cpf;
    }
}
