<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receita extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'receitas';
     protected $fillable = ['codigo','descricao','data','valor','covid19'];

    /**
     * The attachments that belong to the Receita
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'attachment_receitas');
    }

    public function setValorAttribute($value)
    {
        if (count(explode(',', $value)) > 1) {
            $this->attributes['valor'] = str_replace(['.',','],['','.'], $value);
        } else {
            $this->attributes['valor'] = $value;
        }
    }

    public function getValorFormatAttribute($value)
    {
    return number_format($this->valor,2,',','.');
    }

    public function getDataFormatAttribute($value)
    {
    return date('d/m/Y', strtotime($this->data));
    }
}