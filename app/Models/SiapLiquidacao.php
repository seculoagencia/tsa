<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiapLiquidacao extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $table = 'siap_liquidacao';
    protected $fillable = [
        'Numero',
        'NumeroEmpenho',
        'CodigoUnidadeOrcamentaria',
        'ContaContabil',
        'Data',
        'TipoDocumento',
        'NumeroDocumentoFiscal',
        'ChaveAcesso',
        'SerieDocumentoFiscal',
        'Valor',
        'Justificativa',
        'Exercicio',
        'Mes',
    ];

   public function getDataAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

}
