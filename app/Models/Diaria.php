<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Diaria extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
     protected $table = 'diarias';
     protected $fillable = [
        'favorecido',
        'cargo_id',
        'data',
        'destino',
        'motivo',
        'valor'
     ];

    /**
     * Get the cargo that owns the Diaria
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Cargo::class);
    }

    /**
     * The attachments that belong to the Diaria
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attachments(): BelongsToMany
    {
        return $this->belongsToMany(Attachment::class, 'attachment_diarias');
    }

    public function getDataFormatAttribute($value)
    {
        return date('d/m/Y', strtotime($this->data));
    }

    public function getValorAttribute($value)
    {
        return number_format($value,2,',','.');
    }

    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = str_replace(['.',','],['','.'],$value);
    }
}