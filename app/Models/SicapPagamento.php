<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SicapPagamento extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sicap_pagamento';
   protected $fillable = [
      'CodUndGestora',
      'CodigoUA',
      'Bimestre',
      'Exercicio',
      'NumEmpenho',
      'NumPagamento',
      'NumLiquidacao',
      'DataPagamento',
      'Valor',
      'Sinal',
      'Historico',
      'CodOperacao',
      'NumProcesso'
   ];

   /**
    * Get the pagamentoFinanceiro that owns the SicapPagamento
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function pagamentoFinanceiro(): BelongsTo
   {
       return $this->belongsTo(SicapPagamentoFinanceiro::class, 'NumPagamento', 'NumPagamento');
   }
   /**
    * Get the empenho that owns the SicapPagamento
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function empenho(): BelongsTo
   {
       return $this->belongsTo(SicapEmpenho::class, 'NumEmpenho', 'NumEmpenho');
   }

   public function getDataPagamentoAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

}