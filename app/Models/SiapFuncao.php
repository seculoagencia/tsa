<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiapFuncao extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
   protected $table = 'siap_funcao';
   protected $fillable = [
      'Codigo',
      'Descricao'
   ];

}
