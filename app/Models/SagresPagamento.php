<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SagresPagamento extends Model
{
    /** 
     * The attributes that are mass assignable
     * 
     * @var array 
     */
   protected $table = 'sagres_pagamentos';
   protected $fillable = [
      'ano',
      'cod_unid_orc',
      'num_empenho',
      'num_parc_pagamento',
      'data',
      'valor',
      'cod_banco',
      'num_agencia_bancaria',
      'num_conta_bancaria',
      'num_seq'
   ];

   public function getValorAttribute($value)
   {
      return number_format($value,2,',','.');
   }

   public function getDataAttribute($value)
   {
      return date('d/m/Y', strtotime($value));
   }
}