<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttachmentDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
    * @return array
     */
    public function rules()
    {
        $max_upload = (int)(ini_get('upload_max_filesize'));
        return [
            'file'=>'max:'.$max_upload*1000
        ];
    }

    public function messages()
    {
        $max_upload = (int)(ini_get('upload_max_filesize'));
        return [
            'file.max' => 'O tamanho do arquivo não pode ser maior que '.$max_upload.' MB',
        ];
    }
}
