<?php

namespace App\Http\Controllers;

use App\Models\Empenho;
use App\Http\Requests\EmpenhoRequest;

class EmpenhoController extends Controller
{
    public function __construct() {
        $page_title = 'Empenhos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Empenho::class, 'empenho');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $empenhos = Empenho::where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->whereYear('data',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('data',request('month'));
            }
            if (request()->has('numero') && request('numero') != '') {
                $q->where('numero','like','%'.request('numero').'%');
            }
            if (request()->has('credor') && request('credor') != '') {
                $q->where('credor','like','%'.request('credor').'%');
            }
        })->orderBy('data','desc')->paginate($offset);

        return view('adm.empenhos.index', compact('page_description', 'offset', 'empenhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.empenhos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EmpenhoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(EmpenhoRequest $request)
    {
        try {
            $empenho = Empenho::create($request->except(['_token','covid19']) + ['covid19' => $request->has('covid19')]);
            return redirect()->route('empenhos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empenho $empenho
     * @return \Illuminate\Http\Response
    */
    public function show(Empenho $empenho)
    {
        $page_description = 'Detalhes';
        return view('adm.empenhos.show', compact('page_description', 'empenho'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empenho $empenho
     * @return \Illuminate\Http\Response
    */
    public function edit(Empenho $empenho)
    {
        $page_description = 'Edição';
        $pagamentos = $empenho->pagamentos;
        $liquidacoes = $empenho->liquidacoes;
        $attachments = $empenho->attachments;
        $table = 'empenhos';
        $item_id = $empenho->id;
        return view('adm.empenhos.edit', compact('page_description','table','item_id','empenho','pagamentos','liquidacoes','attachments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EmpenhoRequest  $request
     * @param  \App\Models\Empenho $empenho
     * @return \Illuminate\Http\Response
    */
    public function update(EmpenhoRequest $request, Empenho $empenho)
    {
        try {
            if ($request->has('covid19')) {
                $empenho->update($request->except(['_token', 'covid19']) + ['covid19' => $request->has('covid19')]);
            } else {
                $empenho->update($request->except(['_token', 'covid19']));
            }
            
            return redirect()->route('empenhos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empenho $empenho
     * @return \Illuminate\Http\Response
    */
    public function destroy(Empenho $empenho)
    {
        try {
            $empenho->pagamentos()->delete();
            $empenho->liquidacoes()->delete();
            $empenho->attachments()->delete();

            foreach ($empenho->attachments as $key => $attachment) {
                $empenho->attachments()->detach($attachment->id);
                $attachment->delete();
            }

            $empenho->delete();
            return redirect()->route('empenhos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.index')->with('message_error', $th->getMessage());
        }
    }
}
