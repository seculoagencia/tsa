<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    public function __construct() {
        $page_title = 'Roles';
        view()->share('page_title', $page_title);

        // $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        // $this->middleware('permission:role-create', ['only' => ['create','store']]);
        // $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->authorizeResource(Role::class, 'role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $roles = Role::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('name','asc')->paginate($offset);

        return view('adm.roles.index', compact('page_description', 'offset', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        $permissions = Permission::orderBy('name','asc')->get();
        $rolePermissions = [];
        return view('adm.roles.create', compact('page_description','permissions','rolePermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|unique:roles,name',
                'permissions' => 'required'
            ]);
            DB::transaction(function () use ($request) {
                $role = Role::create($request->except('_token'));
                $role->syncPermissions($request->permissions);
            });
        } catch (\Throwable $th) {
            return redirect()->route('roles.index')->with('message_error', $th);
        }
        return redirect()->route('roles.index')->with('message_success', 'Cadastro feito com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
    */
    public function show(Role $role)
    {
        $page_description = 'Detalhes';
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$role->id)->get();
        return view('adm.roles.show', compact('page_description', 'role', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
    */
    public function edit(Role $role)
    {
        $page_description = 'Edição';
        $permissions = Permission::orderBy('name','asc')->get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$role->id)
        ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        ->all();
        return view('adm.roles.edit', compact('page_description', 'role', 'permissions', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Role $role)
    {
        $this->validate($request, ['name' => 'required','permissions' => 'required']);
        try {
            $role->update($request->except('_token'));
            $role->syncPermissions($request->permissions);
        } catch (\Throwable $th) {
            return redirect()->route('roles.index')->with('message_error', $th);
        }
        return redirect()->route('roles.index')->with('message_success', 'Registro editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
    */
    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->route('roles.index')->with('message_success', 'Registro removido com sucesso!');
    }
}
