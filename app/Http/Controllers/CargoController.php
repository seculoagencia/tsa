<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use App\Http\Requests\CargoRequest;

class CargoController extends Controller
{
    public function __construct() {
        $page_title = 'Cargos';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $cargos = Cargo::where(function ($q) {
            if (request()->has('nome') && request('nome') != '') {
                $q->where('nome','like','%'.request('nome').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.cargos.index', compact('page_description', 'offset', 'cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.cargos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CargoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(CargoRequest $request)
    {
        try {
            $cargo = Cargo::create($request->except('_token'));
            return redirect()->route('cargos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('cargos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cargo $cargo
     * @return \Illuminate\Http\Response
    */
    public function show(Cargo $cargo)
    {
        $page_description = 'Detalhes';
        return view('adm.cargos.show', compact('page_description', 'cargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cargo $cargo
     * @return \Illuminate\Http\Response
    */
    public function edit(Cargo $cargo)
    {
        $page_description = 'Edição';
        return view('adm.cargos.edit', compact('page_description', 'cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CargoRequest  $request
     * @param  \App\Models\Cargo $cargo
     * @return \Illuminate\Http\Response
    */
    public function update(CargoRequest $request, Cargo $cargo)
    {
        try {
            $cargo->update($request->except('_token'));
            return redirect()->route('cargos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('cargos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cargo $cargo
     * @return \Illuminate\Http\Response
    */
    public function destroy(Cargo $cargo)
    {
        try {
            $cargo->delete();
            return redirect()->route('cargos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('cargos.index')->with('message_error', $th->getMessage());
        }
    }
}
