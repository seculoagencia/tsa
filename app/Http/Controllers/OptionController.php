<?php

namespace App\Http\Controllers;

use App\Models\rc;
use App\Models\SubMenu;
use Illuminate\Http\Request;
use App\Models\OptionSearch;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
        $page_title = 'Controle de Menu';
        view()->share('page_title', $page_title);

    }
    public function index()
    {

        if(isset(auth()->user()->superuser)){
            $options = OptionSearch::all();
            $subOptions = SubMenu::all();
            $page_description = 'Listagem';
            $page_descriptionTwo = 'Listagem';
            return view("adm.controle-menu.index",compact("page_description","page_descriptionTwo","options","subOptions"));
        }else {
            return redirect()->back();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rc  $rc
     * @return \Illuminate\Http\Response
     */
    public function show(OptionSearch $OptionSearch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rc  $rc
     * @return \Illuminate\Http\Response
     */

    public function edit(OptionSearch $OptionSearch,$id)
    {

        $page_description = 'Edit';
        $options = $OptionSearch->get()->where('id',$id);

        if(session()->has('success') ){
            $success = session('success');
            return view('adm.controle-menu.edit',compact('options','page_description','success'));
        }else if(session()->has('error')){
            $error = session('error');
            return view('adm.controle-menu.edit',compact('options','page_description','error'));
        }else{
            return view('adm.controle-menu.edit',compact('options','page_description'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\rc  $rc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OptionSearch $OptionSearch)
    {
        $visible = $request->all('visible');

        if(isset($visible["visible"])){
            $items = $request->except(["_method","_token"]);
            $items["visible"] = true;

            $item = $OptionSearch->where('id',$request->id)->update($items);

            return redirect()->route('controle-menu.index');
        }else{
            try{
            $items = $request->except(["_method","_token","id"]);
            $items["visible"] = 0;

            $OptionSearch->where('id', $request->id)->update($items);

            return redirect()->route('controle-menu.index');
                //return redirect()->back()->with('success','Sucesso ao Atualizar');
            }catch(Exception $e){
                return redirect()->route('controle-menu.index');
                // return redirect()->back()->with('error','Erro ao Atualizar');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rc  $rc
     * @return \Illuminate\Http\Response
     */
    public function destroy(OptionSearch $OptionSearch)
    {
        //
    }

    public function editSubmenu(SubMenu $OptionSearch,$id){
        $page_description = 'Edit';
        $options = $OptionSearch->get()->where('id',$id);

        if(session()->has('success') ){
            $success = session('success');

            return view('adm.controle-menu.edit_submenu',compact('options','page_description','success'));
        }else if(session()->has('error')){
            $error = session('error');
            return view('adm.controle-menu.edit_submenu',compact('options','page_description','error'));
        }else{
            return view('adm.controle-menu.edit_submenu',compact('options','page_description'));
        }
    }

    public function updateSubmenu(Request $request, SubMenu $OptionSearch)
    {
        $visible = $request->all('visible');

        if(isset($visible["visible"])){
            $items = $request->except(["_method","_token"]);
            $items["visible"] = true;

            $item = $OptionSearch->where('id',$request->id)->update($items);

            return redirect()->route('controle-menu.index');
        }else{
            try{
                $items = $request->except(["_method","_token","id"]);
                $items["visible"] = 0;

                $OptionSearch->where('id', $request->id)->update($items);

                return redirect()->route('controle-menu.index');
                //return redirect()->back()->with('success','Sucesso ao Atualizar');
            }catch(Exception $e){
                return redirect()->route('controle-menu.index');
                // return redirect()->back()->with('error','Erro ao Atualizar');
            }
        }
    }
}
