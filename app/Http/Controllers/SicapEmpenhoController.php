<?php

namespace App\Http\Controllers;

use App\Models\SicapEmpenho;
use App\Models\SicapCredor;
use App\Models\SicapUndOrcamentaria;
use App\Models\SicapFuncao;
use App\Models\SicapSubFuncao;
use App\Models\SicapPrograma;
use App\Models\SicapProjetoAtividade;
use App\Models\SicapRecursoVinculado;
use App\Models\SicapRubricaDespesa;
use App\Models\SicapPagamento;
use App\Models\SicapPagamentoFinanceiro;
use App\Models\SicapLiquidacao;
use App\Http\Requests\SicapEmpenhoRequest;
use Illuminate\Support\Facades\DB;
use Storage;

class SicapEmpenhoController extends Controller
{
    public function __construct() {
        $page_title = 'Empenhos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(SicapEmpenho::class, 'sicap_empenho');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $sicapEmpenho = SicapEmpenho::withTrashed()->where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->where('Exercicio',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('DataEmpenho',request('month'));
            }
            if (request()->has('numero') && request('numero') != '') {
                $q->where('NumEmpenho','like','%'.request('numero').'%');
            }
            if (request()->has('credor') && request('credor') != '') {
                $q->whereIn('codCredor', SicapCredor::select('codCredor')->where('Nome','like','%'.request('credor').'%'));
            }
        })->orderBy('DataEmpenho','desc')->paginate($offset);

        return view('adm.sicap-empenho.index', compact('page_description', 'offset', 'sicapEmpenho'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.sicap-empenho.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SicapEmpenhoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(SicapEmpenhoRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                foreach ($request->file('sicap') as $key => $file) {
                    switch (explode('.',$file->getClientOriginalName())[0]) {
                        case 'Credor':
                            $this->_credor($file);
                            break;
                        case 'Empenho':
                            $this->_empenho($file);
                            break;
                        case 'UndOrcamentaria':
                            $this->_undOrcamentaria($file);
                            break;
                        case 'Funcao':
                            $this->_funcao($file);
                            break;
                        case 'SubFuncao':
                            $this->_subFuncao($file);
                            break;
                        case 'Programa':
                            $this->_programa($file);
                            break;
                        case 'ProjetoAtividade':
                            $this->_projetoAtividade($file);
                            break;
                        case 'RecursoVinculado':
                            $this->_recursoVinculado($file);
                            break;
                        case 'RubricasRecDesp':
                            $this->_rubricasRecDesp($file);
                            break;
                        case 'Pagamento':
                            $this->_pagamento($file);
                            break;
                        case 'PagamentoFinanceiro':
                            $this->_pagamentoFinanceiro($file);
                            break;
                        case 'Liquidacao':
                            $this->_liquidacao($file);
                            break;
                    }
                }
            });
            // $sicapEmpenho = SicapEmpenho::create($request->except('_token'));
            return redirect()->route('sicap-empenho.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SicapEmpenho $sicapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function show(SicapEmpenho $sicapEmpenho)
    {
        $page_description = 'Detalhes';
        return view('adm.sicap-empenho.show', compact('page_description', 'sicapEmpenho'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SicapEmpenho $sicapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function edit(SicapEmpenho $sicapEmpenho)
    {
        $page_description = 'Edição';
        return view('adm.sicap-empenho.edit', compact('page_description','empenho','attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SicapEmpenhoRequest  $request
     * @param  \App\Models\SicapEmpenho $sicapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function update(SicapEmpenhoRequest $request, SicapEmpenho $sicapEmpenho)
    {
        try {
            $sicapEmpenho->update($request->except('_token'));
            return redirect()->route('sicap-empenho.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SicapEmpenho $sicapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function destroy(SicapEmpenho $sicapEmpenho)
    {
        try {
            $sicapEmpenho->delete();
            return redirect()->route('sicap-empenho.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
    */
    public function restore($id)
    {
        try {
            $sicapEmpenho = SicapEmpenho::withTrashed()->find($id);
            $sicapEmpenho->restore();
            return redirect()->route('sicap-empenho.index')->with('message_success', 'Registro restaurado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    public function _credor($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $credor = SicapCredor::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodCredor', trim($object->CodCredor))->first();

            if (!$credor) {
                SicapCredor::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodCredor' => trim($object->CodCredor),
                    'Nome' => trim($object->Nome),
                    'InscricaoEstadual' => trim($object->InscricaoEstadual),
                    'InscricaoMunicipal' => trim($object->InscricaoMunicipal),
                    'Endereco' => trim($object->Endereco),
                    'Cidade' => trim($object->Cidade),
                    'UF' => trim($object->UF),
                    'Cep' => trim($object->Cep),
                    'Fone' => trim($object->Fone),
                    'Fax' => trim($object->Fax),
                    'Tipo' => trim($object->Tipo),
                    'NumeroDoRegistro' => trim($object->NumeroDoRegistro),
                    'TipoRegistro' => trim($object->TipoRegistro)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _empenho($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $dataEmpenho = !empty(trim($object->DataEmpenho)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataEmpenho)))) : null;
            $dataProcesso = !empty(trim($object->DataProcesso)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataProcesso)))) : null;
            $dataContrato = !empty(trim($object->DataContrato)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataContrato)))) : null;
            $dataConvenio = !empty(trim($object->DataConvenio)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataConvenio)))) : null;

            $empenho = SicapEmpenho::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('NumEmpenho', trim($object->NumEmpenho))->first();

            if (!$empenho) {
                SicapEmpenho::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'NumEmpenho' => trim($object->NumEmpenho),
                    'DataEmpenho' => $dataEmpenho,
                    'Valor' => trim($object->Valor),
                    'Sinal' => trim($object->Sinal),
                    'Tipo' => trim($object->Tipo),
                    'CodOrgao' => trim($object->CodOrgao),
                    'CodUndOrcamentaria' => trim($object->CodUndOrcamentaria),
                    'CodFuncao' => trim($object->CodFuncao),
                    'CodSubFuncao' => trim($object->CodSubFuncao),
                    'CodPrograma' => trim($object->CodPrograma),
                    'CodProjAtividade' => trim($object->CodProjAtividade),
                    'CodContaDespesa' => trim($object->CodContaDespesa),
                    'CodRecVinculado' => trim($object->CodRecVinculado),
                    'ContraPartida' => trim($object->ContraPartida),
                    'CodCredor' => trim($object->CodCredor),
                    'ModalLicita' => trim($object->ModalLicita),
                    'RegistroDePreco' => trim($object->RegistroDePreco),
                    'ReferenciaLegal' => trim($object->ReferenciaLegal),
                    'NumProcesso' => trim($object->NumProcesso),
                    'DataProcesso' => $dataProcesso,
                    'NumContrato' => trim($object->NumContrato),
                    'DataContrato' => $dataContrato,
                    'NumConvenio' => trim($object->NumConvenio),
                    'DataConvenio' => $dataConvenio,
                    'NumObra' => trim($object->NumObra),
                    'Historico' => trim($object->Historico)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _undOrcamentaria($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $undOrcamentaria = SicapUndOrcamentaria::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodUndOrcamentaria', trim($object->CodUndOrcamentaria))->first();

            if (!$undOrcamentaria) {
                SicapUndOrcamentaria::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodOrgao' => trim($object->CodOrgao),
                    'CodUndOrcamentaria' => trim($object->CodUndOrcamentaria),
                    'CNPJ' => trim($object->CNPJ),
                    'Nome' => trim($object->Nome),
                    'Identificador' => trim($object->Identificador),
                    'Descricao' => trim($object->Descricao)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _funcao($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $funcao = SicapFuncao::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodFuncao', trim($object->CodFuncao))->first();

            if (!$funcao) {
                SicapFuncao::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodFuncao' => trim($object->CodFuncao),
                    'Nome' => trim($object->Nome)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _subFuncao($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $subFuncao = SicapSubFuncao::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodSubFuncao', trim($object->CodSubFuncao))->first();

            if (!$subFuncao) {
                SicapSubFuncao::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodSubFuncao' => trim($object->CodSubFuncao),
                    'Nome' => trim($object->Nome)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _programa($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $programa = SicapPrograma::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodPrograma', trim($object->CodPrograma))->first();

            if (!$programa) {
                SicapPrograma::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodPrograma' => trim($object->CodPrograma),
                    'Nome' => trim($object->Nome),
                    'Objetivo' => trim($object->Objetivo),
                    'PublicoAlvo' => trim($object->PublicoAlvo)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _projetoAtividade($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $projetoAtividade = SicapProjetoAtividade::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodProjAtividade', trim($object->CodProjAtividade))->first();

            if (!$projetoAtividade) {
                SicapProjetoAtividade::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodProjAtividade' => trim($object->CodProjAtividade),
                    'Identificador' => trim($object->Identificador),
                    'Nome' => trim($object->Nome),
                    'TipoPrograma' => trim($object->TipoPrograma)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _recursoVinculado($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $recursoVinculado = SicapRecursoVinculado::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodRecVinculado', trim($object->CodRecVinculado))->first();

            if (!$recursoVinculado) {
                SicapRecursoVinculado::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodRecVinculado' => trim($object->CodRecVinculado),
                    'Nome' => trim($object->Nome),
                    'Finalidade' => trim($object->Finalidade),
                    'Tipo' => trim($object->Tipo)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _rubricasRecDesp($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $rubricaDespesa = SicapRubricaDespesa::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodRubrica', trim($object->CodRubrica))->first();

            if (!$rubricaDespesa) {
                SicapRubricaDespesa::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodRubrica' => trim($object->CodRubrica),
                    'Especificacao' => trim($object->Especificacao),
                    'TipoNivelConta' => trim($object->TipoNivelConta),
                    'NumNivelConta' => trim($object->NumNivelConta)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _pagamento($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $dataPagamento = !empty(trim($object->DataPagamento)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataPagamento)))) : null;

            $pagamento = SicapPagamento::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('NumPagamento', trim($object->NumPagamento))->first();

            if (!$pagamento) {
                SicapPagamento::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'NumEmpenho' => trim($object->NumEmpenho),
                    'NumPagamento' => trim($object->NumPagamento),
                    'NumLiquidacao' => trim($object->NumLiquidacao),
                    'DataPagamento' => $dataPagamento,
                    'Valor' => trim($object->Valor),
                    'Sinal' => trim($object->Sinal),
                    'Historico' => trim($object->Historico),
                    'CodOperacao' => trim($object->CodOperacao),
                    'NumProcesso' => trim($object->NumProcesso)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _pagamentoFinanceiro($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $dataLiquidacao = !empty(trim($object->DataLiquidacao)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataLiquidacao)))) : null;

            $pagamentoFinanceiro = SicapPagamentoFinanceiro::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('NumPagamento', trim($object->NumPagamento))->first();

            if (!$pagamentoFinanceiro) {
                SicapPagamentoFinanceiro::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'NumEmpenho' => trim($object->NumEmpenho),
                    'NumPagamento' => trim($object->NumPagamento),
                    'CodContaBalancete' => trim($object->CodContaBalancete),
                    'Valor' => trim($object->Valor),
                    'CodBanco' => trim($object->CodBanco),
                    'CodAgenciaBanco' => trim($object->CodAgenciaBanco),
                    'NumContaBancaria' => trim($object->NumContaBancaria),
                    'NumDocumento' => trim($object->NumDocumento),
                    'NumLiquidacao' => trim($object->NumLiquidacao),
                    'Sinal' => trim($object->Sinal),
                    'TipoPagamento' => trim($object->TipoPagamento),
                    'TipoConta' => trim($object->TipoConta)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _liquidacao($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {

            $dataLiquidacao = !empty(trim($object->DataLiquidacao)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataLiquidacao)))) : null;

            $liquidacao = SicapLiquidacao::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('NumLiquidacao', trim($object->NumLiquidacao))->first();

            if (!$liquidacao) {
                SicapLiquidacao::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'NumEmpenho' => trim($object->NumEmpenho),
                    'NumLiquidacao' => trim($object->NumLiquidacao),
                    'DataLiquidacao' => $dataLiquidacao,
                    'Valor' => trim($object->Valor),
                    'Sinal' => trim($object->Sinal),
                    'CodOperacao' => trim($object->CodOperacao),
                    'NumProcesso' => trim($object->NumProcesso),
                    'CodCredor' => trim($object->CodCredor),
                    'Referencia' => trim($object->Referencia),
                    'Historico' => trim($object->Historico)
                ]);
            }
        }
        Storage::delete($file_path);
    }
}
