<?php

namespace App\Http\Controllers;

use App\Models\Example;
use App\Http\Requests\ExampleRequest;

class ExampleController extends Controller
{
    public function __construct() {
        $page_title = 'Exemplos';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_description = 'Listagem';
        $examples = Example::orderBy('id','desc')->paginate(20);
        return view('adm.examples.index', compact('page_description', 'examples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.examples.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ExampleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExampleRequest $request)
    {
        $example = Example::create($request->except('_token'));
        return redirect()->route('examples.index')->with('message_success', 'Cadastro feito com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function show(Example $example)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function edit(Example $example)
    {
        $page_description = 'Edição';
        return view('adm.examples.edit', compact('page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ExampleRequest  $request
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function update(ExampleRequest $request, Example $example)
    {
        $example->update($request->except('_token'));
        return redirect()->route('examples.index')->with('message_success', 'Registro editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function destroy(Example $example)
    {
        $example->delete();
        return redirect()->route('examples.index')->with('message_success', 'Registro removido com sucesso!');
    }
}
