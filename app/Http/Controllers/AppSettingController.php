<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Http\Requests\AppSettingRequest;

class AppSettingController extends Controller
{
    public function __construct() {
        $page_title = 'Configurações';
        view()->share('page_title', $page_title);
        $this->authorizeResource(AppSetting::class, 'app_setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $appSetting = AppSetting::orderBy('id','desc')->first();
        if ($appSetting) {
            return $this->edit($appSetting);
        } else {
            return $this->create();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.app-settings.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AppSettingRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AppSettingRequest $request)
    {
        $appSetting = AppSetting::create($request->except('_token'));
        return redirect()->route('app-settings.index')->with('message_success', 'Cadastro feito com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AppSetting $appSetting
     * @return \Illuminate\Http\Response
    */
    public function show(AppSetting $appSetting)
    {
        $page_description = 'Detalhes';
        return view('adm.app-settings.show', compact('page_description', 'appSetting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AppSetting $appSetting
     * @return \Illuminate\Http\Response
    */
    public function edit(AppSetting $appSetting)
    {
        $page_description = 'Edição';
        return view('adm.app-settings.edit', compact('page_description', 'appSetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AppSettingRequest  $request
     * @param  \App\Models\AppSetting $appSetting
     * @return \Illuminate\Http\Response
    */
    public function update(AppSettingRequest $request, AppSetting $appSetting)
    {
        try {
            $data = $request->except('_token');
            $data['view_last_update'] = $request->has('view_last_update');
            $data['display_covid'] = $request->has('display_covid');
            if ($request->file('logo')) {
                $filepath = $request->logo->store('app-settings/logo');
                $data['logo'] = 'storage/'.$filepath;
            }
            if ($request->file('cover')) {
                $filepath = $request->cover->store('app-settings/cover');
                $data['cover'] = 'storage/'.$filepath;
            }
            $appSetting->update($data);
            return redirect()->route('app-settings.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('app-settings.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AppSetting $appSetting
     * @return \Illuminate\Http\Response
    */
    public function destroy(AppSetting $appSetting)
    {
        $appSetting->delete();
        return redirect()->route('app-settings.index')->with('message_success', 'Registro removido com sucesso!');
    }
}
