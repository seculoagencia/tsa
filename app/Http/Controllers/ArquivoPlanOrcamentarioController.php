<?php

namespace App\Http\Controllers;

use App\Models\ArquivoPlanOrcamentario;
use App\Http\Requests\ArquivoPlanOrcamentarioRequest;
use App\Models\Attachment;
use App\Models\TipoPlanejamentoOrcamentario;

class ArquivoPlanOrcamentarioController extends Controller
{
    public function __construct() {
        $page_title = 'Arquivo Plan Orcamentarios';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $arquivoPlanOrcamentarios = ArquivoPlanOrcamentario::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.arquivo-plan-orcamentarios.index', compact('page_description', 'offset', 'arquivoPlanOrcamentarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        $tipos = TipoPlanejamentoOrcamentario::orderBy('name','asc')->get();
        return view('adm.arquivo-plan-orcamentarios.create', compact('page_description', 'tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ArquivoPlanOrcamentarioRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ArquivoPlanOrcamentarioRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);
            
            $arquivoPlanOrcamentario = ArquivoPlanOrcamentario::create($request->except(['_token','file']) + ['attachment_id' => $attachment->id]);
            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArquivoPlanOrcamentario $arquivoPlanOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function show(ArquivoPlanOrcamentario $arquivoPlanOrcamentario)
    {
        $page_description = 'Detalhes';
        return view('adm.arquivo-plan-orcamentarios.show', compact('page_description', 'arquivoPlanOrcamentario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArquivoPlanOrcamentario $arquivoPlanOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function edit(ArquivoPlanOrcamentario $arquivoPlanOrcamentario)
    {
        $page_description = 'Edição';
        $tipos = TipoPlanejamentoOrcamentario::orderBy('name','asc')->get();
        return view('adm.arquivo-plan-orcamentarios.edit', compact('page_description', 'arquivoPlanOrcamentario','tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ArquivoPlanOrcamentarioRequest  $request
     * @param  \App\Models\ArquivoPlanOrcamentario $arquivoPlanOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function update(ArquivoPlanOrcamentarioRequest $request, ArquivoPlanOrcamentario $arquivoPlanOrcamentario)
    {
        try {
            if ($request->hasFile('file')) {
                $original_name = $request->file('file')->getClientOriginalName();
                $path = $request->file->store('attachments');
                $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

                $arquivoPlanOrcamentario->update($request->except(['_token','file']) + ['attachment_id' => $attachment->id]);
            } else {
                $arquivoPlanOrcamentario->update($request->except('_token'));
            }

            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArquivoPlanOrcamentario $arquivoPlanOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function destroy(ArquivoPlanOrcamentario $arquivoPlanOrcamentario)
    {
        try {
            $arquivoPlanOrcamentario->delete();
            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-plan-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }
}
