<?php

namespace App\Http\Controllers;

use App\Models\AttachmentDocument;
use App\Models\Attachment;
use App\Models\Document;
use App\Http\Requests\AttachmentDocumentRequest;

class AttachmentDocumentController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Documents';
        view()->share('page_title', $page_title);
    }

    public function store(AttachmentDocumentRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $document = Document::find($request->document_id);
            $document->attachments()->syncWithoutDetaching($attachment->id);
            
            return redirect()->route('documents.edit', $request->document_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('documents.edit', $request->document_id)->with('message_error', $th->getMessage());
        }
    }
}
