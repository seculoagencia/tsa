<?php

namespace App\Http\Controllers;

use App\Models\Verba;
use App\Http\Requests\VerbaRequest;

class VerbaController extends Controller
{
    public function __construct() {
        $page_title = 'Verbas Indenizatórias';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Verba::class, 'verbas' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
	$verbas = Verba::where (function ($q) {
		if (request()->has('id') && request('id') != '') {
			$q->where('id',request('id'));
		}
	        if (request()->has('date') && request('date') != '') {
			$q->whereData('data',request('date'));
		}
	        if (request()->has('year') && request('year') != '') {
        	        $q->whereYear('data',request('year'));
	        }
            	if (request()->has('month') && request('month') != '') {
                	$q->whereMonth('data',request('month'));
		}
                if (request()->has('beneficiario') && request('beneficiario') != '') {
                        $q->where('beneficiario',request('beneficiario'));
                }
            	if (request()->has('referencia') && request('referencia') != '') {
                	$q->where('referencia',request('referencia'));
	        }
                if (request()->has('valor') && request('valor') != '') {
                        $q->where('valor',request('valor'));
                }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.verbas.index', compact('page_description', 'offset', 'verbas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.verbas.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\VerbaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(VerbaRequest $request)
    {
        try {
            $verba = Verba::create($request->except('_token'));
            return redirect()->route('verbas.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('verbas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Verba $verba
     * @return \Illuminate\Http\Response
    */
    public function show(Verba $verba)
    {
        $page_description = 'Detalhes';
        return view('adm.verbas.show', compact('page_description', 'verba'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Verba $verba
     * @return \Illuminate\Http\Response
    */
    public function edit(Verba $verba)
    {
        $page_description = 'Edição';
        $offset = request()->has('offset') ? request('offset') : 10;
        $attachments = $verba->attachments()->paginate($offset);
        $table = 'verbas';
        $item_id = $verba->id;
        return view('adm.verbas.edit', compact('page_description', 'verbas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\VerbaRequest  $request
     * @param  \App\Models\Verba $verba
     * @return \Illuminate\Http\Response
    */
    public function update(VerbaRequest $request, Verba $verba)
    {
        try {
            $verba->update($request->except('_token'));
            return redirect()->route('verbas.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('verbas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Verba $verba
     * @return \Illuminate\Http\Response
    */
    public function destroy(Verba $verba)
    {
        try {
            foreach ($verba->attachments as $key => $attachment) {
                $verba->attachments()->detach($attachment->id);
                $attachment->delete();
            }

            $verba->delete();
            return redirect()->route('verbas.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('verbas.index')->with('message_error', $th->getMessage());
        }
    }
}
