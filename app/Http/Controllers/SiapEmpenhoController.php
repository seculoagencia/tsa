<?php

namespace App\Http\Controllers;

use App\Models\SiapEmpenho;
use App\Models\SiapCredor;
use App\Models\SiapUndOrcamentaria;
use App\Models\SiapFuncao;
use App\Models\SiapSubFuncao;
use App\Models\SiapPagamento;
use App\Models\SiapLiquidacao;
use App\Http\Requests\SiapEmpenhoRequest;
use App\Models\SiapAcao;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SiapEmpenhoController extends Controller
{
    public function __construct() {
        $page_title = 'Empenhos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(SiapEmpenho::class, 'siap_empenho');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        // $siapEmpenho = SiapEmpenho::orderBy('DataEmissao','desc')->paginate($offset);

        $siapEmpenho = SiapEmpenho::withTrashed()->where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->where('Exercicio',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('DataEmissao',request('month'));
            }
            if (request()->has('numero') && request('numero') != '') {
                $q->where('NumEmpenho','like','%'.request('numero').'%');
            }
            if (request()->has('credor') && request('credor') != '') {
                $q->whereIn('Credor', SiapCredor::select('Codigo')->where('Nome','like','%'.request('credor').'%'));
            }
        })->orderBy('DataEmissao','desc')->paginate($offset);


        return view('adm.siap-empenho.index', compact('page_description', 'offset', 'siapEmpenho'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.siap-empenho.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SiapEmpenhoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(SiapEmpenhoRequest $request)
    {
        try {

            $db = DB::transaction(function () use ($request) {

                foreach ($request->file('siap') as $file) {

                    switch (explode('.',$file->getClientOriginalName())[0]) {
                        case 'Fornecedor':

                            $this->_credor($file);
                            break;
                        case 'Empenho':

                            $this->_empenho($file);
                            break;
                        case 'UnidadeOrcamentaria':

                            $this->_undOrcamentaria($file);
                            break;
                        case 'Funcao':

                            $this->_funcao($file);
                            break;
                        case 'SubFuncao':

                            $this->_subFuncao($file);
                            break;
                        case 'Acao':

                            $this->_acao($file);
                            break;
                        case 'PagamentoEmpenho':

                            $this->_pagamento($file);
                            break;
                        case 'LiquidacaoEmpenho':

                            $this->_liquidacao($file);
                            break;
                        case 'AnulacaoDotacao':
                            $this->_empenho($file);
                            break;

                    }
                }
            });

             return redirect()->route('siap-empenho.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('siap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiapEmpenho $siapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function show(SiapEmpenho $siapEmpenho)
    {
        $page_description = 'Detalhes';
        return view('adm.siap-empenho.show', compact('page_description', 'siapEmpenho'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiapEmpenho $siapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function edit(SiapEmpenho $siapEmpenho)
    {
        $page_description = 'Edição';
        return view('adm.siap-empenho.edit', compact('page_description','empenho','attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SiapEmpenhoRequest  $request
     * @param  \App\Models\SiapEmpenho $siapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function update(SiapEmpenhoRequest $request, SiapEmpenho $siapEmpenho)
    {
        try {
            $siapEmpenho->update($request->except('_token'));
            return redirect()->route('siap-empenho.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('siap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiapEmpenho $siapEmpenho
     * @return \Illuminate\Http\Response
    */
    public function destroy(SiapEmpenho $siapEmpenho)
    {
        try {
            $siapEmpenho->delete();
            return redirect()->route('siap-empenho.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('siap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
    */
    public function restore($id)
    {
        try {
            $siapEmpenho = SiapEmpenho::withTrashed()->find($id);
            $siapEmpenho->restore();
            return redirect()->route('siap-empenho.index')->with('message_success', 'Registro restaurado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('siap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    public function _credor($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString =  utf8_encode(file_get_contents(public_path('storage/'.$file_path)));
        $xml = utf8_encode(str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString));
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        for ($i=0; $i < count($xmlObject->Fornecedor); $i++) {
            $object = $xmlObject->Fornecedor[$i];

            $credor = SiapCredor::where('Codigo', trim($object->Codigo))
            ->where('Nome', trim($object->Nome))->where('CNAE', trim($object->CNAE))
            ->first();

            if (!$credor) {
                SiapCredor::create([
                    'Codigo' => trim($object->Codigo),
                    'Nome' => trim($object->Nome),
                    'CNAE' => trim($object->CNAE),
                    'Tipo' => trim($object->Tipo)
                ]);
            }
        }

        Storage::delete($file_path);
    }

    public function _empenho($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = utf8_encode(file_get_contents(public_path('storage/'.$file_path)));
        $xml = utf8_encode(str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString));
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        for ($i=0; $i < count($xmlObject->Empenho); $i++) {
            $object = $xmlObject->Empenho[$i];

            $empenho = SiapEmpenho::where('CodigoUnidadeGestora', trim($object->CodigoUnidadeGestora))
            ->where('Ano', trim($object->Ano))->where('Mes', trim($xmlObject->Mes))
            ->where('NumeroEmpenho', trim($object->NumeroEmpenho))->first();

            if (!$empenho ) {
                $caguei = [
                    'NumeroEmpenho' => trim($object->NumeroEmpenho),
                    'Tipo' => trim($object->Tipo),
                    'Ano' => intval( trim($object->Ano)),
                    'Mes' => intval(trim($xmlObject->Mes)),
                    'CodigoUnidadeGestora' => trim($object->CodigoUnidadeGestora),
                    'CodigoUnidadeOrcamentaria' => trim($object->CodigoUnidadeOrcamentaria),
                    'FuncaoSubfuncao' => trim($object->FuncaoSubfuncao),
                    'CodigoPrograma' => trim($object->CodigoPrograma),
                    'NumeroAcao' => trim($object->NumeroAcao),
                    'ContaContabil' => trim($object->ContaContabil),
                    'NaturezaDespesa' => trim($object->NaturezaDespesa),
                    'DataEmissao' => trim($object->DataEmissao),
                    'TipoContratacao' => intval(trim($object->TipoContratacao)),
                    'NumeroContratacao' => trim($object->NumeroContratacao) == ''? "nulo" : trim($object->NumeroContratacao),
                    'NumeroLicitacao' => trim($object->NumeroLicitacao)== ''? "nulo" : trim($object->NumeroLicitacao),
                    'NumeroContrato' => trim($object->NumeroContrato)== ''? "nulo" :trim($object->NumeroContrato),
                    'NumeroConvenio' => trim($object->NumeroConvenio)== ''? "nulo" : trim($object->NumeroConvenio),
                    'NumeroProcesso' => trim($object->NumeroProcesso) == ''? "nulo" : trim($object->NumeroProcesso),
                    'Credor' => trim($object->Credor),
                    'Valor' => floatval(trim($object->Valor))
                ];

                $teste =  SiapEmpenho::create($caguei);

            }
        }

        Storage::delete($file_path);
    }

    public function _undOrcamentaria($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        for ($i=0; $i < count($xmlObject->UnidadeOrcamentaria); $i++) {
            $object = $xmlObject->UnidadeOrcamentaria[$i];

            $undOrcamentaria = SiapUndOrcamentaria::where('Codigo', trim($object->Codigo))->first();

            if (!$undOrcamentaria) {
                SiapUndOrcamentaria::create([
                    'Codigo' => trim($object->Codigo),
                    'Descricao' => $object->Descricao
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _funcao($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        foreach ($xmlObject as $key => $object) {

            $funcao = SiapFuncao::where('Codigo', trim($object->Codigo))->first();

            if (!$funcao) {
                SiapFuncao::create([
                    'Codigo' => trim($object->Codigo),
                    'Descricao' => $object->Descricao
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _subFuncao($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        foreach ($xmlObject as $key => $object) {

            $subFuncao = SiapSubFuncao::where('Codigo', trim($object->Codigo))->first();

            if (!$subFuncao) {
                SiapSubFuncao::create([
                    'Codigo' => trim($object->Codigo),
                    'Descricao' => $object->Descricao
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _acao($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        for ($i=0; $i < count($xmlObject->Acao); $i++) {
            $object = $xmlObject->Acao[$i];

            $acao = SiapAcao::where('Numero', trim($object->Numero))
            ->where('Tipo', trim($object->Tipo))
            ->where('Exercicio', trim($xmlObject->Exercicio))
            ->where('Mes', trim($xmlObject->Mes))
            ->first();

            if (!$acao) {
                SiapAcao::create([
                    'Numero' => trim($object->Numero),
                    'Descricao' => $object->Descricao,
                    'Tipo' => trim($object->Tipo),
                    'CodigoUnidadeGestora' => trim($object->CodigoUnidadeGestora),
                    'CodigoUnidadeOrcamentaria' => trim($object->CodigoUnidadeOrcamentaria),
                    'CodigoFuncao' => trim($object->CodigoFuncao),
                    'CodigoSubfuncao' => trim($object->CodigoSubfuncao),
                    'CodigoPrograma' => trim($object->CodigoPrograma),
                    'Exercicio' => trim($xmlObject->Exercicio),
                    'Mes' => trim($xmlObject->Mes)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _pagamento($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = utf8_encode(file_get_contents(public_path('storage/'.$file_path)));
        $xml = utf8_encode(str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString));
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        for ($i=0; $i < count($xmlObject->PagamentoEmpenho); $i++) {
            $object = $xmlObject->PagamentoEmpenho[$i];

            $pagamento = SiapPagamento::where('Numero', trim($object->Numero))
            ->where('NumeroEmpenho', trim($object->NumeroEmpenho))
            ->where('Exercicio', trim($xmlObject->Exercicio))
            ->where('Mes', intval(trim($xmlObject->Mes)))
            ->where('Data', intval(trim($object->Data)))->first();

            if (!$pagamento) {
                SiapPagamento::create([
                    'Numero' => trim($object->Numero),
                    'NumeroEmpenho' => trim($object->NumeroEmpenho),
                    'CodigoUnidadeOrcamentaria' => trim($object->CodigoUnidadeOrcamentaria),
                    'ContaContabil' => trim($object->ContaContabil),
                    'Data' => trim($object->Data),
                    'Valor' => trim($object->Valor),
                    'Descricao' => $object->Descricao,
                    'Exercicio' => intval(trim($xmlObject->Exercicio)),
                    'Mes' => intval(trim($xmlObject->Mes))
                ]);
            }
        }

        Storage::delete($file_path);
    }

    public function _liquidacao($file)
    {
        $file_path = $file->store('siap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);


        for ($i=0; $i < count($xmlObject->LiquidacaoEmpenho); $i++) {
            $object = $xmlObject->LiquidacaoEmpenho[$i];

            $liquidacao = SiapLiquidacao::where('Numero', trim($object->Numero))
            ->where('NumeroEmpenho', trim($object->NumeroEmpenho))->where('Data', trim($object->Data))
            ->where('TipoDocumento', trim($object->TipoDocumento))
            ->where('Exercicio', trim($xmlObject->Exercicio))
            ->where('Mes', trim($xmlObject->Mes))->first();

            if (!$liquidacao) {
                SiapLiquidacao::create([
                    'Numero' => trim($object->Numero),
                    'NumeroEmpenho' => trim($object->NumeroEmpenho),
                    'CodigoUnidadeOrcamentaria' => trim($object->CodigoUnidadeOrcamentaria),
                    'ContaContabil' => trim($object->ContaContabil),
                    'Data' => trim($object->Data),
                    'TipoDocumento' => trim($object->TipoDocumento),
                    'NumeroDocumentoFiscal' => trim($object->NumeroDocumentoFiscal),
                    'ChaveAcesso' => trim($object->ChaveAcesso),
                    'SerieDocumentoFiscal' => trim($object->SerieDocumentoFiscal),
                    'Valor' => trim($object->Valor),
                    'Justificativa' => $object->Justificativa,
                    'Exercicio' => trim($xmlObject->Exercicio),
                    'Mes' => trim($xmlObject->Mes)
                ]);
            }
        }

        Storage::delete($file_path);
    }
}
