<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\TypeDocument;
use App\Http\Requests\DocumentRequest;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    public function __construct() {
        $page_title = 'Documentos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Document::class, 'document');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $documents = Document::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
            if (request()->has('year') && request('year') != '') {
                $q->where('year',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->where('month',request('month'));
            }
            if (request()->has('type_document_id') && request('type_document_id') != '') {
                $q->where('type_document_id',request('type_document_id'));
            }
        })->orderBy('year','desc')->orderBy('month','desc')->orderBy('name','desc')->paginate($offset);

        $typeDocuments = TypeDocument::orderBy('name','asc')->get();

        return view('adm.documents.index', compact('page_description', 'offset', 'documents', 'typeDocuments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        $typeDocuments = TypeDocument::orderBy('name','asc')->get();
        return view('adm.documents.create', compact('page_description','typeDocuments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DocumentRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(DocumentRequest $request)
    {
        try {
            $year = explode('-',$request->month_year)[0];
            $month = explode('-',$request->month_year)[1];

            $data = $request->except(['_token','month_year']);
            $data = $data + ['year'=>$year, 'month'=>$month, 'user_id'=>Auth::user()->id];

            $document = Document::create($data);
            return redirect()->route('documents.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('documents.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document $document
     * @return \Illuminate\Http\Response
    */
    public function show(Document $document)
    {
        $page_description = 'Detalhes';
        return view('adm.documents.show', compact('page_description', 'document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document $document
     * @return \Illuminate\Http\Response
    */
    public function edit(Document $document)
    {
        $page_description = 'Edição';
        $offset = request()->has('offset') ? request('offset') : 10;
        $typeDocuments = TypeDocument::orderBy('name','asc')->get();
        $attachments = $document->attachments()->paginate($offset);
        $table = 'documents';
        $item_id = $document->id;
        return view('adm.documents.edit', compact('page_description', 'document', 'typeDocuments', 'attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DocumentRequest  $request
     * @param  \App\Models\Document $document
     * @return \Illuminate\Http\Response
    */
    public function update(DocumentRequest $request, Document $document)
    {
        $document->update($request->except('_token'));
        return redirect()->route('documents.index')->with('message_success', 'Registro editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document $document
     * @return \Illuminate\Http\Response
    */
    public function destroy(Document $document)
    {
        try {
            foreach ($document->attachments as $key => $attachment) {
                $document->attachments()->detach($attachment->id);
                $attachment->delete();
            }
            $document->delete();
            return redirect()->route('documents.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('documents.index')->with('message_error', $th->getMessage());
        }
    }
}
