<?php

namespace App\Http\Controllers;

use App\Models\AttachmentContrato;
use App\Models\Attachment;
use App\Models\Contrato;
use App\Http\Requests\AttachmentContratoRequest;

class AttachmentContratoController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Contratos';
        view()->share('page_title', $page_title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentContratoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentContratoRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $contrato = Contrato::find($request->contrato_id);
            $contrato->attachments()->syncWithoutDetaching($attachment->id);
            return redirect()->route('contratos.edit', $request->contrato_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('contratos.edit', $request->contrato_id)->with('message_error', $th->getMessage());
        }
    }
}
