<?php

namespace App\Http\Controllers;

use App\Exports\OpenDataExport;
use App\Models\OptionSearch;
use App\Models\SubMenu;
use Illuminate\Http\Request;
use App\Mail\MailPedidoEsic;
use App\Models\Pedido;
use App\Models\Resposta;
use App\Models\AppSetting;
use App\Models\Contrato;
use App\Models\CovidResumo;
use App\Models\Diaria;
use App\Models\Document;
use App\Models\Empenho;
use App\Models\Estrutura;
use App\Models\Receita;
use App\Models\Responsabilidade;
use App\Models\SagresEmpenho;
use App\Models\Servidor;
use App\Models\SiapCredor;
use App\Models\SiapEmpenho;
use App\Models\SicapCargosServidores;
use App\Models\SicapCredor;
use App\Models\SicapEmpenho;
use App\Models\SicapReceita;
use App\Models\SicapServidor;
use App\Models\TipoPlanejamentoOrcamentario;
use App\Models\TypeDocument;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\ArrayToXml\ArrayToXml;
use App\Models\Verba;


class PagesController extends Controller
{
    public function __construct() {
        $appSetting = AppSetting::first();
        view()->share('appSetting', $appSetting);
    }

    public function index()
    {
        $page_title = 'Portal da Transparência';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $subOptionsCompleted = SubMenu::where('visible','=',1)->get();

        return view('pages.index', compact('page_title', 'page_description','optionsCompleted','subOptionsCompleted'));
    }

    public function orcamentos()
    {
        $page_title = 'Planejamento Orçamentário';
        $page_description = '';

        $offset = request()->has('offset') ? request('offset') : 10;
        $tipos = TipoPlanejamentoOrcamentario::with('arquivos')->orderBy('name','asc')->get();
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        if (request()->has("export") && request('export') != "") {
            return $this->_export($tipos, 'orcamentos');
        } else {
            $last_update = TipoPlanejamentoOrcamentario::orderBy('created_at','desc')->first();
            return view('pages.orcamentos', compact('page_title', 'page_description','tipos','offset','last_update','optionsCompleted'));
        }

    }


    public function despesas()
    {
        $page_title = 'Despesas';
        $page_description = '';
        $appSetting = AppSetting::orderBy('id','desc')->first();
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;

        switch ($appSetting->prioridade) {
            case 'manual':
                $empenhos = Empenho::where('covid19',false)
                    ->where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->whereYear('data',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('data',request('month'));
                    }
                    if (request()->has('numero') && request('numero') != '') {
                        $q->where('numero','like','%'.request('numero').'%');
                    }
                    if (request()->has('credor') && request('credor') != '') {
                        $q->where('credor','like','%'.request('credor').'%');
                    }
                })->orderBy('data','desc')->paginate($offset);
                $last_update = Empenho::where('covid19',false)->orderBy('created_at', 'desc')->first();
                break;

            case 'sicap':
                $empenhos = SicapEmpenho::where(function ($q) {
                    $q->whereNull('deleted_at');
                    if (request()->has('year') && request('year') != '') {
                        $q->where('Exercicio',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('DataEmpenho',request('month'));
                    }
                    if (request()->has('numero') && request('numero') != '') {
                        $q->where('NumEmpenho','like','%'.request('numero').'%');
                    }
                    if (request()->has('credor') && request('credor') != '') {
                        $q->whereIn('codCredor', SicapCredor::select('codCredor')->where('Nome','like','%'.request('credor').'%'));
                    }
                })->orderBy('DataEmpenho','desc')->paginate($offset);
                $last_update = SicapEmpenho::orderBy('created_at', 'desc')->first();
                break;

            case 'siap':
                $empenhos = SiapEmpenho::where(function ($q) {
                    $q->whereNull('deleted_at');
                    if (request()->has('year') && request('year') != '') {
                        $q->where('Ano',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('Mes',request('month'));
                    }
                    if (request()->has('numero') && request('numero') != '') {
                        $q->where('NumeroEmpenho','like','%'.request('numero').'%');
                    }
                    if (request()->has('credor') && request('credor') != '') {
                        $q->whereIn('Credor', SiapCredor::select('Codigo')->where('Nome','like','%'.request('credor').'%'));
                    }
                })->orderBy('DataEmissao','desc')->paginate($offset);
                $last_update = SiapEmpenho::orderBy('created_at', 'desc')->first();
                break;

            case 'sagres':
                $empenhos = SagresEmpenho::where(function ($q) {
                    // if (request()->has('name') && request('name') != '') {
                    //     $q->where('name','like','%'.request('name').'%');
                    // }
                })->orderBy('id','desc')->paginate($offset);
                $last_update = SagresEmpenho::orderBy('created_at', 'desc')->first();
                break;

            default:
                $empenhos = Empenho::where('covid19',false)
                    ->where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->whereYear('data',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('data',request('month'));
                    }
                    if (request()->has('numero') && request('numero') != '') {
                        $q->where('numero','like','%'.request('numero').'%');
                    }
                    if (request()->has('credor') && request('credor') != '') {
                        $q->where('credor','like','%'.request('credor').'%');
                    }
                })->orderBy('data','desc')->paginate($offset);
                $last_update = Empenho::where('covid19',false)->orderBy('created_at', 'desc')->first();
                break;
        }

        if (request()->has("export") && request('export') != "") {
            return $this->_export($empenhos, 'despesas');
        } else {
            return view('pages.despesas', compact('page_title', 'page_description','empenhos','offset','last_update','optionsCompleted'));
        }


    }

    public function receitas()
    {
        $page_title = 'Receitas';
        $page_description = '';

        $offset = request()->has('offset') ? request('offset') : 10;

        $appSetting = AppSetting::orderBy('id','desc')->first();
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        switch ($appSetting->prioridade_receita) {
            case 'manual':
                $receitas = Receita::where('covid19',false)
                    ->where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->whereYear('data',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('data',request('month'));
                    }
                    if (request()->has('codigo') && request('codigo') != '') {
                        $q->where('codigo','like','%'.request('codigo').'%');
                    }
                    if (request()->has('descricao') && request('descricao') != '') {
                        $q->where('descricao','like','%'.request('descricao').'%');
                    }
                })->orderBy('data','desc')->paginate($offset);
                $last_update = Receita::where('covid19',false)->orderBy('created_at', 'desc')->first();
                break;

            case 'sicap':
                $receitas = SicapReceita::where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->where('Exercicio',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('DataArrecadacao',request('month'));
                    }
                })->orderBy('DataArrecadacao','desc')->paginate($offset);
                $last_update = SicapReceita::orderBy('created_at', 'desc')->first();
                break;

            default:
                $receitas = Receita::where('covid19',false)
                    ->where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->whereYear('data',request('year'));
                    }
                    if (request()->has('month') && request('month') != '') {
                        $q->whereMonth('data',request('month'));
                    }
                    if (request()->has('codigo') && request('codigo') != '') {
                        $q->where('codigo','like','%'.request('codigo').'%');
                    }
                    if (request()->has('descricao') && request('descricao') != '') {
                        $q->where('descricao','like','%'.request('descricao').'%');
                    }
                })->orderBy('data','desc')->paginate($offset);
                $last_update = Receita::where('covid19',false)->orderBy('created_at', 'desc')->first();
                break;
        }

        if (request()->has("export") && request('export') != "") {
            return $this->_export($receitas, 'receitas');
        } else {
            return view('pages.receitas', compact('page_title', 'page_description','receitas','offset','last_update','optionsCompleted'));
        }

    }

    public function servidores()
    {
        $page_title = 'Servidores';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;

        $appSetting = AppSetting::orderBy('id','desc')->first();
        $countCpf = 0;
        $countAdmissao = 0;
        switch ($appSetting->prioridade_servidores) {
            case 'manual':
                $countCpf = Servidor::select('cpf')->count('cpf');
                $countAdmissao = Servidor::select('admissao')->count('admissao');
                $servidores = Servidor::where(function ($q) {
                    if (request()->has('nome') && request('nome') != '') {
                        $q->where('nome','like','%'.request('nome').'%');
                    }
                    if (request()->has('referencia') && request('referencia') != '') {
                        $q->where('referencia',request('referencia'));
                    }
                })->orderBy('id','desc')->paginate($offset);
                $last_update = Servidor::orderBy('created_at', 'desc')->first();
                break;

            case 'sicap':
                $servidores = SicapServidor::where(function ($q) {
                    if (request()->has('year') && request('year') != '') {
                        $q->where('Exercicio',request('year'));
                    }
                    if (request()->has('nome') && request('nome') != '') {
                        $q->where('Nome','like','%'.request('nome').'%');
                    }
                    if (request()->has('cargo') && request('cargo') != '') {
                        $q->whereIn('codCargo', SicapCargosServidores::select('codCargo')->where('Descricao','like','%'.request('cargo').'%'));
                    }
                })->orderBy('Exercicio','desc')->orderBy('Nome','asc')->paginate($offset);
                $last_update = SicapServidor::orderBy('created_at', 'desc')->first();
                break;

            default:
                $servidores = Servidor::where(function ($q) {
                    if (request()->has('nome') && request('nome') != '') {
                        $q->where('nome','like','%'.request('nome').'%');
                    }
                    if (request()->has('referencia') && request('referencia') != '') {
                        $q->where('referencia',request('referencia'));
                    }
                })->orderBy('id','desc')->paginate($offset);
                $last_update = Servidor::orderBy('created_at', 'desc')->first();
                break;
        }

        if (request()->has("export") && request('export') != "") {
            return $this->_export($servidores, 'servidores');
        } else {
            return view('pages.servidores', compact('page_title', 'page_description','servidores','offset','last_update','countCpf','countAdmissao','optionsCompleted'));
        }

    }

    public function licitacoes()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $page_title = 'Licitações';
        $page_description = '';

        $offset = request()->has('offset') ? request('offset') : 10;
        $contratos = Contrato::where('covid19',false)
            ->where('tipo','W')
            ->orWhere('tipo','I')
            ->orWhere('tipo','L')
            ->orWhere('tipo','R')
            ->orWhere('tipo','P')
            ->orWhere('tipo','O')
            ->orWhere('tipo','T')
            ->orWhere('tipo','A')
        ->where(function ($q) {
            if (request()->has('orgao') && request('orgao') != '') {
                $q->where('orgao','like','%'.request('orgao').'%');
            }
            if (request()->has('modalidade') && request('modalidade') != '') {
                $q->where('modalidade','like','%'.request('modalidade').'%');
            }
            if (request()->has('data_hora') && request('data_hora') != '') {
                $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
            }
            if (request()->has('valor') && request('valor') != '') {
                $q->where('valor', str_replace(['.',','],['','.'],request('valor')));
            }
            if (request()->has('status') && request('status') != '') {
                $q->where('status', request('status'));
            }
            if (request()->has('vencedor') && request('vencedor') != '') {
                $q->where('vencedor','like','%'.request('vencedor').'%');
            }
            if (request()->has('objeto') && request('objeto') != '') {
                $q->where('objeto','like','%'.request('objeto').'%');
            }
        })->orderBy('data_hora','desc')->paginate($offset);
        $last_update = Contrato::where('covid19',false)->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {
            return $this->_export($contratos, 'licitacoes');
        } else {
            return view('pages.contratos', compact('page_title', 'offset', 'contratos','last_update','optionsCompleted'));
        }

    }


    public function contratos(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

            $page_title = 'Contrato';
            $page_description = '';
            $contratos = Contrato::where('covid19', false)
                ->where('tipo','C')
                ->orWhere('tipo','D')
                ->orWhere('tipo','F')
                ->orWhere('tipo','J')
                ->where(function ($q) {
                    if (request()->has('orgao') && request('orgao') != '') {
                        $q->where('orgao', 'like', '%' . request('orgao') . '%');
                    }
                    if (request()->has('modalidade') && request('modalidade') != '') {
                        $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                    }
                    if (request()->has('data_hora') && request('data_hora') != '') {
                        $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                    }
                    if (request()->has('valor') && request('valor') != '') {
                        $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                    }
                    if (request()->has('status') && request('status') != '') {
                        $q->where('status', request('status'));
                    }
                    if (request()->has('vencedor') && request('vencedor') != '') {
                        $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                    }
                    if (request()->has('objeto') && request('objeto') != '') {
                        $q->where('objeto', 'like', '%' . request('objeto') . '%');
                    }
                })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->orderBy('created_at', 'desc')->first();

            if (request()->has("export") && request('export') != "") {

                return $this->_export($contratos, 'contratos');
            } else {

                $offset = request()->has('offset') ? request('offset') : 10;
                return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
            }




    }

    public function dispensa(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Dispensa de Licitação ';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','D')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }
    public function concorrencia(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Concorrência';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','R')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function cartaConvite(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Carta Convite';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','W')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function contratoPessoaFisica(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Contrato Pessoa Física';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','F')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function contratoPessoaJuridica(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Contrato Pessoa Jurídica';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','J')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function pregaoPresencial(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Pregão Presencial';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','P')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function pregaoEletronico(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Pregão Eletrônico';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','p')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }

    public function tomadaDePrecos(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Tomada de Preço';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','T')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'D')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }


    public function inexigibilidade(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Inexigibilidade';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','I')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'I')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));

        }
    }

    public function atas(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? intval(request('offset')) : 10;

        $page_title = 'Atas de Registro de Preço';
        $page_description = '';
        $contratos = Contrato::where('covid19', false)->where('tipo','A')
            ->where(function ($q) {
                if (request()->has('orgao') && request('orgao') != '') {
                    $q->where('orgao', 'like', '%' . request('orgao') . '%');
                }
                if (request()->has('modalidade') && request('modalidade') != '') {
                    $q->where('modalidade', 'like', '%' . request('modalidade') . '%');
                }
                if (request()->has('data_hora') && request('data_hora') != '') {
                    $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
                }
                if (request()->has('valor') && request('valor') != '') {
                    $q->where('valor', str_replace(['.', ','], ['', '.'], request('valor')));
                }
                if (request()->has('status') && request('status') != '') {
                    $q->where('status', request('status'));
                }
                if (request()->has('vencedor') && request('vencedor') != '') {
                    $q->where('vencedor', 'like', '%' . request('vencedor') . '%');
                }
                if (request()->has('objeto') && request('objeto') != '') {
                    $q->where('objeto', 'like', '%' . request('objeto') . '%');
                }
            })->orderBy('data_hora', 'desc')->paginate($offset);

        $last_update = Contrato::where('covid19', false)->where('tipo',  'A')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {

            return $this->_export($contratos, 'contratos');
        } else {

            $offset = request()->has('offset') ? request('offset') : 10;
            return view('pages.contratos', compact('page_title', 'offset', 'contratos', 'last_update', 'optionsCompleted','request'));
        }
    }


    public function verbas()
    {
	$page_title = 'Verbas Indenizatorias';
	$page_description = '';
    $optionsCompleted = OptionSearch::where('visible','=',1)->get();
	$offset = request()->has('offset') ? request('offset') : 10;

        $verbas = Verba::where(function ($q) {
            if (request()->has('beneficiario') && request('beneficiario') != '') {
                $q->where('beneficiario','like','%'.request('beneficiario').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        $last_update = Verba::orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {
            return $this->_export($verbas, 'verbas');
        } else {
            return view('pages.verbas', compact('page_title', 'offset', 'verbas','last_update','optionsCompleted'));
        }
        return view('pages.verbas', compact('page_title','offset','last_update','optionsCompleted' ));
    }



    public function diarias()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $page_title = 'Diárias';
        $page_description = '';

        $offset = request()->has('offset') ? request('offset') : 10;
        $diarias = Diaria::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);
        $last_update = Diaria::orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {
            return $this->_export($diarias, 'diarias');
        } else {
            return view('pages.diarias', compact('page_title', 'offset', 'diarias','last_update','optionsCompleted'));
        }

    }

    public function publicacoesOficiais()
    {
        $page_title = 'Publicações Oficiais';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $documents = Document::select('documents.*')
            ->join('type_documents as td','documents.type_document_id','=','td.id')
            ->where('td.name','Publicações Oficiais')
            ->where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
            if (request()->has('year') && request('year') != '') {
                $q->where('year',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->where('month',request('month'));
            }
        })->orderBy('year','desc')->orderBy('month','desc')->paginate($offset);

        $typeDocuments = TypeDocument::orderBy('name','asc')->get();

        $last_update = Document::select('documents.*')
        ->join('type_documents as td','documents.type_document_id','=','td.id')
        ->where('td.name','Publicações Oficiais')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {
            return $this->_export($documents, 'publicacoes_oficiais');
        } else {
            return view('pages.documentos', compact('page_title', 'offset', 'documents','typeDocuments','last_update','optionsCompleted'));
        }

    }

    public function legislacoes()
    {
        $page_title = 'Legislações';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $documents = Document::select('documents.*')
            ->join('type_documents as td','documents.type_document_id','=','td.id')
            ->where('td.name','Legislação')
            ->where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
            if (request()->has('year') && request('year') != '') {
                $q->where('year',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->where('month',request('month'));
            }
        })->orderBy('year','desc')->orderBy('month','desc')->orderBy('name','desc')->paginate($offset);

        $typeDocuments = TypeDocument::orderBy('name','asc')->get();

        $last_update = Document::select('documents.*')
        ->join('type_documents as td','documents.type_document_id','=','td.id')
        ->where('td.name','Legislação')->orderBy('created_at', 'desc')->first();

        if (request()->has("export") && request('export') != "") {
            return $this->_export($documents, 'legislacoes');
        } else {
            return view('pages.documentos', compact('page_title', 'offset', 'documents','typeDocuments','last_update','optionsCompleted'));
        }

    }

    public function covid19()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $appSetting = AppSetting::first();
        if ($appSetting && !$appSetting->display_covid) {
            return redirect('/');
        }
        $covidResumo = CovidResumo::first();
        return view('pages.covid19', compact('covidResumo','optionsCompleted'));
    }

    public function covid19Desespesas()
    {
        $page_title = 'Despesas';
        $page_description = '';

        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $empenhos = Empenho::where('covid19',true)
            ->where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->whereYear('data',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('data',request('month'));
            }
            if (request()->has('numero') && request('numero') != '') {
                $q->where('numero','like','%'.request('numero').'%');
            }
            if (request()->has('credor') && request('credor') != '') {
                $q->where('credor','like','%'.request('credor').'%');
            }
        })->orderBy('data','desc')->paginate($offset);

        $last_update = Empenho::where('covid19',true)->orderBy('created_at', 'desc')->first();

        return view('pages.despesas-covid19', compact('page_title', 'page_description','empenhos','offset','last_update','optionsCompleted'));
    }

    public function covid19Receitas()
    {
        $page_title = 'Receitas';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $receitas = Receita::where('covid19',true)
            ->where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->whereYear('data',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('data',request('month'));
            }
            if (request()->has('codigo') && request('codigo') != '') {
                $q->where('codigo','like','%'.request('codigo').'%');
            }
            if (request()->has('descricao') && request('descricao') != '') {
                $q->where('descricao','like','%'.request('descricao').'%');
            }
        })->orderBy('data','desc')->paginate($offset);

        $last_update = Receita::where('covid19',true)->orderBy('created_at', 'desc')->first();

        return view('pages.receitas-covid19', compact('page_title', 'page_description','receitas','offset','last_update','optionsCompleted'));
    }

    public function covid19Licitacoes()
    {
        $page_title = 'Licitações';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $contratos = Contrato::where('covid19',true)->where('tipo','L')
            ->where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('data_hora','desc')->paginate($offset);

        $last_update = Contrato::where('covid19',true)->where('tipo','L')->orderBy('created_at', 'desc')->first();

        return view('pages.contratos', compact('page_title', 'offset', 'contratos','last_update','optionsCompleted'));
    }

    public function covid19Contratos()
    {
        $page_title = 'Contratos';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $contratos = Contrato::where('covid19',true)
            ->where('tipo','C')
            ->orWhere('tipo','D')
            ->orWhere('tipo','F')
            ->orWhere('tipo','J')
            ->where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('data_hora','desc')->paginate($offset);

        $last_update = Contrato::where('covid19',true)->where('tipo','C')->orderBy('created_at', 'desc')->first();

        return view('pages.contratos', compact('page_title', 'offset', 'contratos','last_update','optionsCompleted'));
    }

    public function estatisticas()
    {
        $page_title = 'Estatísticas';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;

        $pedidos = Pedido::where(function ($q) {
            if (request()->has('data_inicial') && request('data_inicial') != '') {
                $q->where('created_at','>=',request('data_inicial'));
            }
            if (request()->has('data_final') && request('data_final') != '') {
                $q->where('created_at','<=', date('Y-m-d H:i:s', strtotime(request('data_final') . ' + 1 day')));
            }
        })->orderBy('id','desc')->paginate($offset);

        $abertos = Pedido::where('status', 0)
        ->where(function ($q) {
            if (request()->has('data_inicial') && request('data_inicial') != '') {
                $q->where('created_at','>=',request('data_inicial'));
            }
            if (request()->has('data_final') && request('data_final') != '') {
                $q->where('created_at','<=', date('Y-m-d H:i:s', strtotime(request('data_final') . ' + 1 day')));
            }
        })->orderBy('id','desc')->count();

        $respondidos = Pedido::where('status', 1)
        ->where(function ($q) {
            if (request()->has('data_inicial') && request('data_inicial') != '') {
                $q->where('created_at','>=',request('data_inicial'));
            }
            if (request()->has('data_final') && request('data_final') != '') {
                $q->where('created_at','<=', date('Y-m-d H:i:s', strtotime(request('data_final') . ' + 1 day')));
            }
        })->orderBy('id','desc')->count();

        $tramitacao = Pedido::where('status', 2)
        ->where(function ($q) {
            if (request()->has('data_inicial') && request('data_inicial') != '') {
                $q->where('created_at','>=',request('data_inicial'));
            }
            if (request()->has('data_final') && request('data_final') != '') {
                $q->where('created_at','<=', date('Y-m-d H:i:s', strtotime(request('data_final') . ' + 1 day')));
            }
        })->orderBy('id','desc')->count();

        $negados = Pedido::where('status', 3)
        ->where(function ($q) {
            if (request()->has('data_inicial') && request('data_inicial') != '') {
                $q->where('created_at','>=',request('data_inicial'));
            }
            if (request()->has('data_final') && request('data_final') != '') {
                $q->where('created_at','<=', date('Y-m-d H:i:s', strtotime(request('data_final') . ' + 1 day')));
            }
        })->orderBy('id','desc')->count();

        return view('pages.estatisticas', compact('page_title', 'offset', 'abertos','respondidos', 'tramitacao', 'negados','pedidos','optionsCompleted'));
    }

    public function estruturas()
    {
        $page_title = 'Estrutura Organizacional';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $estruturas = Estrutura::where(function ($q) {
            if (request()->has('title') && request('title') != '') {
                $q->where('title','like','%'.request('title').'%');
                $q->orWhere('description','like','%'.request('description').'%');
            }
        })->orderBy('order','asc')->get();

        $last_update = Estrutura::where(function ($q) {
            if (request()->has('title') && request('title') != '') {
                $q->where('title','like','%'.request('title').'%');
                $q->orWhere('description','like','%'.request('description').'%');
            }
        })->orderBy('created_at','desc')->first();

        return view('pages.estruturas', compact('page_title', 'estruturas', 'last_update','optionsCompleted'));
    }

    public function lrf()
    {
        $page_title = 'LRF - Lei de Responsabilidade Fiscal';
        $page_description = '';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $offset = request()->has('offset') ? request('offset') : 10;
        $tipos = Responsabilidade::orderBy('name','asc')->get();
        $last_update = Responsabilidade::orderBy('created_at','desc')->first();

        return view('pages.orcamentos', compact('page_title', 'page_description','tipos','offset','last_update','optionsCompleted'));
    }

    public function esic()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $page_title = 'Pedidos e-SIC';
        $page_description = '';

        return view('pages.esic', compact('page_title', 'page_description','optionsCompleted'));
    }

    public function salvarPedido(Request $request)
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'tipo_pessoa' => 'required',
            'cpf' => 'required',
            'phone' => 'required',
            'priority' => 'required',
            'type_answer' => 'required',
            'typerequest_id' => 'required',
            'location' => 'required',
            'subject' => 'required',
            'message' => 'required',
            'captcha' => 'required|captcha'
        ]);

        try {
            $data = $request->except('_token');

            if ($request->file('anexo')) {
                $path = $request->anexo->store('pedidos');
                $data = $data + ['anexo' => 'storage/'.$path];
            }

            $pedido = Pedido::create($data);
            $pedido->update(['serial'=>date('YmdHis').str_pad($pedido->id, 4, "0", STR_PAD_LEFT)]);

            Mail::to($pedido->email)
                ->send(new MailPedidoEsic($pedido));

            return redirect()->back()->with("message_success", "Pedido enviado com sucesso! Seu número de protocolo é :<strong>".$pedido->serial."</strong> e foi enviado por email. Verifique sua caixa de <strong>Spam!</strong>");
        } catch (\Throwable $th) {
            return redirect()->back()->with("message_error", $th->getMessage());
        }
    }

    public function consultarPedido()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        $page_title = 'Consultar Pedidos e-SIC';
        $page_description = '';

        $pedido = null;
        $respostas = null;
        $appSettingName = AppSetting::first() ? AppSetting::first()->name : env('APP_NAME');

        if (request()->has('email') && request()->has('serial')) {
            $pedido = Pedido::where('email', request('email'))->where("serial", request('serial'))->first();

            if (!$pedido) {
                return redirect()->route('consultarPedido')->with("message_error", "Pedido não encontrado");
            }
        }

        return view('pages.esic-consulta', compact('page_title', 'page_description', 'pedido','appSettingName','optionsCompleted'));
    }

    public function responderPedido(Request $request)
    {
        try {
            $resposta = Resposta::create([
                'description' => $request->description,
                'pedido_id' => $request->pedido_id
            ]);

            return redirect()->back()->with("message_resp_success", "Resposta salva com sucesso.");
        } catch (\Throwable $th) {
            return redirect()->back()->with("message_resp_error", $th->getMessage());
        }
    }

    public function _export($collection, $name)
    {
        switch (request('export')) {
            case 'xml':
                $xml = view('pages.xmls._'.$name, compact('collection',));
                return response($xml, 200, [
                    'Content-Disposition' => 'attachment; filename="'.$name.'_'.date('YmdHis').'.xml"',
                    'Content-Type' => 'application/xml',
                    'charset' => 'utf-8'
                ]);
                break;

            case 'csv':
                return Excel::download(new OpenDataExport($collection), $name.'_'.date('YmdHis').'.csv', \Maatwebsite\Excel\Excel::CSV);
                break;

            case 'json':
                return response(json_encode($collection), 200, [
                    'Content-Disposition' => 'attachment; filename="'.$name.'_'.date('YmdHis').'.json"',
                    'Content-Type' => 'application/json',
                    'charset' => 'utf-8'
                ]);
                break;
        }
    }

    public function dashboards()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.dashboard', compact('page_title', 'page_description','optionsCompleted'));
    }

    /**
     * Demo methods below
     */

    // Datatables
    public function datatables()
    {
        $page_title = 'Datatables';
        $page_description = 'This is datatables test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.datatables', compact('page_title', 'page_description','optionsCompleted'));
    }

    // KTDatatables
    public function ktDatatables()
    {
        $page_title = 'KTDatatables';
        $page_description = 'This is KTdatatables test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.ktdatatables', compact('page_title', 'page_description','optionsCompleted'));
    }

    // Select2
    public function select2()
    {
        $page_title = 'Select 2';
        $page_description = 'This is Select2 test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.select2', compact('page_title', 'page_description','optionsCompleted'));
    }

    // jQuery-mask
    public function jQueryMask()
    {
        $page_title = 'jquery-mask';
        $page_description = 'This is jquery masks test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.jquery-mask', compact('page_title', 'page_description','optionsCompleted'));
    }

    // custom-icons
    public function customIcons()
    {
        $page_title = 'customIcons';
        $page_description = 'This is customIcons test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.custom-icons', compact('page_title', 'page_description','optionsCompleted'));
    }

    // flaticon
    public function flaticon()
    {
        $page_title = 'flaticon';
        $page_description = 'This is flaticon test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.flaticon', compact('page_title', 'page_description','optionsCompleted'));
    }

    // fontawesome
    public function fontawesome()
    {
        $page_title = 'fontawesome';
        $page_description = 'This is fontawesome test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.fontawesome', compact('page_title', 'page_description','optionsCompleted'));
    }

    // lineawesome
    public function lineawesome()
    {
        $page_title = 'lineawesome';
        $page_description = 'This is lineawesome test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.lineawesome', compact('page_title', 'page_description','optionsCompleted'));
    }

    // socicons
    public function socicons()
    {
        $page_title = 'socicons';
        $page_description = 'This is socicons test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.socicons', compact('page_title', 'page_description','optionsCompleted'));
    }

    // svg
    public function svg()
    {
        $page_title = 'svg';
        $page_description = 'This is svg test page';
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('pages.icons.svg', compact('page_title', 'page_description','optionsCompleted'));
    }

    // Quicksearch Result
    public function quickSearch()
    {
        $optionsCompleted = OptionSearch::where('visible','=',1)->get();
        return view('layout.partials.extras._quick_search_result','optionsCompleted');
    }
}
