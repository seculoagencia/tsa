<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\AppSetting;
use App\Http\Requests\PedidoRequest;

class PedidoController extends Controller
{
    public function __construct() {
        $page_title = 'Pedidos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Pedido::class, 'pedido');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $pedidos = Pedido::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.pedidos.index', compact('page_description', 'offset', 'pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.pedidos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PedidoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(PedidoRequest $request)
    {
        try {
            $pedido = Pedido::create($request->except('_token'));
            return redirect()->route('pedidos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('pedidos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pedido $pedido
     * @return \Illuminate\Http\Response
    */
    public function show(Pedido $pedido)
    {
        $page_description = 'Detalhes';
        $appSettingName = AppSetting::first() ? AppSetting::first()->name : env('APP_NAME');
        return view('adm.pedidos.show', compact('page_description', 'pedido', 'appSettingName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pedido $pedido
     * @return \Illuminate\Http\Response
    */
    public function edit(Pedido $pedido)
    {
        $page_description = 'Edição';
        return view('adm.pedidos.edit', compact('page_description', 'pedido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PedidoRequest  $request
     * @param  \App\Models\Pedido $pedido
     * @return \Illuminate\Http\Response
    */
    public function update(PedidoRequest $request, Pedido $pedido)
    {
        try {
            $pedido->update($request->except('_token'));
            return redirect()->route('pedidos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('pedidos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pedido $pedido
     * @return \Illuminate\Http\Response
    */
    public function destroy(Pedido $pedido)
    {
        try {
            $pedido->delete();
            return redirect()->route('pedidos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('pedidos.index')->with('message_error', $th->getMessage());
        }
    }
}
