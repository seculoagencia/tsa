<?php

namespace App\Http\Controllers;

use App\Models\Resposta;
use App\Http\Requests\RespostaRequest;

class RespostaController extends Controller
{
    public function __construct() {
        $page_title = 'Respostas';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $respostas = Resposta::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.respostas.index', compact('page_description', 'offset', 'respostas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.respostas.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RespostaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(RespostaRequest $request)
    {
        try {
            $resposta = Resposta::create($request->except('_token'));
            return redirect()->back()->with('message_resp_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->back()->with('message_resp_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resposta $resposta
     * @return \Illuminate\Http\Response
    */
    public function show(Resposta $resposta)
    {
        $page_description = 'Detalhes';
        return view('adm.respostas.show', compact('page_description', 'resposta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resposta $resposta
     * @return \Illuminate\Http\Response
    */
    public function edit(Resposta $resposta)
    {
        $page_description = 'Edição';
        return view('adm.respostas.edit', compact('page_description', 'resposta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RespostaRequest  $request
     * @param  \App\Models\Resposta $resposta
     * @return \Illuminate\Http\Response
    */
    public function update(RespostaRequest $request, Resposta $resposta)
    {
        try {
            $resposta->update($request->except('_token'));
            return redirect()->route('respostas.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('respostas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resposta $resposta
     * @return \Illuminate\Http\Response
    */
    public function destroy(Resposta $resposta)
    {
        try {
            $resposta->delete();
            return redirect()->route('respostas.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('respostas.index')->with('message_error', $th->getMessage());
        }
    }
}
