<?php

namespace App\Http\Controllers;

use App\Models\AttachmentEmpenho;
use App\Models\Attachment;
use App\Models\Empenho;
use App\Http\Requests\AttachmentEmpenhoRequest;

class AttachmentEmpenhoController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Empenhos';
        view()->share('page_title', $page_title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentEmpenhoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentEmpenhoRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $empenho = Empenho::find($request->empenho_id);
            $empenho->attachments()->syncWithoutDetaching($attachment->id);
            
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_error', $th->getMessage());
        }
    }
}
