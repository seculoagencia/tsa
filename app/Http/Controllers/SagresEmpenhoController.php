<?php

namespace App\Http\Controllers;

use App\Models\SagresEmpenho;
use App\Models\SagresCredor;
use App\Models\SagresUndOrcamentaria;
use App\Models\SagresFuncao;
use App\Models\SagresSubFuncao;
use App\Models\SagresPrograma;
use App\Models\SagresAcao;
use App\Models\SagresElementoDespesa;
use App\Models\SagresLiquidacao;
use App\Models\SagresPagamento;
use App\Http\Requests\SagresEmpenhoRequest;
use Storage;
use DB;

class SagresEmpenhoController extends Controller
{
    public function __construct() {
        $page_title = 'Empenhos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(SagresEmpenho::class, 'sagres_empenho');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $sagresEmpenhos = SagresEmpenho::where(function ($q) {
            // if (request()->has('name') && request('name') != '') {
            //     $q->where('name','like','%'.request('name').'%');
            // }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.sagres-empenhos.index', compact('page_description', 'offset', 'sagresEmpenhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.sagres-empenhos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SagresEmpenhoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(SagresEmpenhoRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                foreach ($request->file('sagres') as $key => $file) {
                    $filename = substr($file->getClientOriginalName(), 12,-4);
                    switch ($filename) {
                        case 'Fornecedores':
                            $this->_credor($file);
                            break;
                        case 'Empenhos':
                            $this->_empenho($file);
                            break;
                        case 'UnidadeOrcamentaria':
                            $this->_undOrcamentaria($file);
                            break;
                        case 'Funcoes':
                            $this->_funcao($file);
                            break;
                        case 'SubFuncoes':
                            $this->_subFuncao($file);
                            break;
                        case 'Programas':
                            $this->_programa($file);
                            break;
                        case 'Acao':
                            $this->_acao($file);
                            break;
                        case 'ElementoDespesa':
                            $this->_elementoDespesa($file);
                            break;
                        case 'Liquidacao':
                            $this->_liquidacao($file);
                            break;
                        case 'Pagamentos':
                            $this->_pagamento($file);
                            break;
                        case 'ItemPagamento':
                            $this->_itemPagamento($file);
                            break;
                    }
                }
            });
            // $sagresEmpenho = SagresEmpenho::create($request->except('_token'));
            return redirect()->route('sagres-empenhos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sagres-empenhos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SagresEmpenho $sagresEmpenho
     * @return \Illuminate\Http\Response
    */
    public function show(SagresEmpenho $sagresEmpenho)
    {
        $page_description = 'Detalhes';
        return view('adm.sagres-empenhos.show', compact('page_description', 'sagresEmpenho'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SagresEmpenho $sagresEmpenho
     * @return \Illuminate\Http\Response
    */
    public function edit(SagresEmpenho $sagresEmpenho)
    {
        $page_description = 'Edição';
        return view('adm.sagres-empenhos.edit', compact('page_description', 'sagresEmpenho'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SagresEmpenhoRequest  $request
     * @param  \App\Models\SagresEmpenho $sagresEmpenho
     * @return \Illuminate\Http\Response
    */
    public function update(SagresEmpenhoRequest $request, SagresEmpenho $sagresEmpenho)
    {
        try {
            $sagresEmpenho->update($request->except('_token'));
            return redirect()->route('sagres-empenhos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sagres-empenhos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SagresEmpenho $sagresEmpenho
     * @return \Illuminate\Http\Response
    */
    public function destroy(SagresEmpenho $sagresEmpenho)
    {
        try {
            $sagresEmpenho->delete();
            return redirect()->route('sagres-empenhos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sagres-empenhos.index')->with('message_error', $th->getMessage());
        }
    }

    public function _credor($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {
                $credor = SagresCredor::where('cpf_cnpj', trim(substr($linha, 6, 14)))->first();

                if (!$credor) {
                    SagresCredor::create([
                        'cpf_cnpj' => strlen(trim(substr($linha, 6, 14))) > 0 ? trim(substr($linha, 6, 14)) : null,
                        'nome' => strlen(trim(substr($linha, 20, 80))) > 0 ? substr($linha, 20, 80) : null,
                        'tipo_credor' => strlen(trim(substr($linha, 100, 1))) > 0 ? substr($linha, 100, 1) : null,
                        'sigla_uf' => strlen(trim(substr($linha, 101, 2))) > 0 ? substr($linha, 101, 2) : null,
                        'municipio' => strlen(trim(substr($linha, 103, 60))) > 0 ? substr($linha, 103, 60) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _empenho($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {
                $data_dia = substr(substr($linha, 61, 8), 0, 2);
                $data_mes = substr(substr($linha, 61, 8), 2, 2);
                $data_ano = substr(substr($linha, 61, 8), 4, 4);

                $empenho = SagresEmpenho::where('ano', substr($linha, 6, 4))
                ->where('num_empenho', substr($linha, 53, 7))->first();

                if (!$empenho) {
                    SagresEmpenho::create([
                        'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                        'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null, // Unidade Orçamentária
                        'funcao' => (strlen(trim(substr($linha, 20, 2))) > 0) ? substr($linha, 20, 2) : null, // Função
                        'subfuncao' => (strlen(trim(substr($linha, 22, 3))) > 0) ? substr($linha, 22, 3) : null, // Subfunção
                        'programa' => (strlen(trim(substr($linha, 25, 4))) > 0) ? substr($linha, 25, 4) : null, // Programa
                        'acao' => (strlen(trim(substr($linha, 29, 6))) > 0) ? substr($linha, 29, 6) : null, // Projeto / Atividade
                        'id_acao' => (strlen(trim(substr($linha, 35, 1))) > 0) ? substr($linha, 35, 1) : null,
                        'cod_cat_econ' => (strlen(trim(substr($linha, 42, 1))) > 0) ? substr($linha, 42, 1) : null,
                        'cod_nat_desp' => (strlen(trim(substr($linha, 43, 1))) > 0) ? substr($linha, 43, 1) : null,
                        'modalidade_aplic' => (strlen(trim(substr($linha, 44, 2))) > 0) ? substr($linha, 44, 2) : null,
                        'cod_elem_desp_dot' => (strlen(trim(substr($linha, 46, 2))) > 0) ? substr($linha, 46, 2) : null,
                        'sub_elem_desp' => (strlen(trim(substr($linha, 48, 3))) > 0) ? substr($linha, 48, 3) : null,
                        'modalidade_licit' => (strlen(trim(substr($linha, 51, 2))) > 0) ? substr($linha, 51, 2) : null,
                        'num_empenho' => (strlen(trim(substr($linha, 53, 7))) > 0) ? substr($linha, 53, 7) : null, // Número
                        'tipo_empenho' => (strlen(trim(substr($linha, 60, 1))) > 0) ? substr($linha, 60, 1) : null,
                        'data_emissao_empenho' => (strlen(trim(substr($linha, 61, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null, // Data
                        'valor_empenhado' => (strlen(trim(substr($linha, 69, 16))) > 0) ? str_replace(['.',','],['','.'],substr($linha, 69, 16)) : null, // Valor
                        'historico' => (strlen(trim(substr($linha, 85, 510))) > 0) ? substr($linha, 85, 510) : null, // Histórico
                        'cpf_cnpj_credor' => (strlen(trim(substr($linha, 585, 25))) > 0) ? explode(' ', trim(substr($linha, 585, 25)))[0] : null, // Credor
                        'num_procedimento' => (strlen(trim(substr($linha, 609, 9))) > 0) ? substr($linha, 609, 9) : null, // Numero do Processo
                        'font_recurso' => (strlen(trim(substr($linha, 618, 6))) > 0) ? substr($linha, 618, 6) : null, // Fonte de Recurso
                        'cpf_ordenador' => (strlen(trim(substr($linha, 624, 11))) > 0) ? trim(substr($linha, 624, 11)) : null,
                        'cod_elem_desp' => (strlen(trim(substr($linha, 635, 2))) > 0) ? substr($linha, 635, 2) : null, // Elemento de Despesa
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _undOrcamentaria($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $undOrcamentaria = SagresUndOrcamentaria::where('codigo', substr($linha, 6, 10))->first();

                if (!$undOrcamentaria) {
                    SagresUndOrcamentaria::create([
                        'codigo' => (strlen(trim(substr($linha, 6, 10))) > 0) ? substr($linha, 6, 10) : null,
                        'denominacao' => (strlen(trim(substr($linha, 16, 50))) > 0) ? substr($linha, 16, 50) : null,
                        'num_unid_jurisd' => (strlen(trim(substr($linha, 66, 6))) > 0) ? substr($linha, 66, 6) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _funcao($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $funcao = SagresFuncao::where('codigo', substr($linha, 6, 10))->first();

                if (!$funcao) {
                    SagresFuncao::create([
                        'codigo' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                        'nome' => (strlen(trim(substr($linha, 2, 30))) > 0) ? utf8_decode(substr($linha, 2, 30)) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _subFuncao($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $subfuncao = SagresSubFuncao::where('codigo', substr($linha, 6, 10))->first();

                if (!$subfuncao) {
                    SagresSubFuncao::create([
                        'cod_funcao' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                        'codigo' => (strlen(trim(substr($linha, 2, 3))) > 0) ? substr($linha, 2, 3) : null,
                        'nome' => (strlen(trim(substr($linha, 5, 68))) > 0) ? utf8_decode(substr($linha, 5, 68)) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _programa($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $programa = SagresPrograma::where('codigo', substr($linha, 6, 10))->first();

                if (!$programa) {
                    SagresPrograma::create([
                        'codigo' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                        'nome' => (strlen(trim(substr($linha, 10, 70))) > 0) ? substr($linha, 10, 70) : null,
                        'descricao' => (strlen(trim(substr($linha, 80, 150))) > 0) ? substr($linha, 80, 150) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _acao($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $acao = SagresAcao::where('codigo', substr($linha, 6, 10))->first();

                if (!$acao) {
                    SagresAcao::create([
                        'codigo' => (strlen(trim(substr($linha, 6, 6))) > 0) ? substr($linha, 6, 6) : null,
                        'nome' => (strlen(trim(substr($linha, 12, 70))) > 0) ? substr($linha, 12, 70) : null,
                        'identificacao' => (strlen(trim(substr($linha, 82, 1))) > 0) ? substr($linha, 82, 1) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _elementoDespesa($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {

                $acao = SagresElementoDespesa::where('codigo', substr($linha, 6, 10))->first();

                if (!$acao) {
                    SagresElementoDespesa::create([
                        'codigo' => (strlen(trim(substr($linha, 0, 2))) > 0) ? substr($linha, 0, 2) : null,
                        'descricao' => (strlen(trim(substr($linha, 2, 87))) > 0) ? utf8_decode(substr($linha, 2, 87)) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _liquidacao($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {
                $data_dia = substr(substr($linha, 34, 8), 0, 2);
                $data_mes = substr(substr($linha, 34, 8), 2, 2);
                $data_ano = substr(substr($linha, 34, 8), 4, 4);

                $liquidacao = SagresLiquidacao::where('num_liquidacao', substr($linha, 27, 7))
                ->where('num_empenho', substr($linha, 20, 7))->where('ano', substr($linha, 6, 4))->first();

                if (!$liquidacao) {
                    SagresLiquidacao::create([
                        'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                        'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null,
                        'num_empenho' => (strlen(trim(substr($linha, 20, 7))) > 0) ? substr($linha, 20, 7) : null,
                        'num_liquidacao' => (strlen(trim(substr($linha, 27, 7))) > 0) ? substr($linha, 27, 7) : null,
                        'data' => (strlen(trim(substr($linha, 34, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null, // Data
                        'valor' => (strlen(trim(substr($linha, 42, 16))) > 0) ? str_replace(['.',','],['','.'],substr($linha, 42, 16)) : null, // Valor
                        'tipo_doc' => (strlen(trim(substr($linha, 58, 1))) > 0) ? substr($linha, 58, 1) : null,
                        'num_chave' => (strlen(trim(substr($linha, 59, 44))) > 0) ? substr($linha, 59, 44) : null,
                        'historico' => (strlen(trim(substr($linha, 103, 510))) > 0) ? substr($linha, 103, 510) : null,
                        'cod_fonte_recurso' => (strlen(trim(substr($linha, 613, 6))) > 0) ? substr($linha, 613, 6) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _pagamento($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {
                $data_dia = substr(substr($linha, 34, 8), 0, 2);
                $data_mes = substr(substr($linha, 34, 8), 2, 2);
                $data_ano = substr(substr($linha, 34, 8), 4, 4);

                $pagamento = SagresPagamento::where('num_parc_pagamento', substr($linha, 27, 7))
                ->where('num_empenho', substr($linha, 20, 7))->where('ano', substr($linha, 6, 4))->first();

                if (!$pagamento) {
                    SagresPagamento::create([
                        'ano' => (strlen(trim(substr($linha, 6, 4))) > 0) ? substr($linha, 6, 4) : null,
                        'cod_unid_orc' => (strlen(trim(substr($linha, 10, 10))) > 0) ? substr($linha, 10, 10) : null,
                        'num_empenho' => (strlen(trim(substr($linha, 20, 7))) > 0) ? substr($linha, 20, 7) : null,
                        'num_parc_pagamento' => (strlen(trim(substr($linha, 27, 7))) > 0) ? substr($linha, 27, 7) : null,
                        'data' => (strlen(trim(substr($linha, 34, 8))) > 0) ? $data_ano.'-'.$data_mes.'-'.$data_dia : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }

    public function _itemPagamento($file)
    {
        $file_path = $file->store('sagres');
        $file_txt = fopen ('storage/'.$file_path, "r");
        $items = array();
        while (!feof ($file_txt)) {
            $linha = utf8_encode(fgets($file_txt, 4096));
            if(trim($linha) != '') {
                $ano = substr($linha, 6, 4);
                $num_empenho = substr($linha, 20, 7);
                $num_parc_pagamento = substr($linha, 27, 7);

                $pagamento = SagresPagamento::where('num_parc_pagamento', $num_parc_pagamento)
                ->where('num_empenho', $num_empenho)->where('ano', $ano)->first();

                if ($pagamento) {
                    $pagamento->update([
                        'valor' => (strlen(trim(substr($linha, 34, 16))) > 0) ? str_replace(['.',','],['','.'],substr($linha, 34, 16)) : null,
                        'cod_banco' => (strlen(trim(substr($linha, 79, 3))) > 0) ? substr($linha, 79, 3) : null,
                        'num_agencia_bancaria' => (strlen(trim(substr($linha, 82, 6))) > 0) ? substr($linha, 82, 6) : null,
                        'num_conta_bancaria' => (strlen(trim(substr($linha, 88, 12))) > 0) ? substr($linha, 88, 12) : null,
                        'num_seq' => (strlen(trim(substr($linha, 107, 7))) > 0) ? substr($linha, 107, 7) : null
                    ]);
                }
            }
        }
        Storage::delete($file_path);
    }
}
