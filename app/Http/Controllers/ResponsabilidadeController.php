<?php

namespace App\Http\Controllers;

use App\Models\Responsabilidade;
use App\Http\Requests\ResponsabilidadeRequest;

class ResponsabilidadeController extends Controller
{
    public function __construct() {
        $page_title = 'LRF - Lei de Responsabilidade Fiscal';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $responsabilidades = Responsabilidade::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.responsabilidades.index', compact('page_description', 'offset', 'responsabilidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.responsabilidades.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ResponsabilidadeRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ResponsabilidadeRequest $request)
    {
        try {
            $responsabilidade = Responsabilidade::create($request->except('_token'));
            return redirect()->route('responsabilidades.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Responsabilidade $responsabilidade
     * @return \Illuminate\Http\Response
    */
    public function show(Responsabilidade $responsabilidade)
    {
        $page_description = 'Detalhes';
        return view('adm.responsabilidades.show', compact('page_description', 'responsabilidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Responsabilidade $responsabilidade
     * @return \Illuminate\Http\Response
    */
    public function edit(Responsabilidade $responsabilidade)
    {
        $page_description = 'Edição';
        return view('adm.responsabilidades.edit', compact('page_description', 'responsabilidade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ResponsabilidadeRequest  $request
     * @param  \App\Models\Responsabilidade $responsabilidade
     * @return \Illuminate\Http\Response
    */
    public function update(ResponsabilidadeRequest $request, Responsabilidade $responsabilidade)
    {
        try {
            $responsabilidade->update($request->except('_token'));
            return redirect()->route('responsabilidades.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Responsabilidade $responsabilidade
     * @return \Illuminate\Http\Response
    */
    public function destroy(Responsabilidade $responsabilidade)
    {
        try {
            $responsabilidade->delete();
            return redirect()->route('responsabilidades.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }
}
