<?php

namespace App\Http\Controllers;

use App\Models\Servidor;
use App\Http\Requests\ServidorRequest;
use App\Imports\ServidorImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ServidorController extends Controller
{
    public function __construct() {
        $page_title = 'Servidores';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Servidor::class, 'servidore');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $countCpf = Servidor::select('cpf')->count('cpf');
        try {
            $countAdmissao = Servidor::select('admissao')->count('admissao');
        } catch (\Throwable $th) {
            $countAdmissao = 0;
        }

        try {
            $servidores = Servidor::withTrashed()->where(function ($q) {
                if (request()->has('nome') && request('nome') != '') {
                    $q->where('nome','like','%'.request('nome').'%');
                }
                if (request()->has('referencia') && request('referencia') != '') {
                    $q->where('referencia',request('referencia'));
                }
            })->orderBy('id','desc')->paginate($offset);
        } catch (\Throwable $th) {
            $servidores = Servidor::where(function ($q) {
                if (request()->has('nome') && request('nome') != '') {
                    $q->where('nome','like','%'.request('nome').'%');
                }
                if (request()->has('referencia') && request('referencia') != '') {
                    $q->where('referencia',request('referencia'));
                }
            })->orderBy('id','desc')->paginate($offset);
        }

        return view('adm.servidores.index', compact('page_description', 'offset', 'servidores', 'countCpf','countAdmissao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.servidores.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ServidorRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ServidorRequest $request)
    {
        try {
            if ($request->has('tipo')) {
                if ($request->tipo == 'import') {
                    DB::transaction(function () use ($request) {
                        $this->_importServidores($request->file('servidores'));
                    });
                }
                if ($request->tipo == 'import_excel') {
                    DB::transaction(function () use ($request) {
                        $this->_importServidoresExcel($request->file('servidores'));
                    });
                }
            } else {
                $servidore = Servidor::create($request->except('_token'));
            }
            return redirect()->route('servidores.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('servidores.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Servidor $servidore
     * @return \Illuminate\Http\Response
    */
    public function show(Servidor $servidore)
    {
        $page_description = 'Detalhes';
        return view('adm.servidores.show', compact('page_description', 'servidore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Servidor $servidore
     * @return \Illuminate\Http\Response
    */
    public function edit(Servidor $servidore)
    {
        $page_description = 'Edição';
        $attachments = $servidore->attachments;
        $table = 'servidores';
        $item_id = $servidore->id;
        return view('adm.servidores.edit', compact('page_description','servidore','attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ServidorRequest  $request
     * @param  \App\Models\Servidor $servidore
     * @return \Illuminate\Http\Response
    */
    public function update(ServidorRequest $request, Servidor $servidore)
    {
        try {
            $servidore->update($request->except('_token'));
            return redirect()->route('servidores.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('servidores.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Servidor $servidore
     * @return \Illuminate\Http\Response
    */
    public function destroy(Servidor $servidore)
    {
        try {
            foreach ($servidore->attachments as $key => $attachment) {
                $servidore->attachments()->detach($attachment->id);
                $attachment->delete();
            }
            $servidore->delete();
            return redirect()->back()->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->back()->with('message_error', $th->getMessage());
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
    */
    public function restore($id)
    {
        try {
            $sicapEmpenho = Servidor::withTrashed()->find($id);
            $sicapEmpenho->restore();
            return redirect()->route('servidores.index')->with('message_success', 'Registro restaurado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('servidores.index')->with('message_error', $th->getMessage());
        }
    }

    public function _importServidores($file)
    {
        $file_path = $file->store('servidores');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(utf8_encode(str_replace(['í'],['i'],$xml)), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {
            $servidor = Servidor::where('nome', trim($object->nome))->where('referencia', trim($object->ano).'-'.trim($object->mes))->first();

            if (!$servidor) {
                Servidor::create([
                    'nome' => trim($object->nome),
                    'cpf' => trim($object->cpf),
                    'regime' => trim($object->regime),
                    'cargo' => trim($object->cargo),
                    'secretaria' => trim($object->secretaria),
                    'valor_bruto' => trim($object->bruto),
                    'descontos' => trim($object->desconto),
                    'valor_liquido' => trim($object->liquido),
                    'referencia' => trim($object->ano).'-'.trim($object->mes)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _importServidoresExcel($file)
    {
        $file_path = $file->store('servidores');
        Excel::import(new ServidorImport, $file_path);
        // Servidor::create([
            // 'nome' => trim($object->nome),
            // 'cpf' => trim($object->cpf),
            // 'regime' => trim($object->regime),
            // 'cargo' => trim($object->cargo),
            // 'secretaria' => trim($object->secretaria),
            // 'valor_bruto' => trim($object->bruto),
            // 'descontos' => trim($object->desconto),
            // 'valor_liquido' => trim($object->liquido),
            // 'referencia' => trim($object->ano).'-'.trim($object->mes)
        // ]);
        Storage::delete($file_path);
    }
}
