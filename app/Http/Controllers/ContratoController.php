<?php

namespace App\Http\Controllers;

use App\Models\Contrato;
use App\Http\Requests\ContratoRequest;
use Illuminate\Http\Request;

class ContratoController extends Controller
{
    public function __construct() {
        $page_title = 'Licitações e Contratos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Contrato::class, 'contrato');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 11;

        $contratos = Contrato::where(function ($q) {
            if (request()->has('tipo') && request('tipo') != '') {
                $q->where('tipo', request('tipo'));
            }
            if (request()->has('orgao') && request('orgao') != '') {
                $q->where('orgao','like','%'.request('orgao').'%');
            }
            if (request()->has('modalidade') && request('modalidade') != '') {
                $q->where('modalidade','like','%'.request('modalidade').'%');
            }
            if (request()->has('data_hora') && request('data_hora') != '') {
                $q->where('data_hora', date('Y-m-d H:i:s', strtotime(request('data_hora'))));
            }
            if (request()->has('valor') && request('valor') != '') {
                $q->where('valor', str_replace(['.',','],['','.'],request('valor')));
            }
            if (request()->has('status') && request('status') != '') {
                $q->where('status', request('status'));
            }
            if (request()->has('vencedor') && request('vencedor') != '') {
                $q->where('vencedor','like','%'.request('vencedor').'%');
            }
            if (request()->has('objeto') && request('objeto') != '') {
                $q->where('objeto','like','%'.request('objeto').'%');
            }
        })->orderBy('data_hora','desc')->paginate($offset);

        return view('adm.contratos.index', compact('page_description', 'offset', 'contratos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.contratos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ContratoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ContratoRequest $request)
    {
        try {
            $contrato = Contrato::create($request->except(['_token','arquivo']) + ['covid19' => $request->has('covid19')]);
            return redirect()->route('contratos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('contratos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contrato $contrato
     * @return \Illuminate\Http\Response
    */
    public function show(Contrato $contrato)
    {
        $page_description = 'Detalhes';

        return view('adm.contratos.show', compact('page_description', 'contrato'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contrato $contrato
     * @return \Illuminate\Http\Response
    */
    public function edit(Contrato $contrato)
    {
        $page_description = 'Edição';
        $attachments = $contrato->attachments;
        $table = 'contratos';
        $item_id = $contrato->id;
        return view('adm.contratos.edit', compact('page_description', 'contrato','attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ContratoRequest  $request
     * @param  \App\Models\Contrato $contrato
     * @return \Illuminate\Http\Response
    */
    public function update(ContratoRequest $request, Contrato $contrato)
    {
        try {
            $contrato->update($request->except(['_token', 'covid19']) + ['covid19' => $request->has('covid19')]);
            // return redirect()->route('contratos.index')->with('message_success', 'Registro editado com sucesso!');
            return redirect()->back()->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            // return redirect()->route('contratos.index')->with('message_error', $th->getMessage());
            return redirect()->back()->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contrato $contrato
     * @return \Illuminate\Http\Response
    */
    public function destroy(Contrato $contrato)
    {
        try {
            foreach ($contrato->attachments as $key => $attachment) {
                $contrato->attachments()->detach($attachment->id);
                $attachment->delete();
            }
            $contrato->delete();
            return redirect()->route('contratos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('contratos.index')->with('message_error', $th->getMessage());
        }
    }
}
