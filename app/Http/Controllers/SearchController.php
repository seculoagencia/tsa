<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\OptionSearch;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct() {
        $appSetting = AppSetting::first();
        view()->share('appSetting', $appSetting);
    }
   public function searchOptions(Request $request) {

    $page_title = 'Portal da Transparência';
    $page_description = '';
    $option_search = $request->search;
    $optionsCompleted = OptionSearch::where('visible','=',1)->get();
    $options = OptionSearch::where('name','LIKE',$option_search.'%')->where('visible','=',1)->get();

    $resultSearch = [
        "searchItem" => $option_search,
        "qtdSearchFind" => count($options),
    ];

    return view('pages.index', compact('page_title', 'page_description','options','resultSearch','optionsCompleted'));

   }
}
