<?php

namespace App\Http\Controllers;

use App\Models\SicapServidor;
use App\Models\SicapCargosServidores;
use App\Http\Requests\SicapServidorRequest;
use Storage;
use DB;

class SicapServidorController extends Controller
{
    public function __construct() {
        $page_title = 'Servidores';
        view()->share('page_title', $page_title);
        $this->authorizeResource(SicapServidor::class, 'sicap_servidor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $sicapServidores = SicapServidor::where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->where('Exercicio',request('year'));
            }
            if (request()->has('nome') && request('nome') != '') {
                $q->where('Nome','like','%'.request('nome').'%');
            }
            if (request()->has('cargo') && request('cargo') != '') {
                $q->whereIn('codCargo', SicapCargosServidores::select('codCargo')->where('Descricao','like','%'.request('cargo').'%'));
            }
        })->orderBy('Exercicio','desc')->orderBy('Nome','asc')->paginate($offset);

        return view('adm.sicap-servidores.index', compact('page_description', 'offset', 'sicapServidores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.sicap-servidores.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SicapServidorRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(SicapServidorRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                foreach ($request->file('sicap') as $key => $file) {
                    switch (explode('.',$file->getClientOriginalName())[0]) {
                        case 'CargosServidores':
                            $this->_cargosServidores($file);
                            break;
                        case 'Servidores':
                            $this->_servidores($file);
                            break;
                    }
                }
            });
            // $sicapServidor = SicapServidor::create($request->except('_token'));
            return redirect()->route('sicap-servidores.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-servidores.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SicapServidor $sicapServidor
     * @return \Illuminate\Http\Response
    */
    public function show(SicapServidor $sicapServidor)
    {
        $page_description = 'Detalhes';
        return view('adm.sicap-servidores.show', compact('page_description', 'sicapServidor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SicapServidor $sicapServidor
     * @return \Illuminate\Http\Response
    */
    public function edit(SicapServidor $sicapServidor)
    {
        $page_description = 'Edição';
        return view('adm.sicap-servidores.edit', compact('page_description', 'sicapServidor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SicapServidorRequest  $request
     * @param  \App\Models\SicapServidor $sicapServidor
     * @return \Illuminate\Http\Response
    */
    public function update(SicapServidorRequest $request, SicapServidor $sicapServidor)
    {
        try {
            $sicapServidor->update($request->except('_token'));
            return redirect()->route('sicap-servidores.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-servidores.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SicapServidor $sicapServidor
     * @return \Illuminate\Http\Response
    */
    public function destroy(SicapServidor $sicapServidor)
    {
        try {
            $sicapServidor->delete();
            return redirect()->route('sicap-servidores.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-servidores.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
    */
    public function restore($id)
    {
        try {
            $sicapServidor = SicapServidor::withTrashed()->find($id);
            $sicapServidor->restore();
            return redirect()->route('sicap-empenho.index')->with('message_success', 'Registro restaurado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-empenho.index')->with('message_error', $th->getMessage());
        }
    }

    public function _cargosServidores($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {
            $cargoServidor = SicapCargosServidores::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Exercicio', trim($object->Exercicio))->where('CodCargo', trim($object->CodCargo))->first();

            if (!$cargoServidor) {
                SicapCargosServidores::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodCargo' => trim($object->CodCargo),
                    'Descricao' => trim($object->Descricao)
                ]);
            }
        }
        Storage::delete($file_path);
    }

    public function _servidores($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(asset('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {
            $servidor = SicapServidor::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Exercicio', trim($object->Exercicio))->where('Matricula', trim($object->Matricula))->first();

            if (!$servidor) {
                SicapServidor::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'Cpf' => trim($object->Cpf),
                    'Nome' => trim($object->Nome),
                    'DataNascimento' => trim($object->DataNascimento),
                    'NomeMae' => trim($object->NomeMae),
                    'NomePai' => trim($object->NomePai),
                    'PisPasep' => trim($object->PisPasep),
                    'TituloEleitoral' => trim($object->TituloEleitoral),
                    'DataAdmissao' => trim($object->DataAdmissao),
                    'CodVinculoEmpregaticio' => trim($object->CodVinculoEmpregaticio),
                    'CodRegimePrevidenciario' => trim($object->CodRegimePrevidenciario),
                    'CodEscolaridade' => trim($object->CodEscolaridade),
                    'SobCessao' => trim($object->SobCessao),
                    'CnpjEntidade' => trim($object->CnpjEntidade),
                    'NomeEntidade' => trim($object->NomeEntidade),
                    'DataCessao' => trim($object->DataCessao),
                    'DataRetornoCessao' => trim($object->DataRetornoCessao),
                    // 'SalarioBruto' => trim($object->SalarioBruto),
                    // 'Salarioliquido' => trim($object->Salarioliquido),
                    'MargemConsignada' => trim($object->MargemConsignada),
                    'CBO' => trim($object->CBO),
                    'CodCargo' => trim($object->CodCargo),
                    'CodLotacao' => trim($object->CodLotacao),
                    'CodFuncao' => trim($object->CodFuncao),
                    'Matricula' => trim($object->Matricula)
                ]);
            }
        }
        Storage::delete($file_path);
    }
}
