<?php

namespace App\Http\Controllers;

use App\Models\CovidResumo;
use App\Http\Requests\CovidResumoRequest;

class CovidResumoController extends Controller
{
    public function __construct() {
        $page_title = 'Covid Resumos';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $covidResumo = CovidResumo::orderBy('id','desc')->first();
        if ($covidResumo) {
            return $this->edit($covidResumo);
        } else {
            return $this->create();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.covid-resumos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CovidResumoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(CovidResumoRequest $request)
    {
        try {
            $data = $request->except(['_token','arquivo']);
            if ($request->hasFile('arquivo')) {
                $filepath = $request->arquivo->store('covid-resumos/arquivo');
                $data['arquivo'] = 'storage/'.$filepath;
            }
            CovidResumo::create($data);
            return redirect()->route('covid-resumos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('covid-resumos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CovidResumo $covidResumo
     * @return \Illuminate\Http\Response
    */
    public function show(CovidResumo $covidResumo)
    {
        $page_description = 'Detalhes';
        return view('adm.covid-resumos.show', compact('page_description', 'covidResumo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CovidResumo $covidResumo
     * @return \Illuminate\Http\Response
    */
    public function edit(CovidResumo $covidResumo)
    {
        $page_description = 'Edição';
        return view('adm.covid-resumos.edit', compact('page_description', 'covidResumo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CovidResumoRequest  $request
     * @param  \App\Models\CovidResumo $covidResumo
     * @return \Illuminate\Http\Response
    */
    public function update(CovidResumoRequest $request, CovidResumo $covidResumo)
    {
        try {
            $data = $request->except(['_token','arquivo']);
            if ($request->hasFile('arquivo')) {
                $filepath = $request->arquivo->store('covid-resumos/arquivo');
                $data['arquivo'] = 'storage/'.$filepath;
            }
            $covidResumo->update($request->except('_token'));
            return redirect()->route('covid-resumos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('covid-resumos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CovidResumo $covidResumo
     * @return \Illuminate\Http\Response
    */
    public function destroy(CovidResumo $covidResumo)
    {
        try {
            $covidResumo->delete();
            return redirect()->route('covid-resumos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('covid-resumos.index')->with('message_error', $th->getMessage());
        }
    }
}
