<?php

namespace App\Http\Controllers;

use App\Models\TipoPlanejamentoOrcamentario;
use App\Http\Requests\TipoPlanejamentoOrcamentarioRequest;

class TipoPlanejamentoOrcamentarioController extends Controller
{
    public function __construct() {
        $page_title = 'Tipo Planejamento Orcamentarios';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $tipoPlanejamentoOrcamentarios = TipoPlanejamentoOrcamentario::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.tipo-planejamento-orcamentarios.index', compact('page_description', 'offset', 'tipoPlanejamentoOrcamentarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.tipo-planejamento-orcamentarios.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TipoPlanejamentoOrcamentarioRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(TipoPlanejamentoOrcamentarioRequest $request)
    {
        try {
            $tipoPlanejamentoOrcamentario = TipoPlanejamentoOrcamentario::create($request->except('_token'));
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function show(TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario)
    {
        $page_description = 'Detalhes';
        return view('adm.tipo-planejamento-orcamentarios.show', compact('page_description', 'tipoPlanejamentoOrcamentario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function edit(TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario)
    {
        $page_description = 'Edição';
        return view('adm.tipo-planejamento-orcamentarios.edit', compact('page_description', 'tipoPlanejamentoOrcamentario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TipoPlanejamentoOrcamentarioRequest  $request
     * @param  \App\Models\TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function update(TipoPlanejamentoOrcamentarioRequest $request, TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario)
    {
        try {
            $tipoPlanejamentoOrcamentario->update($request->except('_token'));
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario
     * @return \Illuminate\Http\Response
    */
    public function destroy(TipoPlanejamentoOrcamentario $tipoPlanejamentoOrcamentario)
    {
        try {
            $tipoPlanejamentoOrcamentario->delete();
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-planejamento-orcamentarios.index')->with('message_error', $th->getMessage());
        }
    }
}
