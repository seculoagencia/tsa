<?php

namespace App\Http\Controllers;

use App\Models\Liquidacao;
use App\Http\Requests\LiquidacaoRequest;

class LiquidacaoController extends Controller
{
    public function __construct() {
        $page_title = 'Liquidacoes';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $liquidacoes = Liquidacao::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.liquidacoes.index', compact('page_description', 'offset', 'liquidacoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.liquidacoes.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\LiquidacaoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(LiquidacaoRequest $request)
    {
        try {
            $liquidaco = Liquidacao::create($request->except('_token'));
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_liq_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_liq_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Liquidacao $liquidaco
     * @return \Illuminate\Http\Response
    */
    public function show(Liquidacao $liquidaco)
    {
        $page_description = 'Detalhes';
        return view('adm.liquidacoes.show', compact('page_description', 'liquidacao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Liquidacao $liquidaco
     * @return \Illuminate\Http\Response
    */
    public function edit(Liquidacao $liquidaco)
    {
        $page_description = 'Edição';
        return view('adm.liquidacoes.edit', compact('page_description', 'liquidaco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\LiquidacaoRequest  $request
     * @param  \App\Models\Liquidacao $liquidaco
     * @return \Illuminate\Http\Response
    */
    public function update(LiquidacaoRequest $request, Liquidacao $liquidaco)
    {
        try {
            $liquidaco->update($request->except('_token'));
            return redirect()->route('empenhos.edit', $liquidaco->empenho_id)->with('message_liq_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $liquidaco->empenho_id)->with('message_liq_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Liquidacao $liquidaco
     * @return \Illuminate\Http\Response
    */
    public function destroy(Liquidacao $liquidaco)
    {
        try {
            $liquidaco->delete();
            return redirect()->route('empenhos.edit', $liquidaco->empenho_id)->with('message_liq_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $liquidaco->empenho_id)->with('message_liq_error', $th->getMessage());
        }
    }
}
