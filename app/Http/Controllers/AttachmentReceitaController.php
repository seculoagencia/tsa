<?php

namespace App\Http\Controllers;

use App\Models\AttachmentReceita;
use App\Models\Attachment;
use App\Models\Receita;
use App\Http\Requests\AttachmentReceitaRequest;

class AttachmentReceitaController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Receitas';
        view()->share('page_title', $page_title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentReceitaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentReceitaRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $receita = Receita::find($request->receita_id);
            $receita->attachments()->syncWithoutDetaching($attachment->id);
            return redirect()->route('receitas.edit', $request->receita_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('receitas.edit', $request->receita_id)->with('message_error', $th->getMessage());
        }
    }
}
