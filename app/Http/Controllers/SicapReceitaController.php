<?php

namespace App\Http\Controllers;

use App\Models\SicapReceita;
use App\Http\Requests\SicapReceitaRequest;
use Storage;
use DB;

class SicapReceitaController extends Controller
{
    public function __construct() {
        $page_title = 'Receitas';
        view()->share('page_title', $page_title);
        $this->authorizeResource(SicapEmpenho::class, 'sicap_receita');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $sicapReceitas = SicapReceita::withTrashed()->where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->where('Exercicio',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('DataArrecadacao',request('month'));
            }
        })->orderBy('DataArrecadacao','desc')->paginate($offset);

        return view('adm.sicap-receitas.index', compact('page_description', 'offset', 'sicapReceitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.sicap-receitas.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SicapReceitaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(SicapReceitaRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                foreach ($request->file('sicap') as $key => $file) {
                    switch (explode('.',$file->getClientOriginalName())[0]) {
                        case 'ReceitaArrecadada':
                            $this->_receitaArrecadada($file);
                            break;
                    }
                }
            });
            // $sicapReceita = SicapReceita::create($request->except('_token'));
            return redirect()->route('sicap-receitas.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-receitas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SicapReceita $sicapReceita
     * @return \Illuminate\Http\Response
    */
    public function show(SicapReceita $sicapReceita)
    {
        $page_description = 'Detalhes';
        return view('adm.sicap-receitas.show', compact('page_description', 'sicapReceita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SicapReceita $sicapReceita
     * @return \Illuminate\Http\Response
    */
    public function edit(SicapReceita $sicapReceita)
    {
        $page_description = 'Edição';
        return view('adm.sicap-receitas.edit', compact('page_description', 'sicapReceita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SicapReceitaRequest  $request
     * @param  \App\Models\SicapReceita $sicapReceita
     * @return \Illuminate\Http\Response
    */
    public function update(SicapReceitaRequest $request, SicapReceita $sicapReceita)
    {
        try {
            $sicapReceita->update($request->except('_token'));
            return redirect()->route('sicap-receitas.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-receitas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SicapReceita $sicapReceita
     * @return \Illuminate\Http\Response
    */
    public function destroy(SicapReceita $sicapReceita)
    {
        try {
            $sicapReceita->delete();
            return redirect()->route('sicap-receitas.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-receitas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
    */
    public function restore($id)
    {
        try {
            $sicapReceita = SicapReceita::withTrashed()->find($id);
            $sicapReceita->restore();
            return redirect()->route('sicap-receitas.index')->with('message_success', 'Registro restaurado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('sicap-receitas.index')->with('message_error', $th->getMessage());
        }
    }

    public function _receitaArrecadada($file)
    {
        $file_path = $file->store('sicap');
        $xmlDataString = file_get_contents(public_path('storage/'.$file_path));
        $xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $xmlDataString);
        $xmlObject = simplexml_load_string(str_replace('í','i',$xml), "SimpleXMLElement", LIBXML_NOWARNING);

        foreach ($xmlObject as $key => $object) {
            $dataArrecadacao = null;
            if (!empty(trim($object->DataArrecadacao))) {
                $dataArrecadacao = date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataArrecadacao))));
            }
            if (!empty(trim($object->Data))) {
                $dataArrecadacao = date('Y-m-d', strtotime(str_replace('/','-',trim($object->Data))));
            }
            $dataRegistro = !empty(trim($object->DataRegistro)) ? date('Y-m-d', strtotime(str_replace('/','-',trim($object->DataRegistro)))) : null;

            $receita = SicapReceita::where('CodUndGestora', trim($object->CodUndGestora))
            ->where('Bimestre', trim($object->Bimestre))->where('Exercicio', trim($object->Exercicio))
            ->where('CodContaReceita', trim($object->CodContaReceita))
            ->where('DataArrecadacao', '=', $dataArrecadacao)
            ->first();

            if (!$receita) {
                SicapReceita::create([
                    'CodUndGestora' => trim($object->CodUndGestora),
                    'CodigoUA' => trim($object->CodigoUA),
                    'Bimestre' => trim($object->Bimestre),
                    'Exercicio' => trim($object->Exercicio),
                    'CodOrgao' => trim($object->CodOrgao),
                    'CodUndOrcamentaria' => trim($object->CodUndOrcamentaria),
                    'CodContaReceita' => trim($object->CodContaReceita),
                    'CodBanco' => trim($object->CodBanco),
                    'CodAgencia' => trim($object->CodAgencia),
                    'NumConta' => trim($object->NumConta),
                    'CodContaAtivo' => trim($object->CodContaAtivo),
                    'DataArrecadacao' => $dataArrecadacao ? $dataArrecadacao : $dataArrecadacao2,
                    'Valor' => count(explode(',', trim($object->Valor))) > 1 ? str_replace(['.',','],['','.'],trim($object->Valor)) : trim($object->Valor),
                    'CodRecVinculado' => trim($object->CodRecVinculado),
                    'FormaArrecadacao' => trim($object->FormaArrecadacao),
                    'DataRegistro' => $dataRegistro
                ]);
            }
        }
        Storage::delete($file_path);
    }
}
