<?php

namespace App\Http\Controllers;

use App\Models\ArquivoResponsabilidade;
use App\Http\Requests\ArquivoResponsabilidadeRequest;
use App\Models\Attachment;
use App\Models\Responsabilidade;

class ArquivoResponsabilidadeController extends Controller
{
    public function __construct() {
        $page_title = 'Arquivos - Lei de Responsabilidade Fiscal';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $arquivoResponsabilidades = ArquivoResponsabilidade::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.arquivo-responsabilidades.index', compact('page_description', 'offset', 'arquivoResponsabilidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        $tipos = Responsabilidade::orderBy('name','asc')->get();
        return view('adm.arquivo-responsabilidades.create', compact('page_description','tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ArquivoResponsabilidadeRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ArquivoResponsabilidadeRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);
            
            $arquivoResponsabilidade = ArquivoResponsabilidade::create($request->except(['_token','file']) + ['attachment_id' => $attachment->id]);
            return redirect()->route('arquivo-responsabilidades.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArquivoResponsabilidade $arquivoResponsabilidade
     * @return \Illuminate\Http\Response
    */
    public function show(ArquivoResponsabilidade $arquivoResponsabilidade)
    {
        $page_description = 'Detalhes';
        return view('adm.arquivo-responsabilidades.show', compact('page_description', 'arquivoResponsabilidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArquivoResponsabilidade $arquivoResponsabilidade
     * @return \Illuminate\Http\Response
    */
    public function edit(ArquivoResponsabilidade $arquivoResponsabilidade)
    {
        $page_description = 'Edição';
        $tipos = Responsabilidade::orderBy('name','asc')->get();
        return view('adm.arquivo-responsabilidades.edit', compact('page_description', 'arquivoResponsabilidade','tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ArquivoResponsabilidadeRequest  $request
     * @param  \App\Models\ArquivoResponsabilidade $arquivoResponsabilidade
     * @return \Illuminate\Http\Response
    */
    public function update(ArquivoResponsabilidadeRequest $request, ArquivoResponsabilidade $arquivoResponsabilidade)
    {
        try {
            if ($request->hasFile('file')) {
                $original_name = $request->file('file')->getClientOriginalName();
                $path = $request->file->store('attachments');
                $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

                $arquivoResponsabilidade->update($request->except(['_token','file']) + ['attachment_id' => $attachment->id]);
            } else {
                $arquivoResponsabilidade->update($request->except('_token'));
            }
            
            return redirect()->route('arquivo-responsabilidades.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArquivoResponsabilidade $arquivoResponsabilidade
     * @return \Illuminate\Http\Response
    */
    public function destroy(ArquivoResponsabilidade $arquivoResponsabilidade)
    {
        try {
            $arquivoResponsabilidade->delete();
            return redirect()->route('arquivo-responsabilidades.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('arquivo-responsabilidades.index')->with('message_error', $th->getMessage());
        }
    }
}
