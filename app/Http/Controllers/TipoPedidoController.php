<?php

namespace App\Http\Controllers;

use App\Models\TipoPedido;
use App\Http\Requests\TipoPedidoRequest;

class TipoPedidoController extends Controller
{
    public function __construct() {
        $page_title = 'Tipo Pedidos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(TipoPedido::class, 'tipo_pedido');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $tipoPedidos = TipoPedido::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.tipo-pedidos.index', compact('page_description', 'offset', 'tipoPedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.tipo-pedidos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TipoPedidoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(TipoPedidoRequest $request)
    {
        try {
            $tipoPedido = TipoPedido::create($request->except('_token'));
            return redirect()->route('tipo-pedidos.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-pedidos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TipoPedido $tipoPedido
     * @return \Illuminate\Http\Response
    */
    public function show(TipoPedido $tipoPedido)
    {
        $page_description = 'Detalhes';
        return view('adm.tipo-pedidos.show', compact('page_description', 'tipoPedido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TipoPedido $tipoPedido
     * @return \Illuminate\Http\Response
    */
    public function edit(TipoPedido $tipoPedido)
    {
        $page_description = 'Edição';
        return view('adm.tipo-pedidos.edit', compact('page_description', 'tipoPedido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TipoPedidoRequest  $request
     * @param  \App\Models\TipoPedido $tipoPedido
     * @return \Illuminate\Http\Response
    */
    public function update(TipoPedidoRequest $request, TipoPedido $tipoPedido)
    {
        try {
            $tipoPedido->update($request->except('_token'));
            return redirect()->route('tipo-pedidos.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-pedidos.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoPedido $tipoPedido
     * @return \Illuminate\Http\Response
    */
    public function destroy(TipoPedido $tipoPedido)
    {
        try {
            $tipoPedido->delete();
            return redirect()->route('tipo-pedidos.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('tipo-pedidos.index')->with('message_error', $th->getMessage());
        }
    }
}
