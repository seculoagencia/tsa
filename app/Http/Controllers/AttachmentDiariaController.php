<?php

namespace App\Http\Controllers;

use App\Models\AttachmentDiaria;
use App\Models\Attachment;
use App\Models\Diaria;
use App\Http\Requests\AttachmentDiariaRequest;

class AttachmentDiariaController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Diarias';
        view()->share('page_title', $page_title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentDiariaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentDiariaRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $diaria = Diaria::find($request->diaria_id);
            $diaria->attachments()->syncWithoutDetaching($attachment->id);
            return redirect()->route('diarias.edit', $request->diaria_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('diarias.edit', $request->diaria_id)->with('message_error', $th->getMessage());
        }
    }
}
