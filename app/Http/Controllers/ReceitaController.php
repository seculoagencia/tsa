<?php

namespace App\Http\Controllers;

use App\Models\Receita;
use App\Http\Requests\ReceitaRequest;

class ReceitaController extends Controller
{
    public function __construct() {
        $page_title = 'Receitas';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Receita::class, 'receita');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $receitas = Receita::where(function ($q) {
            if (request()->has('year') && request('year') != '') {
                $q->whereYear('data',request('year'));
            }
            if (request()->has('month') && request('month') != '') {
                $q->whereMonth('data',request('month'));
            }
            if (request()->has('codigo') && request('codigo') != '') {
                $q->where('codigo','like','%'.request('codigo').'%');
            }
            if (request()->has('descricao') && request('descricao') != '') {
                $q->where('descricao','like','%'.request('descricao').'%');
            }
        })->orderBy('data','desc')->paginate($offset);

        return view('adm.receitas.index', compact('page_description', 'offset', 'receitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.receitas.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ReceitaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(ReceitaRequest $request)
    {
        try {
            $receita = Receita::create($request->except(['_token','covid19']) + ['covid19' => $request->has('covid19')]);
            return redirect()->route('receitas.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('receitas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Receita $receita
     * @return \Illuminate\Http\Response
    */
    public function show(Receita $receita)
    {
        $page_description = 'Detalhes';
        return view('adm.receitas.show', compact('page_description', 'receita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Receita $receita
     * @return \Illuminate\Http\Response
    */
    public function edit(Receita $receita)
    {
        $page_description = 'Edição';
        $attachments = $receita->attachments;
        $table = 'receitas';
        $item_id = $receita->id;
        return view('adm.receitas.edit', compact('page_description','table','item_id','receita','attachments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ReceitaRequest  $request
     * @param  \App\Models\Receita $receita
     * @return \Illuminate\Http\Response
    */
    public function update(ReceitaRequest $request, Receita $receita)
    {
        try {
            if ($request->has('covid19')) {
                $receita->update($request->except(['_token', 'covid19']) + ['covid19' => $request->has('covid19')]);
            } else {
                $receita->update($request->except(['_token', 'covid19']));
            }
            return redirect()->route('receitas.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('receitas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Receita $receita
     * @return \Illuminate\Http\Response
    */
    public function destroy(Receita $receita)
    {
        try {
            foreach ($receita->attachments as $key => $attachment) {
                $receita->attachments()->detach($attachment->id);
                $attachment->delete();
            }
            $receita->delete();
            return redirect()->route('receitas.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('receitas.index')->with('message_error', $th->getMessage());
        }
    }
}
