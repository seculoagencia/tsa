<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use App\Http\Requests\PermissionRequest;

class PermissionController extends Controller
{
    public function __construct() {
        $page_title = 'Permissions';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $permissions = Permission::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.permissions.index', compact('page_description', 'offset', 'permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.permissions.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PermissionRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(PermissionRequest $request)
    {
        $permission = Permission::create($request->except('_token'));
        return redirect()->route('permissions.index')->with('message_success', 'Cadastro feito com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission $permission
     * @return \Illuminate\Http\Response
    */
    public function show(Permission $permission)
    {
        $page_description = 'Detalhes';
        return view('adm.permissions.show', compact('page_description', 'permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission $permission
     * @return \Illuminate\Http\Response
    */
    public function edit(Permission $permission)
    {
        $page_description = 'Edição';
        return view('adm.permissions.edit', compact('page_description', 'permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PermissionRequest  $request
     * @param  \App\Models\Permission $permission
     * @return \Illuminate\Http\Response
    */
    public function update(PermissionRequest $request, Permission $permission)
    {
        $permission->update($request->except('_token'));
        return redirect()->route('permissions.index')->with('message_success', 'Registro editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission $permission
     * @return \Illuminate\Http\Response
    */
    public function destroy(Permission $permission)
    {
        $permission->delete();
        return redirect()->route('permissions.index')->with('message_success', 'Registro removido com sucesso!');
    }
}
