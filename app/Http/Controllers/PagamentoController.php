<?php

namespace App\Http\Controllers;

use App\Models\Pagamento;
use App\Http\Requests\PagamentoRequest;

class PagamentoController extends Controller
{
    public function __construct() {
        $page_title = 'Pagamentos';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $pagamentos = Pagamento::where(function ($q) {
            if (request()->has('banco') && request('banco') != '') {
                $q->where('banco','like','%'.request('banco').'%');
            }
            if (request()->has('agencia') && request('agencia') != '') {
                $q->where('agencia','like','%'.request('agencia').'%');
            }
            if (request()->has('conta') && request('conta') != '') {
                $q->where('conta','like','%'.request('conta').'%');
            }
            if (request()->has('documento') && request('documento') != '') {
                $q->where('documento','like','%'.request('documento').'%');
            }
            if (request()->has('empenho_id') && request('empenho_id') != '') {
                $q->where('empenho_id',request('empenho_id'));
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.pagamentos.index', compact('page_description', 'offset', 'pagamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.pagamentos.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PagamentoRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(PagamentoRequest $request)
    {
        try {
            $pagamento = Pagamento::create($request->except('_token'));
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_pag_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_pag_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pagamento $pagamento
     * @return \Illuminate\Http\Response
    */
    public function show(Pagamento $pagamento)
    {
        $page_description = 'Detalhes';
        return view('adm.pagamentos.show', compact('page_description', 'pagamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pagamento $pagamento
     * @return \Illuminate\Http\Response
    */
    public function edit(Pagamento $pagamento)
    {
        $page_description = 'Edição';
        return view('adm.pagamentos.edit', compact('page_description', 'pagamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PagamentoRequest  $request
     * @param  \App\Models\Pagamento $pagamento
     * @return \Illuminate\Http\Response
    */
    public function update(PagamentoRequest $request, Pagamento $pagamento)
    {
        try {
            $pagamento->update($request->except('_token'));
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_pag_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $request->empenho_id)->with('message_pag_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pagamento $pagamento
     * @return \Illuminate\Http\Response
    */
    public function destroy(Pagamento $pagamento)
    {
        try {
            $pagamento->delete();
            return redirect()->route('empenhos.edit', $pagamento->empenho_id)->with('message_pag_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('empenhos.edit', $pagamento->empenho_id)->with('message_pag_error', $th->getMessage());
        }
    }
}
