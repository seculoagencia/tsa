<?php

namespace App\Http\Controllers;

use App\Models\AttachmentVerba;
use App\Models\Verba;
use App\Models\Attachment;
use App\Http\Requests\AttachmentVerbaRequest;

class AttachmentVerbaController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Indemnity Funds';
        view()->share('page_title', $page_title);
    }

    public function store(AttachmentVerbaRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $verba = Verba::find($request->verba_id);
            $verba->attachments()->syncWithoutDetaching($attachment->id);
            return redirect()->route('verbas.edit', $request->verba_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('verbas.edit', $request->verba_id)->with('message_error', $th->getMessage());
        }
    }

}
