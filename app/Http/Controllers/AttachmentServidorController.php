<?php

namespace App\Http\Controllers;

use App\Models\AttachmentServidor;
use App\Models\Attachment;
use App\Models\Servidor;
use App\Http\Requests\AttachmentServidorRequest;

class AttachmentServidorController extends Controller
{
    public function __construct() {
        $page_title = 'Attachment Servidores';
        view()->share('page_title', $page_title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentServidorRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentServidorRequest $request)
    {
        try {
            $original_name = $request->file('file')->getClientOriginalName();
            $path = $request->file->store('attachments');
            $attachment = Attachment::create(['original_name'=>$original_name, 'path'=>'storage/'.$path]);

            $servidor = Servidor::find($request->servidor_id);
            $servidor->attachments()->syncWithoutDetaching($attachment->id);
            return redirect()->route('servidores.edit', $request->servidor_id)->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('servidores.edit', $request->servidor_id)->with('message_error', $th->getMessage());
        }
    }
}
