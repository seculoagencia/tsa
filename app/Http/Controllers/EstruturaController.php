<?php

namespace App\Http\Controllers;

use App\Models\Estrutura;
use App\Http\Requests\EstruturaRequest;

class EstruturaController extends Controller
{
    public function __construct() {
        $page_title = 'Estrutura Organizacional';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $estruturas = Estrutura::where(function ($q) {
            if (request()->has('title') && request('title') != '') {
                $q->where('title','like','%'.request('title').'%');
                $q->orWhere('description','like','%'.request('title').'%');
            }
        })->orderBy('order','asc')->paginate($offset);

        return view('adm.estruturas.index', compact('page_description', 'offset', 'estruturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.estruturas.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EstruturaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(EstruturaRequest $request)
    {
        try {
            $estrutura = Estrutura::create($request->except('_token'));
            return redirect()->route('estruturas.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('estruturas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Estrutura $estrutura
     * @return \Illuminate\Http\Response
    */
    public function show(Estrutura $estrutura)
    {
        $page_description = 'Detalhes';
        return view('adm.estruturas.show', compact('page_description', 'estrutura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Estrutura $estrutura
     * @return \Illuminate\Http\Response
    */
    public function edit(Estrutura $estrutura)
    {
        $page_description = 'Edição';
        return view('adm.estruturas.edit', compact('page_description', 'estrutura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EstruturaRequest  $request
     * @param  \App\Models\Estrutura $estrutura
     * @return \Illuminate\Http\Response
    */
    public function update(EstruturaRequest $request, Estrutura $estrutura)
    {
        try {
            $estrutura->update($request->except('_token'));
            return redirect()->route('estruturas.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('estruturas.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estrutura $estrutura
     * @return \Illuminate\Http\Response
    */
    public function destroy(Estrutura $estrutura)
    {
        try {
            $estrutura->delete();
            return redirect()->route('estruturas.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('estruturas.index')->with('message_error', $th->getMessage());
        }
    }
}
