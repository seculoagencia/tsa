<?php

namespace App\Http\Controllers;

use App\Models\TypeDocument;
use App\Http\Requests\TypeDocumentRequest;

class TypeDocumentController extends Controller
{
    public function __construct() {
        $page_title = 'Tipos de Documentos';
        view()->share('page_title', $page_title);
        $this->authorizeResource(TypeDocument::class, 'type_document');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $typeDocuments = TypeDocument::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.type-documents.index', compact('page_description', 'offset', 'typeDocuments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.type-documents.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TypeDocumentRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(TypeDocumentRequest $request)
    {
        $typeDocument = TypeDocument::create($request->except('_token'));
        return redirect()->route('type-documents.index')->with('message_success', 'Cadastro feito com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeDocument $typeDocument
     * @return \Illuminate\Http\Response
    */
    public function show(TypeDocument $typeDocument)
    {
        $page_description = 'Detalhes';
        return view('adm.type-documents.show', compact('page_description', 'typeDocument'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeDocument $typeDocument
     * @return \Illuminate\Http\Response
    */
    public function edit(TypeDocument $typeDocument)
    {
        $page_description = 'Edição';
        return view('adm.type-documents.edit', compact('page_description', 'typeDocument'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TypeDocumentRequest  $request
     * @param  \App\Models\TypeDocument $typeDocument
     * @return \Illuminate\Http\Response
    */
    public function update(TypeDocumentRequest $request, TypeDocument $typeDocument)
    {
        $typeDocument->update($request->except('_token'));
        return redirect()->route('type-documents.index')->with('message_success', 'Registro editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeDocument $typeDocument
     * @return \Illuminate\Http\Response
    */
    public function destroy(TypeDocument $typeDocument)
    {
        $typeDocument->delete();
        return redirect()->route('type-documents.index')->with('message_success', 'Registro removido com sucesso!');
    }
}
