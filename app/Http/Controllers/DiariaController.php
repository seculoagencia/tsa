<?php

namespace App\Http\Controllers;

use App\Models\Diaria;
use App\Models\Cargo;
use App\Http\Requests\DiariaRequest;

class DiariaController extends Controller
{
    public function __construct() {
        $page_title = 'Diárias';
        view()->share('page_title', $page_title);
        $this->authorizeResource(Diaria::class, 'diaria');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $diarias = Diaria::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.diarias.index', compact('page_description', 'offset', 'diarias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        $cargos = Cargo::orderBy('nome','asc')->get();
        return view('adm.diarias.create', compact('page_description','cargos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DiariaRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(DiariaRequest $request)
    {
        try {
            $diaria = Diaria::create($request->except('_token'));
            return redirect()->route('diarias.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('diarias.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Diaria $diaria
     * @return \Illuminate\Http\Response
    */
    public function show(Diaria $diaria)
    {
        $page_description = 'Detalhes';
        return view('adm.diarias.show', compact('page_description', 'diaria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Diaria $diaria
     * @return \Illuminate\Http\Response
    */
    public function edit(Diaria $diaria)
    {
        $page_description = 'Edição';
        $cargos = Cargo::orderBy('nome','asc')->get();
        $attachments = $diaria->attachments;
        $table = 'diarias';
        $item_id = $diaria->id;
        return view('adm.diarias.edit', compact('page_description','diaria','cargos','attachments','table','item_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DiariaRequest  $request
     * @param  \App\Models\Diaria $diaria
     * @return \Illuminate\Http\Response
    */
    public function update(DiariaRequest $request, Diaria $diaria)
    {
        try {
            $diaria->update($request->except('_token'));
            return redirect()->route('diarias.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('diarias.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Diaria $diaria
     * @return \Illuminate\Http\Response
    */
    public function destroy(Diaria $diaria)
    {
        try {
            foreach ($diaria->attachments as $key => $attachment) {
                $diaria->attachments()->detach($attachment->id);
                $attachment->delete();
            }
            $diaria->delete();
            return redirect()->route('diarias.index')->with('message_success', 'Registro removido com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('diarias.index')->with('message_error', $th->getMessage());
        }
    }
}
