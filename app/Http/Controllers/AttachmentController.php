<?php

namespace App\Http\Controllers;

use App\Models\Attachment;
use App\Models\Document;
use App\Models\Servidor;
use App\Models\Contrato;
use App\Models\Empenho;
use App\Models\Verba;
use App\Http\Requests\AttachmentRequest;
use Storage;

class AttachmentController extends Controller
{
    public function __construct() {
        $page_title = 'Attachments';
        view()->share('page_title', $page_title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $page_description = 'Listagem';
        $offset = request()->has('offset') ? request('offset') : 10;
        $attachments = Attachment::where(function ($q) {
            if (request()->has('name') && request('name') != '') {
                $q->where('name','like','%'.request('name').'%');
            }
        })->orderBy('id','desc')->paginate($offset);

        return view('adm.attachments.index', compact('page_description', 'offset', 'attachments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $page_description = 'Cadastrar';
        return view('adm.attachments.create', compact('page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentRequest  $request
     * @return \Illuminate\Http\Response
    */
    public function store(AttachmentRequest $request)
    {
        try {
            $attachment = Attachment::create($request->except('_token'));
            return redirect()->route('attachments.index')->with('message_success', 'Cadastro feito com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('attachments.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
    */
    public function show(Attachment $attachment)
    {
        $page_description = 'Detalhes';
        return view('adm.attachments.show', compact('page_description', 'attachment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
    */
    public function edit(Attachment $attachment)
    {
        $page_description = 'Edição';
        return view('adm.attachments.edit', compact('page_description', 'attachment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AttachmentRequest  $request
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
    */
    public function update(AttachmentRequest $request, Attachment $attachment)
    {
        try {
            $attachment->update($request->except('_token'));
            return redirect()->route('attachments.index')->with('message_success', 'Registro editado com sucesso!');
        } catch (\Throwable $th) {
            return redirect()->route('attachments.index')->with('message_error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
    */
    public function destroy(Attachment $attachment)
    {
        if (request()->has('table')) {
            try {
                switch (request('table')) {
                    case 'documents':
                        $document = Document::find(request('item_id'));
                        $document->attachments()->detach($attachment->id);
                        break;
                    case 'verbas':
                        $verba = Verba::find(request('item_id'));
                        $verba->attachments()->detach($attachment->id);
                        break;
                    case 'empenhos':
                        $empenho = Empenho::find(request('item_id'));
                        $empenho->attachments()->detach($attachment->id);
                        break;
                    case 'servidores':
                        $servidor = Servidor::find(request('item_id'));
                        $servidor->attachments()->detach($attachment->id);
                        break;
                    case 'contratos':
                        $contrato = Contrato::find(request('item_id'));
                        $contrato->attachments()->detach($attachment->id);
                        break;
                }
                $attachment->delete();
                return redirect()->route(request('table').'.edit', request('item_id'))->with('message_success', 'Registro removido com sucesso!');
            } catch (\Throwable $th) {
                return redirect()->route(request('table').'.edit', request('item_id'))->with('message_error', $th->getMessage());
            }
        } else {
            try {
                $attachment->delete();
                return redirect()->route('attachments.index')->with('message_success', 'Registro removido com sucesso!');
            } catch (\Throwable $th) {
                return redirect()->route('attachments.index')->with('message_error', $th->getMessage());
            }
        }
            
    }
}
