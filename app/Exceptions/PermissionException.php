<?php

namespace App\Exceptions;

use Exception;

class PermissionException extends Exception
{
    public function render($request, Exception $exception){
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            abort('403', 'Você não tem permissão para acessar essa página');
        }
        return parent::render($request, $exception);
    }
}
