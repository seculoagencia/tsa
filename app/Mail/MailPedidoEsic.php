<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\AppSetting;

class MailPedidoEsic extends Mailable
{
    use Queueable, SerializesModels;

    protected $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting_name = AppSetting::first() ? AppSetting::first()->name : env('APP_NAME');
        return $this->html('
            <h3>'.$setting_name.' - Pedido de Informação Recebido!</h3>
            <p>O Pedido de informação de código: '.$this->pedido->serial.', foi recebido. Use seu código e email para consultar.</p>
        ')->subject($setting_name.' - Pedido de Informação Recebido!');
    }
}
