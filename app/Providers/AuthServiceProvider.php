<?php

namespace App\Providers;

use App\Models\AppSetting;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Spatie\Permission\Models\Role' => 'App\Policies\RolePolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\AppSetting' => 'App\Policies\AppSettingPolicy',
        'App\Models\TypeDocument' => 'App\Policies\TypeDocumentPolicy',
        'App\Models\Document' => 'App\Policies\DocumentPolicy',
        'App\Models\Verba' => 'App\Policies\VerbaPolicy',
        'App\Models\Empenho' => 'App\Policies\EmpenhoPolicy',
        'App\Models\Receita' => 'App\Policies\ReceitaPolicy',
        'App\Models\Servidor' => 'App\Policies\ServidorPolicy',
        'App\Models\Contrato' => 'App\Policies\ContratoPolicy',
        'App\Models\Diaria' => 'App\Policies\DiariaPolicy',
        'App\Models\SicapEmpenho' => 'App\Policies\SicapEmpenhoPolicy',
        'App\Models\SicapReceita' => 'App\Policies\SicapReceitaPolicy',
        'App\Models\SicapServidor' => 'App\Policies\SicapServidorPolicy',
        'App\Models\SagresEmpenho' => 'App\Policies\SagresEmpenhoPolicy',
        'App\Models\Pedido' => 'App\Policies\PedidoPolicy',
        'App\Models\TipoPedido' => 'App\Policies\TipoPedidoPolicy',
        'App\Models\SiapEmpenho' => 'App\Policies\SiapEmpenhoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // $appSetting = AppSetting::orderBy('id','desc')->first();

        // $menu_header =  [
        //     'items' => [
        //         [],
        //         [
        //             'title' => $appSetting->name,
        //             'root' => true,
        //             'page' => $appSetting ? $appSetting->site_url : '#',
        //             'new-tab' => true,
        //         ]
        //     ]
        // ];

        // session()->put('menu_header', $menu_header);
    }
}
