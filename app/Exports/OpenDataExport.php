<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class OpenDataExport implements FromCollection
{
    private $collect;
    public function __construct($collect) {
        $this->collect = $collect;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->collect;
    }
}
